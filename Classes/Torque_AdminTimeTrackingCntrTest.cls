/**
    @Name           :   Torque_AdminTimeTrackingCntrTest
    @Date           :   15 Dec, 2015
    @Description    :   Test code for Torque_AdminTimeTrackingCntr.cls.
*/
@isTest
private class Torque_AdminTimeTrackingCntrTest {

    static testMethod void myUnitTest1() {
     
        
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
        
        User objuser = new User(ProfileId = p.Id,
                                Username = System.now().millisecond() + 'test2@test.com',
                                Alias = 'teAdm',
                                Email='test.adminPage@test.com',
                                EmailEncodingKey='UTF-8',
                                Firstname='Test',
                                Lastname='Admin',
                                LanguageLocaleKey='en_US',
                                LocaleSidKey='en_US',
                                TimeZoneSidKey='America/Chicago',
                                Allow_Time_Logging__c = true,
                                isActive = true
                              );
                                            
        insert objuser;
        
        GlobalSettings__c objcustomsetting = new GlobalSettings__c();
        objcustomsetting.Torque_Admin_Timeframe__c = 2;
        objcustomsetting.InstanceHealthCheckCaseOwnerId__c = objuser.Id;
        objcustomsetting.Account_Credit_Alert__c = 'Test Record';
        objcustomsetting.PTODaysInTheFuture__c = 123.0;
        objcustomsetting.Default_Billing_Reason__c = 'TestReason';
        insert objcustomsetting;
        
        //Custom setting
        ProjectDateSettings__c ProjectDateSettings = new ProjectDateSettings__c();          
        ProjectDateSettings.NonImplementationEndDateIncrement__c =  30;
        ProjectDateSettings.BuildConfigTestingEndDayIncrement__c = 14;
        ProjectDateSettings.BuildConfigTestingStartDayIncrement__c = 15;
        ProjectDateSettings.DesignDiscoveryEndDaysIncrement__c = -1;
        ProjectDateSettings.GoLiveAndPostGoLiveEndDayIncrement__c = 7;
        insert ProjectDateSettings;
        
        //Create a test account record
        Account objAccount = new Account(Name = 'Test Acc 0077', Payment_Terms__c = 2);
        insert objAccount; 
        
        //Create a test project record
        Project__c objProject = new Project__c(Name = 'Test Project 0077', Account__c = objAccount.Id, Hourly_Rate__c = 10 ,
                                                    Assigned_To__c = objuser.Id , Kickoff__c = date.today(), Go_Live_Date__c = date.today(), Status__c = 'Planning',
                                                    Current_State__c = 'Maintenance');
        insert objProject;
        
        //Test Deliverable Record    
        Deliverable__c objDeliverable = new Deliverable__c(Name = 'Test Deliverable', Project__c = objProject.id);
        insert objdeliverable;
        
        //Test BudgetItem Record 1    
        List<Budget_Item__c> listBudget = new List<Budget_Item__c>();
        Budget_Item__c objBudgetItem1 = new Budget_Item__c(Deliverable__c = objdeliverable.id, Project__c = objProject.id);
        listBudget.add(objBudgetItem1);
        
        Budget_Item__c objBudgetItem2 = new Budget_Item__c(Deliverable__c = objdeliverable.id, Project__c = objProject.id);
        listBudget.add(objBudgetItem2);
        
        insert listBudget;
        
        //Create a test time card record 1
        Time_Card__c timeCardRecord1  = new Time_Card__c();
        timeCardRecord1.Name = 'Test Time Card';
        insert timeCardRecord1;
        
        List<Billing_Reason_Code__c> listBillingReasonCode = new List<Billing_Reason_Code__c>();
        Billing_Reason_Code__c billingReason = new Billing_Reason_Code__c(Name = 'Test Billing', Billable__c=true, Non_Billable__c=false, Unbilled_del__c=false);
        listBillingReasonCode.add(billingReason);
        
        Billing_Reason_Code__c billingReason1 = new Billing_Reason_Code__c(Name = 'Test Billing1', Billable__c=true, Non_Billable__c=false, Unbilled_del__c=false);
        listBillingReasonCode.add(billingReason1);
        insert listBillingReasonCode;
        
        List<Invoice__c> lstInvoicesForTE = new List<Invoice__c>();
        //Create a test invoice record 1
        Invoice__c invoiceRecord1 = new Invoice__c();
        invoiceRecord1.Account__c = objAccount.Id; 
        invoiceRecord1.CurrencyIsoCode = 'USD';
        invoiceRecord1.Invoice_Status__c = 'Pending';    
        lstInvoicesForTE.add(invoiceRecord1);
        
        Invoice__c invoiceRecord2 = new Invoice__c();
        invoiceRecord2.Account__c = objAccount.Id; 
        invoiceRecord2.CurrencyIsoCode = 'USD';
        invoiceRecord2.Invoice_Status__c = 'Canceled';                                                        
        lstInvoicesForTE.add(invoiceRecord2);
        
        insert lstInvoicesForTE;
        Set<Id> setInvIds = new Set<id>();
        
        for(Invoice__c objInv : lstInvoicesForTE) {
            
            setInvIds.add(objInv.Id);
        }
        system.debug('-----invoiceRecord1----'+invoiceRecord1);
        lstInvoicesForTE = new List<Invoice__c>();
        
        lstInvoicesForTE = [SELECT Name FROM Invoice__c WHERE Id IN :setInvIds];
        
        //Create a test invoice line record 1
        Invoice_Line__c invoiceLineRecord1 = new Invoice_Line__c();
        invoiceLineRecord1.Invoice__c = invoiceRecord1.Id;   
        invoiceLineRecord1.CurrencyIsoCode = 'USD'; 
        insert invoiceLineRecord1; 
        
        //Create a time entry record
        Time_Entry__c timeentryrecord1 = new Time_Entry__c();
        timeentryrecord1.Project__c = objProject.Id;
        timeentryrecord1.Billable__c = true;
        timeentryrecord1.Time_Card__c = timeCardRecord1.Id;  
        timeentryrecord1.Time__c = 10;
        timeentryrecord1.CurrencyIsoCode = 'USD';
        timeentryrecord1.Date__c = date.today();
        timeentryrecord1.Case_Number__c = '123';       
        timeentryrecord1.Notes__c = 'testTestTest';    
        timeentryrecord1.Budget_Item__c = objBudgetItem1.Id;   
        timeentryrecord1.Invoice_Line__c = invoiceLineRecord1.Id;   
        timeentryrecord1.Billing_Reason__c = billingReason.Id;
        insert timeentryrecord1;
        
        invoiceLineRecord1.Time_Entry__c = timeentryrecord1.Id;  
        update invoiceLineRecord1;
        
        Time_card__c timeCardRecord = new Time_card__c (Name = 'Time card', Week_Beginning__c = date.today().toStartofWeek(),User__c = userinfo.getuserID());
        insert timeCardRecord;
        
         //Create a test invoice line record 1
        Invoice_Line__c invoiceLineRecord2 = new Invoice_Line__c();
        invoiceLineRecord2.Invoice__c = invoiceRecord2.Id;   
        invoiceLineRecord2.CurrencyIsoCode = 'USD'; 
        insert invoiceLineRecord2; 
        
        //Create a time entry record
        Time_Entry__c timeentryrecord2 = new Time_Entry__c();
        timeentryrecord2.Project__c = objProject.Id;
        timeentryrecord2.Billable__c = true;
        timeentryrecord2.Time_Card__c = timeCardRecord1.Id;  
        timeentryrecord2.Time__c = 10;
        timeentryrecord2.CurrencyIsoCode = 'USD';
        timeentryrecord2.Date__c = date.today();
        timeentryrecord2.Case_Number__c = '123'; 
        timeentryrecord2.Notes__c = 'testTestTest';    
        timeentryrecord2.Invoice_Line__c = invoiceLineRecord2.Id;
        timeentryrecord2.Budget_Item__c = objbudgetItem2.Id;
        //timeentryrecord2.Billing_Reason__c = billingReason.Id;
        insert timeentryrecord2;  
                
        system.debug('-----timeentryrecord1----'+timeentryrecord1);
        
        invoiceLineRecord2.Time_Entry__c =   timeentryrecord2.Id;  
        update invoiceLineRecord2;
        
  test.startTest();         
        
        Torque_AdminTimeTrackingCntr objPage = new Torque_AdminTimeTrackingCntr();
        
        //Torque_AdminTimeTrackingCntr.WrapperEditableTE objPage1 = new Torque_AdminTimeTrackingCntr.WrapperEditableTE(objPage.objNewTE);
        
        objPage.recIdTest = timeentryrecord1.Id;
        objPage.projIdTest = objProject.Id;
        objPage.objTEFilter1.Project__c = objProject.Id;
        objPage.sortFieldTest = 'Project__c';
        
        //Multiselect picklist.
        objPage.lstLeftSelectedUserIDs.add(objuser.Id);
        objPage.selectClick();
        objPage.getSelectedValues();
        
        objPage.lstRightSelectedUserIDs.add(objuser.Id);
        objPage.unselectClick();
        objPage.getUnSelectedValues();
        
        //Page message errors.
        objPage.objProjFilter12.Dev_Start_Date__c = Date.today();
        objPage.btnSearchTimeSheets();
        
        objPage.objProjFilter12.Dev_Start_Date__c = null;
        objPage.objProjFilter12.Go_Live_Date__c = Date.today();
        objPage.btnSearchTimeSheets();
        
        objPage.objProjFilter12.Dev_Start_Date__c = Date.today() + 1;
        objPage.objProjFilter12.Go_Live_Date__c = Date.today();
        objPage.btnSearchTimeSheets();
        
        objPage.objProjFilter12.Dev_Start_Date__c = Date.today();
        objPage.objProjFilter12.Go_Live_Date__c = Date.today() + 1;
        objPage.btnSearchTimeSheets();
        
        objPage.editTimeEntry();
        objPage.getInvoicesAndBIBasedOnProjEdit();
        objPage.btnCancel();
        
        objPage.mapTEIdBRBeforeUpdate.put(timeentryrecord1.Id, billingReason1.Id);
        objPage.btnSaveTimeEntry();
        
        
        
        //objPage.recIdTest = timeentryrecord1.Id;
        objPage.createNewTimeEntry();
        objPage.selectedInvoice = invoiceRecord1.Id;
        objPage.objNewTE = new Time_Entry__c(Time__c = 8, Budget_Item__c = objBudgetItem1.Id, Date__c =date.today().toStartofWeek()+1, Project__c = objProject.Id, Notes__c = 'testTestTest', Billing_Reason__c= billingReason.Id, Performed_By__c=userinfo.getuserID());
        objPage.getInvoicesBasedOnProj();
        objPage.getBIsBasedOnProj();
        
        
        objPage.btnSaveNewTE();      //original code line
        
        objPage.objNewTE.Date__c = null;
        objPage.btnSaveNewTE();             //added till here
        
        objPage.btnCancelNewTE();
        objPage.getPendingInvoices(lstInvoicesForTE);
        objPage.sortTableBasedOnHeader();
        objPage.sortFieldTest = 'Budget_Item__c';
        objPage.sortTableBasedOnHeader();
        objPage.sortFieldTest = 'Performed_By__c';
        objPage.sortTableBasedOnHeader();
        objPage.sortFieldTest = 'Billing_Reason__c';
        objPage.sortTableBasedOnHeader();
        objPage.sortFieldTest = 'Invoice_Line__c';
        objPage.sortTableBasedOnHeader();
        Boolean nxt = objPage.hasNext;
        Boolean prev = objPage.hasPrevious;
        objPage.first();
        objPage.last();
        objPage.previous();
        objPage.next();
        
       // objPage1.selected = false;
        
        Torque_CustomProjectLookupController objCustomLookup = new Torque_CustomProjectLookupController();
        objCustomLookup.search();
        objCustomLookup.objProject = new Project__c(Name = 'Test Project 0078', Account__c = objAccount.Id, Hourly_Rate__c = 10 ,
                                                    Assigned_To__c = objuser.Id , Kickoff__c = date.today(), Go_Live_Date__c = date.today(), Status__c = 'Planning',
                                                    Current_State__c = 'Maintenance');
        objCustomLookup.saveAccount();
        objCustomLookup.getFormTag();
        objCustomLookup.getTextBox();
        
        Torque_AdminTimeTrackingCntr.WrapperEditableTE objWrapper = objPage.mapTEIdWrapperPendingTE.get(timeentryrecord1.Id);
        objWrapper.selected = true;
        
        objPage.showPopupToMassUpdateTEs();
        objPage.saveSelectedTimeEntries();
        objPage.closePopup();
        
        Torque_AdminTimeTrackingCntr objPage1 = new Torque_AdminTimeTrackingCntr();
        objPage1.mapProjectOnTE = new Map<Id, Project__c>();
        objPage1.objTEForCustomLookup.Project__c = objProject.Id;
        objPage1.getBIsBasedOnProj();
        
        objPage1.objNewTE = new Time_Entry__c(Time__c = 8, Budget_Item__c = objBudgetItem1.Id, Date__c =date.today().toStartofWeek()+1, Project__c = objProject.Id, Notes__c = 'testTestTest', Billing_Reason__c= billingReason.Id, Performed_By__c=userinfo.getuserID());
        objPage1.getInvoicesBasedOnProj();
        
		Torque_AdminTimeTrackingCntr objPage2 = new Torque_AdminTimeTrackingCntr();        
        objPage2.mapProjectOnTE = new Map<Id, Project__c>();
        objPage2.recIdTest = timeentryrecord1.Id;
        objPage2.projIdTest = objProject.Id;
        objPage2.mapTEIdWrapperPendingTE = new Map<Id, Torque_AdminTimeTrackingCntr.WrapperEditableTE>();
        objPage2.mapTEIdWrapperPendingTE.putAll(objPage.mapTEIdWrapperPendingTE);
        objPage2.editTimeEntry();
        objPage2.getInvoicesAndBIBasedOnProjEdit();
        
		test.stopTest(); 
    
    }
}