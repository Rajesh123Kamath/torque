/**
  * Handler Name : Torque_TimeEntryHandler
  * Created By   : Trekbin          
  * Created On   : 19-Nov-2015
  * Modified On  : 27-Jan-2016
  * Modified by  : Trekbin
  * Description  : Handler class for the trigger 'Torque_TimeEntryTrigger'
**/
public with sharing class Torque_TimeEntryHandler { 
    
    /**
	  @Name: validateTEsAndDisplayError
	  @Description: validates the Time Entry before update for status and before delete for invoice line and displays validation error. 
	*/
	public static void validateTEsAndDisplayError(List<Time_Entry__c> listNewTE, List<Time_Entry__c> listOldTE) {
	     
		/* Added this boolean variable By 'Trekbin' on '10-feb-2016' as per case '25221' */
		//Boolean variable for updating/ deleting based on the permission
        Boolean chkInvoiceOverridePermission = [Select Id ,Invoice_Override_Permission__c From User Where Id = :UserInfo.getUserId()].Invoice_Override_Permission__c; 
        
		//For update.
		if(listNewTE != null) {   
			            
			//Iterate over the time entry records which needs to be updated.
			for(Time_Entry__c objTimeEntry : listNewTE) {           
			    
				/* Last Modified By 'Trekbin' on '10-feb-2016' as per case '25221' */
				//If the status is Sent then throw the error.  
				if(objTimeEntry.Invoice_Status__c == 'Sent' && chkInvoiceOverridePermission == false)
					objTimeEntry.addError('You cannot update the time entry record fields as the invoice status is sent');
			}
		}//For delete.
		else if(listOldTE != null) {
		 	
			//Iterate over the time entry records which needs to be deleted.
			for(Time_Entry__c objTimeEntry : listOldTE) {           
			        
				/* Last Modified By 'Trekbin' on '10-feb-2016' as per case '25221' */
				//If there is an invoice line linked to this time entry then throw error.  
				if(objTimeEntry.Invoice_Line__c != null && chkInvoiceOverridePermission == false)
					objTimeEntry.addError('You cannot delete this time entry record');
			}
		}
	}
    
    /**
	  @Name: updateLogHoursOnBudgetItem
	  @Description: 1. After insert/update/delete of Time Entry, update log hours on the related budget item with sum of all child time entry hours. 
	  				2. After insert/update of Time Entry, if it is associated with a Pending Invoice Line, then the corresponding Invoice Line has to be updated to reflect the changes to Time Entry.
	*/
	public static void updateInvLineAndHrsOnBudgetItem(List<Time_Entry__c> lstTERecords, Map<Id, Time_Entry__c> mapOldTimeEntries) {
        
		//Set to hold Budget Item Id
		set<Id> setBudgetItemIds = new set<Id>();
		List<Invoice_Line__c> lstInvoiceLineToBeUpdated = new List<Invoice_Line__c>();
		
		for(Time_Entry__c objTimeEntry : lstTERecords)  {
		
			if(objTimeEntry.Budget_Item__c != null) {
			    
				setBudgetItemIds.add(objTimeEntry.Budget_Item__c);
				
				//In case of update.
				if(mapOldTimeEntries != null) {
					
					//get the id of previous Budget Item.
					if(mapOldTimeEntries.get(objTimeEntry.id).Budget_Item__c != null)
						setBudgetItemIds.add(mapOldTimeEntries.get(objTimeEntry.id).Budget_Item__c);
					
					//Update the related Invoice Line with the data on Time Entry.
					if(objTimeEntry.Invoice_Line__c != null && objTimeEntry.Invoice_Status__c == 'Pending') {
						
						Invoice_Line__c objInvLine = new Invoice_Line__c(	Id = objTimeEntry.Invoice_Line__c, 
																			Invoice_Line_Notes__c = objTimeEntry.Notes__c,
																			Date_Delivered__c = objTimeEntry.Date__c,
		    																Budget_Item__c = objTimeEntry.Budget_Item__c,
		    																Deliverable__c = objTimeEntry.Deliverable__c,
		    																Performed_By__c = objTimeEntry.Performed_By__c,
		    																Quantity__c = objTimeEntry.Time__c
			    														);
			    		lstInvoiceLineToBeUpdated.add(objInvLine);
					}
				}
			}
		}
        
        //Update Invoice Lines.
        if(!lstInvoiceLineToBeUpdated.isEmpty())
        	update lstInvoiceLineToBeUpdated;
        
        if(!setBudgetItemIds.isEmpty()) {
            
            //Map of Budget Id and decimal value (sum of time__c of time entry) according to diifferent conditions
            Map<Id, decimal> mapBIIdTotalHoursLogged = new Map<Id, decimal>();
            Map<Id, decimal> mapBIIdHoursBurned = new Map<Id, decimal>();
            Map<Id, decimal> mapBIIdNonBillableHrs = new Map<Id, decimal>();
            Map<Id, decimal> mapBIIdBackedOutHrs = new Map<Id, decimal>();
            Map<Id, decimal> mapBIIdHrsInvoiced = new Map<Id, decimal>();
            
            //Map of Budget Id and Budget Object to update Budget item logged hours
            Map<Id, Budget_Item__c> mapBudgetItemsToBeUpdated = new Map<Id, Budget_Item__c>([	SELECT Id, Total_Hours_Logged__c 
					        																	FROM Budget_Item__c 
					        																	WHERE id in : setBudgetItemIds
			            																   ]);
            
            //Aggregate query to calculate total logged hours for each Budget Item
            for(AggregateResult a : [	SELECT Sum(Time__c) totalHoursLogged, Budget_Item__c 
            							FROM Time_Entry__c 
            							WHERE Budget_Item__c IN : setBudgetItemIds 
            							GROUP BY Budget_Item__c
            						]) {  
            							
                mapBIIdTotalHoursLogged.put((id)a.get('Budget_Item__c'),(Decimal)a.get('totalHoursLogged'));
            }
            
            //Aggregate query to calculate burned hours for each Budget Item (Time Entry Billable__c = true)
            for(AggregateResult a : [	SELECT Sum(Time__c) hoursBurned ,Budget_Item__c 
            							FROM Time_Entry__c 
            							WHERE Budget_Item__c IN : setBudgetItemIds 
            							AND Billable__c = true 
            							AND Unbilled__c = FALSE 
            							GROUP BY Budget_Item__c
            						]) {  
                
                mapBIIdHoursBurned.put((id)a.get('Budget_Item__c'),(Decimal)a.get('hoursBurned'));
            }
            
            //Aggregate query to find total Non Billable hours for each Budget Item (Time Entry Non_Billable__c = true)
            for(AggregateResult a : [	SELECT Sum(Time__c) nonBillableHours ,Budget_Item__c 
            							FROM Time_Entry__c 
            							WHERE Budget_Item__c in : setBudgetItemIds 
            							AND Non_Billable__c = true 
            							GROUP BY Budget_Item__c
            						]) {  
                mapBIIdNonBillableHrs.put((id)a.get('Budget_Item__c'),(Decimal)a.get('nonBillableHours'));
            }
            
            //Aggregate query to find Backed out hours for each Budget Item (Time Entry Unbilled__c = true)
            for(AggregateResult a : [	SELECT Sum(Time__c) backedOutHours ,Budget_Item__c 
            							FROM Time_Entry__c 
            							WHERE Budget_Item__c in : setBudgetItemIds 
            							AND Unbilled__c = true 
            							GROUP BY Budget_Item__c
            						]) {  
            							
                mapBIIdBackedOutHrs.put((id)a.get('Budget_Item__c'),(Decimal)a.get('backedOutHours'));
            }
            
            //updated code according to requirement to calculate and update invoiced hours on budget 14978 date 12/14/2015
            //Aggregate query to calculate hours to be invoice for each Budget Item (Time Entry Billable__c = true)
            for(AggregateResult a : [	SELECT Sum(Time__c) hoursInvoiced ,Budget_Item__c 
            							FROM Time_Entry__c 
            							WHERE Budget_Item__c in : setBudgetItemIds 
            							And Billable__c = true 
            							AND Unbilled__c = FALSE 
            							AND Invoice_Line__c = NULL 
            							GROUP BY Budget_Item__c
            						]) {  
            								
                mapBIIdHrsInvoiced.put((id)a.get('Budget_Item__c'),(Decimal)a.get('hoursInvoiced'));
            }
            
            //Iterate on Time Entry Records
            for(Time_Entry__c objTimeEntry : lstTERecords) {
                
                //update new budget on time entry
                if(mapBudgetItemsToBeUpdated.containsKey(objTimeEntry.Budget_Item__c)) {
                
                    //Log Total Hours to Budget Item by rolling up to total of time__c of Time Card Entry
                    if(mapBIIdTotalHoursLogged.containsKey(objTimeEntry.Budget_Item__c))
                        mapBudgetItemsToBeUpdated.get(objTimeEntry.Budget_Item__c).Total_Hours_Logged__c = mapBIIdTotalHoursLogged.get(objTimeEntry.Budget_Item__c);
                    else
                        mapBudgetItemsToBeUpdated.get(objTimeEntry.Budget_Item__c).Total_Hours_Logged__c = 0;
                    
                    //Log Burned Hours to Budget Item by rolling up to total of time__c of Time Card Entry where Billable__c = true
                    if(mapBIIdHoursBurned.containsKey(objTimeEntry.Budget_Item__c))
                        mapBudgetItemsToBeUpdated.get(objTimeEntry.Budget_Item__c).Hours_Burned__c = mapBIIdHoursBurned.get(objTimeEntry.Budget_Item__c);
                    else
                        mapBudgetItemsToBeUpdated.get(objTimeEntry.Budget_Item__c).Hours_Burned__c = 0;
                        
                    //Log Non Billable Hours to Budget Item by rolling up to total of time__c of Time Card Entry where Non_Billable__c = true
                    if(mapBIIdNonBillableHrs.containsKey(objTimeEntry.Budget_Item__c))
                        mapBudgetItemsToBeUpdated.get(objTimeEntry.Budget_Item__c).Non_Billable_Hours__c = mapBIIdNonBillableHrs.get(objTimeEntry.Budget_Item__c);
                    else
                        mapBudgetItemsToBeUpdated.get(objTimeEntry.Budget_Item__c).Non_Billable_Hours__c = 0;
                        
                    //Log Backed Out Hours to Budget Item by rolling up to total of time__c of Time Card Entry where Unbilled__c = true
                    if(mapBIIdBackedOutHrs.containsKey(objTimeEntry.Budget_Item__c))
                        mapBudgetItemsToBeUpdated.get(objTimeEntry.Budget_Item__c).Backed_Out_Hours__c = mapBIIdBackedOutHrs.get(objTimeEntry.Budget_Item__c);
                    else
                        mapBudgetItemsToBeUpdated.get(objTimeEntry.Budget_Item__c).Backed_Out_Hours__c = 0;
                    
                    //updated code according to requirement to update invoiced hours on budget 14978 date 12/14/2015
                    
                    //Log Invoiced Hours to Budget Item by rolling up to total of time__c of Time Card Entry where billed = true, Unbilled__c = false and invoice line equals null
                    if(mapBIIdHrsInvoiced.containsKey(objTimeEntry.Budget_Item__c))
                        mapBudgetItemsToBeUpdated.get(objTimeEntry.Budget_Item__c).Hours_to_be_invoiced__c = mapBIIdHrsInvoiced.get(objTimeEntry.Budget_Item__c);
                    else
                        mapBudgetItemsToBeUpdated.get(objTimeEntry.Budget_Item__c).Hours_to_be_invoiced__c = 0;
                }
                
                //updated code according to requirement to update hours on budget prior value 14978 date 12/8/2015
                
                // update old budget (prior value) on time entry
                if(mapOldTimeEntries != null && mapOldTimeEntries.containsKey(objTimeEntry.id)){
                
                    //Log Total Hours to Budget Item by rolling up to total of time__c of Time Card Entry
                    if(mapBIIdTotalHoursLogged.containsKey(mapOldTimeEntries.get(objTimeEntry.id).Budget_Item__c))
                        mapBudgetItemsToBeUpdated.get(mapOldTimeEntries.get(objTimeEntry.id).Budget_Item__c).Total_Hours_Logged__c = mapBIIdTotalHoursLogged.get(mapOldTimeEntries.get(objTimeEntry.id).Budget_Item__c);
                    else
                        mapBudgetItemsToBeUpdated.get(mapOldTimeEntries.get(objTimeEntry.id).Budget_Item__c).Total_Hours_Logged__c = 0;
                        
                    //Log Burned Hours to Budget Item by rolling up to total of time__c of Time Card Entry where Billable__c = true
                    if(mapBIIdHoursBurned.containsKey(mapOldTimeEntries.get(objTimeEntry.id).Budget_Item__c))
                        mapBudgetItemsToBeUpdated.get(mapOldTimeEntries.get(objTimeEntry.id).Budget_Item__c).Hours_Burned__c = mapBIIdHoursBurned.get(mapOldTimeEntries.get(objTimeEntry.id).Budget_Item__c);
                    else
                        mapBudgetItemsToBeUpdated.get(mapOldTimeEntries.get(objTimeEntry.id).Budget_Item__c).Hours_Burned__c = 0;
                        
                    //Log Non Billable Hours to Budget Item by rolling up to total of time__c of Time Card Entry where Non_Billable__c = true
                    if(mapBIIdNonBillableHrs.containsKey(mapOldTimeEntries.get(objTimeEntry.id).Budget_Item__c))
                        mapBudgetItemsToBeUpdated.get(mapOldTimeEntries.get(objTimeEntry.id).Budget_Item__c).Non_Billable_Hours__c = mapBIIdNonBillableHrs.get(mapOldTimeEntries.get(objTimeEntry.id).Budget_Item__c);
                    else
                        mapBudgetItemsToBeUpdated.get(mapOldTimeEntries.get(objTimeEntry.id).Budget_Item__c).Non_Billable_Hours__c = 0;
                        
                    //Log Backed Out Hours to Budget Item by rolling up to total of time__c of Time Card Entry where Unbilled__c = true
                    if(mapBIIdBackedOutHrs.containsKey(mapOldTimeEntries.get(objTimeEntry.id).Budget_Item__c))
                        mapBudgetItemsToBeUpdated.get(mapOldTimeEntries.get(objTimeEntry.id).Budget_Item__c).Backed_Out_Hours__c = mapBIIdBackedOutHrs.get(mapOldTimeEntries.get(objTimeEntry.id).Budget_Item__c);
                    else
                        mapBudgetItemsToBeUpdated.get(mapOldTimeEntries.get(objTimeEntry.id).Budget_Item__c).Backed_Out_Hours__c = 0;
                        
                    //updated code according to requirement to update Invoiced hours on budget prior value 14978 date 12/14/2015
                    
                    //Log Invoiced Hours to Budget Item by rolling up to total of time__c of Time Card Entry where billed = true, Unbilled__c = false and invoice line equals null
                    if(mapBIIdHrsInvoiced.containsKey(mapOldTimeEntries.get(objTimeEntry.id).Budget_Item__c))
                        mapBudgetItemsToBeUpdated.get(mapOldTimeEntries.get(objTimeEntry.id).Budget_Item__c).Hours_to_be_invoiced__c = mapBIIdHrsInvoiced.get(mapOldTimeEntries.get(objTimeEntry.id).Budget_Item__c);
                    else
                        mapBudgetItemsToBeUpdated.get(mapOldTimeEntries.get(objTimeEntry.id).Budget_Item__c).Hours_to_be_invoiced__c = 0;
                }
            }
            
            //Update Budget Item Hours
            if(!mapBudgetItemsToBeUpdated.isEmpty())
                update mapBudgetItemsToBeUpdated.values();
        }
    }
}