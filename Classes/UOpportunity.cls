public class UOpportunity 
{
    private static ID SaleRecordTypeID; 
    private static List<AutoFollowSalesOpptySettings__c> Autofollows;
    @TestVisible private static boolean alreadRunAddTopic = false;

    public static void runOpportunityAddTopic(List <Opportunity> records, Map <id, Opportunity> oldMap){


        if(alreadRunAddTopic){
            return;
        }

        alreadRunAddTopic = true;


        List <Opportunity> newRecords = new List <Opportunity>();
        
        for(Opportunity t : records)
        {
            Opportunity tt = t.clone(true, true, true);
            newRecords.add(tt);
        }

        BREeze.processRules.ruleDataSet rds = new BREeze.processRules.ruleDataSet(); 
        rds.records = newRecords; 
        rds.oldMap = oldMap; 
        	
        BREeze.ProcessRules.processRuleLogic('OpportunityAddTopic', false, rds);

        update rds.records;

        BREeze.ProcessRules.executePostProcesses(rds.postProcessExecutes);


    }

    /*Last Modified by trekbin on 2-march-2016 as per case: 00025458 */
    public static void AfterInsertUpdateChatter(List<Opportunity> records)
    {   
        /*Created by trekbin on 2-march-2016 as per case: 00025458 */
        createProjectAndItsChildRecords(records);
            
       /* if (SaleRecordTypeID == null)
        {
            SaleRecordTypeID = [select id, name from recordtype where name='Sales' limit 1][0].ID;
        } */
        
        SaleRecordTypeID = Sobjecttype.Opportunity.getRecordTypeInfosByName().get('Sales').getRecordTypeId();
        
        //'sales' Sean and Harry will auto-follow... 
        if (Autofollows == null)
        {
            Autofollows = AutoFollowSalesOpptySettings__c.GetAll().Values();
        }
                
        List <EntitySubscription> SubsToInsert = new List<EntitySubscription>();
        for (Opportunity o: records)
        {
            if(SaleRecordTypeID != null && o.RecordTypeId==SaleRecordTypeID)
            {
                SubsToInsert.add(uChatterUtilities.createFollowSubscription(o.id, o.CreatedById));
                
                for (AutoFollowSalesOpptySettings__c a : Autofollows)
                {
                    SubsToInsert.add(uChatterUtilities.createFollowSubscription(o.id, (ID)a.UserID__c));                    
                }
            }
        }
        
        if (SubsToInsert.size() > 0)
        {
            uChatterUtilities.follow(SubsToInsert);
        }   
    }
    
    
    /**
        @ Method name      : createProjectAndItsChildRecords 
        @ Method purpose   : To create Project, Deliverable and Budget Item upon insert of opportunity records and 
                             assign created project as a value to the lookup Opportunity.Project__c 
        @ Created by       : Trekbin
        @ Created date     : 2-March-2016
        @ Last Modified By : Trekbin on 9-march-2016 for excluding 'TorqueOpptySettings__c' customsetting types records from processing
    */
    private static void createProjectAndItsChildRecords(List<Opportunity> lstNewOpportunityRecords)
    {
        /* Initialize lists and maps */
        String strExcludeOpptyRecordType = '';
        set<Id> setOpportunityID = new set<Id>();
        list<Opportunity> lstOpportunityToUpdate = new list<Opportunity>();
        map<Id, Project__c> mapOpportunityIDToProject = new map<Id, Project__c>();
        map<Id, Deliverable__c> mapProjectIDToDeliverable = new map<Id, Deliverable__c>();
        map<Id, Budget_Item__c> mapDeliverableIDToBudgetItem = new map<Id, Budget_Item__c>();
        
        //Hierarchy based "TorqueOpptySettings__c" custom setting records 
        TorqueOpptySettings__c objTorqueOpptySettings = TorqueOpptySettings__c.getInstance(userinfo.getuserId());
        
        //RecordTypeID for oppoty records that needs to be excluded from making the trigger fire 
        strExcludeOpptyRecordType = objTorqueOpptySettings.ExcludeOpptyRecordType__c;
        
        //Iterate over new opportunities and store the ID in a set
        for(Opportunity objOpportunity : lstNewOpportunityRecords) 
        {
            if(strExcludeOpptyRecordType != objOpportunity.RecordTypeId)
            	setOpportunityID.add(objOpportunity.Id);
        }
        
        //Iterate over the opportunity records formed in the above set
        for(Opportunity objOpportunity : [ Select Id, Name, AccountId, Account.Hourly_Rate__c From Opportunity Where Id IN :setOpportunityID]) 
        {
            //Create a map of opportunityID to project record 
            mapOpportunityIDToProject.put(objOpportunity.Id, new Project__c(Name = objOpportunity.Name, Account__c = objOpportunity.AccountId, Hourly_Rate__c = objOpportunity.Account.Hourly_Rate__c));
        }
        
        //Insert project records    
        insert mapOpportunityIDToProject.values();
         
        //Iterate over the opportunity keyset to populate lookup field of project onto it
        for(Id objOpportunityID : mapOpportunityIDToProject.keyset()) 
        {
            //Create an instance of opportunity so it can be used to update
            Opportunity objOpportunity = new Opportunity(Id = objOpportunityID);
            objOpportunity.Project__c = mapOpportunityIDToProject.get(objOpportunityID).Id;
            lstOpportunityToUpdate.add(objOpportunity);
        }
        
        //Update the opportunity records to populate project lookup 
        update lstOpportunityToUpdate;
        
        //Iterate over project records from the map values
        for(Project__c objProject : mapOpportunityIDToProject.values())
        {
            //Create a map of projectID to deliverable to be created
            mapProjectIDToDeliverable.put(objProject.Id, new Deliverable__c(Project__c = objProject.Id, Name = 'Pre-Sales Placeholder')); 
        }
        
        //Insert deliverable records
        insert mapProjectIDToDeliverable.values();
        
        
        //Iterate over deliverable records
        for(Deliverable__c objDeliverable : mapProjectIDToDeliverable.values())
        {
            //Create a map of deliverableID to budgetItem to create
            mapDeliverableIDToBudgetItem.put(objDeliverable.Id, new Budget_Item__c(Name = 'Pre-Sales Placeholder', Practice_Area__c = 'Pre-Sales', Scope_Type__c = 'Pre-Sales', Status__c = 'Active', Deliverable__c = objDeliverable.Id, Project__c = objDeliverable.Project__c));
        }
        
        //Insert the budgetItem records
        insert mapDeliverableIDToBudgetItem.values();
        
    }
    
}