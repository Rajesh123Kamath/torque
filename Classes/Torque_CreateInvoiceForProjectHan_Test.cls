/**
	Class Name : Torque_CreateInvoiceForProjectHan_Test
	Created By : Trekbin
	Created On : 27-Jan-2016
	Description: Webservice class to create a "Invoice" record on click of a project detail page button "Create Invoice".
 */
 
@isTest(SeeAllData =  false)
private class Torque_CreateInvoiceForProjectHan_Test {

	/* Test Method */
    static testMethod void myUnitTest() {
		
		//Custom setting Test record
        ProjectDateSettings__c ProjectDateSettings = new ProjectDateSettings__c();          
        ProjectDateSettings.NonImplementationEndDateIncrement__c =  30;
        ProjectDateSettings.BuildConfigTestingEndDayIncrement__c = 14;
        ProjectDateSettings.BuildConfigTestingStartDayIncrement__c = 15;
        ProjectDateSettings.DesignDiscoveryEndDaysIncrement__c = -1;
        ProjectDateSettings.GoLiveAndPostGoLiveEndDayIncrement__c = 7;
        insert ProjectDateSettings;
            
		//Fetch a profile record
		Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
           
        //Create a test user record    
        User objUser = new User(Alias = 'test123', Email='trekbin@test.com', EmailEncodingKey='UTF-8', 
    							LastName='Testing', LanguageLocaleKey='en_US', LocaleSidKey='en_US', 
    							ProfileId = p.Id, TimeZoneSidKey='America/Los_Angeles', UserName='test@testrecord123.com');
        insert objUser;
            
		//Create a test account record
        Account objAccount = new Account(Name = 'Test Acc 0077', Payment_Terms__c = 2);
        insert objAccount;   
            
		//Create a test project record
		Project__c objProject = new Project__c(Name = 'Test Project 0077', Account__c = objAccount.Id, Hourly_Rate__c = 10 ,
                                               Assigned_To__c = objUser.Id , Kickoff__c = date.today(), Go_Live_Date__c = date.today(), 
                                               Status__c = 'Planning', Current_State__c = 'Maintenance');
        insert objProject;
        
        //Create a test Deliverable Record    
		Deliverable__c objDeliverable = new Deliverable__c(Name = 'Test Deliverable', Project__c = objProject.id);
		insert objDeliverable;

        //Create a test BudgetItem Record 1    
	    Budget_Item__c objBudgetItem1 = new Budget_Item__c(Deliverable__c = objDeliverable.id, Project__c = objProject.Id);
	    insert objbudgetItem1;
        
        //Create a test time card record 1
        Time_Card__c objTimeCardRecord1  = new Time_Card__c();
        objTimeCardRecord1.Name = 'Test Time Card';
        insert objTimeCardRecord1;
            
        //Create a test time entry record
        Time_Entry__c objTimeEntry1 = new Time_Entry__c();
        objTimeEntry1.Project__c = objProject.Id;
        objTimeEntry1.Billable__c = true;
        objTimeEntry1.Time_Card__c = objTimeCardRecord1.Id;  
        objTimeEntry1.Time__c = 10;
        objTimeEntry1.CurrencyIsoCode = 'USD';
        objTimeEntry1.Date__c = date.today();
        objTimeEntry1.Case_Number__c = '123';           
        objTimeEntry1.Notes__c = 'abc';  
        objTimeEntry1.Budget_Item__c = objBudgetItem1.Id;   
        objTimeEntry1.Invoice_Line__c = null;
        insert objTimeEntry1;         
         
        Expense_Report__c objExpenseReport = new Expense_Report__c();
        objExpenseReport.Name = 'Test Rep Name';
        objExpenseReport.CurrencyIsoCode = 'USD';
        objExpenseReport.Status__c = 'Approved';
        insert objExpenseReport;
        
        //Create a expense record
        Expense__c objExpenseRecord = new Expense__c();
        objExpenseRecord.CurrencyIsoCode = 'USD';
        objExpenseRecord.Project__c = objProject.Id;
        objExpenseRecord.Billable__c = true;
        objExpenseRecord.Expense_Report__c = objExpenseReport.Id;
        objExpenseRecord.Date__c = system.today();
        objExpenseRecord.Amount__c = 100;
        objExpenseRecord.Description__c = 'hello test';
        objExpenseRecord.Invoice_Comment__c = 'Hello';
        insert objExpenseRecord;
                                                
		//Create an instance of the class
		Torque_CreateInvoiceForProjectHandler objCreateInvoice = new Torque_CreateInvoiceForProjectHandler();
		
		//For assert Check if an Invoice Line record has been created for TimeEntry and is assigned
		Id invoiceLineId = [Select Id, Invoice_Line__c From Time_Entry__c Where Id = :objTimeEntry1.Id].Invoice_Line__c;
						
		//Assert check to see if IL has been created for timentry record 
		system.assertEquals(null, invoiceLineId);
		
		//Call the method
		Torque_CreateInvoiceForProjectHandler.generateInvoiceRecords(objProject.Id, objAccount.Id);
		
		//For assert Check if an Invoice Line record has been created for TimeEntry and is assigned
		invoiceLineId = [Select Id, Invoice_Line__c From Time_Entry__c Where Id = :objTimeEntry1.Id].Invoice_Line__c;
				
		//Assert check to see if IL has been created for timentry record 
		system.assert(invoiceLineId != null);  
		
		//For assert Check if an Invoice Line record has been created for Expense and is assigned
		Id invoiceLineId2 = [Select Id, Invoice_Line__c From Expense__c Where Id = :objExpenseRecord.Id].Invoice_Line__c;
				
		//Assert check to see if IL has been created for expense record 
		system.assert(invoiceLineId2 != null);  
		
		//For assert Check if an Invoice record has been created 	
		List<Invoice__c> lstInvoiceToCheck = new List<Invoice__c>([Select Id, Account__c From Invoice__c Where Account__c = :objProject.Account__c]);
			
		//Assert check to see if IL has been created for timentry record 
		system.assertEquals(1, lstInvoiceToCheck.size());  
    }
    
    /* Test Method */
    static testMethod void myUnitTest1() {
		
		//Custom setting Test record
        ProjectDateSettings__c ProjectDateSettings = new ProjectDateSettings__c();          
        ProjectDateSettings.NonImplementationEndDateIncrement__c =  30;
        ProjectDateSettings.BuildConfigTestingEndDayIncrement__c = 14;
        ProjectDateSettings.BuildConfigTestingStartDayIncrement__c = 15;
        ProjectDateSettings.DesignDiscoveryEndDaysIncrement__c = -1;
        ProjectDateSettings.GoLiveAndPostGoLiveEndDayIncrement__c = 7;
        insert ProjectDateSettings;
            
		//Fetch a profile record
		Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
           
        //Create a test user record    
        User objUser = new User(Alias = 'test123', Email='trekbin@test.com', EmailEncodingKey='UTF-8', 
    							LastName='Testing', LanguageLocaleKey='en_US', LocaleSidKey='en_US', 
    							ProfileId = p.Id, TimeZoneSidKey='America/Los_Angeles', UserName='test@testrecord123.com');
        insert objUser;
            
		//Create a test account record
        Account objAccount = new Account(Name = 'Test Acc 0077');
        insert objAccount;   
            
		//Create a test project record
		Project__c objProject = new Project__c(Name = 'Test Project 0077', Account__c = objAccount.Id, Hourly_Rate__c = 10 ,
                                               Assigned_To__c = objUser.Id , Kickoff__c = date.today(), Go_Live_Date__c = date.today(), 
                                               Status__c = 'Planning', Current_State__c = 'Maintenance');
        insert objProject;
        
        //Create a test Deliverable Record    
		Deliverable__c objDeliverable = new Deliverable__c(Name = 'Test Deliverable', Project__c = objProject.id);
		insert objDeliverable;

        //Create a test BudgetItem Record 1    
	    Budget_Item__c objBudgetItem1 = new Budget_Item__c(Deliverable__c = objDeliverable.id, Project__c = objProject.Id);
	    insert objbudgetItem1;
        
        //Create a test time card record 1
        Time_Card__c objTimeCardRecord1  = new Time_Card__c();
        objTimeCardRecord1.Name = 'Test Time Card';
        insert objTimeCardRecord1;
            
        //Create a test time entry record
        Time_Entry__c objTimeEntry1 = new Time_Entry__c();
        objTimeEntry1.Project__c = objProject.Id;
        objTimeEntry1.Billable__c = true;
        objTimeEntry1.Time_Card__c = objTimeCardRecord1.Id;  
        objTimeEntry1.Time__c = 10;
        objTimeEntry1.CurrencyIsoCode = 'USD';
        objTimeEntry1.Date__c = date.today();
        objTimeEntry1.Case_Number__c = '123';           
        objTimeEntry1.Notes__c = 'abc';  
        objTimeEntry1.Budget_Item__c = objBudgetItem1.Id;    
        insert objTimeEntry1;         
         
        Expense_Report__c objExpenseReport = new Expense_Report__c();
        objExpenseReport.Name = 'Test Rep Name';
        objExpenseReport.CurrencyIsoCode = 'USD';
        objExpenseReport.Status__c = 'Approved';
        insert objExpenseReport;
        
        //Create a expense record
        Expense__c objExpenseRecord = new Expense__c();
        objExpenseRecord.CurrencyIsoCode = 'USD';
        objExpenseRecord.Project__c = objProject.Id;
        objExpenseRecord.Billable__c = true;
        objExpenseRecord.Expense_Report__c = objExpenseReport.Id;
        objExpenseRecord.Date__c = system.today();
        objExpenseRecord.Amount__c = 100;
        objExpenseRecord.Description__c = 'hello test';
        objExpenseRecord.Invoice_Comment__c = 'Hello';
        insert objExpenseRecord;
                                                
		//Create an instance of the class
		Torque_CreateInvoiceForProjectHandler objCreateInvoice = new Torque_CreateInvoiceForProjectHandler();
		
		//Call the method
		Torque_CreateInvoiceForProjectHandler.generateInvoiceRecords(objProject.Id, objAccount.Id);
		  			        
    }
}