/**
    @Name           :   Torque_WeeklyViewHandler
    @Date           :   30 Nov, 2015
    @Description    :   An apex page controller for Torque_WeeklyViewPage page.
    @TestClass      :   
*/
public with sharing class Torque_WeeklyViewHandler {
        
    public Decimal totalHoursPrevWeek           { get;set; }
    public Decimal totalBillableHoursPrevWeek   { get;set; }
    public Decimal totalUnBillableHoursPrevWeek { get;set; } 
  
    public Decimal totalHoursThisWeek                   { get;set; }
    public Decimal totalBillableHoursThisWeek   { get;set; }
    public Decimal totalUnBillableHoursThisWeek { get;set; }
    
    public date dtWeekStart                                     { get;set; }
    
    public date dtWeekEnd                                               { get;set; }
    
    public Time_Entry__c timeEntryObject                { get;set; }
    
    public Map<String, WeekEntryWrapperClass> mapConcatStrToWeekEntryWrapper { get; set; }
    
    public String strTimeCardStatus 				{get;set;}			//Last Modified By Trekbin on 18-july, 2016 as per the updated requirement on case: 00019049	[Used to display timecard status in section 1 under statistics]
    
    public set<Id> setTimecard = new set<Id>();
    
    /* Created by Trekbin on 18-July, 2016 as per the requirement on case: 19049  [R]*/
    //Declare a user object	[R]
	public User objUser;
	
	//String variable to display the error message on the page	[R]
	public String strErrorMessageToDisplay		{get; set;}
    
    //String variable to display the alert message on the page	[R]
    public String strAlertMessageToDisplay		{get; set;}
    
    //String variable to display the alert message on the page  [R]
    public String strPTOAlertMessageToDisplay   {get; set;}
    
    public Quick_Note__c objQuickNote  		 	{get; set;}			//Last Modified By Trekbin on 18-july, 2016 as per the updated requirement on case: 00019049	[Used to display quick notes in section 1 and attach to the fields under statistics]
            
    //Default constructor
    public Torque_WeeklyViewHandler() {
    	
    	/* Created by Trekbin on 18-July, 2016 as per the requirement on case: 19049  [R]*/
    	objQuickNote = new Quick_Note__c();
    	
       /*  timeEntryObject = new Time_Entry__c();
      
        timeEntryObject.Date__c = date.today();
        
        totalHoursPrevWeek = 0;
        totalBillableHoursPrevWeek = 0;
        totalUnBillableHoursPrevWeek = 0;
      
        totalHoursThisWeek = 0;
        totalBillableHoursThisWeek = 0;
        totalUnBillableHoursThisWeek = 0;
        
        //Initializing the map
        mapConcatStrToWeekEntryWrapper = new Map<String, WeekEntryWrapperClass>();
        
        Date dtWeekStart;
                Date dtWeekEnd;
                Date dtToday = date.today();
                dtWeekStart = dtToday.toStartOfWeek();
                dtWeekEnd = dtWeekStart.addDays(6);
                
        if(userinfo.getuserId() != null) {
                
            fetchTimeCardHours(userinfo.getuserId());
            fetchTimeEntryRecords(userinfo.getuserId(),dtWeekStart,dtWeekEnd);
        }
         */
        
        /* Last Modified By Trekbin on 1-August, 2016 as per the updated requirement on case: 00026389 [R] - redirection*/ 
  		if(ApexPages.currentPage().getParameters().get('weekDateForRedirection') != null && ApexPages.currentPage().getParameters().get('weekDateForRedirection') != '') {
  			
  			timeEntryObject = new Time_Entry__c();
  			
  			timeEntryObject.Date__c = Date.valueOf(ApexPages.currentPage().getParameters().get('weekDateForRedirection'));
  		}
           
        fetchTodaysRecords();
        
        /* Created by Trekbin on 18-July, 2016 as per the requirement on case: 19049  [R]*/
        
        //Query user object to fetch 'Time__c' field of the current logged-in user  [R]
		objUser = [Select Id, Work_Week_Hours__c From User Where Id = :userInfo.getUserId()];

    }
    
     /*
        Method name : fetchTimeCardHours
        Parameters  : Id currentUserId
        Description : Method to fecth time card hours of previous and current week
    */
    public void fetchTimeCardHours(Id currentUserId){
        
        /* Last Modified By Trekbin on 18-july, 2016 as per the updated requirement on case: 19049	[R]*/
        strTimeCardStatus = '';
        
        for(Time_Card__c objTimeCard : [SELECT Id,User__c, Status__c, Billable_Hours_Logged__c, Total_Hours_Logged__c, Unbilled_Hours__c, Week_Beginning__c
                                                                        FROM Time_Card__c WHERE User__c =:userinfo.getuserId()
                                                                  ]) {
                                                                        
                        setTimecard.add(objTimeCard.Id);
                
        }
        
        //Fetch previous week time entry fields of current logged in user 
        for(Time_Card__c timeEntryObj : [       Select Id,Billable_Hours_Logged__c, Status__c, Total_Hours_Logged__c,Unbilled_Hours__c,Week_Beginning__c ,User__c
                                                                                FROM Time_Card__c
                                                                                WHERE User__c =:currentUserId 
                                                                                AND Week_Beginning__c = LAST_WEEK
                                                                         ]){
                
            totalHoursPrevWeek = totalHoursPrevWeek+timeEntryObj.Total_Hours_Logged__c;
            totalBillableHoursPrevWeek = totalBillableHoursPrevWeek+timeEntryObj.Billable_Hours_Logged__c;
            totalUnBillableHoursPrevWeek = totalUnBillableHoursPrevWeek+timeEntryObj.Unbilled_Hours__c;
            
            //Update the time card status under statistics of section 1 	[R]
            strTimeCardStatus = timeEntryObj.Status__c;
        }
        
        //Fetch current week time entry fields of current logged in user 
        for(Time_Card__c timeEntryObj : [       Select Id,Billable_Hours_Logged__c, Status__c, Total_Hours_Logged__c,Unbilled_Hours__c,Week_Beginning__c
                                                                                FROM Time_Card__c 
                                                                                WHERE User__c =:currentUserId
                                                                            AND Week_Beginning__c = THIS_WEEK 
                                                                        ]) {
          
            totalHoursThisWeek = totalHoursThisWeek+timeEntryObj.Total_Hours_Logged__c;
            totalBillableHoursThisWeek = totalBillableHoursThisWeek+timeEntryObj.Billable_Hours_Logged__c;
            totalUnBillableHoursThisWeek = totalUnBillableHoursThisWeek+timeEntryObj.Unbilled_Hours__c;
            
            //Update the time card status under statistics of section 1 	[R]
            strTimeCardStatus = timeEntryObj.Status__c;
        }
        
    }
   
       
    /*
        Method name : fetchTimeEntryRecords
        Parameters  : Id currentUserId, Date dtWeekStart, Date dtWeekEnd
        Description : Method to fetch time entries
    */
    public void fetchTimeEntryRecords(Id currentUserId,Date dtWeekStart, Date dtWeekEnd) { 
              
        //Fetch all the time entry records for the current logged in user
        for(Time_Entry__c  timeEntryRecord : [  Select Id, Name, Time__c, Time_Card__c,Time_Card__r.Total_Hours_Logged__c,Time_Card__r.User__c, 
                                                Time_Card__r.Billable_Hours_Logged__c,Time_Card__r.Status__c, Time_Card__r.Non_Billable_Hours_Logged__c, Project__c,Budget_Item__r.Name,
                                                Project__r.Name, Performed_By__c, Date__c, CreatedById, Budget_Item__c ,Time_Card__r.Week_Beginning__c,Time_Card__r.Week_End__c
                                                From Time_Entry__c 
                                                Where Time_Card__r.User__c = : userinfo.getuserID()
                                                //AND Time_Card__c IN : setTimecard //='a2I21000000CaR7'
                                                AND Project__c != null
                                                AND Budget_Item__c != null
                                                AND (Date__c >=:dtWeekStart AND Date__c <=: dtWeekEnd)
                                                order by Project__r.Name asc, Budget_Item__r.Name asc
                                                
                                             ]) {
            system.debug('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! timeEntryRecord : ' + timeEntryRecord);
            
            //Declare date
                        Date dateOfTimeEntry;
    
            //Declare time
            Decimal timeOfTimeEntry; 
            
            //WeekEntry wrapper class variable
            WeekEntryWrapperClass weekEntryWrapperInstance;         
            
                        //Check if the map contains the key : For the first time it wont come inside 'if' part as the week entry wrapper map doesnot contain unique key
                        if(mapConcatStrToWeekEntryWrapper.containsKey(timeEntryRecord.Project__r.Name+':'+timeEntryRecord.Budget_Item__r.Name)) {
                                
                        weekEntryWrapperInstance = mapConcatStrToWeekEntryWrapper.get((timeEntryRecord.Project__r.Name+':'+timeEntryRecord.Budget_Item__r.Name));
                        
                        //Common method call
                        conditionalDayCheck(dateOfTimeEntry, timeOfTimeEntry, timeEntryRecord, weekEntryWrapperInstance);
                        }
                        else {
                                
                                //Set all the values as null for wrapper class - 2
                                weekEntryWrapperInstance = new WeekEntryWrapperClass( new DateTimeEntryWrapperClass(null, 0.0, timeEntryRecord), 
                                                                                      new DateTimeEntryWrapperClass(null, 0.0, timeEntryRecord),
                                                                                      new DateTimeEntryWrapperClass(null, 0.0, timeEntryRecord),
                                                                                      new DateTimeEntryWrapperClass(null, 0.0, timeEntryRecord),
                                                                                      new DateTimeEntryWrapperClass(null, 0.0, timeEntryRecord),
                                                                                      new DateTimeEntryWrapperClass(null, 0.0, timeEntryRecord),
                                                                                      new DateTimeEntryWrapperClass(null, 0.0, timeEntryRecord));
                                //Common method call
                                conditionalDayCheck(dateOfTimeEntry, timeOfTimeEntry, timeEntryRecord, weekEntryWrapperInstance);           
                        } 
        }   
    }
    
    
    /*
        Method name : conditionalDayCheck
        Parameters  : Date dateOfTimeEntry, Decimal timeOfTimeEntry, Time_Entry__c  timeEntryRecord, WeekEntryWrapperClass weekEntryWrapperInstance
        Description : Method to get the time entries based on week
    */
    private void conditionalDayCheck(Date dateOfTimeEntry, Decimal timeOfTimeEntry, Time_Entry__c  timeEntryRecord, WeekEntryWrapperClass weekEntryWrapperInstance) {
        
                //Fetch the date of the user's time entry record 
                dateOfTimeEntry = timeEntryRecord.Date__c;
                
                //Fetch the time of the user's time entry record 
                timeOfTimeEntry = timeEntryRecord.Time__c;  
                
                //If the entries contains some values
                if(dateOfTimeEntry != null && timeOfTimeEntry!= null) {
                    //Convert date to date time
                    DateTime dt = (DateTime) dateOfTimeEntry;
                    
                    //Fetch the day of the date in 3 letter format
                    String dayOfWeek = dt.formatGMT('EEE');    
                      
            
            //If the day contains some value
                        if(dayOfWeek != null) {
                        
                                //Assiging values to time entry based on day of week
                                if(dayOfWeek == 'Sun') {
                                        
                                    weekEntryWrapperInstance.sunday.myDay = dayOfWeek;
                                    weekEntryWrapperInstance.sunday.myTime += timeOfTimeEntry;
                                }
                                else if(dayOfWeek == 'Mon') {
                                        
                                    weekEntryWrapperInstance.monday.myDay = dayOfWeek;
                                    weekEntryWrapperInstance.monday.myTime += timeOfTimeEntry;
                                }
                                else if(dayOfWeek == 'Tue') {
                                        
                                    weekEntryWrapperInstance.tuesday.myDay = dayOfWeek;
                                    weekEntryWrapperInstance.tuesday.myTime += timeOfTimeEntry;
                                }
                                else if(dayOfWeek == 'Wed') {
                                        
                                    weekEntryWrapperInstance.wednesday.myDay = dayOfWeek;
                                    weekEntryWrapperInstance.wednesday.myTime += timeOfTimeEntry;
                                }
                                else if(dayOfWeek == 'Thu') {
                                        
                                    weekEntryWrapperInstance.thursday.myDay = dayOfWeek;
                                    weekEntryWrapperInstance.thursday.myTime += timeOfTimeEntry;
                                }
                                else if(dayOfWeek == 'Fri') {
                                        
                                    weekEntryWrapperInstance.friday.myDay = dayOfWeek;
                                    weekEntryWrapperInstance.friday.myTime += timeOfTimeEntry; 
                                }
                                else if(dayOfWeek == 'Sat') {
                                    weekEntryWrapperInstance.saturday.myDay = dayOfWeek;
                                    weekEntryWrapperInstance.saturday.myTime += timeOfTimeEntry;
                                }
                }
        
                //Create a map of unique key to weekEntry wrapper class
                mapConcatStrToWeekEntryWrapper.put(timeEntryRecord.Project__r.Name+':'+timeEntryRecord.Budget_Item__r.Name, weekEntryWrapperInstance);
                system.debug('!!!!!!!!!!!!!!!!!!!!!!!!! mapConcatStrToWeekEntryWrapper last' + mapConcatStrToWeekEntryWrapper);  
        }
 }
    
    /*
        Method name : getPrevRecords
        Description : Method to get the time entries of previous week
    */
    public void getPrevRecords() {
        
        String startdate = ApexPages.currentPage().getParameters().get('startdate');
        String enddate = ApexPages.currentPage().getParameters().get('enddate');
        
        
        
        system.debug('!!!!!!!!!!!!!!!!!!! startdate prev' +startdate);
        system.debug('!!!!!!!!!!!!!!!!!!! enddate prev' +enddate);
        
        date mystartdate = date.parse(startdate);
        date myenddate = date.parse(enddate);
        
        // Added by Raja to change the date on the UI 
        timeEntryObject.Date__c = mystartdate ;
        
        WeekEntryWrapperClass weekEntryWrapperInstance;  
        
        //Initialize wrapper class
                mapConcatStrToWeekEntryWrapper = new Map<String, WeekEntryWrapperClass>();
         
         //Passing previous week dates to fetchTimeEntryRecords method to fetch  previous weeks records
                fetchTimeEntryRecords(userinfo.getuserId(),mystartdate,myenddate);
         
        /* Last Modified By Trekbin on 18-july, 2016 as per the updated requirement on case: 00019049 [R]*/ 
        displayQuickNotesValueForTimeCard();  
        
        /* Last Modified By Trekbin on 18-july, 2016 as per the updated requirement on case: 00019049 [R] - To display updated timecard status*/ 
        displayUpdatedTimeCardStatus();
    }
    
     /*
        Method name : getNextRecords
        Description : Method to get the time entries of next week
    */
    public void getNextRecords() {
        
        String startdate = ApexPages.currentPage().getParameters().get('startdate');
        String enddate = ApexPages.currentPage().getParameters().get('enddate');
        
        date mystartdate = date.parse(startdate);
        date myenddate = date.parse(enddate);
        
        // Added by Raja to change the date on the UI 
        timeEntryObject.Date__c = mystartdate ;
        
                WeekEntryWrapperClass weekEntryWrapperInstance;  
        
        //Initialize wrapper class
                mapConcatStrToWeekEntryWrapper = new Map<String, WeekEntryWrapperClass>();
         
        //Passing Next week dates to fetchTimeEntryRecords method to fetch next weeks records
                fetchTimeEntryRecords(userinfo.getuserId(),mystartdate,myenddate);
        
        /* Last Modified By Trekbin on 18-july, 2016 as per the updated requirement on case: 00019049 [R]*/ 
        displayQuickNotesValueForTimeCard();  
        
        /* Last Modified By Trekbin on 18-july, 2016 as per the updated requirement on case: 00019049 [R] - To display updated timecard status*/ 
        displayUpdatedTimeCardStatus();
    }
    
    /*
        Method name : fetchTodaysRecords
        Description : Method to get the time entries of Todays date
    */
    public pageReference fetchTodaysRecords() { 
       
        mapConcatStrToWeekEntryWrapper = new Map<String, WeekEntryWrapperClass>();
        
        timeEntryObject = new Time_Entry__c();
      
        timeEntryObject.Date__c = date.today();
        
        totalHoursPrevWeek = 0;
        totalBillableHoursPrevWeek = 0;
        totalUnBillableHoursPrevWeek = 0;
      
        totalHoursThisWeek = 0;
        totalBillableHoursThisWeek = 0;
        totalUnBillableHoursThisWeek = 0;
       
       Date dtWeekStart;
                Date dtWeekEnd;
                Date dtToday = date.today();
                dtWeekStart = dtToday.toStartOfWeek();
                dtWeekEnd = dtWeekStart.addDays(6);
                
        if(userinfo.getuserId() != null) {
                
            fetchTimeCardHours(userinfo.getuserId());
            fetchTimeEntryRecords(userinfo.getuserId(),dtWeekStart,dtWeekEnd);
        }
        
        /* Last Modified By Trekbin on 18-july, 2016 as per the updated requirement on case: 00019049 [R]*/ 
        displayQuickNotesValueForTimeCard(); 
        
        return null;
    }
    
    
    
    /**
		@ Method name 	: 	checkEnoughWeekHoursExists
		@ Parameters	: 	None
		@ Created By	: 	Trekbin on 18-july, 2016
		@ Description	: 	Method to check enough hours are logged for current week, if not display error in popup	[R]
	 */
	public void checkEnoughWeekHoursExists() {
		
		//Hold the sum total of time for all the timeentries for all the timecards pertaining to the current week
		Decimal totalTimeOfTimeEntriesInWeek = 0.0;
		
		//Initialize to null
		strErrorMessageToDisplay = null;
		
		//Initialize to null
		strAlertMessageToDisplay = null;
		
		//User selected date's week
		Date objWeekOfTheUserSelectedDate = timeEntryObject.Date__c.toStartofWeek();
		
		//Fetch the 'GlobalSettings__c' hierarchy based custom setting record so to display error messages based on condition
		GlobalSettings__c objGlobalSettingCS = GlobalSettings__c.getInstance(objUser.Id);
		
		//Fetch the week's timecard record 
		List<Time_Card__c> lstTimeCards = [SELECT Id, User__c, Week_Beginning__c, Week_End__c, (Select Time_Card__c, Notes__c From Torque_Quick_Notes__r order by LastModifiedDate asc limit 1) FROM Time_Card__c WHERE User__c = :objUser.Id AND (Week_Beginning__c <= :objWeekOfTheUserSelectedDate) AND (Week_End__c >= :objWeekOfTheUserSelectedDate)];
		
		//Proceed only If there is any corresponding timeCard record 
		if(!lstTimeCards.isEmpty())	{
			
			//Iterate over all the timeEntry records pertaining to the timeCard record whose timeCard record matches with the record fetched above
			for(Time_Entry__c objTimeEntry : [SELECT Id, Time__c, Time_Card__c FROM Time_Entry__c WHERE Time_Card__c = :lstTimeCards[0].Id]) {
				
				//Sum and store all the timeentries for all the timecards pertaining to the current week
				totalTimeOfTimeEntriesInWeek += objTimeEntry.Time__c;
			}
			
			/* Last Modified by Trekbin on 26-July, 2016 as per the reported issue on case: 00019049 */
			//If the user has not entered complete hours for the current week & 'totalTimeOfTimeEntriesInWeek != 0' check is to prevent devide by 0 error if no timeentries exists for a timecard and user clicks on submit 
			if((totalTimeOfTimeEntriesInWeek != 0.0 && totalTimeOfTimeEntriesInWeek != 0) && (objUser.Work_Week_Hours__c != null) && ((totalTimeOfTimeEntriesInWeek/objUser.Work_Week_Hours__c) < 0.85)) {
				
				//Display an error message stored in the custom setting field
				strErrorMessageToDisplay = objGlobalSettingCS.Submission_Alert_Time_Threshold__c;
			}
			
			/* For second message in popup relating to quick notes */
			//Check if there exists any quick note records 							 
			if(!lstTimeCards[0].Torque_Quick_Notes__r.isEmpty()) {
				
				//Display an alert message stored in the custom setting field
				strAlertMessageToDisplay = objGlobalSettingCS.Submission_Alert_Quick_Notes__c;
			}
		}
		
		checkPTOHoursForTimeCard();
	}
 	
	
	 /**
		@ Method name 	: 	checkPTOHoursForTimeCard
		@ Parameters	: 	None
		@ Created By	: 	Trekbin on 19-july, 2016
		@ Description	: 	Method to check if too much pto hours are logged for a day, if so then display an alert in popup	[R]
	 */
	public void checkPTOHoursForTimeCard() {
		
		system.debug('------==--checkPTOHoursForTimeCard--==-IN--');
		
		//Initialize to null
		strPTOAlertMessageToDisplay = null;
		
		//Hold the custom setting related budgetItem record ID's in a set
		set<Id> setPTOBIRecordIDs = new set<Id>();
		
		//Set to hold the time entry dates for the time entry records
		set<Date> setDatesForTEWithPTO = new set<Date>();
		
		//Form a map of date to all time entry time tomal for that date
		Map<Date, Decimal> mapDateToAllDateWiseTESum = new Map<Date, Decimal>();
		
		//User selected date's week
		Date objWeekOfTheUserSelectedDate = timeEntryObject.Date__c.toStartofWeek();
		
		/* Hierarchy based custom setting for checking if time entries are logged against the records present in this custom setting*/
		//Fetch the 'GlobalSettings__c' hierarchy based custom setting record for checking if time entries are logged against the records present in this custom setting 
		GlobalSettings__c objGlobalSettingCS = GlobalSettings__c.getInstance(objUser.Id);
		
		/* List based custom setting for fetching alert message in case of condition fail*/
		//List to hold the list based 'Chatter_PTOBudgetItems__c' custom setting records
		List<Chatter_PTOBudgetItems__c> lstPTOBudgetItems = Chatter_PTOBudgetItems__c.getAll().values();
		
		//Iterate over all the custom setting records and store the IDs in a set
		for(Chatter_PTOBudgetItems__c objPTOCS : lstPTOBudgetItems) {
			
			//Hold the budgetItem record ID's present in custom setting records into a set
			setPTOBIRecordIDs.add(objPTOCS.id__c);
		}
		 
		//Fetch the current logged in users timecard record that lies within weekbegging and week end i.e. 1 timecard per week
		List<Time_Card__c> lstTimeCards = [SELECT Id, User__c, Week_Beginning__c, Week_End__c FROM Time_Card__c WHERE User__c = :objUser.Id AND (Week_Beginning__c <= :objWeekOfTheUserSelectedDate) AND (Week_End__c >= :objWeekOfTheUserSelectedDate)];
		
		//Proceed only If there is any corresponding timeCard record 
		if(!lstTimeCards.isEmpty())	{
			
			system.debug('------==--lstTimeCards--=NOT EMPTY=---' +lstTimeCards);
			 
			//Iterate over all the time entry records for the particular time card whose budgetItem is present in the set 'setPTOBIRecordIDs' formed above  
			for(Time_Entry__c objTimeEntry : [SELECT Id, Budget_Item__c, Date__c, Time_Card__c FROM Time_Entry__c WHERE Time_Card__c = :lstTimeCards[0].Id AND Budget_Item__c IN :setPTOBIRecordIDs]) {
				
				//Set to hold the time entry dates for the time entry records whose budget item is present in the date set formed above
				setDatesForTEWithPTO.add(objTimeEntry.Date__c);
			}	
			
			//Proceed if the set contains any values 
			if(!setDatesForTEWithPTO.isEmpty()) {
				
				system.debug('------==--setDatesForTEWithPTO--=NOT EMPTY=---' +setDatesForTEWithPTO);
				
				//Iterate over all the time entrie records whose date value exists in the set formed above
				for(Time_Entry__c objTimeEntry : [SELECT Id, Budget_Item__c, Time__c, Date__c, Time_Card__c FROM Time_Entry__c WHERE Time_Card__c = :lstTimeCards[0].Id AND Date__c IN :setDatesForTEWithPTO]) {
					
					//Form a map of date to sum of Time__c of all the time entries for each date
					if(!mapDateToAllDateWiseTESum.containsKey(objTimeEntry.Date__c))
						mapDateToAllDateWiseTESum.put(objTimeEntry.Date__c, 0.0);
						 
					mapDateToAllDateWiseTESum.put(objTimeEntry.Date__c, mapDateToAllDateWiseTESum.get(objTimeEntry.Date__c) + objTimeEntry.Time__c);
				}
				
				//If the map formed above contains any values then proceed
				if(!mapDateToAllDateWiseTESum.keySet().isEmpty()) {
					
					system.debug('------==--mapDateToAllDateWiseTESum--=NOT EMPTY=---' +mapDateToAllDateWiseTESum);
					
					Decimal userWeekWorkHours = objUser.Work_Week_Hours__c/5;
					
					//If any of the hours exceeds userWeekWorkHours then set the boolean variable to true so as to display alert message
					Boolean displayPTOAlert = false;
					
					//Iterate over all the values from the map formed above 'mapDateToAllDateWiseTESum'
					for(Decimal dblTotalHours : mapDateToAllDateWiseTESum.values()) {
						
						system.debug('------==--dblTotalHours=---' +dblTotalHours);
						
						//If the time entry total hours exceeds userWeekWorkHours
						if(dblTotalHours > userWeekWorkHours) {
							
							//Set the the boolean variable to true
							displayPTOAlert = true;
							
							system.debug('------==--displayPTOAlert=---' +displayPTOAlert);
						}
					}
					
					//If there is a boolean with true then display an alert message
					if(displayPTOAlert) {
						
						strPTOAlertMessageToDisplay = objGlobalSettingCS.Submission_Alert_PTO_Threshold__c;
						
						system.debug('------==--strPTOAlertMessageToDisplay--==---' +strPTOAlertMessageToDisplay);
					}
				}
			}
		}
	}	
				
	/**
		@ Method Name	:	displayQuickNotesValueForTimeCard
		@ Parameters	: 	None
		@ Created By	: 	Trekbin on 18-july, 2016
		@ Description	: 	display quick notes on click of prev and next button in section 1	[R]
	 */
	public void displayQuickNotesValueForTimeCard() {
		
		//User selected date
		Date objWeekOfTheUserSelectedDate = timeEntryObject.Date__c.toStartofWeek();
		
		//Fetch timecard with its childs for the current week 
		List<Time_Card__c> lstTimeCardRecords  = [ SELECT Id, Week_Beginning__c, Week_End__c, (Select Time_Card__c, Notes__c From Torque_Quick_Notes__r order by LastModifiedDate asc limit 1) FROM Time_Card__c WHERE User__c = :userInfo.getUserId() AND (Week_Beginning__c <= :objWeekOfTheUserSelectedDate) AND (Week_End__c >= :objWeekOfTheUserSelectedDate)];
		
		//If there is any corresponding time card with the criteria then proceed   
		if(!lstTimeCardRecords.isEmpty())	{			
			
			//Check if there exists any quick note records 							 
			if(!lstTimeCardRecords[0].Torque_Quick_Notes__r.isEmpty()) {
				
				//Assign the notes to the set variable
				objQuickNote.Notes__c = lstTimeCardRecords[0].Torque_Quick_Notes__r[0].Notes__c;
			}
			else {
				
				//Set the notes as empty
				objQuickNote.Notes__c = '';
			}
		}
		else {

			//Set the notes as empty
			objQuickNote.Notes__c = '';
		}
	}
	
	
	/**
		@ Method name 	: 	submitTimeCardRecord
		@ Parameters	: 	None
		@ Created By	: 	Trekbin on 18-july, 2016
		@ Description	: 	Method to change the status of timeCard and display updated status in statistics section	[R]
	 */
	public void submitTimeCardRecord() {
		
		//User selected date
		Date objWeekOfTheUserSelectedDate = timeEntryObject.Date__c.toStartofWeek();
		
		//List of timeCard records to update 
		List<Time_Card__c> lstTimeCardsToUpdate = new List<Time_Card__c>();
		
		//Fetch timecard for the current week 
		List<Time_Card__c> lstTimeCardRecords  = [ SELECT Id, Status__c, Week_Beginning__c, Week_End__c FROM Time_Card__c WHERE User__c = :userInfo.getUserId() AND (Week_Beginning__c <= :objWeekOfTheUserSelectedDate) AND (Week_End__c >= :objWeekOfTheUserSelectedDate)];
		
		//If there is any corresponding time card with the criteria then proceed   
		if(!lstTimeCardRecords.isEmpty())	{
			
			//Update the timecard record and add it to the list to perform dml updates
			Time_Card__c objTimeCard = new Time_Card__c(Id = lstTimeCardRecords[0].Id);
			objTimeCard.Status__c = 'Submitted';
			lstTimeCardsToUpdate.add(objTimeCard);
		}
		
		//Perform updates
		if(!lstTimeCardsToUpdate.isEmpty())
			update lstTimeCardsToUpdate; 
	}
	
	
	/**
		@ Method Name	:	displayUpdatedTimeCardStatus
		@ Parameters	: 	None
		@ Created By	: 	Trekbin on 18-july, 2016
		@ Description	: 	display timecard status on click of prev and next button in section 1	[R]
	 */
	public void displayUpdatedTimeCardStatus() {
		
		//User selected date
		Date objWeekOfTheUserSelectedDate = timeEntryObject.Date__c.toStartofWeek();
		
		//Update the time card status under statistics of section 1 	[R]
        List<Time_Card__c> lstTimeCard = [ SELECT Id, Status__c, User__c, Week_Beginning__c, Week_End__c FROM Time_Card__c WHERE User__c = :objUser.Id AND (Week_Beginning__c <= :objWeekOfTheUserSelectedDate) AND (Week_End__c >= :objWeekOfTheUserSelectedDate)];	
	    
	    //If the list returns any value then change the status of that in ui
	    if(!lstTimeCard.isEmpty())
	    	strTimeCardStatus = lstTimeCard[0].Status__c;
	}
	
	/**
		@ Method Name	:	performRedirectionToDetailViewPageAsPerDate
		@ Parameters	: 	None
		@ Created By	: 	Trekbin on 18-july, 2016
		@ Description	: 	redirect to daily view page on the particular day on click of the day link	[R]
	 */
	public PageReference performRedirectionToDetailViewPageAsPerDate(){
		
	   	//Fetch the date passed from the page where user clicked so that it can be passed in the url of dailyViewPage
	   	String strDateForRedirection = ApexPages.currentPage().getParameters().get('weekDateToPassToController');
	    
	    //Declare the url and perform redirection
	   	String url = '';
        url = System.URL.getSalesforceBaseUrl().toExternalForm();       
        PageReference redirect = new PageReference(url + '/apex/Torque_DailyViewPage?dateForRedirection='+strDateForRedirection);  
        redirect.setRedirect(true);
        
        return redirect;
	}
	
	
    /*
        Wrapper class - 1
        Class name    : DateTimeEntryWrapperClass
        Description   : To hold the day and time for the log entry
    */
    public class DateTimeEntryWrapperClass {
        
        //String variables
        public String myDay                              {get; set;}     
        public Decimal myTime                            {get; set;} 
        public Time_Entry__c TimeEntryRecord {get; set;}  
        
        //Wrapper class constructor
        public DateTimeEntryWrapperClass(String myDay, Decimal myTime, Time_Entry__c TimeEntryRecord) {
             
            this.myDay = myDay;   
            this.myTime = myTime;
            this.TimeEntryRecord = TimeEntryRecord;   
        }
    }
    
    /*
        Wrapper class - 2 
        Class name    : WeekEntryWrapperClass
        Description   : To hold the 7 instances of the previous wrapper class. Each instance for each day that will have day & time for each day
    */
    public class WeekEntryWrapperClass {
        
        //Wrapper variables or instances for DateTimeEntryWrapperClass
        public DateTimeEntryWrapperClass sunday         {get; set;}    
        public DateTimeEntryWrapperClass monday         {get; set;}      
        public DateTimeEntryWrapperClass tuesday        {get; set;}      
        public DateTimeEntryWrapperClass wednesday  {get; set;}      
        public DateTimeEntryWrapperClass thursday   {get; set;}     
        public DateTimeEntryWrapperClass friday         {get; set;}      
        public DateTimeEntryWrapperClass saturday       {get; set;}      
               
        //Wrapper class constructor
        public WeekEntryWrapperClass(DateTimeEntryWrapperClass sunday, DateTimeEntryWrapperClass monday, DateTimeEntryWrapperClass tuesday, DateTimeEntryWrapperClass wednesday, DateTimeEntryWrapperClass thursday, DateTimeEntryWrapperClass friday, DateTimeEntryWrapperClass saturday) {
            this.sunday = sunday;   
            this.monday = monday;   
            this.tuesday = tuesday; 
            this.wednesday = wednesday; 
            this.thursday = thursday; 
            this.friday = friday; 
            this.saturday = saturday; 
        }
    }
}