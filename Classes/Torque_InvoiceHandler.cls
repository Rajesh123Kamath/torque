/**
  * Handler Name : Torque_InvoiceHandler
  * Created By   : Trekbin          
  * Created On   : 19-Nov-2015
  * Modified On  : 28-Nov-2015
  * Modified by  : Trekbin
  * Description  : Handler class for the trigger 'Torque_InvoiceTrigger'
**/
public with sharing class Torque_InvoiceHandler {   
    
    //Common method 
    public static void validateInvoiceBeforeUpdateAndDelete(List<Invoice__c> lstNewInvoice, Map<Id, Invoice__c> mapOldInvoices) {
         
        /* Added this boolean variable By 'Trekbin' on '10-feb-2016' as per case '25221' */
        //Boolean variable for updating/ deleting based on the permission
        Boolean chkInvoiceOverridePermission = [Select Id ,Invoice_Override_Permission__c From User Where Id = :UserInfo.getUserId()].Invoice_Override_Permission__c; 
        
        //For update:
        if(lstNewInvoice != null) {
        	
        	for(Invoice__c objInvoice : lstNewInvoice) {
	        	
	        	//Iterate over custom setting values
	            for(InvoiceLockingFields__c objInvoiceCS : InvoiceLockingFields__c.getAll().values()) {
	                                    
	                /* Last Modified By 'Trekbin' on '10-feb-2016' as per case '25221' */
	                // Do the comparision of fields as per custom setting and let user to update if 'Invoice_Override_Permission__c' checkbox is set to true else throw an error
	                if(objInvoice.Invoice_Status__c != 'Pending' && (chkInvoiceOverridePermission == false) && (objInvoice.get(objInvoiceCS.Name) != mapOldInvoices.get(objInvoice.Id).get(objInvoiceCS.Name)))
	                    objInvoice.addError('Not all fields are updatable if invoice status is not pending');  
	            }  
	        }
        
        } else {
        	
        	for(Invoice__c objInvoice : mapOldInvoices.values()) {
        		
        		 /* Last Modified By 'Trekbin' on '10-feb-2016' as per case '25221' */
        		 //Only let user to delete the record if 'Invoice_Override_Permission__c' checkbox is set to true else throw an error
        		 if(chkInvoiceOverridePermission == false)
        		 	objInvoice.addError('You cannot delete an invoicing record');
        	}
        }
    }
    
    /**
        @MethodName : updateAccountInvoiceTotal
        @Param      : lstInvoice
        @Description: This method is used to sum the value of invoices in Account field by changing status
    **/
    public static void updateAccountInvoiceTotal(List<Invoice__c> lstInvoices) {
        
        List<Account> lstAccountToUpdate = new List<Account>();
        List<Invoice_Line__c> lstInvoiceLineToUpdate = new List<Invoice_Line__c>();
        List<Expense__c> lstExpenseToUpdate = new List<Expense__c>();
        List<Time_Entry__c> lstTimeEntryToUpdate = new List<Time_Entry__c>();
        
        Map<Id, Invoice__c> mapOfIdToInvoiceLine = new Map<Id, Invoice__c>();
        Map<Id, decimal> mapOfIdToDecimal = new Map<Id, decimal>();
        
        set<Id> setOfIdToInvoice = new set<Id>(); 
        set<Id> setInvoiceRelatedToTimeEntry = new set<Id>();
        set<Id> setInvoiceRelatedToExpense = new set<Id>();

        Invoice_Line__c objInvoiceLineInstance;
       
        //iterate over list of invoices to collect AccountIds where status is cancelled
        for(Invoice__c objInvoice : lstInvoices) {
            
			if(objInvoice.Invoice_Status__c =='Sent' || objInvoice.Invoice_Status__c =='Canceled')
				setOfIdToInvoice.add(objInvoice.Account__c);

			if(objInvoice.Invoice_Status__c =='Canceled')
				mapOfIdToInvoiceLine.put(objInvoice.Id,objInvoice);
        }
        
        //iterate over invoicelines to update TimeEntry To Null If Staus is cancelled
        for(Invoice_Line__c objInvoiceLine : [  SELECT Id, Name,Invoice__c,Time_Entry__c, Gears_Expense__c
                                                FROM Invoice_Line__c
                                                WHERE Invoice__c IN :mapOfIdToInvoiceLine.keyset()   
                                             ]) {                                               
             
            /* Modified on 28/Jan/2016 by Trekbin as per updated requirement in case 00024605 */
            objInvoiceLineInstance = new Invoice_Line__c(Id = objInvoiceLine.Id);
            
			if(objInvoiceLine.Time_Entry__c != null) {
				
				objInvoiceLineInstance.Time_Entry__c = null;            	
				setInvoiceRelatedToTimeEntry.add(objInvoiceLine.Time_Entry__c);
			}
            
			if(objInvoiceLine.Gears_Expense__c != null) {
   	
				objInvoiceLineInstance.Gears_Expense__c = null;
				setInvoiceRelatedToExpense.add(objInvoiceLine.Gears_Expense__c);
			}
            
            if(objInvoiceLine.Time_Entry__c != null || objInvoiceLine.Gears_Expense__c != null)
            	lstInvoiceLineToUpdate.add(objInvoiceLineInstance); 
        }
        
        /* Modified on 10/12/2015 as per updated requirement in case 00024605 */
        
        //iterate over Time Entry related to invoices to update Invoice Lines related to TimeEntry to NULL
        for(Time_Entry__c objTimeEntry : [	SELECT Id,Invoice_Line__c 
        									FROM Time_Entry__c 
        									WHERE Id IN :setInvoiceRelatedToTimeEntry
        								 ]) {
        	
        	if(objTimeEntry.Invoice_Line__c != null)
        		lstTimeEntryToUpdate.add( new Time_Entry__c(Id = objTimeEntry.Id, Invoice_Line__c = null));
        }
        
        /* Modified on 28/Jan/2016 by Trekbin as per updated requirement in case 00024605 */
        for(Expense__c objExpense : [	SELECT Id, Invoice_Line__c 
        								FROM Expense__c 
        								WHERE Id IN :setInvoiceRelatedToExpense
        							]) {
        	
        	if(objExpense.Invoice_Line__c != null)
        		lstExpenseToUpdate.add( new Expense__c(Id = objExpense.Id, Invoice_Line__c = null));
        }
        
        //iterate over invoices to update the Total Invoiced field on Account with sum of its related Invoice Amount value
       for(Invoice__c objInvoice : [    SELECT Id,Name, Invoice_Amount__c,Account__c,Invoice_Status__c  
                                        FROM Invoice__c 
                                        WHERE Invoice_Status__c ='Sent' 
                                        AND Account__c IN:setOfIdToInvoice
                                   ]) {
            
            if(mapOfIdToDecimal.containskey(objInvoice.Account__c) )
                mapOfIdToDecimal.put(objInvoice.Account__c, mapOfIdToDecimal.get(objInvoice.Account__c)+objInvoice.Invoice_Amount__c);
           else
               mapOfIdToDecimal.put(objInvoice.Account__c, objInvoice.Invoice_Amount__c);
        }
        
        for(Invoice__c objInvoice : lstInvoices) {
            
            if(objInvoice.Account__c != null && !mapOfIdToDecimal.containskey(objInvoice.Account__c))
                mapOfIdToDecimal.put(objInvoice.Account__c,0.0);
        }
        
        //iterate over map to update the Account
        for(Id idAccount : mapOfIdToDecimal.KeySet()) {
            
            lstAccountToUpdate.add(new Account(Id = idAccount, Total_Invoiced__c = mapOfIdToDecimal.get(idAccount)));
        } 
        
        if(!lstTimeEntryToUpdate.isEmpty())
        	update lstTimeEntryToUpdate;
        
        if(!lstExpenseToUpdate.isEmpty())
        	update lstExpenseToUpdate; 
        	
        if(!lstAccountToUpdate.isEmpty())
            update lstAccountToUpdate;
            
        if(!lstInvoiceLineToUpdate.isEmpty()) 
            update lstInvoiceLineToUpdate;
    }
    
    public static void manualShareInvoiceRecords(Map<id,Invoice__c> newInvoiceRecordsMap,Map<id,Invoice__c> oldInvoiceRecordsMap) {
        
        //set to hold account Ids
        set<id> accountIds = new set<id>();
        
        //set to hold old account ids
        set<id> oldAccountIds = new set<id>();
        
        //set to hold old invoice ids
        set<id> oldInvoiceIds = new set<id>();
        
        //Iterate over invoice records
        for(Invoice__c invoiceRecord : newInvoiceRecordsMap.values())  {
        
            /* Last Modified By 'Trekbin' on '11-feb-2016' as per case '00024703' */
            //check if invoice status not equals pending/canceled and account <> blank
            if(invoiceRecord.Invoice_Status__c != 'Pending' && invoiceRecord.Invoice_Status__c != 'Canceled' && invoiceRecord.Account__c != null){
            
                //check if trigger envent is insert
                if(oldInvoiceRecordsMap == null)
                    accountIds.add(invoiceRecord.account__c);
                
                //if trigger event is update means when invoice account is updated
                else {
                
                    //check if account is update from previous value, account must be other than before
                    if(invoiceRecord.account__c != oldInvoiceRecordsMap.get(invoiceRecord.id).account__c || invoiceRecord.Invoice_Status__c != oldInvoiceRecordsMap.get(invoiceRecord.id).Invoice_Status__c){
                        
                        //add old invoice on invoice to old account ids set
                        oldInvoiceIds.add(invoiceRecord.id);
                        
                        //add old account on invoice to old account ids set
                        oldAccountIds.add(oldInvoiceRecordsMap.get(invoiceRecord.id).account__c);
                        
                        //add new updated account ids to accountids set
                        accountIds.add(invoiceRecord.account__c);
                    }
                }
            }
            
            /* Last Modified By 'Trekbin' on '12-feb-2016' as per case '00024703' */
            if(oldInvoiceRecordsMap != null && invoiceRecord.Invoice_Status__c != oldInvoiceRecordsMap.get(invoiceRecord.id).Invoice_Status__c && (invoiceRecord.Invoice_Status__c == 'Pending' || invoiceRecord.Invoice_Status__c == 'Canceled') && invoiceRecord.Account__c != null){
            	
            	//add old invoice on invoice to old account ids set
                oldInvoiceIds.add(invoiceRecord.id);
                
            	//Add old account on invoice to old account ids set
                oldAccountIds.add(oldInvoiceRecordsMap.get(invoiceRecord.id).Account__c);
            }
        }
        
        //if account share record for old account is already create for 'portal and role subordinates' group then delete that
        if(!oldAccountIds.isEmpty()){
            
            //fill set id of old account names
            Set<string> oldAccountNames = new set<string>();
            
            /* Last modified by Trekbin on 30 March, 2016 as per the case: 00024704*/
			Set<Id> setAccIdsHavingCustomerUsers = new Set<Id>();
			
			for(User objUser : [Select Id, Name, Contact.AccountId From User Where Contact.AccountId IN :oldAccountIds AND UserType = 'PowerCustomerSuccess']) {
				
				setAccIdsHavingCustomerUsers.add(objUser.Contact.AccountId);
			}

            for(Account accountObj : [select id,name from account where id in :setAccIdsHavingCustomerUsers]){
            	
                oldAccountNames.add(accountObj.name+' Customer Executive');
            }
            
            //fill set of userroles related corresponding account name
            set<id> userRoleIds = new set<id>();
            
            for(userRole uRole : [select id,name from userRole where Name Like : oldAccountNames]){
                userRoleIds.add(uRole.id);
            }
            
            //fill set of group ids related to the above user roles
            set<id> groupIds = new set<id>();
            
            for(group grpObj : [select id from group where relatedid in : userRoleIds]){
                groupIds.add(grpObj.id);
            }
            
            List<Invoice__Share> invoiceShareRecords = [select id from Invoice__Share where parentid in : oldInvoiceIds and userorgroupid in : groupIds and rowCause = 'Manual'];

            if(!invoiceShareRecords.isEmpty())
                delete invoiceShareRecords;
        }
        
        //create account share records for new accounts on invoice on insert and update
        if(!accountIds.isEmpty()){
            
            //initialise set and map used below
            Set<string> accountNames = new set<string>();
            Map<Id, Account> accountMap = new Map<Id,Account>();
            Map<string,id> userRoleMap = new Map<string,id>();
            Map<id,id> uRoleToGroupIdMap = new Map<id,id>();
            Map<id,id> accountToUroleMap = new Map<id,id>();
            List<AccountShare> accountShareRecords =  new List<AccountShare>();
            List<Invoice__share> invoiceShareRecords =  new List<Invoice__share>();
            
            /* Last modified by: Trekbin on 30 March, 2016 as per the case: 00024704*/
			Set<Id> setAccIdsHavingCustomerUsers = new Set<Id>();
			
			for(User objUser : [Select Id, Name, Contact.AccountId From User Where Contact.AccountId IN :accountIds AND UserType = 'PowerCustomerSuccess' AND IsActive = true]) {
				
				setAccIdsHavingCustomerUsers.add(objUser.Contact.AccountId);
			}
			
			for(Account accountObj : [select id,name from account where id in :setAccIdsHavingCustomerUsers]) {
                accountMap.put(accountObj.id,accountObj);
                accountNames.add(accountObj.name+' Customer Executive');
            }
            
            //query userRole corresponding to each account
            for(UserRole uRoleObj : [select id,name from userRole where Name Like : accountNames]){
                userRoleMap.put(uRoleObj.name,uRoleObj.id);
            }
            
            //Based on name fill map of accountid and userrole
            for(Account accountObj : accountMap.values()){
                for(String uRoleName : userRoleMap.Keyset()){
                    if(uRoleName.contains(accountObj.name))
                        accountToUroleMap.put(accountObj.id,userRoleMap.get(uRoleName));
                }
            }
            
            //userRole is related to group with relatedId so query all group of all userrole related to account and fill map
            for(Group grp : [select id,relatedId from group where relatedId in : userRoleMap.values() and type = 'RoleAndSubordinates']){
                uRoleToGroupIdMap.put(grp.relatedId,grp.id);
            }
            
            //Iterate on invoice records to share them to portal role and subordinates group with read only access
            for(Invoice__c invoiceRecord : newInvoiceRecordsMap.values())  {
                if(accountToUroleMap.containsKey(invoiceRecord.account__c)){
                    if(uRoleToGroupIdMap.containsKey(accountToUroleMap.get(invoiceRecord.account__c))){
                        
                        //create invoice share record for the invoice record
                        Invoice__share invoiceShareRecord = new Invoice__share();
                        invoiceShareRecord.ParentId = invoiceRecord.id;
                        invoiceShareRecord.UserOrGroupId = uRoleToGroupIdMap.get(accountToUroleMap.get(invoiceRecord.account__c));
                        invoiceShareRecord.AccessLevel = 'Read';
                        invoiceShareRecords.add(invoiceShareRecord);
                    }
                }
            }
            
            //insert invoice share records
            if(!invoiceShareRecords.isEmpty())
                insert invoiceShareRecords;
        }
    }
}