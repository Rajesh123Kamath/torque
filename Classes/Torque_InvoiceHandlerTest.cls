/**
  * Handler Name : Torque_InvoiceHandlerTest
  * Created By   : Trekbin          
  * Created On   : 19-Nov-2015
  * Modified On  : 20-Nov-2015
  * Modified by  : Trekbin
  * Description  : Test class for the trigger 'Torque_InvoiceTrigger'
**/
@isTest(SeeAllData = false)
private class Torque_InvoiceHandlerTest {

        
    static testMethod void myUnitTest1() {
        
        //Create a test account record
        List<Account> lstAccount = UTestData.getAccounts(2);
        lstAccount[0].Practice__c = 'MidWest';
        lstAccount[1].Practice__c = 'Southwest';
        insert lstAccount;
        
        //Test invoice records
        List<Invoice__c> lstInvoice = new List<Invoice__c>();
        lstInvoice.add(UTestData.getInvoice(lstAccount[0].Id, 'Pending'));
        lstInvoice.add(UTestData.getInvoice(lstAccount[1].Id, 'Pending'));
        insert lstInvoice;
	        
		//Update the invoice status to sent
        lstInvoice[0].Invoice_Status__c = 'Sent';
        update lstInvoice;
        
    }
    
   
    
    /* Created By 'Trekbin' on '10-feb-2016' as per case '25221' */
    static testMethod void myUnitTest2() {
        
        //Test profile record
        Id testProfile = UTestData.getProfileIdByName('System Administrator'); 
        
        //Test user record
        User testUser = UTestData.getUserRecord(testProfile);
        testUser.Invoice_Override_Permission__c = true;
        insert testUser;
        
                  
       	System.RunAs(testUser) {
       		
			//Test account records
	        List<Account> lstAccount = UTestData.getAccounts(2);
	        lstAccount[0].Practice__c = 'MidWest';
	        lstAccount[1].Practice__c = 'Southwest';
	        insert lstAccount;
	        
	      	//Test invoice records
	        List<Invoice__c> lstInvoice = new List<Invoice__c>();
	        lstInvoice.add(UTestData.getInvoice(lstAccount[0].Id, 'Pending'));
            lstInvoice.add(UTestData.getInvoice(lstAccount[1].Id, 'Pending'));
	        insert lstInvoice;
	        
	        try
	        {
		        system.assertEquals([Select Id, Invoice_Override_Permission__c from User Where Id = :testUser.Id].Invoice_Override_Permission__c, true);
		        
		        //Non custom setting fields, Updates as invoice status is pending               
		        lstInvoice[0].Total_Payments_Applied__c = 200; 
		        update lstInvoice[0];
		        
		        system.assertEquals([Select Id, Total_Payments_Applied__c, Credit_Amount__c from Invoice__c Where Id = :lstInvoice[0].Id].Total_Payments_Applied__c, 200);
		        
		        //Custom setting field, Updates as invoice status is pending               
		        lstInvoice[0].Credit_Amount__c = 300;
				update lstInvoice[0];
				
				system.assertEquals([Select Id, Total_Payments_Applied__c, Credit_Amount__c from Invoice__c Where Id = :lstInvoice[0].Id].Credit_Amount__c, 300);
							
		        lstInvoice[0].Invoice_Status__c = 'Sent';
		        update lstInvoice[0];	        
		        	        
		        //Non custom setting fields, Updates    
		        lstInvoice[0].Total_Payments_Applied__c = 300; 
		        update lstInvoice[0];
		        
		        system.assertEquals([Select Id, Total_Payments_Applied__c, Credit_Amount__c from Invoice__c Where Id = :lstInvoice[0].Id].Total_Payments_Applied__c, 300);
		        
		        //Custom setting field, Updates as Invoice_Override_Permission__c is true even though invoice status is sent            
		        lstInvoice[0].Credit_Amount__c = 200;
				update lstInvoice[0];
				
				system.assertEquals([Select Id, Total_Payments_Applied__c, Credit_Amount__c from Invoice__c Where Id = :lstInvoice[0].Id].Credit_Amount__c, 200);
				
				lstInvoice[0].Invoice_Status__c = 'Canceled';
		        update lstInvoice[0];
		        
		        //Non custom setting fields, Updates    
		        lstInvoice[0].Total_Payments_Applied__c = 200; 
		        update lstInvoice[0];
		        
		        system.assertEquals([Select Id, Total_Payments_Applied__c, Credit_Amount__c from Invoice__c Where Id = :lstInvoice[0].Id].Total_Payments_Applied__c, 200);
		        
		        //Custom setting field, Updates as Invoice_Override_Permission__c is true even though invoice status is Canceled            
		        lstInvoice[0].Credit_Amount__c = 300;
				update lstInvoice[0];        
		        
		        system.assertEquals([Select Id, Total_Payments_Applied__c, Credit_Amount__c from Invoice__c Where Id = :lstInvoice[0].Id].Credit_Amount__c, 300);
		        
		        //Deletes the invoice record without validations as Invoice_Override_Permission__c is true
		        delete lstInvoice[0];
	        }        
	        catch(Exception e) 
	        {
	            system.debug('Validation error thrown...Exception Caught');         
	        } 
       	}
    }
    
    /* Created By 'Trekbin' on '10-feb-2016' as per case '25221' */
    static testMethod void myUnitTest3() {
        
        //Test profile record
        Id testProfile = UTestData.getProfileIdByName('System Administrator'); 
        
        //Test user record
        User testUser = UTestData.getUserRecord(testProfile);
        testUser.Invoice_Override_Permission__c = false;
        insert testUser;
        
        //Run as the current user                       
        System.RunAs(testUser) {
        
	        //Create a test account record
	        List<Account> lstAccount = UTestData.getAccounts(1);
	        lstAccount[0].Practice__c = 'Southwest';
	        insert lstAccount;
	        
	        //Test invoice records
	        List<Invoice__c> lstInvoice = new List<Invoice__c>();
	        lstInvoice.add(UTestData.getInvoice(lstAccount[0].Id, 'Pending'));
	        insert lstInvoice;
	        
	        try
	        {
		        system.assertEquals([Select Id, Invoice_Override_Permission__c from User Where Id = :testUser.Id].Invoice_Override_Permission__c, false);
		        
		        //Non custom setting fields, Updates as invoice status is pending               
		        lstInvoice[0].Total_Payments_Applied__c = 200; 
		        update lstInvoice; 
		        
		        system.assertEquals([Select Id, Total_Payments_Applied__c, Credit_Amount__c from Invoice__c Where Id = :lstInvoice[0].Id].Total_Payments_Applied__c, 200);
		        
		        //Custom setting field, Updates as invoice status is pending               
		        lstInvoice[0].Credit_Amount__c = 300;
				update lstInvoice;
							
				system.assertEquals([Select Id, Total_Payments_Applied__c, Credit_Amount__c from Invoice__c Where Id = :lstInvoice[0].Id].Credit_Amount__c, 300);
							
		        lstInvoice[0].Invoice_Status__c = 'Sent';
		        update lstInvoice;
		        
		        //Non custom setting fields, Updates    
		        lstInvoice[0].Total_Payments_Applied__c = 300; 
		        update lstInvoice;
		        
		        system.assertEquals([Select Id, Total_Payments_Applied__c, Credit_Amount__c from Invoice__c Where Id = :lstInvoice[0].Id].Total_Payments_Applied__c, 300);
		        
		        //Custom setting field, validation error will be thrown as Invoice_Override_Permission__c is false and invoice status is sent            
		        lstInvoice[0].Credit_Amount__c = 250;
				update lstInvoice;		
	        }        
	        catch(Exception e)
	        {
	            system.debug('---Validation error thrown...Exception Caught---');         
	            System.Assert(e.getMessage().contains('Not all fields are updatable if invoice status is not pending'));
	        } 
        }
    }
    
    /* Created By 'Trekbin' on '10-feb-2016' as per case '25221' */
    static testMethod void myUnitTest4() {
        
        //Test profile record
        Id testProfile = UTestData.getProfileIdByName('System Administrator'); 

        //Test user record
        User testUser = UTestData.getUserRecord(testProfile);
        testUser.Invoice_Override_Permission__c = false;
        insert testUser;
          
        //Run as the current user         
        System.RunAs(testUser) {
	        
	        //Test account records
	        List<Account> lstAccount = UTestData.getAccounts(2);
	        lstAccount[0].Practice__c = 'MidWest';
	        lstAccount[1].Practice__c = 'Southwest';
	        insert lstAccount;
	       	        
	      	//Test invoice records
	        List<Invoice__c> lstInvoice = new List<Invoice__c>();
	        lstInvoice.add(UTestData.getInvoice(lstAccount[0].Id, 'Pending'));
            lstInvoice.add(UTestData.getInvoice(lstAccount[1].Id, 'Pending'));
	        insert lstInvoice;
	        
	        try
	        {	       						
		        //Update Invoice_Status__c to sent
		        lstInvoice[0].Invoice_Status__c = 'Sent';
		        update lstInvoice[0];
		        
		        //Validation error will be thrown as Invoice_Override_Permission__c is false and invoice status is sent 
		        delete lstInvoice[0];		         
	        }        
	        catch(Exception e)
	        {
	            system.debug('Validation error thrown...Exception Caught');         
	            System.Assert(e.getMessage().contains('You cannot delete an invoicing record'));
	        } 
        }
    }
    
     /* Created by 'Trekbin' on '30 march, 2016' as per case '00024703' */
    static testMethod void myUnitTest5() {
    
    	//Fetch the current loggedIn user
        User thisUser = [ Select Id From User Where Id = :UserInfo.getUserId() ];
        
        //Run as the current user
        System.runAs(thisUser) {
            
            //Fetch the profileID of the 'Customer Community Plus Login User'
            Id idCustCommProfile = UTestData.getProfileIdByName('Customer Community Plus Login User');
            
            //Create 2 test account records 
            List<Account> lstAccount = UTestData.getAccounts(2);
            insert lstAccount;
            
            //Make the first account as the partner Account
            lstAccount[0].IsPartner = true;
            update lstAccount;
            
            //Create 1 test contact record            
            List<Contact> lstContact = UTestData.getContacts(lstAccount[0], 1);                                   
            insert lstContact;
            
            //Create 1 active test user record with 'Customer Community Plus Login User' profile 
            User objCustCommUser1 = UTestData.getUserRecord(idCustCommProfile); 
            objCustCommUser1.IsActive = true;
            objCustCommUser1.ContactId = lstContact[0].Id;
            insert objCustCommUser1;
                       
            //Create a variable to hold the role for account 1 being executive                                  
            String accountName = lstAccount[0].Name+' Executive';
            
            //Create a test userRole record
            userRole uRole = UTestData.getUserRole(accountName);
            insert uRole;
            
            //Create an test contact record with non partner account                       
            List<Contact> lstContact1 = UTestData.getContacts(lstAccount[1], 1);                                                
            insert lstContact1;            
            
            //Create an test user record
            User objNonCustCommUser = UTestData.getUserRecord(idCustCommProfile);
            objNonCustCommUser.IsActive = true;
            objNonCustCommUser.ContactId = lstContact1[0].Id;
            insert objNonCustCommUser;
             
            //Create test invoice records 
            List<Invoice__c> lstInvoice = new List<Invoice__c>();
            lstInvoice.add(UTestData.getInvoice(lstAccount[0].Id, 'Pending'));
            lstInvoice.add(UTestData.getInvoice(lstAccount[0].Id, 'Canceled'));
            lstInvoice.add(UTestData.getInvoice(lstAccount[0].Id, 'Sent'));
            lstInvoice.add(UTestData.getInvoice(lstAccount[1].Id, 'Pending'));
            insert lstInvoice;
            
            //Assert to check if the invoice share has been created - sent record
            List<Invoice__share> sharingRecord3 = [select id from Invoice__share where parentid =: lstInvoice[2].id and rowcause = 'manual'];
            system.assertEquals(1, sharingRecord3.size());
           
           	//Update the invoice status to canceled
            lstInvoice[0].Invoice_Status__c = 'Canceled';
            update lstInvoice;
            
            //Assert to check if the invoice share has been created on IS update to canceled
            List<Invoice__share> sharingRecord5 = [select id from Invoice__share where parentid =: lstInvoice[0].id and rowcause = 'manual'];
            system.assertEquals(0, sharingRecord5.size());      
            
            //Update the invoice status to sent
            lstInvoice[0].Invoice_Status__c = 'Sent';
            update lstInvoice;
            
            //Assert to check if the invoice share has been created on IS update to sent
            List<Invoice__share> sharingRecord6 = [select id from Invoice__share where parentid =: lstInvoice[0].id and rowcause = 'manual'];
            system.assertEquals(1, sharingRecord6.size());
            
            //Update the account record
            lstInvoice[0].Account__c = lstAccount[1].id;
            update lstInvoice;
        }
    }
}