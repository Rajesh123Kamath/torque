global class UCase
{
     /*
        Added by Trekbin on 18 July, 2016
        Name: onBeforeInsert
    */
    
    public static void onAfterInsert (List<Case> lstNewCases){
        
        //checkValidOwner(lstNewCases);
    }
    
     /*
        Added by Trekbin on 18 July, 2016
        Name: onBeforeUpdate
    */
    
    public static void onAfterUpdate(Map<Id, Case> mapNewCase, Map<Id, Case> mapOldCase){
    
        checkOldNewValues(mapNewCase, mapOldCase);
    }
    
    global static void AssignOwner(List<Case> cases)
    {       
        string userKeyPrefix = User.SObjectType.getdescribe().KeyPrefix;
        
        for(Case c : cases)
        {
            if (userKeyPrefix != Null && c.ownerId != null && ((string)c.OwnerId).StartsWith(userKeyPrefix))
            {
                c.Owner__c = c.OwnerId;
            }
        }
    } 
        
    global static void AssignDeveloper(List<Case> cases, Map <id, Case> oldMap)
    {       
        boolean isInsert = false;
        
        if(oldMap == null)
        {
            isInsert = true;
        }
        
        List <Case> smallList = new List <Case>();
        
        string userKeyPrefix = User.SObjectType.getdescribe().KeyPrefix;
        set <id> ownerIds = new set <id>();
        for(Case c : cases)
        {
            if(isInsert == true || (isInsert == false && c.ownerId != oldMap.get(c.id).ownerId))
            {
                if (((string)c.OwnerId).StartsWith(userKeyPrefix))
                {
                    ownerIds.add(c.ownerId);
                    smallList.add(c);
                }
            }
        }
        
        set <id> developers = new set <id>();
        
        if(ownerIds.size() > 0)
        {
            for(User u : [select id, userRole.name from User where id in :ownerIds and userRole.name = 'Developers'])
            {
                developers.add(u.id);
            }
        }
        
        if(developers.size() > 0 && smallList.size() > 0)
        {
            for(Case c : smallList)
            {
                if(developers.contains(c.ownerId))
                {
                    c.Developer_Assigned__c = c.ownerId;
                }
            }
        }

    }
    
    
    global static void ApplyCaseFollowUnfollowRules(List<Case> caselist)
    {
       Set <Id> Cases = new Set <Id>();
       for (Case c : caselist)
       {
          Cases.add(c.id);
       } 
       
       ProcessCaseFollowings(Cases);
       
    }
    
    private static void ProcessCaseFollowings(set<id> cases)
    {
        FollowOpen(cases);
        UnfollowClosed(cases);
    }
    
    private static void FollowOpen(set<id> follow)
    {
        //***********************
       //Follow    
       //***********************  
        List <EntitySubscription> SubsToInsert = new List<EntitySubscription>();
        
        for (Case c : [Select id, Account.ownerId, Description, 
                              CaseNumber, AccountId 
                        From Case 
                       Where id in :follow 
                             and account.ownerid != null 
                             and isclosed = false]) 
        {
               SubsToInsert.add(uChatterUtilities.createFollowSubscription(c.id, c.Account.ownerId));
        }
        
        //this will ignore dupes if we're already subscribed.
        if (SubsToInsert.size() > 0)
        {
            uChatterUtilities.follow(SubsToInsert);
        }
    }
    
    private static void UnfollowClosed(set<id> cases)
    {
        //***********************
       //Unfollow    
       //***********************
       List <EntitySubscription> unfollowThese = new List<EntitySubscription>();
       
       for (Case c : [Select id, Account.ownerId, Description, 
                              CaseNumber, AccountId 
                        From Case 
                       Where id in :cases 
                             and account.ownerid != null 
                             and isClosed = true]) 
        {
               unfollowThese.addAll(uChatterUtilities.getSubscriptionForUnfollow(c.id));
        }
        
        if (unfollowThese.size() > 0)
        {
            uChatterUtilities.unfollow(unfollowThese);
        } 
    }
    
    public static void testEscalate (List <Case> records, Map <id, Case> oldMap, string event, boolean isAfter)
    {
        system.debug('-----Event ' + event);
        
        boolean isInsert = (oldMap == null);
        
        List<case> toUpdate = new List<case>();
        
        for(Case c : records)
        {
            system.debug('-----caseId = ' + c.id);
            system.debug('-----isEscalated = ' + c.isEscalated);
            
            if(isInsert == false) 
            {
                system.debug('-----old Map ' + oldMap);
                system.debug('-----old isEscalated ' + oldMap.get(c.id).isEscalated);
                
                if(c.IsEscalated==true && oldMap.get(c.id).isEscalated == false)
                {
                    c.Case_Escalated__c = true;
                    toUpdate.add(c);
                }   
            }           
        }
        
        if(toUpdate.size()>0 && isAfter)
        {
            update toUpdate;
        }
        
    }
    
    /*Added by Trekbin on 11 May, 2016 for the Case  00026116 */
    //method which will generate the open case count on project 
    //if any case related to the project is closed
    public static void updateOpenCasesCount(Map<Id, Case> oldMapCase, Map<Id, Case> newMapCase)
    {   

        Map<Id, Project__c> mapProjectsToUpdate = new Map<Id, Project__c>();
        Set<Id> setProjectIds = new Set<Id>();
        
        //getting project refernce which need to be updated by their open case count
        for(Case objCase : newMapCase.values())
        {  
            //checking the case inserted/updated by trigger is close or not if close then 
            //only project is considered for updation
            //if(((oldMapCase == null || oldMapCase.isEmpty()) && objCase.isClosed && objCase.Project__c != null) || 
               //(oldMapCase != null && !oldMapCase.isEmpty() && objCase.isClosed && (objCase.isClosed != oldMapCase.get(objCase.Id).isClosed) && objCase.Project__c != null))
            if(objCase.Project__c != null)
            {
                setProjectIds.add(objCase.Project__c);
                if(oldMapCase != null && 
                  !oldMapCase.isEmpty() && 
                  (oldMapCase.get(objCase.Id) != null) &&
                  (objCase.Project__c != null && oldMapCase.get(objCase.Id).Project__c != null) && 
                  (objCase.Project__c != oldMapCase.get(objCase.Id).Project__c) &&
                  !setProjectIds.contains(oldMapCase.get(objCase.Id).Project__c))
                setProjectIds.add(oldMapCase.get(objCase.Id).Project__c);
            } 
        }
        
        //retrieving the project associted with the case updated/inserted
        for(Project__c objProject : [SELECT Id, Open_Cases__c 
                                     FROM Project__c 
                                     WHERE Id IN :setProjectIds])
        {
            objProject.Open_Cases__c = 0;
            mapProjectsToUpdate.put(objProject.Id, objProject);
        }
        
        //getting the open case count associted to the project
        for (AggregateResult queryResult : [SELECT count(Id) CaseCount, Project__c ProjectId
                                            FROM Case 
                                            WHERE  Project__c
                                            IN :setProjectIds
                                            AND IsClosed = false
                                            GROUP BY Project__c])  
        {
            //converting the aggregate global object type value to its respective field types.
            Id projectId = (String)(queryResult.get('ProjectId'));
            Integer caseCount = (Integer)queryResult.get('CaseCount');
            mapProjectsToUpdate.get(projectId ).Open_Cases__c = caseCount;
        }
        
        //updating the project with open case count
        if(!mapProjectsToUpdate.isEmpty() && !mapProjectsToUpdate.values().isEmpty())
            update mapProjectsToUpdate.values();
    }
    
    /*Added by Trekbin on 10 June, 2016 for the Case 00026102*/
    //method which will populate inventory id on related Deployed items
    public static void populateInventoryOnChildDeployedItems(Map<Id, Case> mapOldCase, Map<Id, Case> mapNewCase)
    {
        Set<Id> setIdCase = new Set<Id>();
        List<Deployed_Items__c> lstDeployedItemToUpdate = new List<Deployed_Items__c>();
        
        // iterate over updated cases and collect case Ids
        for(Case objCase : mapNewCase.values())
        {
            // check if Inventory__c is changed and not blank
            if(objCase.Inventory__c != mapOldCase.get(objCase.Id).Inventory__c)
            {
                setIdCase.add(objCase.Id);
            }
        }
        
        // fetch related Deployed items and populate inventory Id.
        for(Deployed_Items__c objDeployedItem : [   SELECT Id, Inventory__c, Related_Case__r.Inventory__c
                                                    FROM  Deployed_Items__c
                                                    WHERE Related_Case__c IN :setIdCase
                                                ])
        {
            lstDeployedItemToUpdate.add(new Deployed_Items__c(Id = objDeployedItem.Id, Inventory__c = objDeployedItem.Related_Case__r.Inventory__c));
        }
        
        if(!lstDeployedItemToUpdate.isEmpty())
            update lstDeployedItemToUpdate;
    }
    
   

     /*
        Added by Trekbin on 18 July, 2016
        Name: checkOldNewValues
    */
    
    private static void checkOldNewValues(Map<Id, Case> mapNewCase, Map<Id, Case> mapOldCase){
        
        List<Case> lstCases = new List<Case>();
        
        for(Case objCase: mapNewCase.values()){
            
            if(objCase.ownerId != mapOldCase.get(objCase.Id).ownerId){
                
                lstCases.add(objCase);
            }
        }
        
        checkValidOwner(lstCases);
    }

    // Checking the validOwner of the Case 
    /*
        Added by Trekbin on 18 July, 2016
        Name: checkValidOwner
    */
    
    private static void checkValidOwner(List<Case> lstNewCases){
      
        
        Map<Id, Id> mapUserId_AccountIdParent =new Map<Id, Id>();
        Map<Id, Id> mapOwnerId_AccountId = new Map<Id, Id>();
        
        List<Group> lstGroup = new List<Group>([    SELECT Id, DeveloperName,Name 
                                                    FROM Group 
                                                    WHERE DeveloperName = 'Trekbin_Queue' 
                                                    LIMIT 1
                                                ]);
    
        List<Account> lstAccount = new List<Account>([  SELECT Id, Name
                                                        FROM Account 
                                                        WHERE Name = 'Trekbin'
                                                        LIMIT 1
                                                    ]);
                                                    
        for(Case objCase : lstNewCases){
            
            if(!lstAccount.isEmpty()) {
                mapOwnerId_AccountId.put(objCase.OwnerId, lstAccount[0].Id);
            }
            mapUserId_AccountIdParent.put(objCase.OwnerId, objCase.AccountId);
        }
        											
        if(!lstGroup.isEmpty()) {
            
            if(mapOwnerId_AccountId.containsKey(lstGroup[0].Id)){
            	
                getUserRecords(mapOwnerId_AccountId, mapUserId_AccountIdParent, lstAccount, true, lstNewCases);
            }
            
            else {
            
                getUserRecords(mapOwnerId_AccountId, mapUserId_AccountIdParent, lstAccount, false, new List<Case>());
            }
        }
        else{
            
            getUserRecords(mapOwnerId_AccountId, mapUserId_AccountIdParent, lstAccount, false, new List<Case>());
        }
        
        
    }
    
    /*
        Added by Trekbin on 18 July, 2016
        Name: getUserRecords
    */
    
    // Fetch the case owner
    private static void getUserRecords(Map<Id, Id> mapOwnerId_AccountId, Map<Id, Id> mapUserId_AccountIdParent, List<Account> lstAccount, Boolean isQueuePresent, List<Case> lstCases){
        
        Map<Id, Id> mapUserId_AccountId = new Map<Id, Id>();
        List<Case> lstAllCasesToBeSharedUnderParentAccount = new List<Case>();
                                         
        List<Contact> lstContact = new List<Contact>([  SELECT Id, Name
                                                        FROM Contact 
                                                        WHERE AccountId IN : lstAccount
                                                    ]);
        if(isQueuePresent){ 
            
            for(User objUser:   [   SELECT Id, Name, ContactId, AccountId
                                    FROM User
                                    WHERE ContactId IN : lstContact
                                    AND IsActive = true
                                ]) {
                
                mapUserId_AccountId.put(objUser.Id, objUser.AccountId);
            }
        }
        else {
            
                for(User objUser:   [   SELECT Id, Name, ContactId, AccountId
                                        FROM User
                                        WHERE Id IN : mapOwnerId_AccountId.KeySet()
                                        AND IsActive = true
                                        AND ContactId != null
                                    ]) {
                                    
                mapUserId_AccountId.put(objUser.Id, objUser.AccountId);
            }
        }
        
        /*
	        Added by Trekbin on 04 Oct, 2016 - Case 00026796
	    */
        for(Case objAllCasesUnderTheAccount : [	SELECT OwnerId
    											FROM Case
    											WHERE AccountId IN :mapUserId_AccountIdParent.values()
    											AND Id NOT IN :lstCases
    											AND OwnerId NOT IN :mapUserId_AccountIdParent.keySet()
											]) {
												
				if((isQueuePresent && !mapUserId_AccountId.containsKey(objAllCasesUnderTheAccount.OwnerId)) || !isQueuePresent)
					lstAllCasesToBeSharedUnderParentAccount.add(objAllCasesUnderTheAccount);
		}
        
        shareAccountRecord(mapUserId_AccountId, mapUserId_AccountIdParent, lstCases, lstAllCasesToBeSharedUnderParentAccount);
    }
    
    /*
        Added by Trekbin on 18 July, 2016  
        Name: getPartnerUser
    */
    private static void shareAccountRecord(Map<Id, Id> mapUserId_AccountId, Map<Id, Id> mapUserId_AccountIdParent, List<Case> lstCases, List<Case> lstAllCasesToBeSharedUnderParentAccount){
    
        List<AccountShare> lstAccountSharesToBeInserted = new List<AccountShare>();
        List<Login__Share> lstLoginSharesToBeInserted = new List<Login__Share>();
        List<Project__Share> lstProjectSharesToBeInserted = new List<Project__Share>();
        
        List<CaseShare> lstCaseShareToBeInserted = new List<CaseShare>();
        List<CaseShare> lstOtherCaseShareToBeInserted = new List<CaseShare>();
        
        for(Case objCase : lstAllCasesToBeSharedUnderParentAccount) {
                                    
            for(Id userId : mapUserId_AccountId.keySet()) {
                
                CaseShare objCaseShare = new CaseShare(
                                    CaseAccessLevel='Edit',
                                    CaseId = objCase.Id,
                                    UserOrGroupId = userId
                    );
                 
                lstOtherCaseShareToBeInserted.add(objCaseShare);
            }
        }                  
            
        for(Project__c objProject : [   SELECT Id, Name, Account__c     
                                        FROM Project__c
                                        WHERE Account__c IN : mapUserId_AccountIdParent.values()
                                    ]){
            
            for(Id userId : mapUserId_AccountId.keySet()) {
             Project__Share objProjectShare = new Project__Share(
                                AccessLevel='Read',
                                ParentId = objProject.Id,
                                UserOrGroupId = userId
                );
                
                lstProjectSharesToBeInserted.add(objProjectShare);
            }   
            
        }
        
        if(!lstCases.isEmpty()) {
        	
	        for(Case objCase : lstCases) {
	                                
	            for(Id userId : mapUserId_AccountId.keySet()) {
	            	
	                 CaseShare objCaseShare = new CaseShare(
	                                    CaseAccessLevel='Edit',
	                                    CaseId = objCase.Id,
	                                    UserOrGroupId = userId
	                    );
	                 
	                lstCaseShareToBeInserted.add(objCaseShare);
	            }
	                                    
	        }
        }
        
        for(Login__c objLogin: [    SELECT Id, Name, Account__c
                                    FROM Login__c
                                    WHERE Account__c IN : mapUserId_AccountIdParent.values()
                               ]) {
            
            for(Id userId : mapUserId_AccountId.keySet())  {
            
                Login__Share objShare = new Login__Share(
                                        AccessLevel='Read',
                                        ParentId = objLogin.Id,
                                        UserOrGroupId = userId
                );
                
                lstLoginSharesToBeInserted.add(objShare);  
            }   
       
        }
        
        for(Id userId : mapUserId_AccountId.keySet()) {
        
            AccountShare objAccountShare = new AccountShare(    userorgroupid = userId, 
                                                                accountid = mapUserId_AccountId.get(userId),
                                                                accountaccesslevel = 'Read', 
                                                                OpportunityAccessLevel = 'Read',
                                                                CaseAccessLevel = 'Edit'); 
                lstAccountSharesToBeInserted.add(objAccountShare);               
            }
            
       if(!lstProjectSharesToBeInserted.isEmpty())
               insert lstProjectSharesToBeInserted;    
       if(!lstLoginSharesToBeInserted.isEmpty() ) 
               insert lstLoginSharesToBeInserted;  
       if(!lstAccountSharesToBeInserted.isEmpty())
              insert lstAccountSharesToBeInserted;    
        if(!lstCaseShareToBeInserted.isEmpty())
               insert lstCaseShareToBeInserted;  
        if(!lstOtherCaseShareToBeInserted.isEmpty())
               insert lstOtherCaseShareToBeInserted;
    
    }   
}