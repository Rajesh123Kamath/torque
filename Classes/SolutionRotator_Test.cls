@isTest (seeallData=true)
private class SolutionRotator_Test 
{

	/*static testMethod void test1() 
    {
    	
    	Solution s = [select id, RotatorInclude__c from Solution where status = 'Approved' limit 1];	
    	s.RotatorInclude__c = true;
    	update s;
    	
    	solutionRotator.rotate();
    	
    	Solution s2 = [select id, RotatorInclude__c, RotatorNext__c from Solution where status = 'Approved' limit 1];	
    	s2.RotatorInclude__c = true;
    	s2.RotatorNext__c = true;
    	update s2;
    	
    	solutionRotator.rotate();
	
    }*/
	
	// Updated by Trekbin as per requirement :  00027345
    static testMethod void testRotate() {
    	
    	// Create Custom setting record
        SolutionRotatorSettings__c objSetting = SolutionRotatorSettings__c.getOrgDefaults();
        objSetting.DefaultPostTest__c = 'Wow! Check this solution out!';
        objSetting.NoRepeatDays__c = -30;
        upsert objSetting SolutionRotatorSettings__c.Id;
        
        Solution ojbSolution1 = new Solution(RotatorPostText__c = 'test',
        									 SolutionName = 'Test Solution1',
        									 status = 'Approved',
        									 RotatorLastRotation__c = Datetime.now().addDays(-40),
        									 RotatorInclude__c = true);
        insert ojbSolution1;
        
        solutionRotator.rotate();
        
         Solution ojbSolution2 = new Solution( RotatorNext__c = true,
         									   SolutionName = 'Test Solution2',
                 							   status = 'Approved',
        									   RotatorPostText__c = 'test',
        									   RotatorLastRotation__c = Datetime.now().addDays(-40),
        									   RotatorInclude__c = true);
        insert ojbSolution2;
       	
       	solutionRotator.rotate(); 
    }
}