global class PTOWatcherSched implements Schedulable {
    
    global void execute(SchedulableContext SC) 
    {
        ptoWatcher.processPTO();      
    }
}