global class BonusUtilizationBatch implements  Database.Batchable<sObject>, Database.AllowsCallouts {

   global final String query; 

	public BonusUtilizationBatch()
	{
		  
       String tmpQuery = ' select day__c '
                         + ' from utilDates__c '
						 + ' where day__c < TODAY '
						 + ' and day__c >= 2016-01-01 ';
                 
	   query = tmpQuery;	
		 
		
	} 
	
     global Database.QueryLocator start(Database.BatchableContext BC)
     {   
         
          return Database.getQueryLocator(query);
     } 
     
     global void execute(Database.BatchableContext BC, List<sObject> scope)
     {  
     	for(sObject s : scope)
     	{
     		BonusUtilization.calculateMonth(date.valueOf(s.get('day__c')));
     	}   
     	
     }
	
	global void finish(Database.BatchableContext BC)
	{
		 
	}
	
	
}