/**
    @Name           :   Torque_GenerateProjectCntlrTest
    @Date           :   1 Mar, 2016
    @Description    :   Test code for Torque_GenerateProjectCntlr.cls.
*/
@isTest(SeeAllData = false)
private class Torque_GenerateProjectCntlrTest {

    static testMethod void myUnitTest() {
     
        //Test profile record
        String strProfile = UTestData.getStandardProfileId('Standard User'); 
       
        //Test user record
     	User objUser = UTestData.getUserRecord(strProfile);
     	objUser.isActive = true;
     	objUser.Department = 'TestDept';
     	insert objUser;
     	
        //Test Custom setting record
        List<ProjectDateSettings__c> lstProjectDateSettingsToInsert = UTestData.getProjectDateSettings(1);
        lstProjectDateSettingsToInsert[0].NonImplementationEndDateIncrement__c =  30;
        lstProjectDateSettingsToInsert[0].BuildConfigTestingEndDayIncrement__c = 14;
        lstProjectDateSettingsToInsert[0].BuildConfigTestingStartDayIncrement__c = 15;
        lstProjectDateSettingsToInsert[0].DesignDiscoveryEndDaysIncrement__c = -1;
        lstProjectDateSettingsToInsert[0].GoLiveAndPostGoLiveEndDayIncrement__c = 7;
        insert lstProjectDateSettingsToInsert;
        
        
        /* List of test account records to insert */
        List<Account> lstTestAccountToInsert = UTestData.getAccounts(2);        
        lstTestAccountToInsert[0].Payment_Terms__c = 2;
        lstTestAccountToInsert[1].Payment_Terms__c = 2;        
        insert lstTestAccountToInsert;
     
        //Create a test project record
        List<Project__c> lstProjects = UTestData.getProjects(1);
        lstProjects[0].Account__c = lstTestAccountToInsert[0].Id;
        lstProjects[0].Hourly_Rate__c = 200.00;        
        lstProjects[0].Assigned_To__c = objuser.Id;
        lstProjects[0].Kickoff__c = date.today();
        lstProjects[0].Go_Live_Date__c = date.today();
        lstProjects[0].Status__c = 'Planning';
        lstProjects[0].Current_State__c = 'Maintenance';
        insert lstProjects;
        
        
        //Test Deliverable Record    
        List<Deliverable__c> lstDeliverableToInsert = UTestData.getDeliverables(1);
        lstDeliverableToInsert[0].Project__c = lstProjects[0].Id;
        insert lstDeliverableToInsert;
        
        
        //Fetch the recordTypeID of sales type for opportunity
        ID objSalesRecordTypeID = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Sales').getRecordTypeId();
        
        //Start a test
		Test.startTest();         
         
        //If the record type contains any value
        if(objSalesRecordTypeID != null)
        { 
	        //Create a test opportunity record         
	        List<Opportunity> lstOpportunityToInsert = UTestData.getOpportunities(lstTestAccountToInsert[1], 1);
	        lstOpportunityToInsert[0].RecordTypeId = objSalesRecordTypeID;
	        lstOpportunityToInsert[0].Amount = 1000.00;
	        lstOpportunityToInsert[0].Project__c = lstProjects[0].Id;
			insert lstOpportunityToInsert;  
	        
	        ApexPages.StandardController stdCntr = new ApexPages.StandardController(lstOpportunityToInsert[0]);
	        
	        //Instantiate the generateProject page controller
	        Torque_GenerateProjectCntlr objpage = new Torque_GenerateProjectCntlr(stdCntr);
	        
	        //Call addDeliverable section
			objpage.addDeliverableToSection();
			
			//Call addBI section by passing parentDeliverableIndex where it has to be added
			apexpages.currentpage().getparameters().put('parentDeliverableIndex', '0');		
			objpage.addBudgetItemToDeliverable();
			
			//Call addResBud section by passing parentDeliverableIndex, parentBudgetItemIndex where it has to be added
			apexpages.currentpage().getparameters().put('parentDeliverableIndex', '0');		
			apexpages.currentpage().getparameters().put('parentBudgetItemIndex', '1');			
			objpage.addResourceBudgetToBudgetItem();
			
			//Call removeResBud by passing parentDeliverableIndex, parentBudgetItemIndex & resourceBudgetIndex from where it has to be removed
			apexpages.currentpage().getparameters().put('parentDeliverableIndex', '0');
			apexpages.currentpage().getparameters().put('parentBudgetItemIndex', '1');	
			apexpages.currentpage().getparameters().put('resourceBudgetIndex', '1');
			objpage.removeResourceBudgetFromTheBudgetItem();
			
			//Call removeBI by passing parentDeliverableIndexToRemove, budgetItemIndex from where it has to be removed
			apexpages.currentpage().getparameters().put('parentDeliverableIndexToRemove', '0');
			apexpages.currentpage().getparameters().put('budgetItemIndex', '1');	
			objpage.removeBudgetItemFromTheDeliverable();
			
			//Call removeDeliverable by passing deliverableIndex from where it has to be removed
			apexpages.currentpage().getparameters().put('deliverableIndex', '0');
			objpage.removeDeliverableFromTheSection();
			
			//Call insert records method which will insert and redirect to project record detail page
			objpage.insertRecordsToDatabase();
					
			//Call cancel method where it has to redirect to oppty detail page
	 		objpage.cancelAndRedirectToOppty();
        }
        	
        Test.stopTest();
    }
}