public class SolutionRotator {

    // 1 get the solution
    // update the date
    // post it

    public static void rotate ()
    {
        Solution s = getNextSolution();
        
        if(s == null)
        {
            return;
        }
        
        
        if(s != null)
        {
            setLastDate(s);
        }

        SolutionRotatorSettings__c r = SolutionRotatorSettings__c.getOrgDefaults(); 
        
        system.debug('----s = ' + s);
        
        
        string postText = s.RotatorPostText__c;
        if(postText == null)
        {
            postText = r.DefaultPostTest__c;
        }
        
        createFeedPost (r.Default_Post_Group__c, postText, null, 'https://' + URL.getSalesforceBaseUrl().getHost() + '/' + s.id, s.solutionName);    
    }

    public static void setLastDate (Solution sol)
    {
        Solution s = new Solution (id = sol.Id);
        s.RotatorLastRotation__c = system.now();
        s.RotatorNext__c = false;
        
        update s;
    }

    public static solution getNextSolution ()
    {
        SolutionRotatorSettings__c r = SolutionRotatorSettings__c.getOrgDefaults(); 
        integer numDays = integer.valueOf(r.NoRepeatDays__c);
        
        dateTime dt = system.now().addDays(numDays);
        system.debug('-----dt = ' + dt);
        
        List <Solution> solutions = new List <Solution>();
        
        for(Solution s : [select id, RotatorNext__c, RotatorPostText__c, solutionName
                          from solution
                          where (RotatorLastRotation__c = null 
                                 or RotatorLastRotation__c < :dt)
                                and RotatorInclude__c = true
                          order by RotatorNext__c desc])
        {
            solutions.add(s);   
        }
        
        if(solutions.size() == 0)
        {
            system.debug('-----nothing found');
            return null;
        }
        
        if(solutions[0].RotatorNext__c == true)
        {
            return solutions[0];
        }
        
        integer randomNumber = randomWithLimit(integer.valueOf(solutions.size()));
        
        return solutions[randomNumber];
        
    }
    
    public static Integer randomWithLimit(Integer upperLimit)
    {
        Integer rand = Math.round(Math.random()*1000);
        return Math.mod(rand, upperLimit);
        
        
    }
    
    public static void createFeedPost (id userId, string pMessage, id pRecordId, string url, string urlName)    
    {
            
        String communityId = null;  
        ConnectApi.FeedType feedType = ConnectApi.FeedType.Record;

        // feed input
        ConnectApi.FeedItemInput input = new ConnectApi.FeedItemInput();
        
        // the message to be added
        ConnectApi.MessageBodyInput messageInput = new ConnectApi.MessageBodyInput();
        // text
        ConnectApi.TextSegmentInput textSegment;
        ConnectApi.TextSegmentInput spaceTextSegment;
        ConnectApi.TextSegmentInput hashTagSegment;
        // mention segment
        ConnectApi.MentionSegmentInput mentionSegment = new ConnectApi.MentionSegmentInput();


        // array of all the segment to complete the message
        messageInput.messageSegments = new List<ConnectApi.MessageSegmentInput>();

        //
        // Create the actual message
        //

        // space after hashtag
        spaceTextSegment = new ConnectApi.TextSegmentInput();
        spaceTextSegment.text = ' ';
        messageInput.messageSegments.add(spaceTextSegment);

        hashTagSegment = new ConnectApi.TextSegmentInput();
        hashTagSegment.text = ' #DailyTips ';
        
        

        // mention
        mentionSegment = new ConnectApi.mentionSegmentInput();
        mentionSegment.id = userId;
        messageInput.messageSegments.add(mentionSegment);
        messageInput.messageSegments.add(hashTagSegment);
        messageInput.messageSegments.add(spaceTextSegment);
        
        // message
        textSegment = new ConnectApi.TextSegmentInput();
        textSegment.text = ' ' + pMessage;
        messageInput.messageSegments.add(textSegment);
        
        ConnectApi.LinkAttachmentInput linkIn = new ConnectApi.LinkAttachmentInput();
        linkIn.urlName = urlName;
        linkIn.url = url;
        input.attachment = linkIn;
        
        
        input.body = messageInput;
       
        // Updated by Trekbin as per requirement :  00027345
        if(!Test.isRunningTest()) {
            
            //ConnectApi.FeedItem feedItemRep = ConnectApi.ChatterFeeds.postFeedItem(communityId, feedType, pRecordId, input, null);
            ConnectApi.FeedItem feedItemRep = ConnectApi.ChatterFeeds.postFeedItem(communityId, feedType, 'me', input, null);
        }
    }
    


}