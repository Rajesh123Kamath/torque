/**
	@ClassName    : Torque_PaymentHandlerTest 
	@CreatedOn    : 
	@CreatedBy    : Trekbin
	@Last modified: Trekbin on 30 March, 2016 as per the case: 00024704
	@Description  : Test class for the handler 'Torque_PaymentHandler'
**/
@isTest
private class Torque_PaymentHandlerTest { 
	
	//Test to sum up payment total on Account when Payment is Inserted/Updated/Deleted
	static testMethod void myUnitTest() {
	    
		//List to store account related data
		List<Account> lstAccount = UTestData.getAccounts(1);
		lstAccount[0].Practice__c = 'MidWest';
		insert lstAccount;
		
		//Fetch the Id of the paymentRecordType
		Id paymentRecordType = Schema.SObjectType.Payment__c.getRecordTypeInfosByName().get('Payment').getRecordTypeId();
		//Fetch the Id of the preBillRecordType
		Id preBillRecordType = Schema.SObjectType.Payment__c.getRecordTypeInfosByName().get('Pre-Bill').getRecordTypeId();
		    
			    
		//List to hold the Payment__c records
		List<Payment__c> lstPayment = new List<Payment__c>{ UTestData.getPaymentRecord(lstAccount[0].Id, preBillRecordType, 100),
															UTestData.getPaymentRecord(lstAccount[0].Id, paymentRecordType, 200),
															UTestData.getPaymentRecord(lstAccount[0].Id, paymentRecordType, 200)
		                                                  };
		insert lstPayment;  
		      
		system.assertEquals([Select Id, Total_Payments__c FROM Account WHERE Id = :lstAccount[0].Id].Total_Payments__c, 400);   
		
		lstPayment[0].Amount__c = 200;   
		update lstPayment;        
		system.assertEquals([Select Id, Total_Payments__c FROM Account WHERE Id = :lstAccount[0].Id].Total_Payments__c, 400);  
		
		lstPayment[1].Amount__c = 400;  
		update lstPayment;        
		system.assertEquals([Select Id, Total_Payments__c FROM Account WHERE Id = :lstAccount[0].Id].Total_Payments__c, 600); 
		
		lstPayment[2].Amount__c = 100; 
		update lstPayment;        
		system.assertEquals([Select Id, Total_Payments__c FROM Account WHERE Id = :lstAccount[0].Id].Total_Payments__c, 500); 
		
		delete lstPayment[2];
		system.assertEquals([Select Id, Total_Payments__c FROM Account WHERE Id = :lstAccount[0].Id].Total_Payments__c, 400); 
		
		delete lstPayment[1];
		system.assertEquals([Select Id, Total_Payments__c FROM Account WHERE Id = :lstAccount[0].Id].Total_Payments__c, 0);
		
		delete lstPayment[0];
		system.assertEquals([Select Id, Total_Payments__c FROM Account WHERE Id = :lstAccount[0].Id].Total_Payments__c, 0);
	}
	
	//Test the creation/deletion of Payment Share records.
	static testMethod void myUnitTest1() {
    
		/* Last Modified By Trekbin on 23-May-2016 as per case: 00025964*/
    	String strProfile = UTestData.getStandardProfileId('System Administrator'); 
    	
    	userRole userRole = new userRole(Name = 'TestAccount-01 Cust Executive' );
    	insert userRole ;
    	
		User thisUser = UTestData.getUserRecord(strProfile);
		thisUser.UserRoleId = userRole.Id;
		insert thisUser;
		
		System.runAs (thisUser) {
    
			Id profileId = UTestData.getProfileIdByName('Customer Community Plus Login User');
	
			List<Account> lstAccountPartner = UTestData.getAccounts(2);
			insert lstAccountPartner;
			
			List<Contact> lstContact = UTestData.getContacts(lstAccountPartner);                                              
			insert lstContact;
			
			List<User> lstUsers = UTestData.getCustomerPortalUsers(lstContact, profileId);
			insert lstUsers;
			
			userRole uRole = new userRole(Name = 'TestAccount-01 Executive' );insert uRole ;
			    
			//Fetch the Id of the paymentRecordType
			Id paymentRecordType = Schema.SObjectType.Payment__c.getRecordTypeInfosByName().get('Payment').getRecordTypeId();
			//Fetch the Id of the preBillRecordType
			Id preBillRecordType = Schema.SObjectType.Payment__c.getRecordTypeInfosByName().get('Pre-Bill').getRecordTypeId();
			
			List<Payment__c> lstPayment = new List<Payment__c>{	UTestData.getPaymentRecord(lstAccountPartner[0].Id, paymentRecordType, 100), 
																UTestData.getPaymentRecord(lstAccountPartner[1].Id, preBillRecordType, 100)
			                                              	  };
			                                                            
			insert lstPayment;
			
			List<payment__share> sharingRecord = [select id from payment__share where parentid =: lstPayment[0].id and rowcause = 'manual'];
			system.assertEquals(sharingRecord.size(), 1);
			
			lstPayment[0].RecordTypeId = preBillRecordType;
			update lstPayment[0];
			
			List<payment__share> sharingRecord1 = [select id from payment__share where parentid =: lstPayment[0].id and rowcause = 'manual'];
			system.assertEquals(sharingRecord1.size(), 0);
		}
	}
}