public class PTOWatcher {

    public class ptoData
    {
        public string userName;
        public date startDate;
        public date endDate;
        public string lastname;
    }
    
    public static void processPTO()
    {
        id lastuser;
        date startDate;
        date lastDate;
        string lastUserName;
        string lastname;
        
        
        // get PTO budget ids
        set <id> ptoIds = new set <id>();
        for(Chatter_PTOBudgetItems__c p : Chatter_PTOBudgetItems__c.getall().values())
        {
            //system.debug('-----' + p.id__c);  
            ptoIds.add((ID)p.id__c);
        }
        system.debug('PTO IDS inside Process PTO $$$$'+ptoIds);
        
        List <ptoData> ptoList = new List <ptoData>();

        for(Time_Entry__c c : [SELECT performed_by__c, performed_by__r.name, date__c,
                                            performed_by__r.work_week_hours__c,
                                            time__c,
                                            performed_by__r.lastname
                                       FROM Time_Entry__c 
                                       WHERE date__c >= TODAY
                                       and Budget_Item__c in :ptoIds
                                       ORDER BY performed_by__r.lastname, date__c])
        {
            
            
            double normalDay;
            
            if(c.performed_by__r.work_week_hours__c == null)
            {
                normalDay = 8;
            }
            else
            {
                normalDay = c.performed_by__r.work_week_hours__c/5;
            }
            
            DateTime dt = datetime.newInstance(c.date__c.year(), c.date__c.month(),c.date__c.day()); 
        
            system.debug('-----day = ' + dt.format('E'));
            system.debug('----- ' + c.time__c);
            system.debug('----- ' + normalDay);
        
            if(dt.format('E') == 'Sat' || dt.format('E') == 'Sun' || c.time__c != normalDay)
            {
                continue;
            }   
            
            if(c.performed_by__c != lastUser)
            {
                
                if(lastUser != null)
                {
                    ptoList = logPTO(lastUserName, startDate, lastDate, ptoList, lastname);
                }
        
                lastUser = c.performed_by__c;
                lastUserName = c.performed_by__r.name;
                startDate = c.date__c;
                lastDate = c.date__c;
                lastname = c.performed_by__r.lastname;
                
            }
            else
            {
                if(dt.format('E') == 'Mon' &&  lastDate.addDays(3) == c.date__c)
                {
                    // continue
                    lastDate = c.date__c;
                    system.debug('-----here ' + c.date__c);
                }
                else if(lastDate.addDays(1) == c.date__c)
                {
                    // continue
                    system.debug('-----continue');
                    lastDate = c.date__c;
                }
                else
                {
                    // there is a gap
                    ptoList = logPTO(lastUserName, startDate, lastDate, ptoList, lastname);
                    system.debug('-----what');
                    startDate = c.date__c;
                    lastDate = c.date__c;
                }
            }
        }
        
        if(startDate != null)
        {
            ptoList = logPTO(lastUserName, startDate, lastDate, ptoList, lastname);
            
            system.debug('ptoList inside Process PTO $$$$'+ptoList);
        }
        
        if(ptoList.size() > 0)
        {
            postPTO(ptoList);
        }
    }
    
    public static List <ptoData> logPTO(string userName, date startDate, date endDate, List <ptoData> ptoList, string lastname)
    {
        GlobalSettings__c gs =  GlobalSettings__c.getOrgDefaults();
        
        date d = system.today().addDays(integer.valueOf(gs.PTODaysInTheFuture__c));
        
        if(startDate > d)
        {
            return ptoList;
        }
        
        ptoData p = new ptoData();
        p.userName = userName;
        p.startDate = startDate;
        p.endDate = endDate;
        p.lastName = lastname;
        
        ptoList.add(p);
        
        return ptoList;
    }
    public static boolean hasCurrent;
    public static boolean hasFuture;
    public static void postPTO(List <ptoData> ptoList)
    {
        List <string> currentText = new List <string>();
        List <string> futureText = new List <string>();
        
        for(PTOText__c pt : PTOText__c.getAll().values())
        {
            if(pt.isCurrent__c == true)
            {
                currentText.add(pt.introductionText__c + '\r\n');
            }
            else
            {
                futureText.add(pt.introductionText__c + '\r\n');
            }
        }
        
        

        integer randomNumber = randomWithLimit(integer.valueOf(currentText.size()));
        integer randomNumberFuture = randomWithLimit(integer.valueOf(futureText.size()));
        
        string currentPost = currentText[randomNumber];
        string futurePost = futureText[randomNumberFuture];

        
        
        
        futurePost = '';
        
        Map<string,string> chatterBodyMap = new map<string,string>();
        
        chatterBodyMap = prepareSortedData(ptoList,currentPost,futurePost);  
        
        System.debug('______chatterBodyMap______' + chatterBodyMap);
        
       
       
        
        if(hasCurrent == true)
        {
            createChattterTextPost(getChatterGroup('PTO'),chatterBodyMap.get('currentpost'));
        }
        if(hasFuture == true)
        {
            createChattterTextPost(getChatterGroup('PTO'),chatterBodyMap.get('futurepost'));
        } 
    }
    
    public static map<string,string>  prepareSortedData(List<ptoData> ptoRawList,string currentPost,string futurePost)
    {
        map<date,map<date,list<string>>> futureRawMap = new map<date,map<date,list<string>>>();
        //map<date,map<date,list<string>>> futureSortedMap = new map<date,map<date,list<string>>>();
       // map<date,map<date,list<string>>> currentRawMap = new map<date,map<date,list<string>>>();
        set<date> startDatesSet = new set<date>();
       // List<date> startDateList = new list<date>();
        set<date> endDatesSet = new set<date>();
        List<date> endDateList = new list<date>();
        map<date,List<date>> datesMap = new map<date,List<date>>();
        
        map<date,set<date>> datesMapDeDup = new map<date,set<date>>();
        
        map<string,string> stringsMap = new map<string,string>();
        
        system.debug('ptoRawList inside prepareSortedData method $$$$'+ptoRawList+'Current Post $$$$'+currentPost+'Future Post $$$$'+futurePost);
        
        for(ptoData p : ptoRawList)    
        {
           // if(p.startDate != system.today())
           // {
                if(!futureRawMap.containsKey(p.startDate))
                {
                    futureRawMap.put(p.startDate,new map<date,List<string>>());
                    futureRawMap.get(p.startDate).put(p.endDate,new List<string>());
                    futureRawMap.get(p.startDate).get(p.endDate).add(P.userName);
                }
                else if(futureRawMap.containsKey(p.startDate) && !futureRawMap.get(p.startDate).ContainsKey(p.endDate))
                {
                    futureRawMap.get(p.startDate).put(p.endDate,new List<string>());
                    futureRawMap.get(p.startDate).get(p.endDate).add(P.userName);
                }
                else if(futureRawMap.containsKey(p.startDate) && futureRawMap.get(p.startDate).ContainsKey(p.endDate))
                {
                    futureRawMap.get(p.startDate).get(p.endDate).add(P.userName); 
                }
          //  }
            
            
            if(!datesMapDeDup.containsKey(p.startDate))
            {
                datesMapDeDup.put(p.startDate,new set<date>());
                datesMapDeDup.get(P.startDate).add(p.endDate);
            }
            else
            {
                datesMapDeDup.get(P.startDate).add(p.endDate);
            }
            
            
            
            
        }
        
        // Add datesMapDeDup to datesMap to enable sorting
        
        if(datesMapDeDup !=null && datesMapDeDup.size()>0)
        {
            for(date dd : datesMapDeDup.keySet())
            {
                    datesMap.put(dd,new List<date>());
                    datesMap.get(dd).addAll(datesMapDeDup.get(dd));
            }
        }
        
        system.debug('DatesMap inside prepareSortedData $$$$'+datesMap);
       
        // Loop to Sort End Dates
        for(date sd : datesMap.keySet())
        {
            if(datesMap.get(sd) != null)
                 datesMap.get(sd).sort();
        }
        
        system.debug('______datesMap____' + datesMap);
        // Loop to sort User Names
        for(date sd : futureRawMap.keySet())
        {
            for(date ed : futureRawMap.get(sd).KeySet())
            {
              futureRawMap.get(sd).get(ed).sort();
            }
        }
        
        // Run through the Sorted Dates to Prepare the Sorted Map
        for(Date sd : datesMap.keySet())
        {    
            DateTime startdt = datetime.newInstance(sd.year(), sd.month(), sd.day()); 
            
            for(Date ed : datesMap.get(sd))
            {           
                DateTime enddt = datetime.newInstance(ed.year(), ed.month(), ed.day()); 
                
                system.debug('futureRawMap User Name  inside prepareSortedData if sd =ed $$$$'+futureRawMap.get(sd));
                for(string userName : futureRawMap.get(sd).get(ed))
                {
                     system.debug('futureRawMap User Name  inside prepareSortedData if sd =ed $$$$'+futureRawMap.get(sd).get(ed)+'SD'+sd+'ED'+ed);
                    if(sd == system.today())
                    {
                        currentPost += '\r\n' + userName + ': thru ' + enddt.format('E') + ' ' + enddt.month() + '/' + enddt.day();
                        hasCurrent = true;
                        system.debug('Current Post  inside prepareSortedData $$$$'+currentPost);
                    }
                    else
                    {
                        futurePost += '\r\n' + userName + ': ' + startdt.format('E') + ' ' + startdt.month() + '/' + startdt.day() ;
                        system.debug('______futurePost____' + futurePost);
                        system.debug('______sd____' + sd);
                        system.debug('______ed____' + ed);
                        hasFuture = true;
                        system.debug('futurePost  inside prepareSortedData if sd =ed $$$$'+futurePost);
                        if(sd !=ed)
                        {
                            system.debug('______futurepostAAAA____' + futurePost);
                            futurePost += ' - ' + enddt.format('E') + ' ' + enddt.month() + '/' + enddt.day();
                            hasFuture = true;
                            system.debug('futurePost  inside prepareSortedData if sd !=ed $$$$'+futurePost);
                        }
                    }
                }
            }
        }
        
        if(futurePost != null)
            stringsMap.put('futurepost',futurePost);
        if(currentPost !=null)
            stringsMap.put('Currentpost',currentPost);
        
        system.debug('stringsMap inside prepareSortedData $$$$'+stringsMap);
        
        return  stringsMap;
    }
    
    public static id getChatterGroup(string name)
    {
        id groupId = [Select id From CollaborationGroup c
                      where name = :name].id;   
        return groupId;
    }
    
    public static void createChattterTextPost(id recordId, string Body) 
    {
        FeedItem post = new FeedItem();
        post.ParentId = recordId;
        post.Body = body;
        post.Type = 'TextPost';
        
        System.debug('______post_____' + post);
        insert post;
    }
    
    public static Integer randomWithLimit(Integer upperLimit)
    {
        Integer rand = Math.round(Math.random()*1000);
        return Math.mod(rand, upperLimit);
    }
    
    
    
    
}