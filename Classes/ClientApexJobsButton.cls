global class ClientApexJobsButton {
    
    public Id scheduledJobId {get; set;}
    public String strAbortJobResponse {get; set;}
    
    public ClientApexJobsButton (ApexPages.StandardController controller){
           
           scheduledJobId = (Id) controller.getId();  
           //scheduleJobSample(objJob.Id);
            system.debug('--scheduledJobId--'+scheduledJobId);    
    }
    webService static string testit(id loginId)
    {
        List <clientApexJobs.apexJobData> d = clientApexJobs.process(loginId);
            
        string x;   
            
        if(d != null && d.size() > 0)
        {
            x = 'Email Sent';
        }
        else
        {
            x = 'No data found';
        }
        
        return x;
    }   
    
    webService static string getBatchData(id loginId)
    {
        clientApexJobs.buildScheduledJobs(loginId);
            
        return 'SUCCESS';
    }   
    
    public void abortJob()
    {	system.debug('--scheduledJobId---'+scheduledJobId);
        strAbortJobResponse = clientApexJobs.abortJob(scheduledJobId);
    }   
    
    public static string scheduleJobSample(id scheduledJobId) {
    
        return clientApexJobs.scheduleJob(scheduledJobId);
    }
    
    webService static string scheduleJob(id scheduledJobId)
    {
        return clientApexJobs.scheduleJob(scheduledJobId);
    }

}