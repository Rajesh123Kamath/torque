/**
    @Name           :   Torque_VFPanelToDsplayUpcmngInvoicesCntr
    @Date           :   24 May, 2016
    @Description    :   An apex page controller for Torque_VFPanelToDisplayUpcomingInvoices.page that displays upcoming Invoice data based on Active Projects on Account layout.
    @TestClass      :   Torque_VFPanelToDsplayUpcmngInvoice_Test.cls
*/
public with sharing class Torque_VFPanelToDsplayUpcmngInvoicesCntr {
    
    /*get set variables*/
	public List<Project__c> activeProjectsOnAccountList {get; set;} 
	
	public Map<String, String> fieldNameToTotalSumMap {get; set;} 
	
	public Integer totalNoOfRecords {get; set;}
	public Integer pageNumber {get; set;}
	public Integer recordsPerPage {get; set;}
	
	public Boolean noRecordsFound {get; set;}
	
	/*private variables*/
	ApexPages.StandardSetController stdSetController;
	Id accId;
	
	/**
        @Name           :   Torque_VFPanelToDisplayInvoicesOnAccCntr
        @Description    :   Main class constructor.
    */
	public Torque_VFPanelToDsplayUpcmngInvoicesCntr(ApexPages.StandardController stdController) {
		
		accId = ((Account) stdController.getRecord()).Id;
		getActiveProjectsOnAccount();
	}
	
	/**
        @Name           :   getActiveProjectsOnAccount
        @Description    :   Get the active Projects on the Account.
    */
	private void getActiveProjectsOnAccount() {
		
		String strQuery = 'SELECT ';
		String strAggregateQuery = 'SELECT ';
		
		fieldNameToTotalSumMap = new Map<String, String>();
		
		List<AggregateResult> aggregateResultsList = new List<AggregateResult>();
		
		//Get the field names from the field set.
		for(Schema.FieldSetMember objFieldSet : Schema.getGlobalDescribe().get('Project__c').getDescribe().FieldSets.getMap().get('Accounts_VF_Panel_Upcoming_Invoices').getFields()) {
			
			strQuery += objFieldSet.getFieldPath() + ', ';
			
			if(objFieldSet.getType() == Schema.DisplayType.Currency || 
		       objFieldSet.getType() == Schema.DisplayType.Double ||
		       objFieldSet.getType() == Schema.DisplayType.Integer ||
		       objFieldSet.getType() == Schema.DisplayType.Percent) {
		        
		        fieldNameToTotalSumMap.put(objFieldSet.getFieldPath(), '0.00');
		        strAggregateQuery += 'sum('+objFieldSet.getFieldPath()+') '+objFieldSet.getFieldPath()+', ';
		    
		    } else
		        fieldNameToTotalSumMap.put(objFieldSet.getFieldPath(), '');
		}
		
		strQuery += 'CreatedDate FROM Project__c WHERE Account__c = :accId AND Is_Active__c = TRUE '; 
		strQuery += 'ORDER BY Projected_Next_Invoice__c DESC LIMIT 10000';
		
		//Instantiate the StandardSetController from the query locator by passing the above query string.
		stdSetController = new ApexPages.StandardSetController(Database.getQueryLocator(strQuery));
		
		//Get the total no. of records returned.
		totalNoOfRecords = stdSetController.getResultSize();
		
		//Set the no. of records to be displayed per page.
		recordsPerPage = 10;
		
		//Display the Projects.
		if(totalNoOfRecords > 0) {
			
			stdSetController.setPageSize(recordsPerPage);  
			getCurrentPageProjects();
			
			strAggregateQuery = strAggregateQuery.removeEnd(', ');
			
			//Calculation to display the total for number/currency fields per column.
			aggregateResultsList = Database.query(strAggregateQuery+' FROM Project__c WHERE Account__c = :accId AND Is_Active__c = TRUE');
			
			for(String str : fieldNameToTotalSumMap.keySet()) {
			    
			    if(fieldNameToTotalSumMap.get(str) == '0.00' && (Double)aggregateResultsList[0].get(str) != null)
			        fieldNameToTotalSumMap.put(str, String.valueOf((Double)aggregateResultsList[0].get(str)));
			}
			
		} else {
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO,'No Active Projects Found.'));
			noRecordsFound = true;
			return;	
		}
	}
	
	/**
        @Name           :   getCurrentPageProjects
        @Description    :   Get the Active Projects to be displayed for the current page.
    */
	private void getCurrentPageProjects() {
		
		activeProjectsOnAccountList = new List<Project__c>();
		
		//Get the Projects for the current page.
		activeProjectsOnAccountList = (List<Project__c>)stdSetController.getRecords();
		
		//Get the current page no. to display on UI.
		pageNumber = stdSetController.getPageNumber();
	}
	
	/**
        @Name           :   hasNext
        @Description    :   indicates whether there are more records after the current page set.
    */
	public Boolean hasNext {
        get {
            return stdSetController.getHasNext();
        }
        set;
    }
    
    /**
        @Name           :   hasPrevious
        @Description    :   indicates whether there are more records before the current page set.
    */
    public Boolean hasPrevious {
        get {
            return stdSetController.getHasPrevious();
        }
        set;
    }
    
    /**
        @Name           :   first
        @Description    :   returns the first page of records.
    */
    public void first() {
        stdSetController.first();
        getCurrentPageProjects();
    }
	
	/**
        @Name           :   last
        @Description    :   returns the last page of records.
    */
	public void last() {
    	
        stdSetController.last();
        getCurrentPageProjects();
    }
    
    /**
        @Name           :   previous
        @Description    :   returns the previous page of records.
    */
    public void previous() {
        stdSetController.previous();
        getCurrentPageProjects();
    }
    
    /**
        @Name           :   next
        @Description    :   returns the next page of records.
    */
    public void next() {
        stdSetController.next();
        getCurrentPageProjects();
    }
}