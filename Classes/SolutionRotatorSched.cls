global class SolutionRotatorSched implements Schedulable 
{
    global void execute(SchedulableContext SC) 
    {
		SolutionRotator.rotate();	
    } 
}