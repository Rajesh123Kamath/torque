/**
    @Name           :   Torque_TimeCardTriggerHandler_Test
    @Date           :   28 June, 2016
    @Description    :   Test class to test Torque_TimeCardTriggerHandler.cls
*/
@isTest
private class Torque_TimeCardTriggerHandler_Test {

    static testMethod void myUnitTest() {
        
        //Test profile record
       	String strProfile = UTestData.getStandardProfileId('System Administrator'); 
       
        //Test user record
     	User objUser = UTestData.getUserRecord(strProfile);
     	objUser.isActive = true;
     	insert objUser;
     	
        /* List of test account records to insert */
        List<Account> lstTestAccountToInsert = UTestData.getAccounts(1);        
        lstTestAccountToInsert[0].Payment_Terms__c = 2;
        insert lstTestAccountToInsert; 
	    
	    //Test Custom setting record
        List<ProjectDateSettings__c> lstProjectDateSettingsToInsert = UTestData.getProjectDateSettings(1);
        lstProjectDateSettingsToInsert[0].NonImplementationEndDateIncrement__c =  30;
        lstProjectDateSettingsToInsert[0].BuildConfigTestingEndDayIncrement__c = 14;
        lstProjectDateSettingsToInsert[0].BuildConfigTestingStartDayIncrement__c = 15;
        lstProjectDateSettingsToInsert[0].DesignDiscoveryEndDaysIncrement__c = -1;
        lstProjectDateSettingsToInsert[0].GoLiveAndPostGoLiveEndDayIncrement__c = 7;
        insert lstProjectDateSettingsToInsert; 
        
	    //Create a test project record
        List<Project__c> lstProjects = UTestData.getProjects(1);
        lstProjects[0].Account__c = lstTestAccountToInsert[0].Id;
        lstProjects[0].Hourly_Rate__c = 200.00;        
        lstProjects[0].Assigned_To__c = objuser.Id;
        lstProjects[0].Kickoff__c = date.today();
        lstProjects[0].Go_Live_Date__c = date.today();
        lstProjects[0].Status__c = 'Planning';
        lstProjects[0].Current_State__c = 'Maintenance';
        insert lstProjects;
        
        //Test Deliverable Record    
        List<Deliverable__c> lstDeliverableToInsert = UTestData.getDeliverables(1);
        lstDeliverableToInsert[0].Project__c = lstProjects[0].Id;
        insert lstDeliverableToInsert;

        //Test BudgetItem Record 1    
	    List<Budget_Item__c> lstBudgetItem = UTestData.getBudgetItems(1, lstDeliverableToInsert[0], lstProjects[0]);
	    insert lstBudgetItem;
	    
	    //Create a test time card record 1
        List<Time_Card__c> lstTimeCardRecord  = UTestData.getTimeCards(2);
        insert lstTimeCardRecord;
        
        //Test record for Billing Reason
        List<Billing_Reason_Code__c> lstBillingReason = UTestData.getBillingReasonCodes(1);
        lstBillingReason[0].Billable__c = true;
        lstBillingReason[0].Non_Billable__c = false;
        lstBillingReason[0].Unbilled_del__c = false;
    	insert lstBillingReason;
    	
    	//Create a time entry record
        List<Time_Entry__c> lstTimeEntryRecord = UTestData.getTimeEntry(2, lstProjects[0], lstTimeCardRecord[0], lstBillingReason[0], lstBudgetItem[0]);		
		
		for(Time_Entry__c objTE : lstTimeEntryRecord) {
			
			objTE.Billable__c = true;		
			objTE.Time__c = 10;
			objTE.CurrencyIsoCode = 'USD';		
			objTE.Date__c = date.today();
			objTE.Notes__c = 'Test Notes';		
		}		
		insert lstTimeEntryRecord; 
		
		try {  
			delete lstTimeCardRecord;
		} catch(Exception e) {
			System.Assert(e.getMessage().contains('You cannot delete this Time Card'));    
		}
    }
}