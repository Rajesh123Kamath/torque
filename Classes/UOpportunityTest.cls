/**
	@ Test Class Name         : createProjectAndItsChildRecords 
	@ Handler Name            : UOpportunity 					 	
	@ Created by	   		  : Trekbin
	@ Created date     		  : 2-March-2016
	@ Last Modified By 		  : Trekbin on 9-march-2016
*/
@isTest(SeeAllData = false)
private class UOpportunityTest {

    static testMethod void myUnitTest() {

       	//Test profile record
       	String strProfile = UTestData.getStandardProfileId('Standard User'); 
       	 
		//Test user record
        User objUser = UTestData.getUserRecord(strProfile);
     	objUser.isActive = true;
     	objUser.Allow_Time_Logging__c = true;
     	insert objUser;
     
        //Test Custom setting record
        List<ProjectDateSettings__c> lstProjectDateSettingsToInsert = UTestData.getProjectDateSettings(1);
        lstProjectDateSettingsToInsert[0].NonImplementationEndDateIncrement__c =  30;
        lstProjectDateSettingsToInsert[0].BuildConfigTestingEndDayIncrement__c = 14;
        lstProjectDateSettingsToInsert[0].BuildConfigTestingStartDayIncrement__c = 15;
        lstProjectDateSettingsToInsert[0].DesignDiscoveryEndDaysIncrement__c = -1;
        lstProjectDateSettingsToInsert[0].GoLiveAndPostGoLiveEndDayIncrement__c = 7;
        insert lstProjectDateSettingsToInsert;
        

        /* List of test account records to insert */
        List<Account> lstTestAccountToInsert = UTestData.getAccounts(2);        
        insert lstTestAccountToInsert;
     

        //Create a test project record
        List<Project__c> lstProjects = UTestData.getProjects(1);
        lstProjects[0].Account__c = lstTestAccountToInsert[0].Id;
        lstProjects[0].Hourly_Rate__c = 200.00;        
        lstProjects[0].Assigned_To__c = objuser.Id;
        lstProjects[0].Kickoff__c = date.today();
        lstProjects[0].Go_Live_Date__c = date.today();
        lstProjects[0].Status__c = 'Planning';
        lstProjects[0].Current_State__c = 'Maintenance';
        insert lstProjects;
        
        
        //Test Deliverable Record    
        List<Deliverable__c> lstDeliverableToInsert = UTestData.getDeliverables(1);
        lstDeliverableToInsert[0].Project__c = lstProjects[0].Id;
        insert lstDeliverableToInsert;
        
        //Fetch the recordTypeID of sales type for opportunity
        ID objSalesRecordTypeID = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Sales').getRecordTypeId();
        
        //Start a test
		Test.startTest();         
         
        //If the record type contains any value
        if(objSalesRecordTypeID != null)
        {
			//Create a test opportunity record         
	        List<Opportunity> lstOpportunityToInsert = UTestData.getOpportunities(lstTestAccountToInsert[1], 1);
	        lstOpportunityToInsert[0].StageName ='Introduction';
	        lstOpportunityToInsert[0].RecordTypeId = objSalesRecordTypeID;
	        lstOpportunityToInsert[0].Project__c = lstProjects[0].Id;
			insert lstOpportunityToInsert;  
        }  
            
      	//Stop a test 
      	Test.stopTest();
    }


    public static testMethod void testOpportunityAddTopic(){


        //Test Custom setting record
        List<ProjectDateSettings__c> lstProjectDateSettingsToInsert = UTestData.getProjectDateSettings(1);
        lstProjectDateSettingsToInsert[0].NonImplementationEndDateIncrement__c =  30;
        lstProjectDateSettingsToInsert[0].BuildConfigTestingEndDayIncrement__c = 14;
        lstProjectDateSettingsToInsert[0].BuildConfigTestingStartDayIncrement__c = 15;
        lstProjectDateSettingsToInsert[0].DesignDiscoveryEndDaysIncrement__c = -1;
        lstProjectDateSettingsToInsert[0].GoLiveAndPostGoLiveEndDayIncrement__c = 7;
        insert lstProjectDateSettingsToInsert;
        

        List<Account> accts = UTestData.getAccounts(1);
        insert accts;
        List<Opportunity> opps = new List<Opportunity>();

        for(Account a : accts)
        {
            Opportunity opp = new Opportunity();
            opp.AccountId = a.Id;
            opp.Name = 'Test Opportunity #' + system.now();
            opp.CloseDate = Date.today();
            opp.StageName = 'Prospect';
            opp.Services_Included__c = 'Steelbrick';
            opps.add(opp);
        }

        insert opps;
        Set<Id> oppIds = new Set<Id>();
        for(Opportunity opp : opps){
            oppIds.add(opp.Id);
        }
        opps = [SELECT Id, Services_Included__c, BREeze_Topic__c FROM Opportunity WHERE Id IN:oppIds];
        List<Topic> tops = [SELECT Id, Name, Description, TalkingAbout FROM Topic];
        system.debug('teh opps---' + opps);
        system.debug('teh topics----' + tops);        
    }
}