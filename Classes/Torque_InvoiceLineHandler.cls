/**
  *	Handler Name : Torque_InvoiceLineHandler
  *	Created By   : Trekbin			
  *	Created On   : 19-Nov-2015
  * Modified On  : 28-Jan-2016
  * Modified by  : Trekbin 
  * Description  : Handler class for the trigger 'Torque_InvoiceLineTrigger'
**/
public with sharing class Torque_InvoiceLineHandler {	
	
	/*
		@Method Name: validateInvLineBeforeUpdateAndDelete
		@Description: Before update/delete: If status is not pending then display validation error message on the record.
	*/
	public static void validateInvLineBeforeUpdateAndDelete(List<Invoice_Line__c> lstNewInvoiceLines, Map<Id, Invoice_Line__c> mapOldInvoiceLines) {
		
		/* Added this boolean variable By 'Trekbin' on '10-feb-2016' as per case '25221' */
		//Boolean variable for updating/ deleting based on the permission
        Boolean chkInvoiceOverridePermission = [Select Id ,Invoice_Override_Permission__c From User Where Id = :UserInfo.getUserId()].Invoice_Override_Permission__c; 
        
		//For update.
		if(lstNewInvoiceLines != null) {
			
			for(Invoice_Line__c objInvoiceLine : lstNewInvoiceLines) {
				
				//If Invoice status is not pending then handle this
				if(objInvoiceLine.Invoice_Status__c != 'Pending') { 
					
					//If the status is Cancelled, then check for change in Time entry. If no change, then display error message.
					if(objInvoiceLine.Invoice_Status__c == 'Canceled') 
					{	
						/* Last Modified By 'Trekbin' on '12-feb-2016' as per case '25221' */
						if((objInvoiceLine.Time_Entry__c == mapOldInvoiceLines.get(objInvoiceLine.Id).Time_Entry__c) && (objInvoiceLine.Gears_Expense__c == mapOldInvoiceLines.get(objInvoiceLine.Id).Gears_Expense__c) && (chkInvoiceOverridePermission == false))
							objInvoiceLine.addError('You cannot update the record fields as the Invoice Line Status is not Pending.');
					} 
					else //IN CASE OF 'STATUS IS != PENDING/CANCELLED'
					{
						/* Last Modified By 'Trekbin' on '10-feb-2016' as per case '25221' */
        		 		//Only let user to edit the record if 'Invoice_Override_Permission__c' checkbox is set to true else throw an error 
						if(chkInvoiceOverridePermission == false)
							objInvoiceLine.addError('You cannot update the record fields as the Invoice Line Status is not Pending.');
					}
				}
			}
		
		} else {
			
			//For delete event simply throw an error if the status is not pending.
			for(Invoice_Line__c objInvoiceLine : mapOldInvoiceLines.values()) {
			
				/* Last Modified By 'Trekbin' on '10-feb-2016' as per case '25221' */
    		 	//Only let user to delete the record if 'Invoice_Override_Permission__c' checkbox is set to true else throw an error
				if(objInvoiceLine.Invoice_Status__c != 'Pending' && chkInvoiceOverridePermission == false)
					objInvoiceLine.addError('You cannot delete this Invoice Line record.');
			}
		}
	}
}