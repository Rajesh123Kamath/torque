public with sharing class Torque_HoursBurnedByResourceBatch implements Database.Batchable<sObject>,  Database.Stateful {
	
	Map<String, List<String>> objectNameToListOfDepartmentMap;
	Map<String, String>	departmentToFieldToBeUpdatedMap;
	
	public Database.QueryLocator start(Database.BatchableContext BC) {
		
		//Get all active projects and also Deliverables and Budget Items under each of them.
		String strQuery = 'SELECT Id, (SELECT Id FROM Deliverables__r), (SELECT Id FROM Budget_Entries__r) FROM Project__c WHERE Is_Active__c = true';
		
		objectNameToListOfDepartmentMap = new Map<String, List<String>>();
		departmentToFieldToBeUpdatedMap = new Map<String, String>();
		
		//Get the department on time entry for which the hours has to be calculated and the respective field name on project/deliverable/budget item from custom setting.
		for(TorqueResourceBurnRollUps__c objCustomSetting : TorqueResourceBurnRollUps__c.getall().values()) {
			
			system.debug('--objCustomSetting---'+objCustomSetting);
			
			if(!objectNameToListOfDepartmentMap.containsKey(objCustomSetting.Object__c))
				objectNameToListOfDepartmentMap.put(objCustomSetting.Object__c, new List<String>());
				
			objectNameToListOfDepartmentMap.get(objCustomSetting.Object__c).add(objCustomSetting.Department__c);
			
			departmentToFieldToBeUpdatedMap.put(objCustomSetting.Department__c+'_'+objCustomSetting.Object__c, objCustomSetting.Field__c);
		}
		
		return Database.getQueryLocator(strQuery);
	}
    
    public void execute(Database.BatchableContext BC, List<Project__c> activeProjectsList) {
    	
    	system.debug('--activeProjectsList---'+activeProjectsList.size());
    	system.debug('--objectNameToListOfDepartmentMap---'+objectNameToListOfDepartmentMap);
    	system.debug('--departmentToFieldToBeUpdatedMap---'+departmentToFieldToBeUpdatedMap);
    	
    	Set<Id> activeProjectIdsSet = new Set<Id>();
    	Set<Id> deliverableIdsOnactiveProjectsSet = new Set<Id>();
    	Set<Id> budgetItemIdsOnactiveProjectsSet = new Set<Id>();
    	
    	//Map<String, Double> departmentToTotalHoursMap = new Map<String, Double>();
    	
    	List<Project__c> projectsToBeUpdatedList = new List<Project__c>();
    	List<Deliverable__c> deliverablesOnActiveProjectsList = new List<Deliverable__c>();
    	List<Budget_Item__c> budgetItemsOnActiveProjectsList = new List<Budget_Item__c>();
    	
    	//Active project Ids.
    	for(Project__c objProject : activeProjectsList) {
    		
    		activeProjectIdsSet.add(objProject.Id);
    		
    		if(!objProject.Deliverables__r.isEmpty())
    			deliverablesOnActiveProjectsList.addAll(objProject.Deliverables__r);
    			
    		if(!objProject.Budget_Entries__r.isEmpty())
    			budgetItemsOnActiveProjectsList.addAll(objProject.Budget_Entries__r);
    	}
    	
    	//Active Deliverable Ids.
    	for(Deliverable__c objDeliverable : deliverablesOnActiveProjectsList) {
    			
    		deliverableIdsOnActiveProjectsSet.add(objDeliverable.Id);
		}
		
		//Active Budget Item Ids.
		for(Budget_Item__c objBI : budgetItemsOnActiveProjectsList) {
			
			budgetItemIdsOnActiveProjectsSet.add(objBI.Id);
		}
    	
    	if(objectNameToListOfDepartmentMap.containsKey('Project')) {
	    	
	    	//Calculate the total hours taken by each department on each project and then update the sum on respective field on Project.
	    	for(AggregateResult aggrRes : [	SELECT sum(Time__c) totalHours, Project__c, Performed_By__r.Department
	    									FROM Time_Entry__c
	    									WHERE Project__c IN :activeProjectIdsSet 
	    									AND Department__c IN :objectNameToListOfDepartmentMap.get('Project')
	    									AND Billable__c = TRUE
	    									GROUP BY Project__c, Performed_By__r.Department
	    								])	{
	    										
	    		system.debug('---aggrRes--'+aggrRes);	
	    		Double totalHours = (Double)aggrRes.get('totalHours');
	            String strDepartment = (STRING)aggrRes.get('Department');
	            Id projId = (Id)aggrRes.get('Project__c');
	            
	    		//departmentToTotalHoursMap.put(strDepartment, totalHours);	
	    		
	    		if(departmentToFieldToBeUpdatedMap.containsKey(strDepartment+'_Project')) {
	    			Project__c objProjToBeUpdated = new Project__c(Id = projId);
		    		objProjToBeUpdated.put(departmentToFieldToBeUpdatedMap.get(strDepartment+'_Project'), totalHours);
		    		projectsToBeUpdatedList.add(objProjToBeUpdated);	
		    		system.debug('----objProjToBeUpdated---'+objProjToBeUpdated);
	    		}
	    	}
    	}
    	
    	deliverablesOnActiveProjectsList.clear();
    	budgetItemsOnActiveProjectsList.clear();
    	
    	if(objectNameToListOfDepartmentMap.containsKey('Deliverable')) {
	    	
	    	//Calculate the total hours taken by each department on each deliverable and then update the sum on respective field on deliverable.
	    	for(AggregateResult aggrRes : [	SELECT sum(Time__c) totalHours, Deliverable__c, Performed_By__r.Department
	    									FROM Time_Entry__c
	    									WHERE Deliverable__c IN :deliverableIdsOnActiveProjectsSet 
	    									AND Department__c IN :objectNameToListOfDepartmentMap.get('Deliverable')
	    									AND Billable__c = TRUE
	    									GROUP BY Deliverable__c, Performed_By__r.Department
	    								])	{
	    										
	    		system.debug('---aggrRes--'+aggrRes);	
	    		Double totalHours = (Double)aggrRes.get('totalHours');
	            String strDepartment = (STRING)aggrRes.get('Department');
	            Id deliverableId = (Id)aggrRes.get('Deliverable__c');
	            
	    		if(departmentToFieldToBeUpdatedMap.containsKey(strDepartment+'_Deliverable')) {
	    			Deliverable__c objDeliverableToBeUpdated = new Deliverable__c(Id = deliverableId);
		    		objDeliverableToBeUpdated.put(departmentToFieldToBeUpdatedMap.get(strDepartment+'_Deliverable'), totalHours);
		    		deliverablesOnActiveProjectsList.add(objDeliverableToBeUpdated);	
		    		system.debug('----objDeliverableToBeUpdated---'+objDeliverableToBeUpdated);
	    		}
	    	}
    	}
    	
    	if(objectNameToListOfDepartmentMap.containsKey('Budget Item')) {
	    	
	    	//Calculate the total hours taken by each department on each budget item and then update the sum on respective field on budget item.
	    	for(AggregateResult aggrRes : [	SELECT sum(Time__c) totalHours, Budget_Item__c, Performed_By__r.Department
	    									FROM Time_Entry__c
	    									WHERE Budget_Item__c IN :budgetItemIdsOnActiveProjectsSet 
	    									AND Department__c IN :objectNameToListOfDepartmentMap.get('Budget Item')
	    									AND Billable__c = TRUE
	    									GROUP BY Budget_Item__c, Performed_By__r.Department
	    								])	{
	    										
	    		system.debug('---aggrRes--'+aggrRes);	
	    		Double totalHours = (Double)aggrRes.get('totalHours');
	            String strDepartment = (STRING)aggrRes.get('Budget Item');
	            Id budgetItemId = (Id)aggrRes.get('Budget_Item__c');
	            
	    		if(departmentToFieldToBeUpdatedMap.containsKey(strDepartment+'_Budget Item')) {
	    			Budget_Item__c objBudgetItemToBeUpdated = new Budget_Item__c(Id = budgetItemId);
		    		objBudgetItemToBeUpdated.put(departmentToFieldToBeUpdatedMap.get(strDepartment+'_Budget Item'), totalHours);
		    		budgetItemsOnActiveProjectsList.add(objBudgetItemToBeUpdated);	
		    		system.debug('----objBudgetItemToBeUpdated---'+objBudgetItemToBeUpdated);
	    		}
	    	}
    	}
    	
    	system.debug('----projectsToBeUpdatedList---'+projectsToBeUpdatedList);
    	system.debug('----budgetItemsOnActiveProjectsList---'+budgetItemsOnActiveProjectsList);
    	system.debug('----deliverablesOnActiveProjectsList---'+deliverablesOnActiveProjectsList);
    	
    	if(!projectsToBeUpdatedList.isEmpty())
    		Database.update(projectsToBeUpdatedList, false);
    	
    	if(!deliverablesOnActiveProjectsList.isEmpty())
    		Database.update(deliverablesOnActiveProjectsList, false);
    	
    	if(!budgetItemsOnActiveProjectsList.isEmpty())
    		Database.update(budgetItemsOnActiveProjectsList, false);
    }
    
    public void finish(Database.BatchableContext BC){}
}