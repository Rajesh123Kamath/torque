global class CaseClone {
    
    webservice static string cloneCase(id oId)
    {
        string objectToClone = 'Case';
        try
        {
            set <string> ignoreFields = new set <string>();
            Map <string, CaseCloneSettings__c> defaults = new Map <string, CaseCloneSettings__c>();
        
            for(CaseCloneSettings__c o : CaseCloneSettings__c.getall().values())
            {
                if(o.Default_Value__c == null)
                {
                    ignoreFields.add(o.field_api_name__c.toLowerCase());
                }
                else
                {
                    defaults.put(o.field_api_name__c.toLowerCase(), o);
                }
            }
        
            //get fields from the object
            SObjectType objToken = Schema.getGlobalDescribe().get(objectToClone);
            DescribeSObjectResult objDef = objToken.getDescribe();
            Map<String, SObjectField> fields = objDef.fields.getMap();

            string whereString = ' ';

            // remove any fields that should be ignored or have a default.
            for(string f : fields.keySet())
            {
                if(!ignoreFields.contains(f.toLowerCase()) 
                	&& !defaults.containsKey(f.toLowerCase()))
                {
                    whereString += f + ', ';    
                }
            }
        
            whereString = whereString.substring(0,whereString.length()-2);
            system.debug('-----whereString ' + whereString);
        
            string sqlstatement = 'select ' + whereString + ' from case where id = :oId';
            system.debug('-----sqlStatement ' + sqlStatement);
        
            Case o = database.query(sqlstatement);
             
            //clone the opportunity
            Case newCase = o.clone();
        
            // set the default values
            for(string s : defaults.keySet())
            {
                newCase.put(s, setValue(defaults.get(s).default_value__c, defaults.get(s).field_type__c));
            }
        
            system.debug('-----newcase == ' + newCase);
        
            insert newCase;
                    
            //cloneRelatedLists(objdef, oid, newCase.Id);
            
            return newCase.Id+'/e?retURL=%2F' + newCase.id;
            
        }
        catch (Exception e)
        {
            return 'Error ' + e.getMessage();
        }
        
    }   
    
    
    public static Object setValue(string value, string fieldType)
    {
        if(fieldType.toUpperCase() == 'STRING') 
        {
            return string.valueOf(value);
        }
        else if(fieldType.toUpperCase() == 'BOOLEAN')
        {
            return boolean.valueOf(value);
        }    
        else if(fieldType.toUpperCase() == 'DATE' ) 
        {
            //return date.valueOf(value);
            return system.today().addDays(integer.valueOf(value));  
        }
        else if(fieldType.toUpperCase() == 'DATETIME' ) 
        {
            //return datetime.valueOf(value);
            return system.now().addDays(integer.valueOf(value));  
        }
        else if(fieldType.toUpperCase() == 'DOUBLE' ) 
        {
            return double.valueOf(value); 
        }
        else if(fieldType.toUpperCase() == 'INTEGER' ) 
        {
            return integer.valueOf(value);
        }
        else if(fieldType.toUpperCase() == 'ID' ) 
        {
            return id.valueOf(value);  
        }
        
        return value;
    }
    

    private static string buildWhereString(Schema.DescribeSObjectResult d)
    {
        Map<String, SObjectField> fields = d.fields.getMap();

        string whereString = ' ';

        for(string f : fields.keySet())
        {
            whereString += f + ', ';    
        }

        whereString = whereString.substring(0,whereString.length()-2);
        return whereString;     
    }


}