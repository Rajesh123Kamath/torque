/**
  * Handler Name : Torque_TimeEntryHandlerTest
  * Created By   : Trekbin          
  * Created On   : 19-Nov-2015
  * Modified On  : 20-Nov-2015
  * Modified by  : Trekbin
  * Description  : Test class for the trigger 'Torque_TimeEntryTrigger'
**/
@isTest(SeeAllData = false)
private class Torque_TimeEntryHandlerTest {

    /* Last modified by 'Trekbin' on '10-feb-2016' as per case '25221' */
    static testMethod void myUnitTest() {
        
        //Test profile record
        Profile testProfile = [SELECT Id FROM Profile WHERE Name = 'System Administrator']; 
        
        //Test user record
        User testUser = new User(Alias = 'standtw5', Email='standarduser@testorg.com', EmailEncodingKey='UTF-8', LastName='Testingw5', LanguageLocaleKey='en_US', LocaleSidKey='en_US', ProfileId = testProfile.Id, 
            				TimeZoneSidKey='America/Los_Angeles', UserName='standarduserw5@testorg.com', Invoice_Override_Permission__c = false);
        insert testUser;
                         
        System.RunAs(testUser) {
	        //Create a test account record
	        Account accountRecord = new Account();
	        accountRecord.Name ='Test My Account 001';
	        accountRecord.Practice__c = 'Southeast';    
	        insert accountRecord;
	        
	        /* Modified by: Trekbin on 14th-Jan-2016 */
	        //create test setting record used in project trigger
	         ProjectDateSettings__c dateSetting = new ProjectDateSettings__c(BuildConfigTestingEndDayIncrement__c = 14,BuildConfigTestingStartDayIncrement__c = 15,
	         DesignDiscoveryEndDaysIncrement__c = -1,GoLiveAndPostGoLiveEndDayIncrement__c= 7,NonImplementationEndDateIncrement__c = 30);
	         insert dateSetting;
	         
	        //Test Project Record 	
			Project__c objProject = new Project__c(Name = 'Test Project', Account__c = accountRecord.id);
			insert objProject;
			     
			//Test Deliverable Record 	 
			Deliverable__c objDeliverable = new Deliverable__c(Name = 'Test Deliverable', Project__c = objProject.id);
			insert objdeliverable;
			    
			//Test BudgetItem Record 1    
			Budget_Item__c objBudgetItem1 = new Budget_Item__c(Deliverable__c = objdeliverable.id);
			insert objbudgetItem1;
			   
			//Test BudgetItem Record 2   
			Budget_Item__c objBudgetItem2 = new Budget_Item__c(Deliverable__c = objdeliverable.id);
			insert objbudgetItem2;
	
	        //Create a test invoice record 1
	        Invoice__c invoiceRecord1 = new Invoice__c();
	        invoiceRecord1.Account__c = accountRecord.Id; 
	        invoiceRecord1.CurrencyIsoCode = 'USD';
	        invoiceRecord1.Invoice_Status__c = 'Canceled';                                                        
	        insert invoiceRecord1;
	        
	        //Create a test time card record 1
	        Time_Card__c timeCardRecord1  = new Time_Card__c();
	        timeCardRecord1.Name = 'Test Time Card';
	        insert timeCardRecord1;
	        
	        Billing_Reason_Code__c billingReason = new Billing_Reason_Code__c(Name = 'Test Billing');
	         insert billingReason;
	        
	        //Create a time entry record
	        Time_Entry__c timeentryrecord1 = new Time_Entry__c();
	        timeentryrecord1.Time_Card__c = timeCardRecord1.Id;  
	        timeentryrecord1.Time__c = 10;
	        timeentryrecord1.CurrencyIsoCode = 'USD';
	        timeentryrecord1.Billing_Reason__c = billingReason.id; 
	        timeentryrecord1.Budget_Item__c = objBudgetItem1.Id;
	        timeentryrecord1.notes__c = 'test123';
	        insert timeentryrecord1;         
	        
	        //Create a expense record
	        aciee2__Expense__c expenseRecord1 = new aciee2__Expense__c();
	        expenseRecord1.CurrencyIsoCode = 'USD';
	        expenseRecord1.aciee2__Date__c = system.today();
	        expenseRecord1.aciee2__Amount__c = 100;
	        expenseRecord1.aciee2__Description__c = 'hello test';
	        insert expenseRecord1;                
	        
	        //Create a test invoice line record 1
	        Invoice_Line__c invoiceLineRecord1 = new Invoice_Line__c();
	        invoiceLineRecord1.Invoice__c = invoiceRecord1.Id;       
	        invoiceLineRecord1.CurrencyIsoCode = 'USD'; 
	        insert invoiceLineRecord1;        
	        
	         
	               
	        //Create a test time entry record
	        Time_Entry__c timeentryrecord2 = new Time_Entry__c();
	        timeentryrecord2.Time_Card__c = timeCardRecord1.Id;        
	        timeentryrecord2.Invoice_Line__c = invoiceLineRecord1.Id;
	        timeentryrecord2.Time__c = 20;
	        timeentryrecord2.CurrencyIsoCode = 'USD';
	        timeentryrecord2.Billing_Reason__c = billingReason.id; 
	        timeentryrecord2.Budget_Item__c = objBudgetItem2.Id;
	        timeentryrecord2.notes__c = 'test1123';
	        insert timeentryrecord2;              
	        
	        try
	        {            
	            invoiceRecord1.Invoice_Status__c = 'Pending';
	            update invoiceRecord1;          
	           
	            //Scenario-1 updates as the invoice status is not sent
	            timeentryrecord2.notes__c = 'hello123';
	            update timeentryrecord2;    
	            
	            system.assertEquals(timeentryrecord2.notes__c, 'hello123');
	            
	            //Scenario 2 [for satisfing the condition to throw an error]
	            invoiceRecord1.Invoice_Status__c = 'Sent';
	            update invoiceRecord1;
	            
	            //Validation error
	            timeentryrecord2.Notes__c = 'hello1123';
	            update timeentryrecord2;                        
	        }        
	        catch(Exception e)
	        {
	            system.debug('Validation error thrown...Exception Caught');     
	            System.Assert(e.getMessage().contains('You cannot update the time entry record fields as the invoice status is sent'));    
	        }
        }
    }
    
     //For checking delete case=== should throw error
     static testMethod void myUnitTest1() {
        
         //Test profile record
        Profile testProfile = [SELECT Id FROM Profile WHERE Name = 'System Administrator']; 
        
        //Test user record
        User testUser = new User(Alias = 'standtw3', Email='standarduser@testorg.com', EmailEncodingKey='UTF-8', LastName='Testingw3', LanguageLocaleKey='en_US', LocaleSidKey='en_US', ProfileId = testProfile.Id, 
            				TimeZoneSidKey='America/Los_Angeles', UserName='standarduserw3@testorg.com', Invoice_Override_Permission__c = false);
        insert testUser;
                         
        System.RunAs(testUser) {
        	
	        //Create a test account record
	        Account accountRecord = new Account();
	        accountRecord.Name ='Test My Account 001';
	        accountRecord.Practice__c = 'Southeast';    
	        insert accountRecord;
	        
	        /* Modified by: Trekbin on 14th-Jan-2016 */
	        
	        //create test setting record used in project trigger
	         ProjectDateSettings__c dateSetting = new ProjectDateSettings__c(BuildConfigTestingEndDayIncrement__c = 14,BuildConfigTestingStartDayIncrement__c = 15,
	         DesignDiscoveryEndDaysIncrement__c = -1,GoLiveAndPostGoLiveEndDayIncrement__c= 7,NonImplementationEndDateIncrement__c = 30);
	         insert dateSetting;
	        
	        //Test Project Record 	
			Project__c objProject = new Project__c(Name = 'Test Project', Account__c = accountRecord.id);
			insert objProject;
			     
			//Test Deliverable Record 	 
			Deliverable__c objDeliverable = new Deliverable__c(Name = 'Test Deliverable', Project__c = objProject.id);
			insert objdeliverable;
			    
			//Test BudgetItem Record 1    
			Budget_Item__c objBudgetItem1 = new Budget_Item__c(Deliverable__c = objdeliverable.id);
			insert objbudgetItem1;
			   
			//Test BudgetItem Record 2   
			Budget_Item__c objBudgetItem2 = new Budget_Item__c(Deliverable__c = objdeliverable.id);
			insert objbudgetItem2;
			
	        //Create a test invoice record 1
	        Invoice__c invoiceRecord1 = new Invoice__c();
	        invoiceRecord1.Account__c = accountRecord.Id; 
	        invoiceRecord1.CurrencyIsoCode = 'USD';
	        invoiceRecord1.Invoice_Status__c = 'Canceled';                                                        
	        insert invoiceRecord1;
	        
	        //Create a test time card record 1
	        Time_Card__c timeCardRecord1  = new Time_Card__c();
	        timeCardRecord1.Name = 'Test Time Card';
	        insert timeCardRecord1;                    
	        
	        //Create a test invoice line record 1
	        Invoice_Line__c invoiceLineRecord1 = new Invoice_Line__c();
	        invoiceLineRecord1.Invoice__c = invoiceRecord1.Id;       
	        invoiceLineRecord1.CurrencyIsoCode = 'USD'; 
	        insert invoiceLineRecord1;        
	               
	               
	        Billing_Reason_Code__c billingReason = new Billing_Reason_Code__c(Name = 'Test Billing');
	         insert billingReason;       
	               
	        //Create a test time entry record
	        Time_Entry__c timeentryrecord2 = new Time_Entry__c();
	        timeentryrecord2.Time_Card__c = timeCardRecord1.Id;        
	        timeentryrecord2.Invoice_Line__c = invoiceLineRecord1.Id;
	        timeentryrecord2.Time__c = 20;
	        timeentryrecord2.CurrencyIsoCode = 'USD';
	        timeentryrecord2.Billing_Reason__c = billingReason.id; 
	        timeentryrecord2.Budget_Item__c = objBudgetItem2.Id;
	        timeentryrecord2.notes__c = 'test123';
	        insert timeentryrecord2;    
	             
	        
	        try
	        {               
	            //Scenario 3: Time entry has related invoice line record. so error while deletion
	            delete timeentryrecord2;        
	                        
	        }        
	        catch(Exception e)
	        {
	            system.debug('Validation error thrown...Exception Caught');   
	            System.Assert(e.getMessage().contains('You cannot delete this time entry record'));      
	        }
        }
    }
    
    //For checking delete case=== should delete
    static testMethod void myUnitTest2() {
    	
    	Billing_Reason_Code__c billingReason = new Billing_Reason_Code__c(Name = 'Test Billing');
         insert billingReason;
        
       //Create a test account record
        Account accountRecord = new Account();
        accountRecord.Name ='Test My Account 001';
        accountRecord.Practice__c = 'Southeast';    
        insert accountRecord;
        
        /* Modified by: Trekbin on 14th-Jan-2016 */
        
        //create test setting record used in project trigger
         ProjectDateSettings__c dateSetting = new ProjectDateSettings__c(BuildConfigTestingEndDayIncrement__c = 14,BuildConfigTestingStartDayIncrement__c = 15,
         DesignDiscoveryEndDaysIncrement__c = -1,GoLiveAndPostGoLiveEndDayIncrement__c= 7,NonImplementationEndDateIncrement__c = 30);
         insert dateSetting;
         
        //Test Project Record 	
		Project__c objProject = new Project__c(Name = 'Test Project', Account__c = accountRecord.id);
		insert objProject;
		     
		//Test Deliverable Record 	 
		Deliverable__c objDeliverable = new Deliverable__c(Name = 'Test Deliverable', Project__c = objProject.id);
		insert objdeliverable;
		    
		//Test BudgetItem Record 1    
		Budget_Item__c objBudgetItem1 = new Budget_Item__c(Deliverable__c = objdeliverable.id);
		insert objbudgetItem1;
		   
		//Test BudgetItem Record 2   
		Budget_Item__c objBudgetItem2 = new Budget_Item__c(Deliverable__c = objdeliverable.id);
		insert objbudgetItem2;
		
        //Create a test invoice record 1
        Invoice__c invoiceRecord1 = new Invoice__c();
        invoiceRecord1.Account__c = accountRecord.Id; 
        invoiceRecord1.CurrencyIsoCode = 'USD';
        invoiceRecord1.Invoice_Status__c = 'Canceled';                                                        
        insert invoiceRecord1;
        
        //Create a test time card record 1
        Time_Card__c timeCardRecord1  = new Time_Card__c();
        timeCardRecord1.Name = 'Test Time Card';
        insert timeCardRecord1;                    
        
        //Create a test invoice line record 1
        Invoice_Line__c invoiceLineRecord1 = new Invoice_Line__c();
        invoiceLineRecord1.Invoice__c = invoiceRecord1.Id;       
        invoiceLineRecord1.CurrencyIsoCode = 'USD'; 
        insert invoiceLineRecord1;        
               
        //Create a test time entry record
        Time_Entry__c timeentryrecord2 = new Time_Entry__c();
        timeentryrecord2.Time_Card__c = timeCardRecord1.Id;        
        timeentryrecord2.Invoice_Line__c = invoiceLineRecord1.Id;
        timeentryrecord2.Time__c = 20;
        timeentryrecord2.CurrencyIsoCode = 'USD';
        timeentryrecord2.Budget_Item__c = objBudgetItem2.Id;
        timeentryrecord2.Billing_Reason__c = billingReason.id;
        
        timeentryrecord2.notes__c = 'test123'; 
        insert timeentryrecord2;    
             
        
        try
        {               
            //Scenario 2 
            invoiceRecord1.Invoice_Status__c = 'Pending';
            update invoiceRecord1;
            
            //Scenario-1 updates as the invoice status is not sent
            timeentryrecord2.Notes__c = 'hello1';
            timeentryrecord2.Invoice_Line__c = null;
            update timeentryrecord2;        
            
            //Deletes as Invoice_Line__c is null for this child
            delete timeentryrecord2;    
                        
        }        
        catch(Exception e)
        {
            system.debug('Validation error thrown...Exception Caught');         
        }
    }
    
    //For checking
     static testMethod void myUnitTest3() {
         
         Billing_Reason_Code__c billingReason = new Billing_Reason_Code__c(Name = 'Test Billing');
         insert billingReason;
         
         //create test account record
         Account accountRecord = new Account(Name = 'Test Account');
         insert accountRecord;
                          
         //create test setting record used in project trigger
         ProjectDateSettings__c dateSetting = new ProjectDateSettings__c(BuildConfigTestingEndDayIncrement__c = 14,BuildConfigTestingStartDayIncrement__c = 15,
         DesignDiscoveryEndDaysIncrement__c = -1,GoLiveAndPostGoLiveEndDayIncrement__c= 7,NonImplementationEndDateIncrement__c = 30);
         insert dateSetting;
         
         //create test project record
         Project__c projectRecord = new Project__c(Name = 'Test Project', Account__c = accountRecord.id);
         insert projectRecord;
         
         //create test delivaerable record
         Deliverable__c deliverableRec = new Deliverable__c(Name = 'Test Deliverable',Project__c = projectRecord.id);
         insert deliverableRec;
         
         //create test budget item record
         Budget_Item__c budgetItem = new Budget_Item__c(Deliverable__c = deliverableRec.id);
         insert budgetItem;
         
         //create test budget item record
         Budget_Item__c budgetItem1 = new Budget_Item__c(Deliverable__c = deliverableRec.id);
         insert budgetItem1;
         
         //create test time card record
         Time_card__c timeCardRecord = new Time_card__c (Name = 'Time card');
         insert timeCardRecord;
         
         //create test time entry record
         Time_Entry__c timeEntryRecord = new Time_Entry__c (Budget_Item__c = budgetItem.id,Time_card__c = timeCardRecord.id,time__c = 10);
         timeEntryRecord.Billing_Reason__c = billingReason.id; 
         timeentryrecord.notes__c = 'test123';
         insert timeEntryRecord;
         
         //update time entry billable checkbox with true
         timeEntryRecord.billable__c = true;
         timeEntryRecord.unbilled__c = false;
         update timeEntryRecord;
         
         //update time entry non billable checkbox with true
         timeEntryRecord.non_billable__c = true;
         update timeEntryRecord;
         
         //query budget item to see the log hours are update or not
         List<Budget_Item__c> budgetItemList = [select id,Total_Hours_Logged__c,Hours_Burned__c,Non_Billable_Hours__c,Backed_Out_Hours__c from Budget_Item__c where id =: budgetItem.id];
         if(budgetItemList.size()>0){
             //check for asserts
             system.assertEquals(budgetItemList[0].Total_Hours_Logged__c,10);
             system.assertEquals(budgetItemList[0].Hours_Burned__c,10);
             system.assertEquals(budgetItemList[0].Non_Billable_Hours__c,10);
         }
         
         //update time entry unbilled checkbox with true
         timeEntryRecord.unbilled__c = true;
         update timeEntryRecord;
         
         //query budget item to see the log hours are update or not
         List<Budget_Item__c> budgetItemList1 = [select id,Total_Hours_Logged__c,Hours_Burned__c,Non_Billable_Hours__c,Backed_Out_Hours__c from Budget_Item__c where id =: budgetItem.id];
         if(budgetItemList1.size()>0){
             system.assertEquals(budgetItemList1[0].Backed_Out_Hours__c,10);
             system.assertEquals(budgetItemList1[0].Hours_Burned__c,0);
         }
         
         //create test time entry record
         Time_Entry__c timeEntryRecord1 = new Time_Entry__c (Budget_Item__c = budgetItem1.id,Time_card__c = timeCardRecord.id,time__c = 10);
         timeentryrecord1.notes__c = 'test123';
         timeEntryRecord1.Billing_Reason__c = billingReason.id; 
         insert timeEntryRecord1;
         
         //create test time entry record
         Time_Entry__c timeEntryRecord2 = new Time_Entry__c (billable__c = true,unbilled__c = false,Budget_Item__c = budgetItem1.id,Time_card__c = timeCardRecord.id,time__c = 10);
         timeentryrecord2.notes__c = 'test123';
         timeEntryRecord2.Billing_Reason__c = billingReason.id; 
         insert timeEntryRecord2;
         
         //create test time entry record
         Time_Entry__c timeEntryRecord3 = new Time_Entry__c (non_billable__c = true,Budget_Item__c = budgetItem1.id,Time_card__c = timeCardRecord.id,time__c = 10);
         timeentryrecord3.notes__c = 'test123sdfgs';
         timeEntryRecord3.Billing_Reason__c = billingReason.id; 
         insert timeEntryRecord3;
         
         //update budget item id
         timeEntryRecord.Budget_Item__c = budgetItem1.id;
         update timeEntryRecord;
         
         //query budget item to see the log hours are update or not
         List<Budget_Item__c> oldbudgetItemList = [select id,Total_Hours_Logged__c,Hours_Burned__c,Non_Billable_Hours__c,Backed_Out_Hours__c from Budget_Item__c where id =: budgetItem.id];
         if(oldbudgetItemList.size()>0){
             //check for asserts
             system.assertEquals(oldbudgetItemList[0].Total_Hours_Logged__c,0);
             system.assertEquals(oldbudgetItemList[0].Hours_Burned__c,0);
             system.assertEquals(oldbudgetItemList[0].Non_Billable_Hours__c,0);
             system.assertEquals(oldbudgetItemList[0].Backed_Out_Hours__c,0);
         }
         
         //query budget item to see the log hours are update or not
         List<Budget_Item__c> newbudgetItemList = [select id,Total_Hours_Logged__c,Hours_Burned__c,Non_Billable_Hours__c,Backed_Out_Hours__c from Budget_Item__c where id =: budgetItem1.id];
         if(newbudgetItemList.size()>0){
             //check for asserts
             system.assertEquals(newbudgetItemList[0].Total_Hours_Logged__c,40);
             system.assertEquals(newbudgetItemList[0].Hours_Burned__c,10);
             system.assertEquals(newbudgetItemList[0].Non_Billable_Hours__c,20);
             system.assertEquals(newbudgetItemList[0].Backed_Out_Hours__c,10);
         }
         
         //update budget item id
         timeEntryRecord1.Budget_Item__c = budgetItem.id;
         update timeEntryRecord1;
     }
     
     
     /* Created By 'Trekbin' on '10-feb-2016' as per case '25221' */
     //For checking delete case=== should delete
     static testMethod void myUnitTest4() {
        
         //Test profile record
        Profile testProfile = [SELECT Id FROM Profile WHERE Name = 'System Administrator']; 
        
        //Test user record
        User testUser = new User(Alias = 'standtw9', Email='standarduser@testorg.com', EmailEncodingKey='UTF-8', LastName='Testingw9', LanguageLocaleKey='en_US', LocaleSidKey='en_US', ProfileId = testProfile.Id, 
            				TimeZoneSidKey='America/Los_Angeles', UserName='standarduserw9@testorg.com', Invoice_Override_Permission__c = true);
        insert testUser;
                         
        System.RunAs(testUser) {
        	
	        //Create a test account record
	        Account accountRecord = new Account();
	        accountRecord.Name ='Test My Account 001';
	        accountRecord.Practice__c = 'Southeast';    
	        insert accountRecord;
	        
	        /* Modified by: Trekbin on 14th-Jan-2016 */
	        
	        //create test setting record used in project trigger
	         ProjectDateSettings__c dateSetting = new ProjectDateSettings__c(BuildConfigTestingEndDayIncrement__c = 14,BuildConfigTestingStartDayIncrement__c = 15,
	         DesignDiscoveryEndDaysIncrement__c = -1,GoLiveAndPostGoLiveEndDayIncrement__c= 7,NonImplementationEndDateIncrement__c = 30);
	         insert dateSetting;
	        
	        //Test Project Record 	
			Project__c objProject = new Project__c(Name = 'Test Project', Account__c = accountRecord.id);
			insert objProject;
			     
			//Test Deliverable Record 	 
			Deliverable__c objDeliverable = new Deliverable__c(Name = 'Test Deliverable', Project__c = objProject.id);
			insert objdeliverable;
			    
			//Test BudgetItem Record 1    
			Budget_Item__c objBudgetItem1 = new Budget_Item__c(Deliverable__c = objdeliverable.id);
			insert objbudgetItem1;
			   
			//Test BudgetItem Record 2   
			Budget_Item__c objBudgetItem2 = new Budget_Item__c(Deliverable__c = objdeliverable.id);
			insert objbudgetItem2;
			
	        //Create a test invoice record 1
	        Invoice__c invoiceRecord1 = new Invoice__c();
	        invoiceRecord1.Account__c = accountRecord.Id; 
	        invoiceRecord1.CurrencyIsoCode = 'USD';
	        invoiceRecord1.Invoice_Status__c = 'Canceled';                                                        
	        insert invoiceRecord1;
	        
	        //Create a test time card record 1
	        Time_Card__c timeCardRecord1  = new Time_Card__c();
	        timeCardRecord1.Name = 'Test Time Card';
	        insert timeCardRecord1;                    
	        
	        //Create a test invoice line record 1
	        Invoice_Line__c invoiceLineRecord1 = new Invoice_Line__c();
	        invoiceLineRecord1.Invoice__c = invoiceRecord1.Id;       
	        invoiceLineRecord1.CurrencyIsoCode = 'USD'; 
	        insert invoiceLineRecord1;        
	               
	               
	        Billing_Reason_Code__c billingReason = new Billing_Reason_Code__c(Name = 'Test Billing');
	         insert billingReason;       
	               
	        //Create a test time entry record
	        Time_Entry__c timeentryrecord2 = new Time_Entry__c();
	        timeentryrecord2.Time_Card__c = timeCardRecord1.Id;        
	        timeentryrecord2.Invoice_Line__c = invoiceLineRecord1.Id;
	        timeentryrecord2.Time__c = 20;
	        timeentryrecord2.CurrencyIsoCode = 'USD';
	        timeentryrecord2.Billing_Reason__c = billingReason.id; 
	        timeentryrecord2.Budget_Item__c = objBudgetItem2.Id;
	        timeentryrecord2.notes__c = 'test123';
	        insert timeentryrecord2;    
	             
	        
	        try
	        {               
	            //Scenario 3: Time entry has related invoice line record. so error while deletion
	            delete timeentryrecord2;        
	            
	            //Assert to check whether the record has been deleted
	            system.assertEquals([Select Id from Time_Entry__c Where Id = :timeentryrecord2.Id].size(), 0);             
	        }        
	        catch(Exception e)
	        {
	            system.debug('Validation error thrown...Exception Caught');   
	        }
        }
    }
    
    
    /* Created by 'Trekbin' on '10-feb-2016' as per case '25221' */
    static testMethod void myUnitTest5() {
        
        //Test profile record
        Profile testProfile = [SELECT Id FROM Profile WHERE Name = 'System Administrator']; 
        
        //Test user record
        User testUser = new User(Alias = 'standtw6', Email='standarduser@testorg.com', EmailEncodingKey='UTF-8', LastName='Testingw6', LanguageLocaleKey='en_US', LocaleSidKey='en_US', ProfileId = testProfile.Id, 
            				TimeZoneSidKey='America/Los_Angeles', UserName='standarduserw6@testorg.com', Invoice_Override_Permission__c = true);
        insert testUser;
                         
        System.RunAs(testUser) {
	        //Create a test account record
	        Account accountRecord = new Account();
	        accountRecord.Name ='Test My Account 001';
	        accountRecord.Practice__c = 'Southeast';    
	        insert accountRecord;
	        
	        /* Modified by: Trekbin on 14th-Jan-2016 */
	        //create test setting record used in project trigger
	         ProjectDateSettings__c dateSetting = new ProjectDateSettings__c(BuildConfigTestingEndDayIncrement__c = 14,BuildConfigTestingStartDayIncrement__c = 15,
	         DesignDiscoveryEndDaysIncrement__c = -1,GoLiveAndPostGoLiveEndDayIncrement__c= 7,NonImplementationEndDateIncrement__c = 30);
	         insert dateSetting;
	         
	        //Test Project Record 	
			Project__c objProject = new Project__c(Name = 'Test Project', Account__c = accountRecord.id);
			insert objProject;
			     
			//Test Deliverable Record 	 
			Deliverable__c objDeliverable = new Deliverable__c(Name = 'Test Deliverable', Project__c = objProject.id);
			insert objdeliverable;
			    
			//Test BudgetItem Record 1    
			Budget_Item__c objBudgetItem1 = new Budget_Item__c(Deliverable__c = objdeliverable.id);
			insert objbudgetItem1;
			   
			//Test BudgetItem Record 2   
			Budget_Item__c objBudgetItem2 = new Budget_Item__c(Deliverable__c = objdeliverable.id);
			insert objbudgetItem2;
	
	        //Create a test invoice record 1
	        Invoice__c invoiceRecord1 = new Invoice__c();
	        invoiceRecord1.Account__c = accountRecord.Id; 
	        invoiceRecord1.CurrencyIsoCode = 'USD';
	        invoiceRecord1.Invoice_Status__c = 'Canceled';                                                        
	        insert invoiceRecord1;
	        
	        //Create a test time card record 1
	        Time_Card__c timeCardRecord1  = new Time_Card__c();
	        timeCardRecord1.Name = 'Test Time Card';
	        insert timeCardRecord1;
	        
	        Billing_Reason_Code__c billingReason = new Billing_Reason_Code__c(Name = 'Test Billing');
	         insert billingReason;
	        
	        //Create a time entry record
	        Time_Entry__c timeentryrecord1 = new Time_Entry__c();
	        timeentryrecord1.Time_Card__c = timeCardRecord1.Id;  
	        timeentryrecord1.Time__c = 10;
	        timeentryrecord1.CurrencyIsoCode = 'USD';
	        timeentryrecord1.Billing_Reason__c = billingReason.id; 
	        timeentryrecord1.Budget_Item__c = objBudgetItem1.Id;
	        timeentryrecord1.notes__c = 'test123';
	        insert timeentryrecord1;         
	        
	        //Create a expense record
	        aciee2__Expense__c expenseRecord1 = new aciee2__Expense__c();
	        expenseRecord1.CurrencyIsoCode = 'USD';
	        expenseRecord1.aciee2__Date__c = system.today();
	        expenseRecord1.aciee2__Amount__c = 100;
	        expenseRecord1.aciee2__Description__c = 'hello test';
	        insert expenseRecord1;                
	        
	        //Create a test invoice line record 1
	        Invoice_Line__c invoiceLineRecord1 = new Invoice_Line__c();
	        invoiceLineRecord1.Invoice__c = invoiceRecord1.Id;       
	        invoiceLineRecord1.CurrencyIsoCode = 'USD'; 
	        insert invoiceLineRecord1;        
	        
	         
	               
	        //Create a test time entry record
	        Time_Entry__c timeentryrecord2 = new Time_Entry__c();
	        timeentryrecord2.Time_Card__c = timeCardRecord1.Id;        
	        timeentryrecord2.Invoice_Line__c = invoiceLineRecord1.Id;
	        timeentryrecord2.Time__c = 20;
	        timeentryrecord2.CurrencyIsoCode = 'USD';
	        timeentryrecord2.Billing_Reason__c = billingReason.id; 
	        timeentryrecord2.Budget_Item__c = objBudgetItem2.Id;
	        timeentryrecord2.notes__c = 'test1123';
	        insert timeentryrecord2;              
	        
	        try
	        {            
	            invoiceRecord1.Invoice_Status__c = 'Pending';
	            update invoiceRecord1;          
	           
	            //Scenario-1 updates as the invoice status is not sent
	            timeentryrecord2.notes__c = 'hello123';
	            update timeentryrecord2;    
	            
	            system.assertEquals(timeentryrecord2.notes__c, 'hello123');
	            
	            //Scenario 2 [for satisfing the condition to throw an error]
	            invoiceRecord1.Invoice_Status__c = 'Sent';
	            update invoiceRecord1;
	            
	            //Will update even though invoice status is sent because Invoice_Override_Permission__c is being set to true
	            timeentryrecord2.Notes__c = 'hello1123';
	            update timeentryrecord2;
	            
	            system.assertEquals([Select Id, Notes__c from Time_Entry__c Where Id = :timeentryrecord2.Id].Notes__c, 'hello1123');                    
	        }        
	        catch(Exception e)
	        {
	            system.debug('Validation error thrown...Exception Caught');     
	        }
        }
    }
}