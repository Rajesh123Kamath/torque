/**
    @ Handler name : Torque_SendInvoiceController
    @ Created By   : Trekbin
    @ Created On   : 2-Dec-2015
    @ Description  : Handler for the page 'Torque_SendInvoicePage'
    
    Modified By    :  Raja Yeccherla(GearsCRM)
    Modified Date  :  7th- Dec 2015
    Description    :  Added Ref ID to the email Template Initial_Invoice_Email_Template__c and 
                      added emailService - SendInvoice Remainder to the reply email address
    
*/

public with sharing class Torque_SendInvoiceController 
{   
    public String strComment {get; set;}
    
    public Invoice__c invoiceRecord;      
       
    //Wrapper class declaration
    public List<InvoicingWrapperClass> lstInvoicingWrapper {get; set;}
    
    //For getting the FN and LN of the current logged in user so to assign to "CommentFrom" field for Invoice comment
    public User currentUser;
    
    public List<Invoice_Comment__c> lstInvoiceCommentToInsert;   
    
    //Variable to hold the primary invoice contact lookup Id from the invoice related account
    public Id parentContactId;
    
    //Used to stop redirection if user selects a contact with no email address, then error has to display instead of redirection
    public Boolean toRedirect = false;
            
    //Constructor
    public Torque_SendInvoiceController()
    {   
        //Initializing wrapper class
        lstInvoicingWrapper = new List<InvoicingWrapperClass>();    
        
        //Initializing the current logged in user
        currentUser = new User();  
        
        //
        /* Step 1 */
        //Declare string variable       
        String tempStr = '';
            
        //Store the invoice recordId from where 'Send Invoice' button is executed
        tempStr = ApexPages.currentPage().getParameters().get('Id');

        //If the string contains some value
        if(tempStr != null)
        {
            //Fetch the respective Invoice record with Account field
            invoiceRecord = [ Select Id, RecordTypeId, CurrencyIsoCode, Account__c, Account__r.Name, Account__r.Date_Last_Invoiced__c, Account__r.Primary_Invoice_Contact__c, Invoice_Status__c, Invoice_Amount__c, Invoice_Date__c, Due_Date__c, Next_Reminder_Date__c, Customer_Emails__c from Invoice__c where Id = :tempStr ];           
        
        	/* Last Modified By: Trekbin On 12-Jan-2016 */
        	//Assign the primary invoice contact lookup Id to a variable
        	parentContactId = invoiceRecord.Account__r.Primary_Invoice_Contact__c;
        }
        
        /** 
       		Last Modified By: Trekbin On 12-Jan-2016 
       		Last Modified content: Passed an extra parameter "invoiceRecord.Account__r.Primary_Invoice_Contact__c" for fetchContactsUsingInvoiceAccount method 
           	Reason of modification: To set the default check to true only for the "invoiceRecord.Account__r.Primary_Invoice_Contact__c" contact record.
         */
        //If there is a invoice record then pass the respective account Id to a function to retrive related contacts
        if(invoiceRecord != NULL)     
            fetchContactsUsingInvoiceAccount(invoiceRecord.Account__c, parentContactId);       
    }
    
    /*
         Method name: fetchContactsUsingInvoiceAccount
    */
    public void fetchContactsUsingInvoiceAccount(Id accountId, Id parentContactId)
    {
        //If there is an account Id
        if(accountId != NULL)
        {
            /** 
               	Last Modified By: Trekbin On 12-Jan-2016 
               	Last Modified content: Implemented 'If' block and also updated logic to default other contacts checkbox to false in the 'For' section
               	Reason: To set the default check to true only for the "invoiceRecord.Account__r.Primary_Invoice_Contact__c" contact record.
             */
            //Condition to check if the parent contact lookup field has a value for the invoice related account.
            if(parentContactId != NULL)
            {	
            	/*Modified by Trekbin on 28 June, 2016 for the case  00026364*/ 
    			//Adding that parent contact and default the checkbox to true for that parent contact
    			lstInvoicingWrapper.add(new InvoicingWrapperClass([Select Id, AccountId, FirstName, LastName, Email, Title, Billing_Contact__c From Contact Where Id = :parentContactId AND Status__c != 'Left Company' limit 1], true)); 
   			}
   			
   			/*Modified by Trekbin on 28 June, 2016 for the case  00026364*/
            //Fetch all the contacts with respect to that particular account
            for(Contact contactRecord : [Select Id, AccountId, FirstName, LastName, Email, Title, Billing_Contact__c From Contact 
                                            Where AccountId = :accountId AND Id != :parentContactId AND Status__c != 'Left Company'])
            {
                //Passing this contact record to wrapper list               
                lstInvoicingWrapper.add(new InvoicingWrapperClass(contactRecord, false));            
            }                               
        }
        
        //If there are no contacts to display simply add a error message
        if(lstInvoicingWrapper.isEmpty())
        {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'There are no contacts to display for invoicing'));   
        }  
    }
    
    
    /*
         Method name: generateInvoiceAndSendMail
         Last Modified by Trekbin on 12-Jan-2016
         Last Modified Description: Changed the return type from void to PageReference so as to handle redirect functionality after invoice is sent
    */
    public PageReference generateInvoiceAndSendMail()
    {                   
        
        /* Now the wrapper list contains contacts both checked and unchecked */
        
        //Send an email only to the checked contacts present in the wrapper list    
        if(!lstInvoicingWrapper.isEmpty())
        {
            //List to hold the contacts which are selected by user for sending invoice
            List<Contact> lstContactsToSendEmail = new List<Contact>();
            
            Id billingContactId;
            
            //Set to hold the contact records whose billing contact has been set to true
            //Set<Id> setContactsToFilterAndSetAsBillingContact = new Set<Id>();
            
            //Iterate over the wrapper list
            for(InvoicingWrapperClass objInvoicingWrapperClass : lstInvoicingWrapper)
            {
                //Add only the contacts to the list whose isChecked is true
                if(objInvoicingWrapperClass.isChecked)
                    lstContactsToSendEmail.add(objInvoicingWrapperClass.contactRecord);      
                
                if(objInvoicingWrapperClass.contactRecord.Billing_Contact__c) 
                	billingContactId = objInvoicingWrapperClass.contactRecord.Id;
                
                //Fetch all the contact records whose billing contact has been set to true and store in a set
                //if(objInvoicingWrapperClass.contactRecord.Billing_Contact__c)   
                //    setContactsToFilterAndSetAsBillingContact.add(objInvoicingWrapperClass.contactRecord.Id);
            }
            
            //This is for creating invoice comment only if the user has selected any contacts for send invoice. 
            //Without selecting any contacts no invoice comment record gets created
            if(!lstContactsToSendEmail.isEmpty())
            {                       
                //Query on user to fetch the first name and last name for assigning to 'invoiceCommentRecord.Comment_From__c'           
                currentUser = [Select Id, Name, firstname, lastname, Email From User Where Id = :userinfo.getuserId()];
                String firstName = currentUser.firstname;
                String lastName = currentUser.lastname;
                
                //Initializing the list of Invoice_Comment__c
                lstInvoiceCommentToInsert = new List<Invoice_Comment__c>();
                                
                //Create an Invoice comment record
                Invoice_Comment__c invoiceCommentRecord = new Invoice_Comment__c();
                invoiceCommentRecord.Invoice__c = invoiceRecord.Id;
                invoiceCommentRecord.Comment_Date__c = System.now();
                invoiceCommentRecord.Comment_From__c = firstName + ' ' + lastName;
                invoiceCommentRecord.Invoice_Comment__c = strComment;
                lstInvoiceCommentToInsert.add(invoiceCommentRecord);    
                
                if(!lstInvoiceCommentToInsert.isEmpty())
                  insert lstInvoiceCommentToInsert;
            }
            
            //Pass those checked contacts to the email method
            sendEmailToCheckedContacts(lstContactsToSendEmail, parentContactId, lstInvoiceCommentToInsert, billingContactId); 
            
            //Condition to stop redirection if user selects a contact with no email address, then error has to display instead of redirection
            if(toRedirect)
            {
            	//Redirect functionality on complete of invoicing
            	PageReference redirect = redirectBack();
            	return redirect;
            }            
        }        
        return null;               
    }   
    
    /*
         Method name: sendEmailToCheckedContacts
    */
    public void sendEmailToCheckedContacts(List<Contact> lstContactsToSendEmail, Id parentContactId,List<Invoice_Comment__c> invCommments, Id billingContactId)
    {       
        /* FOR ATTACHMENT CRITERIA -- STEP 4 */ 
        //Map to hold pricingId to list of attachments
        Map<ID, List<Attachment>> mapInvoiceToLstAttachment = new map<ID, List<Attachment>>(); 
        
        list<EmailServicesAddress> emailServiceAddr = new list<EmailServicesAddress>();
        string emailServAddress;
        emailServiceAddr = [select localPart, EmailDomainName from EmailServicesAddress where localpart = 'SendInvoiceReminders' LIMIT 1];
        
        if (emailServiceAddr != null && emailServiceAddr.size() > 0)
        {
            emailServAddress = emailServiceAddr[0].localPart + '@' + emailServiceAddr[0].EmailDomainName;
        }
    
        //Iterate over attachments for the Invoice record
        for(Attachment attachmentToStick : [select Name, ParentId, Body, BodyLength, LastModifiedDate from Attachment where ParentId = :invoiceRecord.Id order by LastModifiedDate desc Limit 1])
        {
            //Condition to check if the map already contains invoiceId or not
            //If 'not' then create a map of invoiceId with the attachment record
            if(!mapInvoiceToLstAttachment.containsKey(attachmentToStick.ParentId))
                mapInvoiceToLstAttachment.put(attachmentToStick.ParentId, new List<Attachment>());
                    
                //If 'yes' then add the attachment record to the list of attchment records for that invoice in the map
                mapInvoiceToLstAttachment.get(attachmentToStick.ParentId).add(attachmentToStick);   
        }
        
        //Hierarchy based "InvoiceDefaults__c" custom setting records 
        InvoiceDefaults__c invoiceDefaultsRecords = InvoiceDefaults__c.getInstance(currentUser.Id);
        
        //List to hold all emails to send   
        list<Messaging.SingleEmailMessage> lstOfMailsToSend = new list<Messaging.SingleEmailMessage>();  
        
        //List to hold the email addresses of the contacts to be updated on a field 'Customer_Emails__c'
        list<String> lstOfInvoiceSentContactEmailId = new list<String>();
        
        //Declare the templateId
        String templateId = '';
        
        //This variable is for updating the invoice field after email has been sent
        Decimal daysBetweenReminders = null;
        
        daysBetweenReminders = invoiceDefaultsRecords.Days_between_Reminders__c;
            
        //Holds the 'Initial_Invoice_Email_Template__c' field value
        templateId = invoiceDefaultsRecords.Initial_Invoice_Email_Template__c;  
        
        List<Torque_Invoice_Record_Type__mdt> customMetadataTypeList = [	SELECT OrgWideEmailID__c 
			        														FROM Torque_Invoice_Record_Type__mdt 
			        														where InvoiceRecordTypeID__c = :invoiceRecord.RecordTypeId
			        													];
        													
        //If the templateId is having any values then only send the template email to the respective contacts who has an email id
        if(templateId != null && templateId != '' && !lstContactsToSendEmail.isEmpty())
        {
            for(Contact contactToMail : lstContactsToSendEmail)
            {
                if(contactToMail.Email != null )
                {
                    //Send an email message as per the email template to that respective contacts
                    Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();                                
                    //email.setToAddresses(toAddress); 
                    if(emailServAddress != null)
                        email.setReplyTo(emailServAddress);      
                    //email.setSenderDisplayName('GearsCRM Accounting');     
                    
                    if(!customMetadataTypeList.isEmpty())
                    	email.setOrgWideEmailAddressId(customMetadataTypeList[0].OrgWideEmailID__c);
                    	        
                    email.setTargetObjectId(contactToMail.Id);   
                    
                    if (!invCommments.isEmpty())
                        email.setWhatId(invCommments[0].Id);                   
                    else
                        email.setWhatId(invoiceRecord.Id);                   
                    email.setTemplateId( Id.valueOf(templateId));
                    email.saveAsActivity = false;
                    
                    /* FOR ATTACHMENTS TO BE SENT ALONG WITH EMAIL -- STEP 4 */
                    if(mapInvoiceToLstAttachment.containsKey(invoiceRecord.Id))
                    {
                        //List to hold attachment records to send
                        List<Messaging.EmailFileAttachment> lstFileAttachments = new List<Messaging.EmailFileAttachment>();
                        
                        //Iterate over all the attachments
                        for(Attachment attachmentRecord : mapInvoiceToLstAttachment.get(invoiceRecord.Id))
                        {
                            Messaging.EmailFileAttachment efa = new Messaging.EmailFileAttachment();
                            efa.setFileName(attachmentRecord.Name);
                            efa.setBody(attachmentRecord.Body);
                            lstFileAttachments.add(efa);
                        }
                        email.setFileAttachments(lstFileAttachments);
                    }
                    lstOfMailsToSend.add(email);    
                    
                    //List to hold email addresses of the contacts who has been sent an email
                    lstOfInvoiceSentContactEmailId.add(contactToMail.Email);  
                    toRedirect = true;           
                }
                else
                {
                    //Display name of contact along with validation message if that selected contact doesnot have a email address
                    if(contactToMail.FirstName != null)
                        ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'There is no email address for the selected contact '+contactToMail.FirstName+' '+ contactToMail.LastName+'.\n For other selected contacts with email address, an invoice has been sent'));                  
                    else
                        ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'There is no email address for the selected contact '+contactToMail.LastName+'.\n For other selected contacts with email address, an invoice has been sent'));                   
                }              
            }
        }  
        
        //Send an email to all the members in the list
        if(!lstOfMailsToSend.isEmpty())
        {
           // Messaging.sendEmail(lstOfMailsToSend);                
            List<Messaging.SendEmailResult> results = Messaging.sendEmail(lstOfMailsToSend); 

            for(integer i =0;i<lstOfMailsToSend.size();i++)
            {
                if (!results.get(i).isSuccess()) 
                {
                    String errorMessage = results.get(i).getErrors()[i].getMessage();
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.error,'Email not sent'+errorMessage));
                    
                
                }
                else if(results.get(i).isSuccess())
                {
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Confirm,'Email Sent Successfully'));
                }
            }
            
            //Update 2 fields on invoice record after email delivery
            updateInvoiceRecordAfterDelivery(invoiceRecord, daysBetweenReminders, lstOfInvoiceSentContactEmailId, parentContactId, billingContactId);                                        
        }    
    }
    
    
    /*
         Method name: updateInvoiceRecordAfterDelivery
         Last Modified by trekbin on 27-jan-2016
    */
    public void updateInvoiceRecordAfterDelivery(Invoice__c invoiceRecord, Decimal daysBetweenReminders, list<String> lstOfInvoiceSentContactEmailId, Id parentContactId, Id billingContactId)
    {       
        String AccountId18Digit = '';
        
        String Accountid15Digit = '';
        
        String formattedInvoiceDueDate = '';
        
        DateTime invoiceDueDate;       
        
        //Create an invoice instance
        Invoice__c invoiceInstance = [Select Id, CurrencyIsoCode, Account__c, Account__r.Name, Account__r.Date_Last_Invoiced__c, Account__r.Primary_Invoice_Contact__c, Invoice_Status__c, Invoice_Amount__c, Invoice_Date__c, Due_Date__c, Next_Reminder_Date__c, Customer_Emails__c from Invoice__c where Id = :invoiceRecord.Id];
        
        //Fetch the 18digit accountId
        AccountId18Digit = invoiceInstance.Id;        
        
        //Convert 18 digit accountId to 15 digit
        Accountid15Digit = AccountId18Digit.substring(0, 15);
        
        //Conditional check to see if the 
        if(invoiceInstance.Due_Date__c != null)
        {
	        //Format the dueDate to MM/dd/yyyy
	        invoiceDueDate = (DateTime)invoiceInstance.Due_Date__c;
			formattedInvoiceDueDate = invoiceDueDate.formatGMT('MM/dd/yyyy');
        }
        
        //Subject Line to assign to invoice.subject
        String subjectToAssign = '';        
        subjectToAssign = invoiceInstance.Account__r.Name+' - Invoice Notice - '+invoiceInstance.CurrencyIsoCode+' '+invoiceInstance.Invoice_Amount__c+' Due '+formattedInvoiceDueDate+' Ref:'+Accountid15Digit;
         
        String strCustomerEmails = invoiceInstance.Customer_Emails__c;
        
        //List to hold Invoice records which needs to be updated
        List<Invoice__c> lstInvoiceRecordToUpdate = new List<Invoice__c>();
        
        //List to hold Account records which needs to be updated
        List<Account> lstAccountRecordToUpdate = new List<Account>();
               
        //String variable to exclude the billing contact from storing into the customer emails field
        String billingContactEmailIdToExclude = null;
         
         system.debug('--1---'+invoiceInstance);         
         system.debug('--2---'+invoiceInstance.Invoice_Date__c);    
         system.debug('--3---'+daysBetweenReminders);    
         system.debug('--4---'+lstOfInvoiceSentContactEmailId);    
         system.debug('--5---'+subjectToAssign);    
         system.debug('--6---'+parentContactId);    
         system.debug('--7---'+billingContactId); 
         
        /*	Modified by: Trekbin on 12-Jan-2016 */  
        //For updating fields on Invoice record
        if(invoiceInstance != null  && !lstOfInvoiceSentContactEmailId.isEmpty())
        {          
        	if(billingContactId != null)
        	{                   
	            //String variable to exclude the billing contact from storing into the customer emails field
	            billingContactEmailIdToExclude = [Select Id, AccountId, FirstName, LastName, Email, Title, Billing_Contact__c, CreatedDate From Contact Where Id = :billingContactId].Email;
        	}
        	           
            //Create an instance of the current invoice record for updation
            Invoice__c invoiceRecordToUpdate = new Invoice__c();
            invoiceRecordToUpdate.Id = invoiceInstance.Id;
            
            if(invoiceInstance.Invoice_Date__c != null && daysBetweenReminders != null)
            	invoiceRecordToUpdate.Next_Reminder_Date__c = invoiceInstance.Invoice_Date__c + Integer.valueOf(daysBetweenReminders); 
            	
            invoiceRecordToUpdate.Billing_Contact__c = billingContactId;
            invoiceRecordToUpdate.Customer_Emails__c = invoiceInstance.Customer_Emails__c;   
            invoiceRecordToUpdate.Invoice_Status__c = 'Sent';
            
            if(subjectToAssign != null)
            	invoiceRecordToUpdate.Subject__c = subjectToAssign;
            
            //Create an instance of the current account record for updation
            Account accountRecordToUpdate = new Account(Id = invoiceInstance.Account__c);
            accountRecordToUpdate.Date_Last_Invoiced__c = System.today();
            lstAccountRecordToUpdate.add(accountRecordToUpdate);
            
            for(String strEmail : lstOfInvoiceSentContactEmailId)
            {   
            	
            	system.debug('--strEmail---'+strEmail);          
            	system.debug('--billingContactEmailIdToExclude---'+billingContactEmailIdToExclude);       
                //Exclude the billing contact from storing into the customer emails field
                if((strEmail != billingContactEmailIdToExclude))
                {
	                if(invoiceRecordToUpdate.Customer_Emails__c != NULL && invoiceRecordToUpdate.Customer_Emails__c != '')   
	                {
	                    if(!invoiceRecordToUpdate.Customer_Emails__c.contains(strEmail))
	                    {
	                        invoiceRecordToUpdate.Customer_Emails__c = invoiceRecordToUpdate.Customer_Emails__c + ', ' + strEmail;
	                    }
	                }
	                else
	                {
	                    invoiceRecordToUpdate.Customer_Emails__c = (invoiceRecordToUpdate.Customer_Emails__c!=NULL && invoiceRecordToUpdate.Customer_Emails__c!='') ? invoiceRecordToUpdate.Customer_Emails__c + strEmail: strEmail;
	                }
                }
                
            }
            
            system.debug('---Customer_Emails__c--'+invoiceRecordToUpdate.Customer_Emails__c);
            lstInvoiceRecordToUpdate.add(invoiceRecordToUpdate);
        }
        
        /* Update the list of invoice and account records */
        if(!lstInvoiceRecordToUpdate.isEmpty())
            update lstInvoiceRecordToUpdate;       
            
        if(!lstAccountRecordToUpdate.isEmpty())
        	update lstAccountRecordToUpdate;
    }
    
    /* This will redirect the user from the vf page to the current invoice detail page once invoicing gets sent*/
    public PageReference redirectBack()
    {
        //We are getting the base url of the page and simply passing invoice id
        String url = '';
        url = System.URL.getSalesforceBaseUrl().toExternalForm();       
        PageReference redirect = new PageReference(url + '/' +invoiceRecord.Id);        
        return redirect;        
    }
            
             
    /* Wrapper class */
    public class InvoicingWrapperClass
    {
        public Contact contactRecord {get; set;}
        public Boolean isChecked {get; set;}
        
        public InvoicingWrapperClass (Contact contactRecord, Boolean isChecked)
        {
            this.isChecked = isChecked;
            this.contactRecord = contactRecord;
        }
    }
}