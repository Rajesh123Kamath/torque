/**
    @Name			:	Torque_VFPanelToDisplayInvoicesOnAccTest
    @Date			:   15 Feb, 2016
    @Description	:	Test code for Torque_VFPanelToDisplayInvoicesOnAccCntr.cls.
*/
@isTest
private class Torque_VFPanelToDisplayInvoicesOnAccTest {

	static testMethod void myUnitTest() {
		
		//Create a test account record.
		Account objAccount  = UTestData.getAccounts(1)[0];
		insert objAccount;
        
        //Create test invoice records.
        List<Invoice__c> lstInvoices = new List<Invoice__c>{	UTestData.getInvoice(objAccount.Id, 'Pending'),
        														UTestData.getInvoice(objAccount.Id, 'Canceled'),
        														UTestData.getInvoice(objAccount.Id, 'Sent'),
        														UTestData.getInvoice(objAccount.Id, 'Pending'),
        														UTestData.getInvoice(objAccount.Id, 'Pending'),
        														UTestData.getInvoice(objAccount.Id, 'Pending'),
        														UTestData.getInvoice(objAccount.Id, 'Pending'),
        														UTestData.getInvoice(objAccount.Id, 'Pending'),
        														UTestData.getInvoice(objAccount.Id, 'Pending'),
        														UTestData.getInvoice(objAccount.Id, 'Pending'),
        														UTestData.getInvoice(objAccount.Id, 'Pending'),
        														UTestData.getInvoice(objAccount.Id, 'Pending'),
        														UTestData.getInvoice(objAccount.Id, 'Pending'),
        														UTestData.getInvoice(objAccount.Id, 'Sent')
        													};
        insert lstInvoices;
        
		ApexPages.StandardController controller = new ApexPages.StandardController(objAccount);
		
		Torque_VFPanelToDisplayInvoicesOnAccCntr objVFPageCntr = new Torque_VFPanelToDisplayInvoicesOnAccCntr(controller);
		
		system.assertEquals(13, objVFPageCntr.totalNoOfRecords);
		system.assertEquals(true, objVFPageCntr.hasNext);
		system.assertEquals(false, objVFPageCntr.hasPrevious);
		
        objVFPageCntr.next();
        system.assertEquals(2, objVFPageCntr.pageNumber);
        
        objVFPageCntr.previous();
        system.assertEquals(1, objVFPageCntr.pageNumber);
        
        objVFPageCntr.last();
        system.assertEquals(2, objVFPageCntr.pageNumber);
        
		objVFPageCntr.first();
		system.assertEquals(1, objVFPageCntr.pageNumber);
		
	}
}