/**
    @Name      :  Torque_DialyViewHandler
    @Date      :   1 Dec, 2015
    @Description  :  An apex page controller for Torque_DialyViewPage page.
    @TestClass    :  Torque_DialyViewHandlerTest
*/
public with sharing class Torque_DialyViewHandler {
  
  //Time Entry Object
  public Time_Entry__c timeEntryObject{get;set;}
  
  //variables to show total hours for previous week
  public Decimal totalHoursPrevWeek				{get;set;}
  public Decimal totalBillableHoursPrevWeek		{get;set;}
  public Decimal totalUnBillableHoursPrevWeek	{get;set;} 
  
  //variables to show total hours for current week
  public Decimal totalHoursThisWeek{get;set;}
  
  /* Modified by Trekbin on 13-Jan-2016 */
  public Decimal currentMonthUtilizationCalc {get; set;}  
  
  //List to hold the "BudgetItemStatus__c" custom setting records
  public set<String> setBudgetItemStatusRecords;
  
  public Decimal totalBillableHoursThisWeek{get;set;}
  public Decimal totalUnBillableHoursThisWeek{get;set;}
  
  //variable to get record ids selected on page
  public string todayClicked{get;set;}
  public string timeEntryId{get;set;}
  public string recordId{get;set;}
  public string favoriteProjectId{get;set;}
  public string reportId{get;set;}
  
  public String strTimeCardStatus 				{get;set;}			//Last Modified By Trekbin on 5-july, 2016 as per the updated requirement on case: 00026389	[Used to display timecard status in section 1 under statistics]	
  public Quick_Note__c objQuickNote  		 	{get;set;}			//Last Modified By Trekbin on 5-july, 2016 as per the updated requirement on case: 00026389	[Used to display quick notes in section 1 and attach to the fields under statistics]	
  
  //variable to show sum of hours
  public Decimal totalHours{get;set;}
  
  //flag variable to edit time entry row
  public boolean editFlag{get;set;}
  
  //flag variable to show case and reuse field according to internal and external user
  public boolean isReuse{get;set;}
  
  //wrapper class list for time entry and favorite project
  public List<WrapperTimeEntry> wrapperTimeEntryList{get;set;}
  public List<WrapperFavorite> wrapperFavoriteList{get;set;}
  
  //select option list for budget items
  public List<SelectOption> relatedBudgetItems {get;set;}
  
  //fav combo of project/budget/notes/billing reason to fav id map
  Map<string,Favorite_Project__c> favProjectMap;
  
  /* Added by Trekbin on 2 March, 2016 */
  Map<Id, Id> mapBudgtItmDefltBillingReason;
  
  Map<Id, Integer> mapFavoriteProjectIDToIndex;
  
  //Default constructor
  public Torque_DialyViewHandler(){
      
      objQuickNote = new Quick_Note__c();
      
      mapFavoriteProjectIDToIndex = new Map<Id, Integer>();
      
      /* Modified by Trekbin on 13-Jan-2016 */      
      currentMonthUtilizationCalc = 0.0;
      
      //Assign the Current users Current_Month_Utilization_Calculation__c to a variable
      currentMonthUtilizationCalc = [Select Current_Month_Utilization_Calculation__c From User Where Id = :userinfo.getuserID()].Current_Month_Utilization_Calculation__c; 
      
      if(currentMonthUtilizationCalc == null)      
      {
      		currentMonthUtilizationCalc = 0.0;
      }
      //List to hold the "BudgetItemStatus__c" custom setting records 
      setBudgetItemStatusRecords = new Set<String>();  
      
      for(BudgetItemStatus__c objCS :BudgetItemStatus__c.getAll().values())
      {
     	 setBudgetItemStatusRecords.add(objCS.Name);
      }
      
      //Initialise Time Entry Object
      timeEntryObject = new Time_Entry__c();
      
      //Initialise select option list and add first value as none in it
      relatedBudgetItems = new List<SelectOption>();
      relatedBudgetItems.add(new SelectOption('','--None--'));
      
      //Iterate on custom setting internal profiles to check whether current user is inernal or external user
      for(InternalProfiles__c internalProfile : [select ID__c from InternalProfiles__c]){
          
          //check if current user is internal user if yes set reuse to true
          if(internalProfile.ID__c != null){
              if(UserInfo.getProfileId().contains(internalProfile.ID__c)){
                  isReuse = true;
              }
          }
      }
      
      system.debug('----isReuse--'+isReuse);
      /* Commented by Trekbin on 2 March, 2016 */
      //on load set billing reason as default to globalSetting.Default_Billing_Reason__c 
      //defaultBillingReason();
      
      //fetch time card hours for previous and current week on page load
      fetchTimeCardHours();
      
      //fetch favorite projects of current user on page load
      fetchFavoriteRecords();
      //default date to today
      timeEntryObject.Date__c = system.today();
      
      //fetch today's time entry of current user on page load
      fetchTimeEntryRecords(system.today());
      
      /* Last Modified By Trekbin on 6-july, 2016 as per the updated requirement on case: 00026389	[R]*/
      displayQuickNotesValue();
        
      reportId = GlobalSettings__c.getInstance().Weekly_Time_Matrix_Report__c;
      
      /* Last Modified By Trekbin on 18-july, 2016 as per the updated requirement on case: 00019049 [R] - redirection*/ 
      if(ApexPages.currentPage().getParameters().get('dateForRedirection') != null && ApexPages.currentPage().getParameters().get('dateForRedirection') != '') {
      	timeEntryObject.Date__c = Date.valueOf(ApexPages.currentPage().getParameters().get('dateForRedirection'));
      	
      	fetchTimeEntryRecords(timeEntryObject.Date__c);
      }
  }
  
  //set billing reason as default to globalSetting.Default_Billing_Reason__c if selected budget item has no default Billing Reason.
  public void defaultBillingReason(){
      
      /* Modified by Trekbin on 2 March, 2016 */ 
      if(timeEntryObject.Budget_Item__c != null && mapBudgtItmDefltBillingReason.containsKey(timeEntryObject.Budget_Item__c))
      	timeEntryObject.Billing_Reason__c = mapBudgtItmDefltBillingReason.get(timeEntryObject.Budget_Item__c);
      else
      	timeEntryObject.Billing_Reason__c = GlobalSettings__c.getInstance().Default_Billing_Reason__c;
  }
  
  /* Last Modified By Trekbin on 5-july, 2016 as per the updated requirement on case: 00026389	[R]*/
  //fetch Time Card hours for previous and current week
  public void fetchTimeCardHours(){
  
        //Initialise hours variables to 0
        totalHoursPrevWeek = 0;
        totalBillableHoursPrevWeek = 0;
        totalUnBillableHoursPrevWeek = 0;
        
        totalHoursThisWeek = 0;
        totalBillableHoursThisWeek = 0;
        totalUnBillableHoursThisWeek = 0;
      	
      	strTimeCardStatus = '';
      	
        for(Time_Card__c timeEntryObj : [ Select Id, Billable_Hours_Logged__c, Status__c, Total_Hours_Logged__c, Unbilled_Hours__c, Week_Beginning__c, User__c
                    						FROM Time_Card__c
                    						WHERE User__c =:userinfo.getuserID() AND Week_Beginning__c = LAST_WEEK ]){
          
            totalHoursPrevWeek = totalHoursPrevWeek+timeEntryObj.Total_Hours_Logged__c;
            totalBillableHoursPrevWeek = totalBillableHoursPrevWeek+timeEntryObj.Billable_Hours_Logged__c;
            totalUnBillableHoursPrevWeek = totalUnBillableHoursPrevWeek+timeEntryObj.Unbilled_Hours__c;
            
            //Update the time card status under statistics of section 1 	[R]
            strTimeCardStatus = timeEntryObj.Status__c;
        }
        
        for(Time_Card__c timeEntryObj : [Select Id,Billable_Hours_Logged__c, Status__c, Total_Hours_Logged__c,Unbilled_Hours__c,Week_Beginning__c
                   FROM Time_Card__c 
                   WHERE User__c =:userinfo.getuserId() AND Week_Beginning__c = THIS_WEEK ]) {
          
            totalHoursThisWeek = totalHoursThisWeek+timeEntryObj.Total_Hours_Logged__c;
            totalBillableHoursThisWeek = totalBillableHoursThisWeek+timeEntryObj.Billable_Hours_Logged__c;
            totalUnBillableHoursThisWeek = totalUnBillableHoursThisWeek+timeEntryObj.Unbilled_Hours__c;
            
            //Update the time card status under statistics of section 1		[R]
            strTimeCardStatus = timeEntryObj.Status__c;
        }
    }
    
    
    //fetch favorite records of current user to show in section 2
    public void fetchFavoriteRecords(){
        
        timeEntryObject = new Time_Entry__c();
        timeEntryObject.Time__c = 0.0;
        
        /* Commented by Trekbin on 2 March, 2016 */
        //calling method to default billing reason
        //defaultBillingReason();
        
        //Initialise favorite map
        favProjectMap = new Map<string,Favorite_Project__c>();
        
        //fill wrapper class for favorite records as checkbox is to shown to mark as favorite
        wrapperFavoriteList = new List<WrapperFavorite>();
        
        /* Last Modified By Trekbin on 5-july, 2016 as per the updated requirement on case: 00026389	[R]*/
        for(Favorite_Project__c favProj : [select id,BillingReason__c,BudgetItem__c, BudgetItem__r.Name, Project2__c, Project2__r.Is_Active__c, Project2__r.Name, User__c from Favorite_Project__c where User__c =: UserInfo.getUserId() AND Project2__r.Is_Active__c = : true order by Project2__r.Name asc, BudgetItem__r.Name asc]){
            WrapperFavorite favRecord = new WrapperFavorite(favProj,true);
            wrapperFavoriteList.add(favRecord);
            
            //update on 12/18/2015 as per requirement
            //filling map by creating favorite combo of project/ budget item/billing reason
            string favCombo = ''+favProj.Project2__c+favProj.BudgetItem__c+favProj.BillingReason__c;
            favProjectMap.put(favCombo,favProj);
        }
    }
    
    //create, delete favorite record when time entry check box is selected and unselected
    public void createDeleteFavoriteRecord(){
        
        //check if selected id is not null
        if(recordId!= '' && recordId!= null){
            
            Time_Entry__c timeEntry = new Time_Entry__c();
            Time_Entry__c falseTimeEntry = new Time_Entry__c();
            List<Favorite_Project__c> insertFavRecord = new List<Favorite_Project__c>();
            
            //iterate on time entry wrapper class list
            for(WrapperTimeEntry tEntryRecord : wrapperTimeEntryList){
                if(tEntryRecord.timeEntryObject.id == recordId){
                    //check whether the time entry checkbox is true or false
                    if(tEntryRecord.check == true){
                        timeEntry = tEntryRecord.timeEntryObject;
                    }
                    if(tEntryRecord.check == false){    
                        falseTimeEntry = tEntryRecord.timeEntryObject;
                    }
                }
            }
            
            //if time is unselected delete the corresponding favorite record which have fav combo of Project/Budget Entry/Notes/billing reason
            if(falseTimeEntry.id != null){  
                List<Favorite_Project__c> deleteFavRecord = new List<Favorite_Project__c>();
                string tEntryCombo = ''+falseTimeEntry.Project__c+falseTimeEntry.Budget_Item__c+falseTimeEntry.Billing_Reason__c;
                if(favProjectMap.containsKey(tEntryCombo)){
                    Favorite_Project__c favRecord = favProjectMap.get(tEntryCombo);
                    deleteFavRecord.add(favRecord);
                }
                try{
                    if(!deleteFavRecord.isEmpty()){
                        delete deleteFavRecord;
                    }
                }catch(Exception e){
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getmessage()));
                }
            }
            
            //create a favorite record with fav combo of Project/Budget Entry/Notes/billing reason for corresponding time entry
            if(timeEntry.id != null){
                
                Favorite_Project__c favRecord = new Favorite_Project__c();
                favRecord.User__c = UserInfo.getUserId();
                favRecord.Project2__c = timeEntry.Project__c;
                favRecord.BudgetItem__c = timeEntry.budget_item__c;
                favRecord.BillingReason__c = timeEntry.billing_reason__c;
                insertFavRecord.add(favRecord);
                
                //insert favorite record
                try{
                    if(!insertFavRecord.isEmpty()){
                        insert insertFavRecord;
                    }
                }catch(Exception e){
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getmessage()));
                }
            }
            
            date dtSelected = timeEntryObject.date__c;
            //fetch favorite records 
            fetchFavoriteRecords();
            timeEntryObject.date__c = dtSelected;
            
            //fetch current user time entry records
            fetchTimeEntryRecords(timeEntryObject.date__c);
        }
    }
    
    //method to save favorite project when hit save in section 2
    public void saveFavoriteProject(){
    
        if(favoriteProjectId != null && favoriteProjectId != ''){
        	
        	//Initially set the index value to 0
        	Integer rowIndex = 0;
        	
            Favorite_Project__c favRecord = new Favorite_Project__c();
            Time_Entry__c tEntry = new Time_Entry__c();
            
            //update code on 12/18/2015 as per requirement
            //Iterate on wrapper favorite to get favorite record to be updated
            for(WrapperFavorite wrapperFavProj : wrapperFavoriteList){
            	
            	 //Create a map of favoriteProjectID to rowIndex
                mapFavoriteProjectIDToIndex.put(wrapperFavProj.favoriteProject.id, rowIndex);
                    
                if(wrapperFavProj.favoriteProject.id == favoriteProjectId){
                    favRecord = wrapperFavProj.favoriteProject;
                    tEntry.Project__c = favRecord.Project2__c;
                    tEntry.notes__c = wrapperFavProj.favNotes;
                    tEntry.Time__c = wrapperFavProj.favHours;
                    if(isReuse == true)
                        tEntry.Reuse__c = wrapperFavProj.favReuse;
                    
                    /* commented by Trekbin on 2 March, 2016 */
                    //else
                    tEntry.Case_Number__c = wrapperFavProj.favCaseNumber;
                }
                
                //Increment the rowIndex for the next record
                rowIndex++;
            }
             
            List<Time_Entry__c> insertTimeEntry = new List<Time_Entry__c>();
            
            if(favRecord.BudgetItem__c != null){
                tEntry.budget_item__c = favRecord.BudgetItem__c;
                //set time entry deliverable as budget item deliverable value
                For(Budget_Item__c budgetItemObject : [select id,Deliverable__c from budget_item__c where id =: favRecord.BudgetItem__c]){
                
                    if(budgetItemObject.Deliverable__c != null){
                    
                        tEntry.Deliverable__c = budgetItemObject.Deliverable__c;
                    }
                }
            }
			
            //set time billable,non billable, unbilled from billing reasons billable, non billable, unbilled value
            if(favRecord.BillingReason__c != null){
                tEntry.billing_reason__c = favRecord.BillingReason__c;
                for(Billing_Reason_Code__c billingReasonObject : [select id,billable__c,Non_billable__c,Unbilled_del__c from Billing_Reason_Code__c where id =: favRecord.BillingReason__c]){
                    tEntry.Billable__c = billingReasonObject.billable__c;
                    tEntry.Non_Billable__c = billingReasonObject.Non_billable__c;
                    tEntry.Unbilled__c = billingReasonObject.Unbilled_del__c;
                }
            }
			
            if(timeEntryObject.date__c != null){
                
                //find week start date from date selected
                date weekStart = timeEntryObject.date__c.toStartOfWeek();
                
                //As time card is required on time entry fetch time card id
                For(Time_Card__c timeCard : [select id from Time_card__c where Week_Beginning__c =: weekStart And User__c =: UserInfo.getUserId() limit 1]){
                    tEntry.Time_card__c = timeCard.id; 
                }
                
                
                //set performed by to current user
                tEntry.Performed_By__c = UserInfo.getUserId();
                tEntry.Date__c = timeEntryObject.date__c;
            }
            
            //save (update) favorite record
            try{
                if(favRecord.id != null){
                    
                    //update favorite record
                    update favRecord;
                    
                    /* Last Modified by Trekbin on 26-July, 2016 as per the reported issue on case: 00026389 */
                    //insert time entries
                    if(tEntry.Time_card__c != null)
                    	insert tEntry;
                    
                    date dtSelected = timeEntryObject.date__c;
                    //fetch favorite records
                    
                    /* Last Modified By Trekbin on 8-july, 2016 as per the updated requirement on case: 00026389	[R]*/	
                    //fetchFavoriteRecords();	
                    
                    //set date as selected and fetch time entries of related date
                    timeEntryObject.Date__c = dtSelected;
                    fetchTimeEntryRecords(timeEntryObject.date__c);
                    
                    //fetch time card hours to refresh hours statistics
                    fetchTimeCardHours();
                    
                    /* Last Modified By Trekbin on 8-july, 2016 as per the updated requirement on case: 00026389	[R]*/	
                    //Fetch the respective wrapper project to nullify
                    WrapperFavorite objWrapperFavProj = wrapperFavoriteList.get(mapFavoriteProjectIDToIndex.get(favoriteProjectId));                    
                    objWrapperFavProj.favHours = 0.00; 
                    objWrapperFavProj.favNotes = '';
                    objWrapperFavProj.favCaseNumber = '';
                    objWrapperFavProj.favReuse = false;
                }
            }catch(Exception e){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getmessage()));
            }
        }
    }
    
    //when checkbox in section 2 is unselected delete the corresponding favorite project
    public void deleteFavoriteRecord(){
    
        if(recordId!= '' && recordId!= null){
            List<Favorite_Project__c> deleteFavRecord = new List<Favorite_Project__c>();
            
            //Iterate on wrapper favorite list to get favorite record to be deleted
            for(WrapperFavorite favRecord : wrapperFavoriteList){
                if(favRecord.favoriteProject.id == recordId){
                    if(favRecord.check == false){
                        deleteFavRecord.add(favRecord.favoriteProject);
                    }
                }
            }
            
            //delete favorite record
            try{
                if(!deleteFavRecord.isEmpty()){
                    delete deleteFavRecord;
                }
                
                //fetch favorite records and date as it was selected
                date dtSelected = timeEntryObject.date__c;
                fetchFavoriteRecords();
                
                //set date as it was was selected
                timeEntryObject.date__c = dtSelected;
                //fetch time entry records with time card of current user and date
                fetchTimeEntryRecords(timeEntryObject.Date__c);
                
            }catch(Exception e){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getmessage()));
            }
        }
    }
    
    //fetch related budget items of project when project is selected to insert non favorite time entry in section 2
    public void relatedBudgetOfSelectedProject(){
    
        //Initialise select option list and add first value as none in it
        relatedBudgetItems = new List<SelectOption>();
        relatedBudgetItems.add(new SelectOption('','--None--'));
        mapBudgtItmDefltBillingReason = new Map<Id, Id>();
        
        if(timeEntryobject.project__c != null){
        
        	/* Last Modified by : Trekbin on 20-jan-2016 */
        	/* Modified by Trekbin on 2 March, 2016 */
            //fetch budgets of project selected
            For(Project__c projectObject : [select id,(select id,name, Status__c, Default_Billing_Reason__c from Budget_Entries__r Where Status__c IN :setBudgetItemStatusRecords order by name asc) from project__c where id =: timeEntryobject.project__c]){
                
                //Iterate on budgets of project to add in select list 
                for(Budget_Item__c budgetItem : projectObject.Budget_Entries__r){
                
                    relatedBudgetItems.add(new SelectOption(budgetItem.id,budgetItem.name));
                    
                    /* Added by Trekbin on 2 March, 2016 */
                    if(budgetItem.Default_Billing_Reason__c != null)
                    	mapBudgtItmDefltBillingReason.put(budgetItem.id, budgetItem.Default_Billing_Reason__c);
                }
            }
        }
    }
    
    //insert Non favorite time time entry in section 2
    public void insertNonFavoriteTimeEntry(){
        
        List<Time_Entry__c> insertTimeEntry = new List<Time_Entry__c>();
        if(timeEntryObject.Budget_Item__c != null){
            
            //set time entry deliverable as budget item deliverable value
            For(Budget_Item__c budgetItemObject : [select id,Deliverable__c from budget_item__c where id =: timeEntryObject.Budget_Item__c]){
            
                if(budgetItemObject.Deliverable__c != null){
                
                    timeEntryObject.Deliverable__c = budgetItemObject.Deliverable__c;
                }
            }
        }
        
        //set time billable,non billable, unbilled from billing reasons billable, non billable, unbilled value
        if(timeEntryObject.Billing_Reason__c != null){
            
            for(Billing_Reason_Code__c billingReasonObject : [select id,billable__c,Non_billable__c,Unbilled_del__c from Billing_Reason_Code__c where id =: timeEntryObject.Billing_Reason__c]){
                timeEntryObject.Billable__c = billingReasonObject.billable__c;
                timeEntryObject.Non_Billable__c = billingReasonObject.Non_billable__c;
                timeEntryObject.Unbilled__c = billingReasonObject.Unbilled_del__c;
            }
        }
        
        if(timeEntryObject.date__c != null){
        
            //find week start date from date selected
            date weekStart = timeEntryObject.date__c.toStartOfWeek();
             system.debug('--weekStart-----'+weekStart);
            //As time card is required on time entry fetch time card id
            For(Time_Card__c timeCard : [select id from Time_card__c where Week_Beginning__c =: weekStart And User__c =: UserInfo.getUserId() limit 1]){
                timeEntryObject.Time_card__c = timeCard.id; 
            }
            
            //set performed by to current user
            timeEntryObject.Performed_By__c = UserInfo.getUserId();
            insertTimeEntry.add(timeEntryobject);
            system.debug('--timeEntryobject-----'+timeEntryobject);
            //insert time entries
            try{
                if(!insertTimeEntry.isEmpty()){
                    insert timeEntryobject;
                }
                
                date dtSelected = timeEntryObject.date__c;
                
                //fetch favorite records 
                fetchFavoriteRecords();
                
                //set date as it was selected and fetch time entires related to it
                timeEntryObject.Date__c = dtSelected;
                fetchTimeEntryRecords(timeEntryObject.date__c);
                
                //fetch time cards to refresh hours statistics
                fetchTimeCardHours();
                
            }catch(Exception e){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getmessage()));
            }
        }
    }
    
    //method called on click of previous button in section 1
    public void previousDate(){
    	
    	system.debug('---previousDate-----');
        timeEntryObject.date__c = timeEntryObject.date__c.addDays(-1);
        fetchTimeEntryRecords(timeEntryObject.date__c);
        
        /* Last Modified By Trekbin on 6-july, 2016 as per the updated requirement on case: 00026389	[R]*/	
        displayQuickNotesValue();
    }
    
    //method called on click of next button in section 1
    public void nextDate(){
        timeEntryObject.date__c = timeEntryObject.date__c.addDays(1);
        fetchTimeEntryRecords(timeEntryObject.date__c);
        
        /* Last Modified By Trekbin on 6-july, 2016 as per the updated requirement on case: 00026389	[R]*/
        displayQuickNotesValue();
    }
    
    //show time entries according date when date is changed in section 3
    public void showTimeEntries(){
        fetchTimeEntryRecords(timeEntryObject.Date__c);
        
        /* Last Modified By Trekbin on 6-july, 2016 as per the updated requirement on case: 00026389	[R]*/
        displayQuickNotesValue();
    }
    
    //show time entries of today when today button is clicked
    public void timeEntriesAccrdingtoDate(){
        if(todayClicked != null && todayClicked.trim().length()>0 && todayClicked != ''){
            if(todayClicked == 'Today'){
                timeEntryObject.Date__c = system.today();
                fetchTimeEntryRecords(system.today());
                
                /* Last Modified By Trekbin on 6-july, 2016 as per the updated requirement on case: 00026389	[R]*/
                displayQuickNotesValue();
            }
        }
    }
    
    //fetch time entry related to selected date
    /* Last Modified By Trekbin on 6-july, 2016 as per the updated requirement on case: 00026389	[R]*/
    public void fetchTimeEntryRecords(date dateSelected){
    
        totalHours = 0;
        wrapperTimeEntryList = new List<WrapperTimeEntry>();

        for(Time_Entry__c timeEntry : [select id, Date__c, Time_Card__r.Week_End__c, Time_Card__r.Week_Beginning__c, Time_Card__c, Project__c, Project__r.name,Invoice_Status__c,Budget_Item__c,Budget_Item__r.name,Billing_Reason__c,Billing_Reason__r.name,Notes__c,Time__c,Reuse__c, Case_Number__c from Time_Entry__c where date__c =: dateSelected and Time_Card__r.User__c =:userinfo.getuserID()]){
            
            totalHours = totalHours+timeEntry.Time__c;
            string tEntryCombo = ''+timeEntry.Project__c+timeEntry.Budget_Item__c+timeEntry.Billing_Reason__c;
            
            //check if fav combo of project/budget item/notes/billing reason exist with respect to time entry then true checkbox is checked else false
            if(favProjectMap.containsKey(tEntryCombo)){
                  WrapperTimeEntry tEntryRecord = new WrapperTimeEntry(timeEntry,true);
                  wrapperTimeEntryList.add(tEntryRecord);
            }else{
                WrapperTimeEntry tEntryRecord = new WrapperTimeEntry(timeEntry,false);
                wrapperTimeEntryList.add(tEntryRecord);
            }
        }
    }
    
    //to edit time entry in section 3
    public void editTimeEntry(){
        editFlag = true;
    }
    
    //save time entry after change in section 3
    /* Last Modified By Trekbin on 6-july, 2016 as per the updated requirement on case: 00026389	[R]*/
    public void saveTimeEntry(){
    	
    	//Form a map of weekBeginning Date to its respective timeCard record 
    	Map<Date, Time_Card__c> mapWeekBeginningToTimeCard = new Map<Date, Time_Card__c>();
    	
    	//Iterate over all the time card records and form a map of timeCard week Beginning to timeCard
    	for(Time_Card__c objTimeCard  : [ SELECT Id, Week_Beginning__c, Week_End__c FROM Time_Card__c WHERE User__c =: userInfo.getUserId()]) {
    		
    		mapWeekBeginningToTimeCard.put(objTimeCard.Week_Beginning__c, objTimeCard);
    	}
    	
        if(timeEntryId != null && timeEntryId != ''){
            
            string billingReasonId ='';
            
            Time_Entry__c timeEntry = new Time_Entry__c();
            for(WrapperTimeEntry wrapperTentry : wrapperTimeEntryList){
                if(wrapperTentry.timeEntryObject.id == timeEntryId){
                    timeEntry = wrapperTentry.timeEntryObject;
                    if(wrapperTentry.timeEntryObject.Billing_Reason__c != null){
                        billingReasonId = wrapperTentry.timeEntryObject.Billing_Reason__c;
                    }
                    
                    //Check if the timeentryobject date is in between timecard's Week_Beginning__c & Week_End__c
                    if(wrapperTentry.timeEntryObject.Date__c >= wrapperTentry.timeEntryObject.Time_Card__r.Week_Beginning__c && wrapperTentry.timeEntryObject.Date__c <= wrapperTentry.timeEntryObject.Time_Card__r.Week_End__c) {
                    	
                    	//Do Nothing
                    }
                    else {
                    	
                    	Time_Card__c objTimeCard = null;
                    	
                    	//User selected date
						Date objWeekOfTheUserSelectedDate = wrapperTentry.timeEntryObject.Date__c.toStartofWeek();
						
						//Fetch the time card record wherein the time entry date lies in between week beginning and week end
						if(mapWeekBeginningToTimeCard.containsKey(objWeekOfTheUserSelectedDate))
							objTimeCard  = mapWeekBeginningToTimeCard.get(objWeekOfTheUserSelectedDate);
						
						//If there is any time card
                    	if(objTimeCard != null) {
                    		
                    		//Assign the new time card with that date range
                    		timeEntry.Time_Card__c = objTimeCard.Id; 
                    	}
                    }
                }
            } 
            
            //update code on 12/16/2015 as per requirement 
            //update Time entries billable boolean field with billing reason boolean field if billing reason is updated in time entries
            if(billingReasonId != '' && billingReasonId.trim().length()>0){
                 for(Billing_Reason_Code__c billingReasonObject : [select id,billable__c,Non_billable__c,Unbilled_del__c from Billing_Reason_Code__c where id =: billingReasonId]){
                    timeEntry.Billable__c = billingReasonObject.billable__c;
                    timeEntry.Non_Billable__c = billingReasonObject.Non_billable__c;
                    timeEntry.Unbilled__c = billingReasonObject.Unbilled_del__c;
                }
            }
            
            /* Last Modified By Trekbin on 27-1-2016 */
            List<Time_Entry__c> insertTimeEntry = new List<Time_Entry__c>();
	        if(timeEntry.Budget_Item__c != null){
	            
	            //set time entry deliverable as budget item deliverable value
	            For(Budget_Item__c budgetItemObject : [select id, Deliverable__c from budget_item__c where id =: timeEntry.Budget_Item__c]){
	            
	                if(budgetItemObject.Deliverable__c != null){
	                
	                    timeEntry.Deliverable__c = budgetItemObject.Deliverable__c;
	                }
	            }
	        }
            
            try{
                if(timeEntry.id != null){
                    update timeEntry ;
                    editFlag = false;
                    
                    //fetch time entry records with time card of current user and date
                    fetchTimeEntryRecords(timeEntryObject.Date__c);
                    
                    //fetch time card hours to refresh statistics
                    fetchTimeCardHours();
                }
            }catch(Exception e){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getmessage()));
            }
        }
    }
    
    //delete time entry when delete is clicked in section 3
    public void deleteTimeEntry(){
        if(timeEntryId != null && timeEntryId != ''){
            
            Time_Entry__c timeEntry = new Time_Entry__c(); 
            timeEntry = [select id from time_entry__c where id =: timeEntryId];
            
            if(timeEntry.id != null){
                try{
                    delete timeEntry;
                    
                    //fetch time entry records with time card of current user and date
                    fetchTimeEntryRecords(timeEntryObject.Date__c);
                    
                    //fetch time card hours to refresh hours statistics
                    fetchTimeCardHours();
                }catch(Exception e){
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getmessage()));
                }
            }
        }
    }
    
    
    /**
		@ Method Name	:	createQuickNoteOnSave
		@ Parameters	: 	None
		@ Created By	: 	Trekbin on 5-july, 2016
		@ Description	: 	Create a quick note record to a time card on click of save in section 1	[R]
	 */
	public void createQuickNoteOnSave() {	
		
		//User selected date
		Date objWeekOfTheUserSelectedDate = timeEntryObject.Date__c.toStartofWeek();
		
		//List to insert Quick_Note__c record into the database
		List<Quick_Note__c> lstQuickNotesToInsert = new List<Quick_Note__c>();
		
		//Fetch timecard with its childs for the current week 
		List<Time_Card__c> lstTimeCard  = [ SELECT Id, Week_Beginning__c, Week_End__c, (Select Time_Card__c, Notes__c From Torque_Quick_Notes__r order by LastModifiedDate asc limit 1) FROM Time_Card__c
									  		WHERE User__c =: userInfo.getUserId()
									  		AND (Week_Beginning__c <= :objWeekOfTheUserSelectedDate)
									  		AND (Week_End__c >= :objWeekOfTheUserSelectedDate)];
		
		//If there is any corresponding time card with the criteria then proceed   
		if(!lstTimeCard.isEmpty())	{			
			
			//Check if there exists any quick note records - for update case								 
			if(!lstTimeCard[0].Torque_Quick_Notes__r.isEmpty()) {	
				
				//Update the existing quick note and add it to the list		
				Quick_Note__c objQuickNoteClone = new Quick_Note__c(Id = lstTimeCard[0].Torque_Quick_Notes__r[0].Id, Time_Card__c = lstTimeCard[0].Id, Notes__c = objQuickNote.Notes__c);
				lstQuickNotesToInsert.add(objQuickNoteClone); 
			}
			else {
				
				//Create a new quick note and add it to the list		
				Quick_Note__c objQuickNoteClone = new Quick_Note__c(Time_Card__c = lstTimeCard[0].Id, Notes__c = objQuickNote.Notes__c);
				lstQuickNotesToInsert.add(objQuickNoteClone);
			}
			
			//Insert/update the list to the database as per the condition
			if(!lstQuickNotesToInsert.isEmpty())
				upsert lstQuickNotesToInsert;
		}
	}

	/**
		@ Method Name	:	displayQuickNotesValue
		@ Parameters	: 	None
		@ Created By	: 	Trekbin on 6-july, 2016
		@ Description	: 	display quick notes on click of prev and next button in section 1	[R]
	 */
	public void displayQuickNotesValue() {
		
		//User selected date
		Date objWeekOfTheUserSelectedDate = timeEntryObject.Date__c.toStartofWeek();
		
		//Fetch timecard with its childs for the current week 
		List<Time_Card__c> lstTimeCardRecords  = [ SELECT Id, Week_Beginning__c, Week_End__c, (Select Time_Card__c, Notes__c From Torque_Quick_Notes__r order by LastModifiedDate asc limit 1) FROM Time_Card__c WHERE User__c =: userInfo.getUserId() AND (Week_Beginning__c <= :objWeekOfTheUserSelectedDate) AND (Week_End__c >= :objWeekOfTheUserSelectedDate)];
		
		//If there is any corresponding time card with the criteria then proceed   
		if(!lstTimeCardRecords.isEmpty())	{			
			
			//Check if there exists any quick note records 							 
			if(!lstTimeCardRecords[0].Torque_Quick_Notes__r.isEmpty()) {
				
				//Assign the notes to the set variable
				objQuickNote.Notes__c = lstTimeCardRecords[0].Torque_Quick_Notes__r[0].Notes__c;
			}
			else {
				
				//Set the notes as empty
				objQuickNote.Notes__c = '';
			}
		}
		else {
				
			//Set the notes as empty
			objQuickNote.Notes__c = '';
		}
	}
	
	
	/**
		@ Method Name	:	performRedirectionToWeeklyViewPageAsPerDate
		@ Parameters	: 	None
		@ Created By	: 	Trekbin on 1-August, 2016
		@ Description	: 	redirect to weekly view page on the particular day on click of the current week button	[R]
	 */
	public PageReference performRedirectionToWeeklyViewPageAsPerDate(){
		
	   	//Fetch the date passed from the page where user clicked so that it can be passed in the url of weeklyViewPage
	   	String strDateForRedirection = ApexPages.currentPage().getParameters().get('weekDateToPassToController');
	    
	    //Declare the url and perform redirection
	   	String url = '';
        url = System.URL.getSalesforceBaseUrl().toExternalForm();       
        PageReference redirect = new PageReference(url + '/apex/Torque_WeeklyViewPage?weekDateForRedirection='+strDateForRedirection);  
        redirect.setRedirect(true);
              
        return redirect;
	}
	
	
    //wrapper class for favorite records
    public class WrapperFavorite{
        public Favorite_Project__c favoriteProject{get;set;}
        public boolean check{get;set;}
        
        //variable to store hour,reuse,notes and case number of favorite project
        public decimal favHours{get;set;}	
        public string favNotes{get;set;}
        public string favCaseNumber{get;set;}
        public boolean favReuse{get;set;}
        public WrapperFavorite(Favorite_Project__c favoriteProject,boolean check){
            this.favoriteProject = favoriteProject;
            this.check = check;
            this.favHours = 0.00;
            this.favReuse = false;
        }
    }
    
    //wrapper class for time entry records
    public class WrapperTimeEntry{
        public Time_Entry__c timeEntryObject{get;set;}
        public boolean check{get;set;}
        public WrapperTimeEntry(Time_Entry__c timeEntryObject,boolean check){
            this.timeEntryObject = timeEntryObject;
            this.check = check;
        }
    }
}