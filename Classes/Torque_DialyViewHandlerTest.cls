/**
    @Name      :  Torque_DialyViewHandlerTest 
    @Date      :   14 Dec, 2015
    @Description  :  Test class for Torque_DialyViewHandler class
    @Class    :  Torque_DialyViewHandler
*/

@isTest 
private class  Torque_DialyViewHandlerTest 
{
    
     static testMethod void myUnitTest()  
    {       
    	
    	/* Last Modified by Trekbin on 14-Jan-2016 */
    
    	Billing_Reason_Code__c defaultBillingReason = new Billing_Reason_Code__c();
    	defaultBillingReason.Billable__c = true;
    	defaultBillingReason.CurrencyIsoCode = 'USD';
    	defaultBillingReason.Name = 'Billable';
    	insert defaultBillingReason;
    	
    	Group instanceHealthCheckCaseOwner = new Group();
    	instanceHealthCheckCaseOwner.Name = 'BA Queue';
    	insert instanceHealthCheckCaseOwner;
    	    	    	
        GlobalSettings__c objcustomsetting = new GlobalSettings__c();
        objcustomsetting.Account_Credit_Alert__c = 'There is a credit on this Account. Please go to Payments to apply the credit to this Invoice.';
        objcustomsetting.InstanceHealthCheckCaseOwnerId__c = instanceHealthCheckCaseOwner.Id;
        objcustomsetting.PTODaysInTheFuture__c = 60.0;
        objcustomsetting.Default_Billing_Reason__c = defaultBillingReason.Id;        
        insert objcustomsetting;
        
        //create test account record
         Account accountRecord = new Account(Name = 'Test Account');
         insert accountRecord;
         
         //create test setting record used in project trigger
         ProjectDateSettings__c dateSetting = new ProjectDateSettings__c(BuildConfigTestingEndDayIncrement__c = 14,BuildConfigTestingStartDayIncrement__c = 15,
         DesignDiscoveryEndDaysIncrement__c = -1,GoLiveAndPostGoLiveEndDayIncrement__c= 7,NonImplementationEndDateIncrement__c = 30);
         insert dateSetting;
         
         //create test project record
         Project__c projectRecord = new Project__c(Name = 'Test Project', Account__c = accountRecord.id);
         insert projectRecord;
         
         //create test delivaerable record
         Deliverable__c deliverableRec = new Deliverable__c(Name = 'Test Deliverable',Project__c = projectRecord.id);
         insert deliverableRec;
         
         //create test budget item record
         Budget_Item__c budgetItem = new Budget_Item__c(Deliverable__c = deliverableRec.id,Project__c = projectRecord.id);
         insert budgetItem;
         
         //create test budget item record
         Budget_Item__c budgetItem1 = new Budget_Item__c(Deliverable__c = deliverableRec.id);
         insert budgetItem1;
         
         //create Test Billing Reason record
         Billing_Reason_Code__c billingReason = new Billing_Reason_Code__c(Name = 'Test Billing');
         insert billingReason;
   
   
         List<Time_card__c> timeCardList = new List<Time_card__c>();
         
         //create test time card record
         Time_card__c timeCardRecord = new Time_card__c (Name = 'Time card',Week_Beginning__c = system.today(),User__c = userinfo.getuserID());
         Time_card__c timeCardRecord1 = new Time_card__c (Name = 'Time card1',Week_Beginning__c = system.today().adddays(-6),User__c = userinfo.getuserID());
         Time_card__c timeCardRecord2 = new Time_card__c (Name = 'Time card2',Week_Beginning__c = system.today().adddays(-5),User__c = userinfo.getuserID());
         Time_card__c timeCardRecord3 = new Time_card__c (Name = 'Time card2',Week_Beginning__c = system.today().adddays(-4),User__c = userinfo.getuserID());
         Time_card__c timeCardRecord4 = new Time_card__c (Name = 'Time card2',Week_Beginning__c = system.today().adddays(-3),User__c = userinfo.getuserID());
         Time_card__c timeCardRecord5 = new Time_card__c (Name = 'Time card2',Week_Beginning__c = system.today().adddays(-2),User__c = userinfo.getuserID());
         Time_card__c timeCardRecord6 = new Time_card__c (Name = 'Time card2',Week_Beginning__c = system.today().adddays(-1),User__c = userinfo.getuserID());
         Time_card__c timeCardRecord7 = new Time_card__c (Name = 'Time card2',Week_Beginning__c = system.today().adddays(-7),User__c = userinfo.getuserID());
         
         timeCardList.add(timeCardRecord);
         timeCardList.add(timeCardRecord1);
         timeCardList.add(timeCardRecord2);
         timeCardList.add(timeCardRecord3);
         timeCardList.add(timeCardRecord4);
         timeCardList.add(timeCardRecord5);
         timeCardList.add(timeCardRecord6);
         timeCardList.add(timeCardRecord7);
         
         insert timeCardList;
         
         List<Time_Entry__c> timeEntryList = new List<Time_Entry__c>();
         
  
         
         //create test time entry record
         Time_Entry__c timeEntryRecord = new Time_Entry__c (Budget_Item__c = budgetItem.id,Time_card__c = timeCardRecord.id,Billing_Reason__c = billingReason.id,time__c = 10,date__c = system.today(),Project__c = projectRecord.id,Notes__c = 'Testing');
         Time_Entry__c timeEntryRecord1 = new Time_Entry__c (Budget_Item__c = budgetItem.id,Time_card__c = timeCardRecord.id,time__c = 10,date__c = system.today().adddays(1),Project__c = projectRecord.id,Notes__c = 'Testing',Billing_Reason__c = billingReason.id);
         Time_Entry__c timeEntryRecord2 = new Time_Entry__c (Budget_Item__c = budgetItem.id,Time_card__c = timeCardRecord.id,time__c = 10,date__c = system.today().adddays(-1),Project__c = projectRecord.id,Notes__c = 'Testing',Billing_Reason__c = billingReason.id);
         
         timeEntryList.add(timeEntryRecord);
         timeEntryList.add(timeEntryRecord1);
         timeEntryList.add(timeEntryRecord2);
         
         insert timeEntryList;
         
         //create test custom setting internal profiles to identify internal and external users
         InternalProfiles__c internalProfile = new InternalProfiles__c(id__c = UserInfo.getProfileId());
         internalProfile.name = 'internalProfile';
         insert internalProfile;
    
     
         
         //create test favorite project
         Favorite_Project__c favProject = new Favorite_Project__c();
         favProject.BillingReason__c = billingReason.id;
         favProject.BudgetItem__c = budgetItem.id;
         favProject.Project2__c = projectRecord.id;
         favProject.User__c =  UserInfo.getuserId();
         insert favProject;
             
             
         //create class object
         Torque_DialyViewHandler DailyView = new Torque_DialyViewHandler();
         
         //call save favorite project method
         DailyView.favoriteProjectId = favProject.id;
         DailyView.wrapperFavoriteList[0].favHours = 10;
         DailyView.wrapperFavoriteList[0].favNotes = 'Testing';
         DailyView.saveFavoriteProject();
         
         //call method to create favorite project
         DailyView.recordId = timeEntryRecord.id;
         DailyView.wrapperTimeEntryList[0].check = true;
         DailyView.createDeleteFavoriteRecord();
         
         
         //assert to check favorite record is to be created when checkbox is checked in section 3
         List<Favorite_Project__c> favoriteProject = [select id from Favorite_Project__c where User__c =: UserInfo.getUserId() and Project2__c =: projectRecord.id];
         system.assertEquals(favoriteProject.size(),2);
    
                
   
         //call method to delete favorite project creted corresponding to time entry 
         DailyView.recordId = timeEntryRecord.id;
         DailyView.wrapperTimeEntryList[0].check = false;
         DailyView.createDeleteFavoriteRecord();
         
    test.startTest();  
         //assert to check corresponding favorite record is to be deleted when checkbox is unchecked in section 3
         List<Favorite_Project__c> favoriteProject1 = [select id from Favorite_Project__c where User__c =: UserInfo.getUserId() and Project2__c =: projectRecord.id];
         system.assertEquals(favoriteProject1.size(),1);
         
         //call method to delete favorite project on uncheck of checkbox
         DailyView.recordId = favProject.id;
         DailyView.wrapperFavoriteList[0].check = false;
         DailyView.deleteFavoriteRecord();
         
         
         //call method to fetch related budgets when project is selected
         DailyView.timeEntryobject.project__c = projectRecord.id;
         DailyView.relatedBudgetOfSelectedProject();
         
         //call method to insert non favorite time entry
         DailyView.timeEntryObject.Budget_Item__c = budgetItem.id;
         DailyView.timeEntryObject.Billing_Reason__c = billingReason.id;
         DailyView.timeEntryObject.date__c = system.today();
         DailyView.timeEntryObject.Notes__c = 'Testing';
         DailyView.insertNonFavoriteTimeEntry();
         
         DailyView.timeEntryObject.Time__c = 10;
         DailyView.insertNonFavoriteTimeEntry();
         
         //call method to set date as next and previous
         DailyView.previousDate();
         DailyView.nextDate();
         DailyView.showTimeEntries();
         
         //call method to show time entries of today when today button is clicked
         DailyView.todayClicked = 'Today';
         DailyView.timeEntriesAccrdingtoDate();
         
         //call method to edit time entry
         DailyView.editTimeEntry();
         
         //call method to save time entry
         DailyView.timeEntryId = timeEntryRecord.id;
         DailyView.saveTimeEntry();
         
         //call method to delete time entry
         DailyView.timeEntryId = timeEntryRecord.id;
         DailyView.deleteTimeEntry();
         DailyView.defaultBillingReason();
        
        
 test.stopTest();
        
    }
}