@isTest
private class UCase_Test {
    
    private static ProjectDateSettings__c settings;
    
    
    private static testMethod void test_onBeforeInsert_Update() {
        
        List<Account > lstAccount = new List<Account>();
        List<Contact > lstContact = new List<Contact>();
        List<Project__c> lstProject = new List<Project__c>();
        List<Case > lstCase = new List<Case>();
        CaseTeamMemberSettings__c ctmSetting;
        
         //Test profile record
        String strProfile = UTestData.getStandardProfileId('Standard User'); 
        
        //Test user record
        User objUser = UTestData.getUserRecord(strProfile);
        insert objUser;
        
        Group g1 = new Group(Name='Trekbin_Queue', type='Queue');
        insert g1;
        QueuesObject q1 = new QueueSObject(QueueID = g1.id, SobjectType = 'Case');
        insert q1;
        
        System.runAs(objUser) { 
            
            settings = UTestData.getProjectDateSettings(1)[0];
            insert settings;
            system.debug('--settings--'+settings);
            
            ctmSetting = UTestData.getCaseTeamMemberSettings();
            insert ctmSetting;
            
            Account objAccount = new Account(Name = 'Trekbin', Practice__c = 'Other');
            lstAccount.add(objAccount);
            
            Account objAccount1 = new Account(Name = 'Trekbin_Test', Practice__c = 'Other');
            lstAccount.add(objAccount1);
            
            insert lstAccount;
            
            Contact objContact = new Contact(LastName='testContact', AccountId = lstAccount[0].Id, CurrencyIsoCode = 'USD');
            
            lstContact.add(objContact);
            
            Project__c objProject = new Project__c();
            objProject.Account__c = lstAccount[1].Id;
            objProject.name = 'Test';
            objProject.Hourly_rate__c = 222;
            objProject.Kickoff__c = Date.today(); 
            objProject.Go_Live_Date__c =  Date.today().addDays(30);
            objProject.No_Budget__c = true;
            objProject.CashBoardProjectId__c = '123';
            
            lstProject.add(objProject);
            
            insert lstProject;
            
            Project_Phase__c phase = new Project_Phase__c();
            phase.Phase_name__c = 'Build/Config/Testing';
            phase.Start_Date__c = Date.today();
            phase.End_Date__c = Date.today().AddDays(settings.NonImplementationEndDateIncrement__c.intValue());
            phase.Project__c = lstProject[0].id;
            insert phase;
            
            // Updated by Trekbin, as per case requirement :  00027345  
            Login__c objLogin = new Login__c( Name = 'Test login',
                                              Account__c = lstAccount[0].Id,
                                              User_Name__c = 'Test username',
                                              Password__c = 'Test password',
                                              SF_Org_ID__c = '12345');
            insert objLogin;
        
            Case objCase = new Case(AccountId = lstAccount[1].Id, Project__c = lstProject[0].Id, Status= 'New', Development_Required__c = 'Yes', Subject = 'Testing', Requirements__c= 'TestingNew');
            objCase.SF_Login_Record__c = objLogin.Id;
            insert objCase;
            
            
            
            List<Case> lstCaseQuery = new List<Case>([  SELECT Id, OwnerId 
                                                        FROM Case
                                                        WHERE Id = : objCase.Id
                                                    ]);
            
            objCase = new Case(Id = lstCaseQuery[0].Id, OwnerId = g1.Id);
            update objCase;
        }
    }
}