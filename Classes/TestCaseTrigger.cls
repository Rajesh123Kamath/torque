/**
    @Name           :   TestCaseTrigger
    @Created Date   :   11 May, 2016 by Trekbin
    @Description    :   Test code to test UCase.cls
*/
@isTest
private class TestCaseTrigger {
    
    static testMethod void testClosedCaseAfterInsertion1() 
    {
        //Custom setting
        ProjectDateSettings__c objProjectDateSettings = UTestData.getProjectDateSettings(1)[0];          
        insert objProjectDateSettings;
        
        CaseTeamMemberSettings__c ctms = UTestData.getCaseTeamMemberSettings();
        insert ctms;
        
        Account objAccount = UTestData.getAccounts(1)[0];
        objAccount.Practice__c = 'New England';
        insert objAccount;
        
        Project__c objProject = UTestData.getProjects(1)[0];
        objProject.Hourly_Rate__c = 10;
        objProject.Account__c = objAccount.Id;
        insert objProject;
        
        // Updated by Trekbin, as per case requirement :  00027345  
        Login__c objLogin = new Login__c( Name = 'Test login',
                                          Account__c = objAccount.Id,
                                          User_Name__c = 'Test username',
                                          Password__c = 'Test password',
                                          SF_Org_ID__c = '12345',
                                          Parse_Apex_Errors_is_Active__c = false);
        insert objLogin;
        
        List<Case> lstCases = UTestData.getCasesForGivenProjectAndAccount(objAccount.Id, objProject.Id, 10);
        
        for(Case objCase : lstCases ) {
            objCase.SF_Login_Record__c = objLogin.Id;
        }
        insert lstCases;
        
        Case objCase = UTestData.getCasesForGivenProjectAndAccount(objAccount.Id, objProject.Id, 1)[0];
        objCase.Status = 'Closed - Duplicate';
        objCase.SF_Login_Record__c = objLogin.Id;
        insert objCase;

        System.assertEquals([Select Id,Open_Cases__c from Project__c where Id = :objProject.Id].Open_Cases__c, 10);

        Case objCase1 = UTestData.getCasesForGivenProjectAndAccount(objAccount.Id, objProject.Id, 1)[0];
        objCase1.SF_Login_Record__c = objLogin.Id;
        insert objCase1;

        System.assertEquals([Select Id,Open_Cases__c from Project__c where Id = :objProject.Id].Open_Cases__c, 11);
    }

    static testMethod void testClosedCaseAfterInsertion2() 
    {                
        User objUser = UTestData.getUserRecord(String.valueOf(UTestData.getStandardProfileId('System Administrator')));
    	insert objUser;
        
        ProjectDateSettings__c objProjectDateSettings = UTestData.getProjectDateSettings(1)[0];          
    	insert objProjectDateSettings;
	        
        CaseTeamMemberSettings__c ctms = UTestData.getCaseTeamMemberSettings();
        insert ctms;
         
    	Account objAccount = UTestData.getAccounts(1)[0];
        objAccount.Practice__c = 'New England';
        insert objAccount;
        
    	// Updated by Trekbin, as per case requirement :  00027345  
    	Login__c objLogin = new Login__c( Name = 'Test login',
                                      Account__c = objAccount.Id,
                                      User_Name__c = 'Test username',
                                      Password__c = 'Test password',
                                      SF_Org_ID__c = '12345',
                                      Parse_Apex_Errors_is_Active__c = false);
    	insert objLogin;
    	
    	Project__c objProject = UTestData.getProjects(1)[0];
        objProject.Hourly_Rate__c = 10;
        objProject.Account__c = objAccount.Id;
        insert objProject;
        
        Case objCase = UTestData.getCasesForGivenProjectAndAccount(objAccount.Id, objProject.Id, 1)[0];
        objCase.Status = 'Closed - Duplicate';
        objCase.SF_Login_Record__c = objLogin.Id;
        insert objCase;

        System.assertEquals([Select Id,Open_Cases__c from Project__c where Id = :objProject.Id].Open_Cases__c, 0);

        Case objCase1 = UTestData.getCasesForGivenProjectAndAccount(objAccount.Id, objProject.Id, 1)[0];
        objCase1.SF_Login_Record__c = objLogin.Id;
        insert objCase1;

        System.assertEquals([Select Id,Open_Cases__c from Project__c where Id = :objProject.Id].Open_Cases__c, 1);
    }
    
    static testMethod void testClosedCaseAfterUpdate1() 
    {
        ProjectDateSettings__c objProjectDateSettings = UTestData.getProjectDateSettings(1)[0];          
        insert objProjectDateSettings;
        
        CaseTeamMemberSettings__c ctms = UTestData.getCaseTeamMemberSettings();
        insert ctms;
        
        Account objAccount = UTestData.getAccounts(1)[0];
        objAccount.Practice__c = 'New England';
        insert objAccount;
        
        Project__c objProject = UTestData.getProjects(1)[0];
        objProject.Hourly_Rate__c = 10;
        objProject.Account__c = objAccount.Id;
        insert objProject;

        // Updated by Trekbin, as per case requirement :  00027345  
        Login__c objLogin = new Login__c( Name = 'Test login',
                                          Account__c = objAccount.Id,
                                          User_Name__c = 'Test username',
                                          Password__c = 'Test password',
                                          SF_Org_ID__c = '12345',
                                          Parse_Apex_Errors_is_Active__c = false);
        insert objLogin;
        
        
        List<Case> lstCases = UTestData.getCasesForGivenProjectAndAccount(objAccount.Id, objProject.Id, 10);
        for(Case objCase : lstCases ) {
            objCase.SF_Login_Record__c = objLogin.Id;
        }
        insert lstCases;

        Case objCase = UTestData.getCasesForGivenProjectAndAccount(objAccount.Id, objProject.Id, 1)[0];
        objCase.SF_Login_Record__c = objLogin.Id;
        insert objCase;

        System.assertEquals([Select Id,Open_Cases__c from Project__c where Id = :objProject.Id].Open_Cases__c, 11);

        Project__c objProject1 = UTestData.getProjects(1)[0];
        objProject1.Hourly_Rate__c = 10;
        objProject1.Account__c = objAccount.Id;
        insert objProject1;

        objCase.Project__c = objProject1.Id;
        update objCase;

        System.assertEquals([Select Id,Open_Cases__c from Project__c where Id = :objProject.Id].Open_Cases__c, 10);
        System.assertEquals([Select Id,Open_Cases__c from Project__c where Id = :objProject1.Id].Open_Cases__c, 1);

        objCase.Status = 'Closed - Duplicate';
        update objCase;

        System.assertEquals([Select Id,Open_Cases__c from Project__c where Id = :objProject1.Id].Open_Cases__c, 0);

    }

    static testMethod void testClosedCaseAfterUpdate2() 
    {
        //Custom setting
        ProjectDateSettings__c objProjectDateSettings = UTestData.getProjectDateSettings(1)[0];          
        insert objProjectDateSettings;

        CaseTeamMemberSettings__c ctms = UTestData.getCaseTeamMemberSettings();
        insert ctms;
        
        Account objAccount = UTestData.getAccounts(1)[0];
        objAccount.Practice__c = 'New England';
        insert objAccount;
        
        Project__c objProject = UTestData.getProjects(1)[0];
        objProject.Hourly_Rate__c = 10;
        objProject.Account__c = objAccount.Id;
        insert objProject;
        
        // Updated by Trekbin, as per case requirement :  00027345  
        Login__c objLogin = new Login__c( Name = 'Test login',
                                          Account__c = objAccount.Id,
                                          User_Name__c = 'Test username',
                                          Password__c = 'Test password',
                                          SF_Org_ID__c = '12345',
                                          Parse_Apex_Errors_is_Active__c = false);
        insert objLogin;
        
        Case objCase = UTestData.getCasesForGivenProjectAndAccount(objAccount.Id, objProject.Id, 1)[0];
        objCase.SF_Login_Record__c = objLogin.Id;
        insert objCase;

        System.assertEquals([Select Id,Open_Cases__c from Project__c where Id = :objProject.Id].Open_Cases__c, 1);

        objCase.Status = 'Closed - Duplicate';
        update objCase;

        System.assertEquals([Select Id,Open_Cases__c from Project__c where Id = :objProject.Id].Open_Cases__c, 0);
    }

    /*Added by Trekbin on 10 June, 2016 for the Case 00026102*/
   static testMethod void testForPopulationOfInventoryOnRelatedDeployedItems() 
    {
        /** Set up test data - START **/
        List<Deployed_Items__c> lstDeployedItems = new List<Deployed_Items__c>();
        
        //Custom setting
        ProjectDateSettings__c objProjectDateSettings = UTestData.getProjectDateSettings(1)[0];          
        insert objProjectDateSettings;

        CaseTeamMemberSettings__c ctms = UTestData.getCaseTeamMemberSettings();
        insert ctms;
        
        Account objAccount = UTestData.getAccounts(1)[0];
        objAccount.Practice__c = 'New England';
        insert objAccount;
        
        Project__c objProject = UTestData.getProjects(1)[0];
        objProject.Hourly_Rate__c = 10;
        objProject.Account__c = objAccount.Id;
        insert objProject;
        
        // Updated by Trekbin, as per case requirement :  00027345  
        Login__c objLogin = new Login__c( Name = 'Test login',
                                          Account__c = objAccount.Id,
                                          User_Name__c = 'Test username',
                                          Password__c = 'Test password',
                                          SF_Org_ID__c = '12345',
                                          Parse_Apex_Errors_is_Active__c = false);
        insert objLogin;
        
        Case objCase = UTestData.getCasesForGivenProjectAndAccount(objAccount.Id, objProject.Id, 1)[0];
        objCase.SF_Login_Record__c = objLogin.Id;
        insert objCase;
        
        List<Inventory__c> lstInventory = new List<Inventory__c>{   new Inventory__c(Account__c = objAccount.Id, Type__c = 'Approval Process'),
                                                                    new Inventory__c(Account__c = objAccount.Id, Type__c = 'Approval Process')
                                                                };
        insert lstInventory;
        
        for(Integer i=0; i< 3; i++)
        {
            lstDeployedItems.add(new Deployed_Items__c(Name = 'Test DI 1' + i, Type__c = 'Code', Related_Case__c = objCase.Id));
        }
        
        insert lstDeployedItems;
        
        /** Set up test data - END **/
        
        // update case by populating inventory
        objCase.Inventory__c = lstInventory[0].Id;
        update objCase;
        
        for(Deployed_Items__c objDI : [SELECT Inventory__c FROM Deployed_Items__c])
        {
            // verify if inventory id is populated on related delpoyed item.
            system.assertEquals(lstInventory[0].Id, objDI.Inventory__c);
        }
        
        
        // update case by populating some other inventory
        objCase.Inventory__c = lstInventory[1].Id;
        update objCase;
        
        // verify if new inventory id is populated on related delpoyed item.
        system.assertEquals(lstInventory[1].Id, [SELECT Inventory__c FROM Deployed_Items__c][0].Inventory__c);
         
        // update case by blank inventory id
        objCase.Inventory__c = null;
        update objCase;
        
        // verify if related delpoyed item also populated with blank inventory.
        system.assertEquals(null, [SELECT Inventory__c FROM Deployed_Items__c][0].Inventory__c);
        
    }
    
    /* Created By Trekbin on 28-July, 2016 as per the case: 00026102 [R] */
   static testMethod void testApplyCaseFollowUnfollowRules() 
    {
        
        CaseTeamMemberSettings__c ctms = UTestData.getCaseTeamMemberSettings();
        insert ctms;
        
        Account objAccount = UTestData.getAccounts(1)[0];
        objAccount.Name = 'Trekbin';
        objAccount.Practice__c = 'New England';
        insert objAccount;
        
        // Updated by Trekbin, as per case requirement :  00027345  
        Login__c objLogin = new Login__c( Name = 'Test login',
                                          Account__c = objAccount.Id,
                                          User_Name__c = 'Test username',
                                          Password__c = 'Test password',
                                          SF_Org_ID__c = '12345',
                                          Parse_Apex_Errors_is_Active__c = false);
        insert objLogin;
        
        List<Case> lstCases = UTestData.getCases(new List<Account>{objAccount});
        lstCases[0].Status = 'Closed - Duplicate';
        lstCases[0].SF_Login_Record__c = objLogin.Id;
        insert lstCases;
        
        //Call a static method because this method doesnot get invoked using trigger
        UCase.ApplyCaseFollowUnfollowRules(lstCases);
        
    }
    
    static testMethod void testcheckOldNewValues() 
    {   
        
        String strProfile = UTestData.getStandardProfileId('System Administrator'); 
        
        User usr = [Select id from User where Id = :UserInfo.getUserId()];
        userRole userRole;
        User thisUser;
        
        System.RunAs(usr) {
	        test.startTest();
	        
	        userRole = new userRole(Name = 'Developers' );
        	insert userRole ;
        	
        	thisUser = UTestData.getUserRecord(strProfile);
	        thisUser.UserRoleId = userRole.Id;
	        insert thisUser;
        
	        test.stopTest();
        }
        
        System.runAs (thisUser) {
            
            Id profileId = UTestData.getProfileIdByName('Customer Community Plus Login User');
            
            Account objAccount = UTestData.getAccounts(1)[0];
            objAccount.Name = 'Trekbin';
            objAccount.Practice__c = 'New England';
            insert objAccount;
            
            Contact objContact = UTestData.getContacts(new List<Account>{objAccount})[0];
            insert objContact;
            
            List<User> lstUsers = UTestData.getCustomerPortalUsers(new List<Contact>{objContact}, profileId);
            insert lstUsers;
            
            userRole uRole = new userRole(Name = 'TestAccount-01 Executive' );insert uRole ;
            
            CaseTeamMemberSettings__c ctms = UTestData.getCaseTeamMemberSettings();
            insert ctms;
            
            // Updated by Trekbin, as per case requirement :  00027345  
            Login__c objLogin = new Login__c( Name = 'Test login',
                                              Account__c = objAccount.Id,
                                              User_Name__c = 'Test username',
                                              Password__c = 'Test password',
	                                          SF_Org_ID__c = '12345',
	                                          Parse_Apex_Errors_is_Active__c = false);
            insert objLogin;
            
            Case objCase = UTestData.getCases(new List<Account>{objAccount})[0];
            objCase.SF_Login_Record__c = objLogin.Id;
            insert objCase;

            objCase.OwnerId = [SELECT Id FROM Group WHERE DeveloperName = 'Trekbin_Queue'].Id;
            update objCase;
        }
    }
}