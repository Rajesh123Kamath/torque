/**
    Class Name : Torque_CreateInvoiceForProjectHandler
    Created By : Trekbin
    Created On : 22-Jan-2016
    Description: Webservice class to create a "Invoice" record on click of a project detail page button "Create Invoice".
 */
global class Torque_CreateInvoiceForProjectHandler
{   
    /* Project Record Variable */
    public static String paymentTerms;
            
    /* Webservice method */
    webservice static String generateInvoiceRecords(Id projId, Id accountId) 
    {     
        paymentTerms = '';      
        paymentTerms = String.valueOf([ Select Id, Account__c, Account__r.Payment_Terms__c FROM Project__c where Id = :projId limit 1].Account__r.Payment_Terms__c); 
        return generateInvoices(projId, accountId);          
             
    }
     
    /* Generate Invoice method */
    public static String generateInvoices(Id projId, Id accountId)
    {
        //Invoice record instance    
        Invoice__c objinvoice;
        
        //URL to be redirected
        String stringURL;        
       
        //Map variables
        Map<Id,Invoice__c> mapOfProjectIdToInvoice = new Map<Id,Invoice__c>();
        Map<Id,Time_Entry__c> mapOfIdToTimeEntry = new Map<Id,Time_Entry__c>(); 
        Map<Id,Invoice_Line__c> mapTimeEntryToInvoices = new Map<Id,Invoice_Line__c>();
        Map<Id,Expense__c> mapOfIdToExpense = new map<Id,Expense__c>();
        Map<Id,Invoice_Line__c> mapExpenseToInvoices = new Map<Id,Invoice_Line__c>();
        
        //Initialize the invoice record
        objinvoice = new Invoice__c( Invoice_Date__c = Date.Today(), Account__c = accountId); 
        
        //Condition to populate the invoice due date values 
        if(paymentTerms != null && paymentTerms != '')
        {
           system.debug('----paymentTerms--If-' +paymentTerms);
           objinvoice.Due_Date__c = Date.Today() + Integer.valueOf(paymentTerms);
        }
        else 
        {
            system.debug('----paymentTerms--else-' +paymentTerms);
            objinvoice.Due_Date__c = Date.Today();
        }
        
        
        //Create an invoice record on the account for that project
        if(objinvoice != NULL)
            insert objinvoice;
        
        //Create a map of projectId with Invoice
        if(projId != null)
        {
            mapOfProjectIdToInvoice.put(projId, objinvoice);
        }
               
        //Fetch the Time entries for each project
        for(Time_Entry__c objTimeEntry : [  SELECT Id,Project__c,Project__r.Account__c,Date__c,Performed_By__c,Time__c,Billable__c,Notes__c,Case_Number__c,Deliverable__c,Budget_Item__c,Budget_Item__r.Hourly_Rate__c 
                                            FROM Time_Entry__c 
                                            WHERE Project__c = : projId                                            
                                            AND ((Billable__c = TRUE AND Unbilled__c = FALSE) OR Display_on_Invoice__c = TRUE)
                                            AND Invoice_Line__c = NULL
                                            AND Migrated_from_CB__c = FALSE
                                         ]) 
                                        
        {
            //Create a map of timeentry Id to timeentry                                    
            mapOfIdToTimeEntry.put(objTimeEntry.Id, objTimeEntry);
            
            //Create an invoice line record to the timeentry relatd to that project where invoice needed to be generated    
            Invoice_Line__c objInvoiceLine = new Invoice_Line__c(Invoice__c = mapOfProjectIdToInvoice.get(objTimeEntry.Project__c).Id,
                                                                 Time_Entry__c = objTimeEntry.Id,
                                                                 Date_Delivered__c = objTimeEntry.Date__c,
                                                                 Invoice_Line_Type__c = 'Time Entry',
                                                                 Performed_By__c = objTimeEntry.Performed_By__c,
                                                                 Price_Per__c = objTimeEntry.Budget_Item__r.Hourly_Rate__c, 
                                                                 Quantity__c = objTimeEntry.Time__c,
                                                                 Billable__c = objTimeEntry.Billable__c,
                                                                 Invoice_Line_Notes__c = objTimeEntry.Notes__c,
                                                                 Case_Number__c = objTimeEntry.Case_Number__c,
                                                                 Deliverable__c = objTimeEntry.Deliverable__c,
                                                                 Budget_Item__c = objTimeEntry.Budget_Item__c
                                                                );
            
            //Condition to populate the invoiceline total price values    
            if(objTimeEntry.Budget_Item__r.Hourly_Rate__c !=null) {
                    objInvoiceLine.Total_Price__c = (objTimeEntry.Budget_Item__r.Hourly_Rate__c * objTimeEntry.Time__c);
            }
                
            //Create a map of timeentry to invoiceLine
            mapTimeEntryToInvoices.put(objTimeEntry.Id, objInvoiceLine);
                
        }
            
        //Insert InvoiceLines for Time entry
        if(!mapTimeEntryToInvoices.isEmpty()) 
            insert mapTimeEntryToInvoices.values(); 
            
                
        //Finding the Time_Entry record where Time_Entry.Id = Invoice_Line.Time_Entry__c and mapping Time_Entry.Invoice_Line__c = Invoice_Line.Id                   
        for(Invoice_Line__c objInvoiceLine : mapTimeEntryToInvoices.values()) {
            
            if(objInvoiceLine.Time_Entry__c != NULL && objInvoiceLine.Invoice_Line_Type__c == 'Time Entry') {
                
                if(mapTimeEntryToInvoices.containskey(objInvoiceLine.Time_Entry__c)) {
                    
                    mapOfIdToTimeEntry.get(objInvoiceLine.Time_Entry__c).Invoice_Line__c = mapTimeEntryToInvoices.get(objInvoiceLine.Time_Entry__c).Id;
                }
            }   
        }
            
        //Updating TimeEntry
        if(!mapOfIdToTimeEntry.isEmpty())
            update mapOfIdToTimeEntry.values();
            
        //Fetch the Expenses related to Project
        for(Expense__c objExpense : [   SELECT Id,Project__c,Project__r.Account__c,Invoice_Comment__c,Date__c,Amount__c,Expense_Report__r.OwnerId
                                                FROM Expense__c
                                                WHERE Project__c = : projId
                                                AND Billable__c = TRUE
                                                AND Approved__c = 'TRUE' 
                                                AND Invoice_Line__c = NULL 
                                    ]) 
        {
            //Create a map of expenseId to Expense                                    
            mapOfIdToExpense.put(objExpense.Id, objExpense);

            //Create an InvoiceLine record to the expense
            Invoice_Line__c objInvoiceLine = new Invoice_Line__c(Invoice__c = mapOfProjectIdToInvoice.get(objExpense.Project__c).Id,
                                                                 Gears_Expense__c = objExpense.Id,
                                                                 Date_Delivered__c = objExpense.Date__c,
                                                                 Invoice_Line_Type__c = 'Expense',
                                                                 Performed_By__c = objExpense.Expense_Report__r.OwnerId,
                                                                 Price_Per__c = objExpense.Amount__c,
                                                                 Quantity__c = 1,
                                                                 Invoice_Line_Notes__c = objExpense.Invoice_Comment__c
                                                                );
                                                                           
                //Condition to populate the invoiceline total price values                                                       
                if(objExpense.Amount__c != null) {
                    objInvoiceLine.Total_Price__c = (objInvoiceLine.Price_Per__c * objInvoiceLine.Quantity__c);
            }
            
            //Create a map of expenseId to InvoiceLine 
            mapExpenseToInvoices.put(objExpense.Id,objInvoiceLine); 
        }
            
           
        //Insert Invoice Lines related to Project
        if(!mapExpenseToInvoices.isEmpty())
            insert mapExpenseToInvoices.values();
        
        //Finding the Expense__c record where Expense__c.Id = Invoice_Line.Expense__c and setting Expense__c.Invoice_Line__c = Invoice_Line.Id
        for(Invoice_Line__c objInvoiceLine : mapExpenseToInvoices.values()) {
            
            if(objInvoiceLine.Gears_Expense__c != NULL && objInvoiceLine.Invoice_Line_Type__c == 'Expense'){
                
                if(mapExpenseToInvoices.containskey(objInvoiceLine.Gears_Expense__c)){
                    
                    mapOfIdToExpense.get(objInvoiceLine.Gears_Expense__c).Invoice_Line__c = mapExpenseToInvoices.get(objInvoiceLine.Gears_Expense__c).Id;
                }
            }   
        }
            
        //Update Expense record
        if(!mapOfIdToExpense.isEmpty())
            update mapOfIdToExpense.values();    
        
                
        if(objinvoice.id != null)
        {
            stringURL = '/'+objinvoice.id;       
            return stringURL; 
        }    
        return null;     
    }   
}