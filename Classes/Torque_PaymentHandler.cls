/**
@ClassName    : Torque_PaymentHandler 
@CreatedOn    : 
@CreatedBy    : Trekbin
@ModifiedBy   : 
@Last modified: Trekbin on 30-March-2016 as per the case: 00024704
@Description  : This class is used to sum the Amount of all payments related to Account and populate on Account Total_Payments__c field
**/
public with sharing class Torque_PaymentHandler {
    
    
    /**
        @MethodName : onAfterInsert
        @Param      : lstPayment
        @Description: This method will invoke on creating Payment
    **/
    public void onAfterInsert(List<Payment__c> lstPayment, Map<Id, Payment__c> oldPaymentRecordsMap) {
        
        updateAccountPaymentTotal(lstPayment, oldPaymentRecordsMap);
        
        /* Last modified by: Trekbin on 15-feb-2016 as per the case: 00025265*/
        manualSharePaymentRecords(lstPayment, oldPaymentRecordsMap); 
    }
    
    
    /**
        @MethodName : onAfterUpdate
        @Param      : lstPayment
        @Description: This method will invoke on updating Payment
    **/
    public void onAfterUpdate(List<Payment__c> lstPayment, Map<Id, Payment__c> oldPaymentRecordsMap) {
        
        updateAccountPaymentTotal(lstPayment, oldPaymentRecordsMap);
        
        /* Last modified by: Trekbin on 15-feb-2016 as per the case: 00025265*/
        manualSharePaymentRecords(lstPayment, oldPaymentRecordsMap);
    }
    
    
    /**
        @MethodName : onAfterDelete
        @Param      : lstPayment
        @Description: This method will invoke on deleting Payment
    **/
    public void onAfterDelete(List<Payment__c> lstPayment) {
        
        updateAccountPaymentTotal(lstPayment, null);
    }
    
    /**
        @MethodName : onBeforeDelete
        @Param      : lstPayment
        @Description: This method will invoke on deleting Payment to get the credit line Ids
    **/
    public void onBeforeDelete(Map<Id, Payment__c> oldPaymentRecordsMap) {
        
        getCreditlineIds(oldPaymentRecordsMap);
    }
    
    
    /**
        @MethodName : updateAccountPaymentTotal
        @Param      : lstPayment
        @Description: This Method used to sum up payment total on Account when Payment is Inserted/Updated/Deleted
    **/
    private void updateAccountPaymentTotal(List<Payment__c> lstPayment,  Map<Id, Payment__c> oldPaymentRecordsMap) {
        
        List<Account> lstAccountToUpdate = new List<Account>();
        
        List<Payment__c> lstOfpayment = new List<Payment__c>();
        
        List<Credit_Line__c> lstOfCreditline= new List<Credit_Line__c>();
        
        Set<Id> setAccountIds = new Set<Id>();
        
        /* Last modified by: Trekbin on 2-march-2016 */
        Set<Id> setRecordIDs = new Set<Id>();
        
        Map<Id,decimal> mapOfIdToDecimal = new Map<Id,decimal>();
        
        Map<Id,Payment__c> mapOfAccountIdToPayment = new Map<Id,Payment__c>();
        
        /* Last modified by: Trekbin on 15-feb-2016 as per the case: 00025256*/
        for(Cash_Payment_Record_Type__mdt objCashPymtRecordType : [Select id__c, DeveloperName from Cash_Payment_Record_Type__mdt])
        {
        	if(objCashPymtRecordType.id__c != null && objCashPymtRecordType.id__c != '')	
        	{
        		setRecordIDs.add(objCashPymtRecordType.id__c);
        	} 
        }
        
        /* Last modified by: Trekbin on 15-feb-2016 as per the case: 00025256*/
        //iterate over payments tofetch account id
        for(Payment__c objpayment : lstPayment) {
            
            //Proceed further if the Account__c has value
            if(objpayment.Account__c != null)
            {
            	//To check for insert or update of payment.amount__c
            	if(oldPaymentRecordsMap == null || (oldPaymentRecordsMap != null && objpayment.Amount__c != oldPaymentRecordsMap.get(objpayment.Id).Amount__c))
            		setAccountIds.add(objpayment.Account__c);
            		
	                mapOfIdToDecimal.put(objPayment.Account__c,0.0);
            }
                
        }
        
        /* Last modified by: Trekbin on 15-feb-2016 as per the case: 00025256*/
        //If the set contains any value then only proceed further
        if(!setAccountIds.isEmpty() && (!setRecordIDs.isEmpty())) 
        {
	        //iterate over payments to sum the amount whose recordTypeId matches with the custom metadata Id__c
	        for(Payment__c objPayment : [   SELECT Id,Name, Amount__c, Account__c 
	                                        FROM Payment__c 
	                                        WHERE Account__c IN :setAccountIds AND RecordTypeId IN :setRecordIDs]) {
	               
	             if(objPayment.Amount__c == null)
	                    objPayment.Amount__c = 0;

	             if(mapOfIdToDecimal.containskey(objPayment.Account__c)) 
	                mapOfIdToDecimal.put(objPayment.Account__c, mapOfIdToDecimal.get(objPayment.Account__c)+objPayment.Amount__c);
	             else
	                mapOfIdToDecimal.put(objPayment.Account__c, objPayment.Amount__c);                                             
	            
	        }        
	        
	        //iterating over map to update the Account field with sum in payments
	        for(Id idAccount : mapOfIdToDecimal.KeySet()) {
	            
	            lstAccountToUpdate.add(new Account(Id = idAccount, Total_Payments__c =  mapOfIdToDecimal.get(idAccount)));
	        } 
	        if(!lstAccountToUpdate.isEmpty())
	            update lstAccountToUpdate;
        }  
    }
    
     /**
        @MethodName : getCreditlineIds
        @Param      : lstPayment
        @Description: This Method used to get the credit line Id's related to Payment on delete
    **/
    public void getCreditlineIds(Map<Id, Payment__c> oldPaymentRecordsMap) {
        
        List<Credit_Line__c> lstOfCreditline= new List<Credit_Line__c>();
        
        for(Credit_Line__c objcreditline : [ SELECT Id,Payment__c,Invoice__c FROM Credit_Line__c WHERE Payment__c IN : oldPaymentRecordsMap.keyset() ]) {
            
            lstOfCreditline.add(objcreditline);
        }
        
        //calling the Torque_CreditLineHandler for updating the To Total_Payments_Applied__c
        Torque_CreditLineHandler objhandler = new Torque_CreditLineHandler();
        objhandler.updateInvoiceTotal(lstOfCreditline,true);
    }
    
    
    public static void manualSharePaymentRecords(List<Payment__c> lstPayment, Map<id,Payment__c> oldPaymentRecordsMap) {
        
        //set to hold account Ids
        set<id> accountIds = new set<id>();
        
        //set to hold old account ids
        set<id> oldAccountIds = new set<id>();
        
        //set to hold old invoice ids
        set<id> oldPaymentIds = new set<id>();
        
        /* Last modified by: Trekbin on 2-march-2016 */
        Set<Id> setRecordIDs = new Set<Id>();
        
        /* Last modified by: Trekbin on 15-feb-2016 as per the case: 00025265*/
        for(Cash_Payment_Record_Type__mdt objCashPymtRecordType : [Select id__c, DeveloperName from Cash_Payment_Record_Type__mdt])
        {
        	if(objCashPymtRecordType.id__c != null && objCashPymtRecordType.id__c != ''  )	
        	{
        		setRecordIDs.add(objCashPymtRecordType.id__c);
        	}
        }
        
        //Iterate over invoice records
        for(Payment__c paymentRecord : lstPayment)  {
        
            //check if invoice status is sent and account <> blank
            if(paymentRecord.Account__c != null){
            
                /* Last modified by: Trekbin on 15-feb-2016 as per the case: 00025265*/
                //check if trigger envent is insert
                if(oldPaymentRecordsMap == null && (!setRecordIDs.isEmpty()) && (setRecordIDs.contains(paymentRecord.RecordTypeId))){
                	                    
                    //add account id to set
                    accountIds.add(paymentRecord.account__c);
                }
                
                /* Last modified by: Trekbin on 15-feb-2016 as per the case: 00025265*/
                //if trigger event is update means when invoice account is updated & recordtype is same as custom metadata recordtype
                else if(oldPaymentRecordsMap != null){
                
                	/* Last modified by: Trekbin on 19-feb-2016 as per the case: 00025265 as per usecase 3 in the updated requirement*/
                    //check if account is update from previous value, account must be other than before
                    if(paymentRecord.account__c != oldPaymentRecordsMap.get(paymentRecord.id).account__c || (paymentRecord.RecordTypeId != oldPaymentRecordsMap.get(paymentRecord.id).RecordTypeId && (!setRecordIDs.isEmpty()) && (!setRecordIDs.contains(paymentRecord.RecordTypeId)))){
                        	
                        //add old invoice on invoice to old account ids set
                        oldPaymentIds.add(paymentRecord.id);
                        
                        //add old account on invoice to old account ids set
                        oldAccountIds.add(oldPaymentRecordsMap.get(paymentRecord.id).account__c);
                        
                        if((!setRecordIDs.isEmpty()) && (setRecordIDs.contains(paymentRecord.RecordTypeId)))
                        {
	                        //add new updated account ids to accountids set
	                        accountIds.add(paymentRecord.account__c);
                        }
                    }
                }
            }
        }
        
        //if account share record for old account is already create for 'portal and role subordinates' group then delete that
        if(!oldAccountIds.isEmpty()){
            
            //fill set id of old account names
            Set<string> oldAccountNames = new set<string>();
            
            /* Last modified by: Trekbin on 30 March, 2016 as per the case: 00024704*/
            Set<Id> setAccIdsHavingCustomerUsers = new Set<Id>();
            
            for(User objUser : [SELECT id,name, Contact.AccountId from user where Contact.AccountId IN : oldAccountIds AND UserType = 'PowerCustomerSuccess']) {
            	
            	setAccIdsHavingCustomerUsers.add(objUser.Contact.AccountId);
            }

            for(Account accountObj : [select id,name from account where id in : setAccIdsHavingCustomerUsers]){
                oldAccountNames.add(accountObj.name+' Customer Executive');
            }
            
            //fill set of userroles related corresponding account name
            set<id> userRoleIds = new set<id>();
            for(userRole uRole : [select id,name from userRole where Name Like : oldAccountNames]){
                userRoleIds.add(uRole.id);
            }
            
            //fill set of group ids related to the above user roles
            set<id> groupIds = new set<id>();
            for(group grpObj : [select id from group where relatedid in : userRoleIds]){
                groupIds.add(grpObj.id);
            }
            
            List<Payment__Share> paymentShareRecords = [select id from Payment__Share where parentid in : oldPaymentIds and userorgroupid in : groupIds and rowCause = 'Manual'];
            if(!paymentShareRecords.isEmpty()){
                delete paymentShareRecords;
            }
        }
        
        //create account share records for new accounts on invoice on insert and update
        if(!accountIds.isEmpty()){
            
            //initialise set and map used below
            Set<string> accountNames = new set<string>();
            Map<Id, Account> accountMap = new Map<Id,Account>();
            Map<string,id> userRoleMap = new Map<string,id>();
            Map<id,id> uRoleToGroupIdMap = new Map<id,id>();
            Map<id,id> accountToUroleMap = new Map<id,id>();
            List<Payment__share> PaymentShareRecords =  new List<Payment__share>();
            
            /* Last modified by: Trekbin on 30 March, 2016 as per the case: 00024704*/
			Set<Id> setAccIdsHavingCustomerUsers = new Set<Id>();
            
            for(User objUser : [SELECT id,name, Contact.AccountId from user where Contact.AccountId IN : accountIds AND UserType = 'PowerCustomerSuccess' AND IsActive = true ]) {
            	setAccIdsHavingCustomerUsers.add(objUser.Contact.AccountId);
            }
            			
            //query all partner enabled accounts that are inserted or update in inovice
            for(Account accountObj : [select id,name from account where id in : setAccIdsHavingCustomerUsers]){
                
                accountMap.put(accountObj.id,accountObj);
                accountNames.add(accountObj.name+' Customer Executive');
            }
            
            //query userRole corresponding to each account
            for(UserRole uRoleObj : [select id,name from userRole where Name Like : accountNames]){
                userRoleMap.put(uRoleObj.name,uRoleObj.id);
            }
            
            //Based on name fill map of accountid and userrole
            for(Account accountObj : accountMap.values()){
                
                for(String uRoleName : userRoleMap.Keyset()){
                    
                    if(uRoleName.contains(accountObj.name))
                        accountToUroleMap.put(accountObj.id,userRoleMap.get(uRoleName));
                }
            }
            
            //userRole is related to group with relatedId so query all group of all userrole related to account and fill map
            for(Group grp : [select id,relatedId from group where relatedId in : userRoleMap.values() and type = 'RoleAndSubordinates']){
                uRoleToGroupIdMap.put(grp.relatedId,grp.id);
            } 

            //Iterate on invoice records to share them to portal role and subordinates group with read only access
            for(Payment__c PaymentRecord : lstPayment)  {

                if(accountToUroleMap.containsKey(PaymentRecord.account__c) && uRoleToGroupIdMap.containsKey(accountToUroleMap.get(paymentRecord.account__c))){
                    
                    //create invoice share record for the invoice record
                    payment__share paymentShareRecord = new payment__share();
                    paymentShareRecord.ParentId = paymentRecord.id;
                    paymentShareRecord.UserOrGroupId = uRoleToGroupIdMap.get(accountToUroleMap.get(paymentRecord.account__c));
                    paymentShareRecord.AccessLevel = 'Read';
                    paymentShareRecords.add(paymentShareRecord);
                }
            }

            //insert invoice share records
            if(!paymentShareRecords.isEmpty())
                insert paymentShareRecords; 
        }
    }
}