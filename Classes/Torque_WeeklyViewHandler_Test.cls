/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class Torque_WeeklyViewHandler_Test {

    static testMethod void myUnitTest() {
        // TO DO: implement unit test
        
         Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
         
         User objuser = new User(Alias = 'TestUser', Email='trekbintest@test123.com', 
						            EmailEncodingKey='UTF-8', LastName='Testing123', LanguageLocaleKey='en_US', 
						            LocaleSidKey='en_US', ProfileId = p.Id, 
						            TimeZoneSidKey='America/Los_Angeles', UserName='test@testexrecord123.com');
         insert  objuser;
        
		Account objAccount = new Account(Name = 'TestProject');
  		insert objAccount;
      
      //create test setting record used in project trigger
		ProjectDateSettings__c dateSetting = new ProjectDateSettings__c(BuildConfigTestingEndDayIncrement__c = 14,BuildConfigTestingStartDayIncrement__c = 13,
		DesignDiscoveryEndDaysIncrement__c = -2,GoLiveAndPostGoLiveEndDayIncrement__c= 3,NonImplementationEndDateIncrement__c = 12 );
		insert dateSetting; 
       
		Project__c objProject = new Project__c(Name = 'Test Project', Account__c = objAccount.id);
  		insert objProject;
	      
		Deliverable__c objdeliverable = new Deliverable__c(Name = 'Test Deliverable',Project__c = objProject.id);
  		insert objdeliverable;
	      
		Budget_Item__c objbudgetItem = new Budget_Item__c(Deliverable__c = objdeliverable.id,Project__c = objProject.id);
 		insert objbudgetItem;
	      
		Budget_Item__c objbudgetItem2 = new Budget_Item__c(Deliverable__c = objdeliverable.id,Project__c = objProject.id);
  		insert objbudgetItem2;
	      
      
		List<Time_card__c> lsttimeCard = new List<Time_card__c> {new Time_card__c(Name = 'Time card1',Week_Beginning__c = date.today(),User__c = userinfo.getuserID()),
      															new Time_card__c(Name = 'Time card2',Week_Beginning__c = date.today(),User__c = userinfo.getuserID())
      															};
      	insert lsttimeCard;
      
        Billing_Reason_Code__c billingReason = new Billing_Reason_Code__c(Name = 'Test Billing');
         insert billingReason;
      	
      	List<Time_Entry__c > lstTimeEntry = new List<Time_Entry__c > {new Time_Entry__c (Project__c = objProject.Id , Billing_Reason__c = billingReason.id, Budget_Item__c = objbudgetItem.Id, Time_card__c = lsttimeCard[0].Id,time__c = 1,date__c = date.today(), notes__c = 'test123'),
      																new Time_Entry__c (Project__c = objProject.Id ,Billing_Reason__c = billingReason.id, Budget_Item__c = objbudgetItem.Id, Time_card__c = lsttimeCard[0].Id,time__c = 2,date__c = date.today()+1, notes__c = 'test123'),
      																new Time_Entry__c (Project__c = objProject.Id ,Billing_Reason__c = billingReason.id, Budget_Item__c = objbudgetItem.Id, Time_card__c = lsttimeCard[0].Id,time__c = 3,date__c = date.today()+2, notes__c = 'test123'),
      																new Time_Entry__c (Project__c = objProject.Id ,Billing_Reason__c = billingReason.id, Budget_Item__c = objbudgetItem.Id, Time_card__c = lsttimeCard[0].Id,time__c = 4,date__c = date.today()+3, notes__c = 'test123'),
      																new Time_Entry__c (Project__c = objProject.Id ,Billing_Reason__c = billingReason.id, Budget_Item__c = objbudgetItem.Id, Time_card__c = lsttimeCard[0].Id,time__c = 5,date__c = date.today()+4, notes__c = 'test123'),
      																new Time_Entry__c (Project__c = objProject.Id ,Billing_Reason__c = billingReason.id, Budget_Item__c = objbudgetItem.Id, Time_card__c = lsttimeCard[0].Id,time__c = 6,date__c = date.today()+5, notes__c = 'test123'),
      																new Time_Entry__c (Project__c = objProject.Id ,Billing_Reason__c = billingReason.id, Budget_Item__c = objbudgetItem.Id, Time_card__c = lsttimeCard[0].Id,time__c = 7,date__c = date.today()-1, notes__c = 'test123'),
      																new Time_Entry__c (Project__c = objProject.Id ,Billing_Reason__c = billingReason.id, Budget_Item__c = objbudgetItem.Id, Time_card__c = lsttimeCard[0].Id,time__c = 8,date__c = date.today()-2, notes__c = 'test132'),
      																new Time_Entry__c (Project__c = objProject.Id ,Billing_Reason__c = billingReason.id, Budget_Item__c = objbudgetItem.Id, Time_card__c = lsttimeCard[0].Id,time__c = 9,date__c = date.today()-3, notes__c = 'test123'),
      																new Time_Entry__c (Project__c = objProject.Id ,Billing_Reason__c = billingReason.id, Budget_Item__c = objbudgetItem.Id, Time_card__c = lsttimeCard[0].Id,time__c = 10,date__c = date.today()-4, notes__c = 'test132'),
      																new Time_Entry__c (Project__c = objProject.Id ,Billing_Reason__c = billingReason.id, Budget_Item__c = objbudgetItem.Id, Time_card__c = lsttimeCard[0].Id,time__c = 11,date__c = date.today()-5, notes__c = 'test132'),
      																new Time_Entry__c (Project__c = objProject.Id ,Billing_Reason__c = billingReason.id, Budget_Item__c = objbudgetItem.Id, Time_card__c = lsttimeCard[0].Id,time__c = 1,date__c = date.today(), notes__c = 'test132')
      																};
      insert lstTimeEntry;
      
      Date dtWeekStart;
	  Date dtWeekEnd;
	  
	  String prevWeekStart;
	  String prevWeekEnd;
	  
	  Date  mystartdate ;
	  Date myenddate;
	  
		
      date myDate = date.today();
	  dtweekStart = myDate.toStartofWeek();
	  dtWeekEnd = dtWeekStart.addDays(6);
	  
      Torque_WeeklyViewHandler objhandler = new Torque_WeeklyViewHandler();
      objhandler.fetchTimeCardHours(userinfo.getuserID());
      
      objhandler.fetchTimeEntryRecords(userinfo.getuserID(), dtweekStart, dtWeekEnd);
      
      
      dtWeekStart = myDate.toStartofWeek().addDays(-7);
	  dtWeekEnd = dtWeekStart.addDays(6);
	  
	 
	  
	  string startDate = string.valueOfGmt(dtWeekStart);
	  string endDate = string.valueOfGmt(dtWeekEnd);
	  
	  prevWeekStart = ApexPages.currentPage().getParameters().put('startdate', startDate);
      prevWeekEnd = ApexPages.currentPage().getParameters().put('enddate',endDate);
      
      system.debug('!!!!!!!!!!!!!!! prevWeekStart test '+prevWeekStart);
      
     // objhandler.getprevrecords();
      
     // prevWeekStart = ApexPages.currentPage().getParameters().get('startdate');
      //prevWeekEnd = ApexPages.currentPage().getParameters().get('enddate');
      
      //system.debug('!!!!!!!!!!!!!!! prevWeekStart '+prevWeekStart);
      
      //objhandler.fetchTimeEntryRecords(userinfo.getuserID(), dtweekStart, dtWeekEnd);
      
      objhandler.fetchTodaysRecords( );
      objhandler.fetchTimeEntryRecords(userinfo.getuserID(),date.today(), date.today());
      
      ApexPages.currentPage().getParameters().put('startdate', '7/31/2016');
      ApexPages.currentPage().getParameters().put('enddate', '8/6/2016');
      
      objhandler.getPrevRecords();
      objhandler.getNextRecords(); 
      objhandler.checkEnoughWeekHoursExists();
      objhandler.submitTimeCardRecord();
      objhandler.displayUpdatedTimeCardStatus();
      ApexPages.currentPage().getParameters().put('weekDateToPassToController', '2016-8-7');
      objhandler.performRedirectionToDetailViewPageAsPerDate();
    }
}