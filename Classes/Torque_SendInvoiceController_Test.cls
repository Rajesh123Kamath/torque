/**
	@ Handler name : Torque_SendInvoiceController_Test
	@ Created By   : Trekbin
	@ Created On   : 2-Dec-2015
	@ Description  : Test class for Handler 'Torque_SendInvoiceController' & Page 'Torque_SendInvoicePage'
*/

@isTest(SeeAllData = false)
private class Torque_SendInvoiceController_Test {

	/*
		@ Test method : sendInvoiceTest   
	*/
    static testMethod void sendInvoiceTest() {
        
        //Start of test
        Test.startTest();
        	
        	//Try block
        	try
        	{
        		
	        	//Id for the Initial_Invoice_Email_Template__c
	        	String initialInvoiceEmailId = [Select Name, Id From EmailTemplate Where Name = :'New Invoice Notification'].Id;
	        	
	        	//Id for the Reminder_Invoice_Email_Template__c
	        	String reminderInvoiceEmailId = [Select Name, Id From EmailTemplate Where Name = :'Invoice Reminder'].Id; 
	        	
	        	//Custom setting test record
	        	InvoiceDefaults__c invoiceDefaultSettings = new InvoiceDefaults__c();
	        	invoiceDefaultSettings.Days_between_Reminders__c = 1;
	        	invoiceDefaultSettings.Initial_Invoice_Email_Template__c = initialInvoiceEmailId;
	        	invoiceDefaultSettings.Reminder_Invoice_Email_Template__c = reminderInvoiceEmailId;
	        	insert invoiceDefaultSettings;
	        	
	        	//Test record for Account 
	        	Account objAccountMaster = new Account(Name = 'Test My Test Account');
	            insert objAccountMaster; 
	            	            
	            //Test record for Contact1
	            Contact objPrimaryContact1 = new Contact();
	            objPrimaryContact1.FirstName = 'Test';
	            objPrimaryContact1.LastName = 'Contact 101';
	            objPrimaryContact1.Email = 'rajesh.kamath@trekbin.com';
	            objPrimaryContact1.AccountId = objAccountMaster.Id;
	            objPrimaryContact1.CurrencyIsoCode = 'USD';
	            insert objPrimaryContact1;
	             
	            //Test record for Account 
	        	Account objAccount = new Account(Name = 'Test My Test Account1', Primary_Invoice_Contact__c = objPrimaryContact1.Id);
	            insert objAccount;
	            	            
	            //Test record for Contact2
	            Contact objContact2 = new Contact();
	            objContact2.FirstName = 'Test';
	         	objContact2.LastName = 'Contact 102';
	            objContact2.Email = ' ';
	            objContact2.AccountId = objAccount.Id;
	            objContact2.CurrencyIsoCode = 'USD';
	            insert objContact2;
	        	
	        	//Test record for Contact3
	            Contact objContact3 = new Contact();
	            objContact3.FirstName = '';
	         	objContact3.LastName = 'Contact 103';
	            objContact3.Email = 'rajeshkamath123@gmail.com';
	            objContact3.AccountId = objAccount.Id;
	            objContact3.CurrencyIsoCode = 'USD';
	            insert objContact3;
	            
	            //Test record for Contact4
	            Contact objContact4 = new Contact();
	            objContact4.FirstName = '';
	         	objContact4.LastName = 'Contact 104';
	            objContact4.Email = 'sandeep.sankhla@trekbin.com';
	            objContact4.AccountId = objAccount.Id;
	            objContact4.CurrencyIsoCode = 'USD';
	            insert objContact4;
	            
	            //Test record for Invoice
	            Invoice__c invoiceRecord1 = new Invoice__c();
	            invoiceRecord1.Account__c = objAccount.Id; 
	            invoiceRecord1.CurrencyIsoCode = 'USD';
	            invoiceRecord1.Due_Date__c = system.today();
	            invoiceRecord1.Invoice_Date__c = system.today();
	            invoiceRecord1.Invoice_Status__c = 'Pending';                                                        
	            insert invoiceRecord1;
	            
	            //Creating an attachment for email functionality for invoice record
	            Attachment attachment = new Attachment(); 
		        attachment.Name = 'Unit Test attachment'; 
		        Blob bodyBlob = Blob.valueOf('Unit Test attachment Body'); 
		        attachment.body = bodyBlob; 
		        attachment.parentId = invoiceRecord1.Id; 
		        attachment.ContentType = 'application/msword'; 
		        attachment.IsPrivate = false; 
		        attachment.Description = 'Test'; 
		        insert attachment; 
		        
		        //Passing the invoice Id to the current page
		        ApexPages.currentPage().getParameters().put('Id',invoiceRecord1.id);
		        
		        //Initializing the class
		        Torque_SendInvoiceController objSendInvoice = new Torque_SendInvoiceController();
		        
		        /* Scenario - 1 */
		        system.assertEquals(objSendInvoice.lstInvoicingWrapper[0].isChecked, true);
		        
		        objSendInvoice.lstInvoicingWrapper[0].isChecked = true;
		        objSendInvoice.lstInvoicingWrapper[0].contactRecord.Id = objPrimaryContact1.Id;
		        
		        objSendInvoice.lstInvoicingWrapper[1].isChecked = false;
		        objSendInvoice.lstInvoicingWrapper[1].contactRecord.Id = objContact2.Id;
		        
		        objSendInvoice.lstInvoicingWrapper[2].isChecked = true;
		        objSendInvoice.lstInvoicingWrapper[2].contactRecord.Id = objContact3.Id;
		        
		        objSendInvoice.lstInvoicingWrapper[3].isChecked = true;
		        objSendInvoice.lstInvoicingWrapper[3].contactRecord.Id = objContact4.Id;
		                
		        //Method for Scenario - 1
		        objSendInvoice.generateInvoiceAndSendMail();
		        
		        //Querying Invoice 
        		//Invoice__c objTestInvoice = [Select Id, Invoice_Status__c, Name from Invoice__c Where Id =: invoiceRecord1.Id];
        		//system.debug('===1===objTestInvoice====' +objTestInvoice);
		        //system.assertEquals(objTestInvoice.Invoice_Status__c , 'Sent');
		        
		        system.assertEquals([Select Id, Account__r.Date_Last_Invoiced__c, Account__c From Invoice__c Where Id = :invoiceRecord1.Id].Account__r.Date_Last_Invoiced__c, system.today());	        
		        		        
		        //Redirect back function call
		        objSendInvoice.redirectBack();
        	}
        	//Catch block
        	catch(Exception ex)
        	{
        		system.debug('Exception caught');
        	}  
	        
        //End of test	
        Test.stopTest();
    }
    
    /*
		@ Test method : sendInvoiceTest1   
	*/
    static testMethod void sendInvoiceTest1() {
        
        //Start of test
        Test.startTest();
        	
        	//Try block
        	try
        	{
	        	//Id for the Initial_Invoice_Email_Template__c
	        	String initialInvoiceEmailId = [Select Name, Id From EmailTemplate Where Name = :'New Invoice Notification'].Id;
	        	
	        	//Id for the Reminder_Invoice_Email_Template__c
	        	String reminderInvoiceEmailId = [Select Name, Id From EmailTemplate Where Name = :'Invoice Reminder'].Id; 
	        	
	        	//Custom setting test record
	        	InvoiceDefaults__c invoiceDefaultSettings = new InvoiceDefaults__c();
	        	invoiceDefaultSettings.Days_between_Reminders__c = 1;
	        	invoiceDefaultSettings.Initial_Invoice_Email_Template__c = initialInvoiceEmailId;
	        	invoiceDefaultSettings.Reminder_Invoice_Email_Template__c = reminderInvoiceEmailId;
	        	insert invoiceDefaultSettings;
	        	
	        	//Test record for Account 
	        	Account objAccountMaster = new Account(Name = 'Test My Test Account');
	            insert objAccountMaster; 
	            	            
	            //Test record for Contact1
	            Contact objPrimaryContact1 = new Contact();
	            objPrimaryContact1.FirstName = 'Test';
	            objPrimaryContact1.LastName = 'Contact 101';
	            objPrimaryContact1.Email = 'rajesh.kamath@trekbin.com';
	            objPrimaryContact1.AccountId = objAccountMaster.Id;
	            objPrimaryContact1.CurrencyIsoCode = 'USD';
	            insert objPrimaryContact1;
	        	
	        	//Test record for Account 
	        	Account objAccount = new Account(Name = 'Test My Test Account1', Primary_Invoice_Contact__c = objPrimaryContact1.Id);
	            insert objAccount;
	            	            	            
	            //Test record for Invoice
	            Invoice__c invoiceRecord1 = new Invoice__c();
	            invoiceRecord1.Account__c = objAccount.Id; 
	            invoiceRecord1.CurrencyIsoCode = 'USD';
	            invoiceRecord1.Due_Date__c = system.today();
	            invoiceRecord1.Invoice_Date__c = system.today();
	            invoiceRecord1.Invoice_Status__c = 'Pending';                                                        
	            insert invoiceRecord1;
	            
	            //Creating an attachment for email functionality for invoice record
	            Attachment attachment = new Attachment(); 
		        attachment.Name = 'Unit Test attachment'; 
		        Blob bodyBlob = Blob.valueOf('Unit Test attachment Body'); 
		        attachment.body = bodyBlob; 
		        attachment.parentId = invoiceRecord1.Id; 
		        attachment.ContentType = 'application/msword'; 
		        attachment.IsPrivate = false; 
		        attachment.Description = 'Test'; 
		        insert attachment; 
		        
		        //Passing the invoice Id to the current page
		        ApexPages.currentPage().getParameters().put('Id',invoiceRecord1.id);
		        
		        //Initializing the class
		        Torque_SendInvoiceController objSendInvoice1 = new Torque_SendInvoiceController();
		        
		        /* Scenario - 1 */
		        system.assertEquals(objSendInvoice1.lstInvoicingWrapper[0].isChecked, true);
		        		        		                
		        //Method for Scenario - 1
		        objSendInvoice1.generateInvoiceAndSendMail(); 
		       
		       	//system.assertEquals([Select Name, Invoice_Status__c, Id From Invoice__c Where Id = :invoiceRecord1.Id].Invoice_Status__c , 'Sent');
		       	
		       	system.assertEquals([Select Id, Account__r.Date_Last_Invoiced__c, Account__c From Invoice__c Where Id = :invoiceRecord1.Id].Account__r.Date_Last_Invoiced__c, system.today());	        
		       	
		        //Redirect back function call
		        objSendInvoice1.redirectBack();
        	}
        	//Catch block
        	catch(Exception ex)
        	{
        		system.debug('Exception caught');
        	}  
	        
        //End of test	
        Test.stopTest();
    }
     
    /*
		@ Test method : sendInvoiceTest2   
	*/
    static testMethod void sendInvoiceTest2() {
        
        //Start of test
        Test.startTest();
        	
        	//Try block
        	try
        	{
	        	//Id for the Initial_Invoice_Email_Template__c
	        	String initialInvoiceEmailId = [Select Name, Id From EmailTemplate Where Name = :'New Invoice Notification'].Id;
	        	
	        	//Id for the Reminder_Invoice_Email_Template__c
	        	String reminderInvoiceEmailId = [Select Name, Id From EmailTemplate Where Name = :'Invoice Reminder'].Id; 
	        	
	        	//Custom setting test record
	        	InvoiceDefaults__c invoiceDefaultSettings = new InvoiceDefaults__c();
	        	invoiceDefaultSettings.Days_between_Reminders__c = 1;
	        	invoiceDefaultSettings.Initial_Invoice_Email_Template__c = initialInvoiceEmailId;
	        	invoiceDefaultSettings.Reminder_Invoice_Email_Template__c = reminderInvoiceEmailId;
	        	insert invoiceDefaultSettings;
	        	
	        	//Test record for Account 
	        	Account objAccount = new Account(Name = 'Test My Test Account');
	            insert objAccount; 
	            	        
	            //Test record for Contact2
	            Contact objContact2 = new Contact();
	            objContact2.FirstName = 'Test';
	         	objContact2.LastName = 'Contact 102';
	            objContact2.Email = ' ';
	            objContact2.AccountId = objAccount.Id;
	            objContact2.CurrencyIsoCode = 'USD';
	            insert objContact2;
	        	
	        	//Test record for Contact3
	            Contact objContact3 = new Contact();
	            objContact3.FirstName = '';
	         	objContact3.LastName = 'Contact 103';
	            objContact3.Email = 'rajeshkamath123@gmail.com';
	            objContact3.AccountId = objAccount.Id;
	            objContact3.CurrencyIsoCode = 'USD';
	            insert objContact3;
	            
	            //Test record for Contact4
	            Contact objContact4 = new Contact();
	            objContact4.FirstName = '';
	         	objContact4.LastName = 'Contact 104';
	            objContact4.Email = 'sandeep.sankhla@trekbin.com';
	            objContact4.AccountId = objAccount.Id;
	            objContact4.CurrencyIsoCode = 'USD';
	            insert objContact4;
	            	            	            
	            //Test record for Invoice
	            Invoice__c invoiceRecord1 = new Invoice__c();
	            invoiceRecord1.Account__c = objAccount.Id; 
	            invoiceRecord1.CurrencyIsoCode = 'USD';
	            invoiceRecord1.Due_Date__c = system.today();
	            invoiceRecord1.Invoice_Date__c = system.today();
	            invoiceRecord1.Invoice_Status__c = 'Pending';                                                        
	            insert invoiceRecord1;
	            
	            //Creating an attachment for email functionality for invoice record
	            Attachment attachment = new Attachment(); 
		        attachment.Name = 'Unit Test attachment'; 
		        Blob bodyBlob = Blob.valueOf('Unit Test attachment Body'); 
		        attachment.body = bodyBlob; 
		        attachment.parentId = invoiceRecord1.Id; 
		        attachment.ContentType = 'application/msword'; 
		        attachment.IsPrivate = false; 
		        attachment.Description = 'Test'; 
		        insert attachment; 
		        
		        //Passing the invoice Id to the current page
		        ApexPages.currentPage().getParameters().put('Id',invoiceRecord1.id);
		        
		        //Initializing the class
		        Torque_SendInvoiceController objSendInvoice2 = new Torque_SendInvoiceController();
		       	        	        		        		                
		        //Method for Scenario - 1
		        objSendInvoice2.generateInvoiceAndSendMail(); 
		       
		       	System.assertequals('Please select atleast one contact record', ApexPages.GetMessages().get(0).getSummary());
		       	
		        //Redirect back function call
		        objSendInvoice2.redirectBack();
        	}
        	//Catch block
        	catch(Exception ex)
        	{
        		system.debug('Exception caught');
        	}  
	        
        //End of test	
        Test.stopTest();
    }
            
    /*
		@ Test method : sendInvoiceTest3   
	*/
    static testMethod void sendInvoiceTest3() {
        
        //Start of test
        Test.startTest();
        	
        	//Try block
        	try
        	{
	        	//Id for the Initial_Invoice_Email_Template__c
	        	String initialInvoiceEmailId = [Select Name, Id From EmailTemplate Where Name = :'New Invoice Notification'].Id;
	        	
	        	//Id for the Reminder_Invoice_Email_Template__c
	        	String reminderInvoiceEmailId = [Select Name, Id From EmailTemplate Where Name = :'Invoice Reminder'].Id; 
	        	
	        	//Custom setting test record
	        	InvoiceDefaults__c invoiceDefaultSettings = new InvoiceDefaults__c();
	        	invoiceDefaultSettings.Days_between_Reminders__c = 1;
	        	invoiceDefaultSettings.Initial_Invoice_Email_Template__c = initialInvoiceEmailId;
	        	invoiceDefaultSettings.Reminder_Invoice_Email_Template__c = reminderInvoiceEmailId;
	        	insert invoiceDefaultSettings;
	        	
	        	//Test record for Account 
	        	Account objAccount = new Account(Name = 'Test My Test Account');
	            insert objAccount; 
	            	        
	            //Test record for Contact1
	            Contact objContact1 = new Contact();	            
	         	objContact1.LastName = 'Contact 101';
	            objContact1.Email = ' ';
	            objContact1.AccountId = objAccount.Id;
	            objContact1.CurrencyIsoCode = 'USD';
	            insert objContact1;
	            	            	            
	            //Test record for Invoice
	            Invoice__c invoiceRecord1 = new Invoice__c();
	            invoiceRecord1.Account__c = objAccount.Id; 
	            invoiceRecord1.CurrencyIsoCode = 'USD';
	            invoiceRecord1.Due_Date__c = system.today();
	            invoiceRecord1.Invoice_Date__c = system.today();
	            invoiceRecord1.Invoice_Status__c = 'Pending';                                                        
	            insert invoiceRecord1;
	            
	            //Creating an attachment for email functionality for invoice record
	            Attachment attachment = new Attachment(); 
		        attachment.Name = 'Unit Test attachment'; 
		        Blob bodyBlob = Blob.valueOf('Unit Test attachment Body'); 
		        attachment.body = bodyBlob; 
		        attachment.parentId = invoiceRecord1.Id; 
		        attachment.ContentType = 'application/msword'; 
		        attachment.IsPrivate = false; 
		        attachment.Description = 'Test'; 
		        insert attachment; 
		        
		        //Passing the invoice Id to the current page
		        ApexPages.currentPage().getParameters().put('Id',invoiceRecord1.id);
		        
		        //Initializing the class
		        Torque_SendInvoiceController objSendInvoice2 = new Torque_SendInvoiceController();
		        
		        objSendInvoice2.lstInvoicingWrapper[0].isChecked = true;
		        objSendInvoice2.lstInvoicingWrapper[0].contactRecord.Id = objContact1.Id;
		        		        		                
		        //Method for Scenario - 1
		        objSendInvoice2.generateInvoiceAndSendMail(); 
		       
		       	System.assertequals('There is no email address for the selected contact '+objContact1.LastName+'.\n For other selected contacts with email address, an invoice has been sent', ApexPages.GetMessages().get(0).getSummary());
		       	
		        //Redirect back function call
		        objSendInvoice2.redirectBack();
        	}
        	//Catch block
        	catch(Exception ex)
        	{
        		system.debug('Exception caught');
        	}  
	        
        //End of test	
        Test.stopTest();
    }   
}