/* 
    @ClassName   : Torque_GenerateInvoiceForProjectsHandler
    @Description : Controller class for Torque_GenerateInvoiceForProjects page,which creates Invoices related to project and 
                   InvoiceLine related to TimeEntry and Expense

*/
public with sharing class Torque_GenerateInvoiceForProjectsHandler {
    
    public String strAccountId;
    
    public String strProjectId;
    
    public boolean isDisplayPopup { get; set; }
    
    public boolean isGenInvoice {get; set;}
    
    public String strCustomSetting { get; set; }
    public String developerName { get; set; }
    
    public List<wrapperProject> lstWrapperProjectActive { get;set; }
    public List<wrapperProject> lstWrapperProjectInActive { get;set; }
    
    public Project__c objProjDateFilter { get;set; }
    
    /**
    	LastModifiedBy		: Trekbin
    	LastModifiedDate 	: 9-march-2016
    */
    // Wrapper class definition for Project to bind on vf page
    public class wrapperProject {
        
        public boolean isChecked {get;set;}
        public Project__c objproject {get;set;}
        
        //constructor for wrapper class
        public wrapperProject(boolean isChecked, Project__c objproject) {
            
            this.isChecked = isChecked;
            this.objproject = objproject;
        }
    }
    
    public List<Schema.FieldSetMember> getProjectFields() {
        return SObjectType.Project__c.FieldSets.Project_Fields_Generate_Invoice.getFields();
    }

    // constructor definition
    public Torque_GenerateInvoiceForProjectsHandler(ApexPages.StandardController controller) {
        
        init();
        
        if(String.isEmpty(strAccountId))
            return ;
            
        fetchProjectRecords();
    }
    
    // Initialize global varibles
    private void init() {
        
        strAccountId = strCustomSetting = strProjectId = '';
        
        if( ApexPages.currentPage().getParameters().containsKey('Id'))
            strAccountId = ApexPages.currentPage().getParameters().get('Id'); 
         
        if( ApexPages.currentPage().getParameters().containsKey('projectIDToSelectDefault'))
            strProjectId = ApexPages.currentPage().getParameters().get('projectIDToSelectDefault');
                
        lstWrapperProjectActive = new List<wrapperProject>();   
        lstWrapperProjectInActive = new List<wrapperProject>();   
        objProjDateFilter = new Project__c();   
    }
    
    // Method to fetch project records related to Account
    public void fetchProjectRecords() {
        
        if(strAccountId != null) {
            
            String query = 'SELECT ';
            
            for(Schema.FieldSetMember f : this.getProjectFields()) {
                query += f.getFieldPath() + ', ';
            }
            query += 'id,Account__c,Account__r.Payment_Terms__c,Hourly_Rate__c,Account__r.Account_Balance__c, Is_Active__c FROM Project__c WHERE Account__c =: strAccountId';

            //Iterating over project with its related account and adding to listofwrapper class
            for(Project__c objproject : Database.query(query)) {
                
                /* Last Modified by Trekbin on 1-July, 2016 */
                if(strProjectId != null && strProjectId != '') {		//If the user clicks on button in the project detail page 
					
	                if(objproject.Id == strProjectId) {					//If the project equals to the projectId from where the button click is done
	                	
	                	addProjectsToWrapperList(true, objproject);		//Call a common method passing default check as true
	                }
                	else 												//For other projects
                	{
		                addProjectsToWrapperList(false, objproject);	//Call a common method passing default check as false
	                }
                }
                else													//If the click of button is done from the accounts project related list 
                {
                	addProjectsToWrapperList(false, objproject);		//Call a common method passing default check as false 
                }
            }
        }
        
        if(lstWrapperProjectActive.isEmpty() && lstWrapperProjectInActive.isEmpty())
            ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.INFO, ' No projects exists for this Account '));
            
        //custom setting to display text on pop up if ProjectsAccountBalance__c less than zero
        GlobalSettings__c customsetting = GlobalSettings__c.getInstance();
        strCustomSetting = customsetting.Account_Credit_Alert__c;
        
    }
    
    /*
    	@ Method Name	:	addProjectsToWrapperList
    	@ Parameters	:	Boolean isChecked, Project__c objproject
    	@ Created By 	:	Trekbin On 1-july, 2016
    */
    private void addProjectsToWrapperList(Boolean isChecked, Project__c objproject) {
		
		//If the project is an active project add it to active list
		if(objproject.Is_Active__c) {
		                	
			lstWrapperProjectActive.add(new wrapperProject(isChecked, objproject));
		}
		else
		{
			lstWrapperProjectInActive.add(new wrapperProject(isChecked, objproject));
		}
	}

    /* Last Modified By: Trekbin on 19-jan-2016 */
    //create InvoiceLines related to TimeEntry and Expenses
    public String generateinvoices() {
        
        String stringURL;
        
        Id ProjectId;
        
        Invoice__c objinvoice;
        
        Boolean isInvoiceCreated;
        
        
        Map<Id,Project__c> mapOfIdToProject = new Map<Id,Project__c>();
        Map<Id,Invoice__c> mapOfProjectIdToInvoice = new Map<Id,Invoice__c>();
        Map<Id,Invoice__c> mapOfAccountIdToInvoice = new Map<Id,Invoice__c>();             
        
        
        Map<Id,Invoice__c> mapOfProjectIdToInvoiceCreated = new Map<Id,Invoice__c>();
        
        List<Account> lstAccount =  new List<Account>();        
        
        isDisplayPopup = isGenInvoice = isInvoiceCreated = false;
        
        List<wrapperProject> lstWrapperProjects = new List<wrapperProject>();
        lstWrapperProjects.addAll(lstWrapperProjectActive);
        lstWrapperProjects.addAll(lstWrapperProjectInActive);
        
        for(wrapperProject objprojectWrapper : lstWrapperProjects) {
            
            if(objprojectWrapper.ischecked ) {
                
                mapOfIdToProject.put(objprojectWrapper.objproject.Id, objprojectWrapper.objproject);
                
                if(!isInvoiceCreated)        
                {
                    objinvoice = new Invoice__c( Invoice_Date__c = Date.Today(), Account__c = objprojectWrapper.objproject.Account__c); 
                    
                    if(objprojectWrapper.objproject.Account__r.Payment_Terms__c!=null)
                        objinvoice.Due_Date__c = Date.Today() + Integer.valueOf(objprojectWrapper.objproject.Account__r.Payment_Terms__c);
                    else objinvoice.Due_Date__c = Date.Today();
                    
                    isInvoiceCreated = true;
                }
                
            }
        }
        
        Torque_Invoice_Record_Type__mdt customMetadataType = [	SELECT InvoiceRecordTypeID__c, InvoiceLineRecordTypeID__c 
        														FROM Torque_Invoice_Record_Type__mdt 
        														where DeveloperName = :developerName
        													];
        
        objinvoice.RecordTypeId = customMetadataType.InvoiceRecordTypeID__c;
        
        if(objinvoice != NULL)
            insert objinvoice;
        
        
		Date dateFilter;
        Set<Id> setSelectedProjIds = mapOfIdToProject.keySet();
        
        if(objProjDateFilter.Dev_Start_Date__c != null)
        	dateFilter = objProjDateFilter.Dev_Start_Date__c;
		
             
		/**			
			@CreatedBy  : Trekbin
			@CreatedOn  : 7-March-2016
			@Description: Execute Torque_TimeEntryBatch batch for solving gorvernor limit issue for time entry records	
		*/ 
		Torque_TimeEntryBatch objTimeEntryBatch = new Torque_TimeEntryBatch(objinvoice, mapOfIdToProject, setSelectedProjIds, dateFilter, customMetadataType.InvoiceLineRecordTypeID__c);
		Database.executeBatch(objTimeEntryBatch, 50);
		
		if(objinvoice.id != null)
		{
			stringURL = System.URL.getSalesforceBaseUrl().toExternalForm() + '/' +objinvoice.id;              
			return stringURL; 
		}
			
        return null;                
    }
         
    //Method to Redirect back to Account detail page
    public PageReference redirectBack() {    
            
        //We are getting the base url of the page and simply passing invoice id
        String url = '';
        url = System.URL.getSalesforceBaseUrl().toExternalForm();       
        PageReference redirect = new PageReference(url + '/' +strAccountId);        
        return redirect;        
    } 
     
    /* Last Modified By: Trekbin on 19-jan-2016 */
    
    /*
        Method Name: showPopupValidation
        Return Type: string
    */       
    public String showPopupValidation()
    {
        List<Account> lstAccount =  new List<Account>();
                
        //Initialize the counter variable to 0
        Integer counter = 0; 
        
        List<wrapperProject> lstWrapperProjects = new List<wrapperProject>();
        lstWrapperProjects.addAll(lstWrapperProjectActive);
        lstWrapperProjects.addAll(lstWrapperProjectInActive);
        
        //Iterate over wrapper list to check if there are "checked project" records
        for(wrapperProject objprojectWrapper : lstWrapperProjects) {
            
            //If found set the counter variable to 1
            if(objprojectWrapper.ischecked ) {
                                                
                counter = 1;
            }
        }
        
        // If no checked project reocrds are found
        if(counter == 0)
        {    
            //Display a apex message
            ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.INFO, ' Please select atleast one project '));
            
            //disable the popup visibility & invoice generation
            isDisplayPopup = isGenInvoice = false;
        }   
        else
        {
            //query on the account record as per the accountId
            lstAccount = [ select Id, Name , Account_Balance__c from Account where Id =: strAccountId  ];
            
            // Check if the account balance is negative
            if(!lstAccount.isEmpty() && lstAccount[0].Account_Balance__c < 0) 
            {
                //If so display the popup
                isDisplayPopup = true;            
            }
            else
            {
                //If positive account balance, simply generate the invoice
                isGenInvoice = true;
            }
        }
        
        //return statement
        return null;           
    }
}