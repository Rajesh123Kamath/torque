/**
    @Name           :   Torque_VFPanelToDisplayInvoicesOnAccCntr
    @Date           :   13 Feb, 2016
    @Description    :   An apex page controller for Torque_VFPanelToDisplayInvoicesOnAccount.page which displays all Invoices that have actually been 
                        Sent for a particular Account
    @TestClass      :   Torque_VFPanelToDisplayInvoicesOnAccCntr.cls
*/
public with sharing class Torque_VFPanelToDisplayInvoicesOnAccCntr {
    
    /*get set variables - start*/
    public List<Invoice__c> lstInvoices {get; set;}  
    public Integer totalNoOfRecords {get; set;}
    public Integer pageNumber {get; set;}
    public Integer recordsPerPage {get; set;}
    public Boolean noRecords {get; set;}
    /*get set variables - end*/
    
    /*private variables - start*/
    ApexPages.StandardSetController stdSetContrl;
    Id accId;
    /*private variables - end*/
    
    /**
        @Name           :   Torque_VFPanelToDisplayInvoicesOnAccCntr
        @Description    :   Main class constructor.
    */
    public Torque_VFPanelToDisplayInvoicesOnAccCntr(ApexPages.StandardController controller) {
        
        accId = ((Account) controller.getRecord()).Id;
        getInvoicesForAcc();
    }
    
    /**
        @Name           :   getInvoicesForAcc
        @Description    :   Get the invoices on the Account based on the pressed button.
    */
    public void getInvoicesForAcc() {
        
        String strQuery = 'SELECT ';
        
        system.debug('----------'+Schema.getGlobalDescribe().get('Invoice__c').getDescribe().FieldSets.getMap().get('Account_Invoice_VF_Panel').getFields());
        //Get the field names from the field set.
        for(Schema.FieldSetMember objFieldSet : Schema.getGlobalDescribe().get('Invoice__c').getDescribe().FieldSets.getMap().get('Account_Invoice_VF_Panel').getFields()) {
            
            strQuery += objFieldSet.getFieldPath() + ', ';
        }
        
        strQuery += ' CreatedDate FROM Invoice__c WHERE Account__c = :accId '; 
        
        //If Show All button is pressed then get all the invoices irrespective of Invoice Status, else get only Pending and Sent Invoices.
        if(Apexpages.currentpage().getParameters().get('strAll') != 'All')
            strQuery += 'AND (Invoice_Status__c = \'Sent\' OR Invoice_Status__c = \'Pending\') ';
            
        strQuery += 'ORDER BY Invoice_Date__c DESC NULLS LAST LIMIT 10000';
        
        //Instantiate the StandardSetController from the query locator by passing the above query string.
        stdSetContrl = new ApexPages.StandardSetController(Database.getQueryLocator(strQuery));
        
        //Get the total no. of records returned.
        totalNoOfRecords = stdSetContrl.getResultSize();
        
        //Set the no. of records to be displayed per page.
        recordsPerPage = 12;
        
        //Display the invoices.
        if(totalNoOfRecords > 0) {
            stdSetContrl.setPageSize(recordsPerPage);  
            
            getCurrentPageInvoices();
        } else {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO,'No Invoice Found.'));
            noRecords = true;
            return; 
        }
    }
    
    /**
        @Name           :   getCurrentPageInvoices
        @Description    :   Get the invoices to be displayed for the current page.
    */
    private void getCurrentPageInvoices() {
        
        lstInvoices = new List<Invoice__c>();
        
        //Get the Invoices for the current page.
        lstInvoices = (List<Invoice__c>)stdSetContrl.getRecords();
        
        //Get the current page no. to display on UI.
        pageNumber = stdSetContrl.getPageNumber();
    }
    
    /**
        @Name           :   hasNext
        @Description    :   indicates whether there are more records after the current page set.
    */
    public Boolean hasNext {
        get {
            return stdSetContrl.getHasNext();
        }
        set;
    }
    
    /**
        @Name           :   hasPrevious
        @Description    :   indicates whether there are more records before the current page set.
    */
    public Boolean hasPrevious {
        get {
            return stdSetContrl.getHasPrevious();
        }
        set;
    }
    
    /**
        @Name           :   first
        @Description    :   returns the first page of records.
    */
    public void first() {
        stdSetContrl.first();
        getCurrentPageInvoices();
    }
    
    /**
        @Name           :   last
        @Description    :   returns the last page of records.
    */
    public void last() {
        
        stdSetContrl.last();
        getCurrentPageInvoices();
    }
    
    /**
        @Name           :   previous
        @Description    :   returns the previous page of records.
    */
    public void previous() {
        stdSetContrl.previous();
        getCurrentPageInvoices();
    }
    
    /**
        @Name           :   next
        @Description    :   returns the next page of records.
    */
    public void next() {
        stdSetContrl.next();
        getCurrentPageInvoices();
    }
}