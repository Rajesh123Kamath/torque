/**
    @Name           :   Torque_TimeCardTriggerHandler
    @Date           :   28 June, 2016
    @Description    :   Time Card Trigger controller which prevents its deletion if any time card is present on it.
    @TestClass      :   Torque_TimeCardTriggerHandler_Test.cls
*/
public with sharing class Torque_TimeCardTriggerHandler {
    
    public static void isBeforeDelete(Map<Id, Time_Card__c> timeCardsDeletedMap) {
        
        preventTimeCardDeletionIfTimeEntriesPresent(timeCardsDeletedMap);
    }
    
    private static void preventTimeCardDeletionIfTimeEntriesPresent(Map<Id, Time_Card__c> timeCardsDeletedMap) {
        
        for(Time_Entry__c objTE :   [   SELECT Time_Card__c 
                                        FROM Time_Entry__c 
                                        WHERE Time_Card__c 
                                        IN :timeCardsDeletedMap.keySet()
                                    ]) {
            
            timeCardsDeletedMap.get(objTE.Time_Card__c).addError('You cannot delete this Time Card');
        }
    }
}