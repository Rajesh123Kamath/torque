/**
    TsetClassName 	: Torque_GenerateInvoiceForProjects_Test
	Created By 		: Trekbin
    Created on 		: 08-march-2016
    Description		: Test class for the class "Torque_GenerateInvoiceForProjectsHandler"
    LastModifiedBy	: Trekbin on 9-march-2016
*/
@isTest (SeeAllData =  false)
private class Torque_GenerateInvoiceForProjects_Test 
{

    static testMethod void myUnitTest() 
    {
      
        //Test Custom setting record
        List<ProjectDateSettings__c> lstProjectDateSettingsToInsert = UTestData.getProjectDateSettings(1);
        lstProjectDateSettingsToInsert[0].NonImplementationEndDateIncrement__c =  30;
        lstProjectDateSettingsToInsert[0].BuildConfigTestingEndDayIncrement__c = 14;
        lstProjectDateSettingsToInsert[0].BuildConfigTestingStartDayIncrement__c = 15;
        lstProjectDateSettingsToInsert[0].DesignDiscoveryEndDaysIncrement__c = -1;
        lstProjectDateSettingsToInsert[0].GoLiveAndPostGoLiveEndDayIncrement__c = 7;
        insert lstProjectDateSettingsToInsert;        
        
        //Test profile record
       	String strProfile = UTestData.getStandardProfileId('System Administrator'); 
       
        //Test user record
     	User objUser = UTestData.getUserRecord(strProfile);
     	objUser.isActive = true;
     	insert objUser;
        
        System.runAs(objuser){
        	
	        List<GlobalSettings__c> lstGlobalSetting = UTestData.getGlobalSettings(1);
	        lstGlobalSetting[0].Account_Credit_Alert__c = 'Test Record';
	        lstGlobalSetting[0].InstanceHealthCheckCaseOwnerId__c = 'Test';
	        lstGlobalSetting[0].PTODaysInTheFuture__c = 123.0;
	        lstGlobalSetting[0].Default_Billing_Reason__c = 'TestReason';	        
	        insert lstGlobalSetting;
	        
	        
	        /* List of test account records to insert */
	        List<Account> lstTestAccountToInsert = UTestData.getAccounts(1);        
	        lstTestAccountToInsert[0].Payment_Terms__c = 2;
	        insert lstTestAccountToInsert;       
	       
	        
	        //Create a test project record
	        List<Project__c> lstProjects = UTestData.getProjects(1);
	        lstProjects[0].Account__c = lstTestAccountToInsert[0].Id;
	        lstProjects[0].Hourly_Rate__c = 200.00;        
	        lstProjects[0].Assigned_To__c = objuser.Id;
	        lstProjects[0].Kickoff__c = date.today();
	        lstProjects[0].Go_Live_Date__c = date.today();
	        lstProjects[0].Status__c = 'Planning';
	        lstProjects[0].Current_State__c = 'Maintenance';
	        insert lstProjects;
        

			//Test Deliverable Record    
	        List<Deliverable__c> lstDeliverableToInsert = UTestData.getDeliverables(1);
	        lstDeliverableToInsert[0].Project__c = lstProjects[0].Id;
	        insert lstDeliverableToInsert;
	
	
	        //Test BudgetItem Record 1    
		    List<Budget_Item__c> lstBudgetItem = UTestData.getBudgetItems(1, lstDeliverableToInsert[0], lstProjects[0]);
		    insert lstBudgetItem;
	        
	        
	        //Create a test time card record 1
	        List<Time_Card__c> lstTimeCardRecord  = UTestData.getTimeCards(1);
	        insert lstTimeCardRecord;
	        
	        
	        //Test record for Billing Reason
	        List<Billing_Reason_Code__c> lstBillingReason = UTestData.getBillingReasonCodes(1);
	        lstBillingReason[0].Billable__c = true;
	        lstBillingReason[0].Non_Billable__c = false;
	        lstBillingReason[0].Unbilled_del__c = false;
	    	insert lstBillingReason;
	        
	        //Create a test invoice record 1
	        Invoice__c invoiceRecord1 = UTestData.getInvoice(lstTestAccountToInsert[0].Id, 'Canceled');
	        invoiceRecord1.CurrencyIsoCode = 'USD';
	        insert invoiceRecord1;
	    
	        //Create a time entry record
	        List<Time_Entry__c> lstTimeEntryRecord = UTestData.getTimeEntry(1, lstProjects[0], lstTimeCardRecord[0], lstBillingReason[0], lstBudgetItem[0]);		
			lstTimeEntryRecord[0].Billable__c = true;		
			lstTimeEntryRecord[0].Time__c = 10;
			lstTimeEntryRecord[0].CurrencyIsoCode = 'USD';		
			lstTimeEntryRecord[0].Date__c = date.today();
			lstTimeEntryRecord[0].Case_Number__c = '123';           
			lstTimeEntryRecord[0].Notes__c = 'testNote1';  		
			insert lstTimeEntryRecord; 
	                    
	        //Create a test expense report
	        List<Expense_Report__c> lstExpReport = UTestData.getExpenseReport(1, 'Approved');
	        lstExpReport[0].CurrencyIsoCode = 'USD';
	        insert lstExpReport;
	        
	        //Create a expense record 
	        List<Expense__c> lstExpenseRecord = UTestData.getExpense(1, lstProjects[0], lstExpReport[0]);
	        lstExpenseRecord[0].CurrencyIsoCode = 'USD';
	        lstExpenseRecord[0].Billable__c = true;
	        lstExpenseRecord[0].Date__c = system.today();
	        lstExpenseRecord[0].Amount__c = 100;
	        lstExpenseRecord[0].Description__c = 'hello test';
	        lstExpenseRecord[0].Invoice_Comment__c = 'Hello';
	        insert lstExpenseRecord;
	       
	        ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(lstTestAccountToInsert[0]);
	        ApexPages.currentPage().getParameters().put('Id',lstTestAccountToInsert[0].id);
	         
	        Torque_GenerateInvoiceForProjectsHandler objhandler = new Torque_GenerateInvoiceForProjectsHandler(sc); 
	        
	        objhandler.lstWrapperProjectActive[0].isChecked = true; 
	                               
	        objhandler.showPopupValidation();   
	        
	        objhandler.generateinvoices();
	        
	        objhandler.lstWrapperProjectActive[0].isChecked = false; 
	        
	        objhandler.showPopupValidation();         
	            
	        objhandler.redirectBack();  
    	}
  	}
}