/**
  * Handler Name : Torque_InvoiceLineHandlerTest
  * Created By   : Trekbin          
  * Created On   : 19-Nov-2015
  * Modified On  : 10-Dec-2015
  * Modified by  : Trekbin
  * Description  : Test class for the trigger 'Torque_InvoiceLineTrigger'
**/
@isTest(SeeAllData = false)
private class Torque_InvoiceLineHandlerTest {

    //Since we need to cover validation errors for update and delete case we have used different test methods because in each method once an error for each scenario is caught, next scenario wont be checked, so out criteria fails. 
    //So different methods for satisfing all the scenarios
    static testMethod void myUnitTest() {
        
         /* Modified By Trekbin on 14-Jan-2016 */
        //Id Master_Project_RecordType = [Select Id, DeveloperName From RecordType where DeveloperName='Master_Project'].Id;
        
        //Create a test account record
        Account accountRecord = new Account();
        accountRecord.Name ='Test My Account 001';
        accountRecord.Practice__c = 'Southeast';    
        accountRecord.Type = 'Other'; 
        insert accountRecord; 
        
        /* Modified By Trekbin on 14-Jan-2016 */  
        //create test setting record used in project trigger
        ProjectDateSettings__c dateSetting = new ProjectDateSettings__c(BuildConfigTestingEndDayIncrement__c = 14, BuildConfigTestingStartDayIncrement__c = 15, DesignDiscoveryEndDaysIncrement__c = -1, GoLiveAndPostGoLiveEndDayIncrement__c= 7, NonImplementationEndDateIncrement__c = 30.0);
        insert dateSetting;  
        
        /* Modified By Trekbin on 14-Jan-2016 */
        //Test Project Record   
        Project__c objProject = new Project__c(Name = 'Test Project', Account__c = accountRecord.id);           
        objProject.Kickoff__c = Date.today(); 
        objProject.Hourly_Rate__c = 200;
        objProject.Go_Live_Date__c =  Date.today().addDays(30);
        objProject.sendToCashBoard__c = false;      
        objProject.No_Budget__c = true;
        objProject.Type__c = 'Other';
        //objProject.Master_Project_RecordType;
        objProject.Status__c = 'Inactive';      
        insert objProject;      
          
        /* Modified By Trekbin on 14-Jan-2016 */    
        //Test ProjectPhase Record       
        Project_Phase__c phase = new Project_Phase__c();
        phase.Phase_name__c = 'Build/Config/Testing';
        phase.Start_Date__c = Date.today();
        phase.End_Date__c = Date.today().AddDays(dateSetting.NonImplementationEndDateIncrement__c.intValue());
        phase.Project__c = objProject.id;
        insert phase; 
        
        /* Modified By Trekbin on 14-Jan-2016 */
        //Test Deliverable Record    
        Deliverable__c objDeliverable = new Deliverable__c(Name = 'Test Deliverable', Project__c = objProject.id);
        insert objdeliverable;
        
        /* Modified By Trekbin on 14-Jan-2016 */    
        //Test BudgetItem Record 1    
        Budget_Item__c objBudgetItem1 = new Budget_Item__c(Deliverable__c = objdeliverable.id);
        insert objbudgetItem1;
                
        //Create a test invoice record 1
        Invoice__c invoiceRecord1 = new Invoice__c();
        invoiceRecord1.Account__c = accountRecord.Id; 
        invoiceRecord1.CurrencyIsoCode = 'USD';
        invoiceRecord1.Invoice_Status__c = 'Canceled';                                                        
        insert invoiceRecord1;
        
        //Create a test invoice record 2
        Invoice__c invoiceRecord2 = new Invoice__c();
        invoiceRecord2.Account__c = accountRecord.Id; 
        invoiceRecord2.CurrencyIsoCode = 'USD';
        invoiceRecord2.Invoice_Status__c = 'Pending';                                                        
        insert invoiceRecord2;
        
        //Create a test time card record 1
        Time_Card__c timeCardRecord1  = new Time_Card__c();
        timeCardRecord1.Name = 'Test Time Card';
        insert timeCardRecord1;
        
        Billing_Reason_Code__c billingReason = new Billing_Reason_Code__c(Name = 'Test Billing', Billable__c=true, Non_Billable__c=false, Unbilled_del__c=false);
        insert billingReason;
        
        /* Modified By Trekbin on 14-Jan-2016 */
        //Create a time entry record
        Time_Entry__c timeentryrecord1 = new Time_Entry__c();
        timeentryrecord1.Time_Card__c = timeCardRecord1.Id;  
        timeentryrecord1.Time__c = 10;
        timeentryrecord1.CurrencyIsoCode = 'USD';
        timeentryrecord1.Budget_Item__c = objBudgetItem1.Id;
        timeentryrecord1.Billing_Reason__c = billingReason.Id;
        timeentryrecord1.Notes__c = 'Test Notes';
        insert timeentryrecord1;         
        
        //Create a expense record
        /*aciee2__Expense__c expenseRecord1 = new aciee2__Expense__c();
        expenseRecord1.CurrencyIsoCode = 'USD';
        expenseRecord1.aciee2__Date__c = system.today();
        expenseRecord1.aciee2__Amount__c = 100;
        expenseRecord1.aciee2__Description__c = 'hello test';
        insert expenseRecord1;   */             
                        
        //Create a test invoice line record 1
        Invoice_Line__c invoiceLineRecord1 = new Invoice_Line__c();
        invoiceLineRecord1.Invoice__c = invoiceRecord1.Id;       
        invoiceLineRecord1.CurrencyIsoCode = 'USD'; 
        insert invoiceLineRecord1;  
        
        Invoice_Line__c invoiceLineRecord2 = new Invoice_Line__c();
        invoiceLineRecord2.Invoice__c = invoiceRecord1.Id;       
        invoiceLineRecord2.CurrencyIsoCode = 'USD'; 
        insert invoiceLineRecord2;
        
        Invoice_Line__c invoiceLineRecord3 = new Invoice_Line__c();
        invoiceLineRecord3.Invoice__c = invoiceRecord2.Id;       
        invoiceLineRecord3.CurrencyIsoCode = 'USD'; 
        insert invoiceLineRecord3;       
        
        try
        {   
            //Update Invoice_Status__c = 'Pending'
            invoiceRecord1.Invoice_Status__c = 'Pending';    
            update invoiceRecord1;
            
            //Update fields on Invoice Line=== updates          
            //invoiceLineRecord1.Expense__c = expenseRecord1.Id;
            invoiceLineRecord1.Time_Entry__c = timeentryrecord1.Id;    
            invoiceLineRecord1.Quantity__c = 10;
            update invoiceLineRecord1;              
            
            //Update Invoice_Status__c = 'Canceled'
            invoiceRecord1.Invoice_Status__c = 'Canceled';   
            update invoiceRecord1;          
                                
            //Update fields on Invoice Line=== updates          
            invoiceLineRecord1.Expense__c = null;                           
            update invoiceLineRecord1;    
            
            //Update Invoice_Status__c = 'Sent'
            invoiceRecord1.Invoice_Status__c = 'Sent';   
            update invoiceRecord1;          
            
            //Update fields on Invoice Line=== Validation error         
            invoiceLineRecord1.Expense__c = null;
            invoiceLineRecord1.Time_Entry__c = null;    
            invoiceLineRecord1.Quantity__c = 20;
            update invoiceLineRecord1;   
                        
            // Validation error 
            delete invoiceLineRecord2;      
        }        
        catch(Exception e)
        {
            system.debug('Validation error thrown...Exception Caught');         
        }           
    }
    
     /* cancelled based update error check */
     static testMethod void myUnitTest1() {
        
        //Create a test account record
        Account accountRecord = new Account();
        accountRecord.Name ='Test My Account 001';
        accountRecord.Practice__c = 'Southeast';    
        insert accountRecord;           
        
        //Create a test invoice record 2
        Invoice__c invoiceRecord2 = new Invoice__c();
        invoiceRecord2.Account__c = accountRecord.Id; 
        invoiceRecord2.CurrencyIsoCode = 'USD';
        invoiceRecord2.Invoice_Status__c = 'Pending';                                                        
        insert invoiceRecord2;                         
                
        Invoice_Line__c invoiceLineRecord3 = new Invoice_Line__c();
        invoiceLineRecord3.Invoice__c = invoiceRecord2.Id;       
        invoiceLineRecord3.CurrencyIsoCode = 'USD'; 
        insert invoiceLineRecord3;       
        
        try
        {   
            //Update Invoice_Status__c = 'Canceled'
            invoiceRecord2.Invoice_Status__c = 'Canceled';   
            update invoiceRecord2;    
            
            invoiceLineRecord3.Quantity__c = 20;
            update invoiceLineRecord3;      
            
        }
        catch(Exception e)
        {
            system.debug('Validation error thrown...Exception Caught');         
        }           
    } 
    
    /* Last Modified By 'Trekbin' on '10-feb-2016' as per case '25221' */
    /* delete based error check : throw error while deleting as Invoice_Override_Permission__c is false*/
    static testMethod void myUnitTest2() {
        
         //Test profile record
        Profile testProfile = [SELECT Id FROM Profile WHERE Name = 'System Administrator']; 
        
        //Test user record
        User testUser = new User(Alias = 'standtw3', Email='standarduser@testorg.com', EmailEncodingKey='UTF-8', LastName='Testingw3', LanguageLocaleKey='en_US', LocaleSidKey='en_US', ProfileId = testProfile.Id, 
            				TimeZoneSidKey='America/Los_Angeles', UserName='standarduserw3@testorg.com', Invoice_Override_Permission__c = false);
        insert testUser;
                         
        System.RunAs(testUser) {
        
	        //Create a test account record
	        Account accountRecord = new Account();
	        accountRecord.Name ='Test My Account 001';
	        accountRecord.Practice__c = 'Southeast';    
	        insert accountRecord;           
	        
	        //Create a test invoice record 2
	        Invoice__c invoiceRecord3 = new Invoice__c();
	        invoiceRecord3.Account__c = accountRecord.Id; 
	        invoiceRecord3.CurrencyIsoCode = 'USD';
	        invoiceRecord3.Invoice_Status__c = 'Sent';                                                        
	        insert invoiceRecord3;                         
	                
	        Invoice_Line__c invoiceLineRecord4 = new Invoice_Line__c();
	        invoiceLineRecord4.Invoice__c = invoiceRecord3.Id;       
	        invoiceLineRecord4.CurrencyIsoCode = 'USD'; 
	        insert invoiceLineRecord4;       
	        
	        try
	        {               
	            delete invoiceLineRecord4;      	            
	        }
	        catch(Exception e)
	        {
	            system.debug('Validation error thrown...Exception Caught');     
	            System.Assert(e.getMessage().contains('You cannot delete this Invoice Line record'));    
	        }      
        }     
    }
    
    
    /* Created By 'Trekbin' on '10-feb-2016' as per case '25221' */
    /* delete based error check : delete even the status is sent as Invoice_Override_Permission__c is true*/
    static testMethod void myUnitTest3() {
        
         //Test profile record
        Profile testProfile = [SELECT Id FROM Profile WHERE Name = 'System Administrator']; 
        
        //Test user record
        User testUser = new User(Alias = 'standtw4', Email='standarduser@testorg.com', EmailEncodingKey='UTF-8', LastName='Testingw32', LanguageLocaleKey='en_US', LocaleSidKey='en_US', ProfileId = testProfile.Id, 
            				TimeZoneSidKey='America/Los_Angeles', UserName='standarduserw32@testorg.com', Invoice_Override_Permission__c = true);
        insert testUser;
                         
        System.RunAs(testUser) {
        
	        //Create a test account record
	        Account accountRecord = new Account();
	        accountRecord.Name ='Test My Account 001';
	        accountRecord.Practice__c = 'Southeast';    
	        insert accountRecord;           
	        
	        //Create a test invoice record 2
	        Invoice__c invoiceRecord3 = new Invoice__c();
	        invoiceRecord3.Account__c = accountRecord.Id; 
	        invoiceRecord3.CurrencyIsoCode = 'USD';
	        invoiceRecord3.Invoice_Status__c = 'Sent';                                                        
	        insert invoiceRecord3;                         
	                
	        Invoice_Line__c invoiceLineRecord4 = new Invoice_Line__c();
	        invoiceLineRecord4.Invoice__c = invoiceRecord3.Id;       
	        invoiceLineRecord4.CurrencyIsoCode = 'USD'; 
	        insert invoiceLineRecord4;       
	        
	        try
	        {               
	            delete invoiceLineRecord4;  
             	system.assertEquals([Select Id from Invoice_Line__c Where Id = :invoiceLineRecord4.Id].size(), 0);            
	        }
	        catch(Exception e)
	        {
	            system.debug('Validation error thrown...Exception Caught');     
	        }      
        }     
    }
    
    /* Created By 'Trekbin' on '10-feb-2016' as per case '25221' */
    /* Validation error*/
    static testMethod void myUnitTest4() {
        
         //Test profile record
        Profile testProfile = [SELECT Id FROM Profile WHERE Name = 'System Administrator']; 
        
        //Test user record
        User testUser = new User(Alias = 'standtw4', Email='standarduser@testorg.com', EmailEncodingKey='UTF-8', LastName='Testingw32', LanguageLocaleKey='en_US', LocaleSidKey='en_US', ProfileId = testProfile.Id, 
            				TimeZoneSidKey='America/Los_Angeles', UserName='standarduserw32@testorg.com', Invoice_Override_Permission__c = false);
        insert testUser;
                         
        System.RunAs(testUser) {
        
	        ProjectDateSettings__c dateSetting = new ProjectDateSettings__c(BuildConfigTestingEndDayIncrement__c = 14, BuildConfigTestingStartDayIncrement__c = 15, DesignDiscoveryEndDaysIncrement__c = -1, GoLiveAndPostGoLiveEndDayIncrement__c= 7, NonImplementationEndDateIncrement__c = 30.0);
        	insert dateSetting;
        	
	        //Create a test account record
	        Account accountRecord = new Account();
	        accountRecord.Name ='Test My Account 001';
	        accountRecord.Practice__c = 'Southeast';    
	        insert accountRecord;           
	        
	        //Create a test invoice record 2
	        Invoice__c invoiceRecord3 = new Invoice__c();
	        invoiceRecord3.Account__c = accountRecord.Id; 
	        invoiceRecord3.CurrencyIsoCode = 'USD';
	        invoiceRecord3.Invoice_Status__c = 'Pending';                                                        
	        insert invoiceRecord3;                         
	                
	       	//Create a test time card record 1
	        Time_Card__c timeCardRecord1  = new Time_Card__c();
	        timeCardRecord1.Name = 'Test Time Card';
	        insert timeCardRecord1;
	        
	        Billing_Reason_Code__c billingReason = new Billing_Reason_Code__c(Name = 'Test Billing', Billable__c=true, Non_Billable__c=false, Unbilled_del__c=false);
	        insert billingReason;
	        
	        //Test Project Record   
	        Project__c objProject = new Project__c(Name = 'Test Project', Account__c = accountRecord.id);           
	        objProject.Kickoff__c = Date.today(); 
	        objProject.Hourly_Rate__c = 200;
	        objProject.Go_Live_Date__c =  Date.today().addDays(30);
	        objProject.sendToCashBoard__c = false;      
	        objProject.No_Budget__c = true;
	        objProject.Type__c = 'Other';
	        objProject.Status__c = 'Inactive';      
	        insert objProject;
        
        	//Test ProjectPhase Record       
	        Project_Phase__c phase = new Project_Phase__c();
	        phase.Phase_name__c = 'Build/Config/Testing';
	        phase.Start_Date__c = Date.today();
	        phase.End_Date__c = Date.today().AddDays(dateSetting.NonImplementationEndDateIncrement__c.intValue());
	        phase.Project__c = objProject.id;
	        insert phase;
        
	        //Test Deliverable Record    
	        Deliverable__c objDeliverable = new Deliverable__c(Name = 'Test Deliverable', Project__c = objProject.id);
	        insert objdeliverable;
	        
	        //Test BudgetItem Record 1    
	        Budget_Item__c objBudgetItem1 = new Budget_Item__c(Deliverable__c = objdeliverable.id);
	        insert objbudgetItem1;
        
	        //Create a time entry record
	        Time_Entry__c timeentryrecord1 = new Time_Entry__c();
	        timeentryrecord1.Time_Card__c = timeCardRecord1.Id;  
	        timeentryrecord1.Time__c = 10;
	        timeentryrecord1.CurrencyIsoCode = 'USD';
	        timeentryrecord1.Budget_Item__c = objBudgetItem1.Id;
	        timeentryrecord1.Billing_Reason__c = billingReason.Id;
	        timeentryrecord1.Notes__c = 'Test Notes';
	        insert timeentryrecord1;  
        
			Invoice_Line__c invoiceLineRecord4 = new Invoice_Line__c();
	        invoiceLineRecord4.Invoice__c = invoiceRecord3.Id;       
	        invoiceLineRecord4.CurrencyIsoCode = 'USD'; 
	        invoiceLineRecord4.Time_Entry__c = timeentryrecord1.Id;
	        invoiceLineRecord4.Quantity__c = 10;
	        insert invoiceLineRecord4;       
	        
	        try
	        {               
	            //Update Invoice_Status__c = 'Sent'
	            invoiceRecord3.Invoice_Status__c = 'Sent';   
	            update invoiceRecord3;          

	            //Validation check
	            invoiceLineRecord4.Time_Entry__c = null;    
	            invoiceLineRecord4.Quantity__c = 20;
	            update invoiceLineRecord4;                  
	        }
	        catch(Exception e)
	        {
	            system.debug('Validation error thrown...Exception Caught');
	            System.Assert(e.getMessage().contains('You cannot update the record fields as the Invoice Line Status is not Pending.'));    
	        }      
        }     
    }
    
    /* Created By 'Trekbin' on '10-feb-2016' as per case '25221' */
    /* Update record*/
    static testMethod void myUnitTest5() {
        
         //Test profile record
        Profile testProfile = [SELECT Id FROM Profile WHERE Name = 'System Administrator']; 
        
        //Test user record
        User testUser = new User(Alias = 'standtw4', Email='standarduser@testorg.com', EmailEncodingKey='UTF-8', LastName='Testingw32', LanguageLocaleKey='en_US', LocaleSidKey='en_US', ProfileId = testProfile.Id, 
            				TimeZoneSidKey='America/Los_Angeles', UserName='standarduserw32@testorg.com', Invoice_Override_Permission__c = true);
        insert testUser;
                         
        System.RunAs(testUser) {
        
	        ProjectDateSettings__c dateSetting = new ProjectDateSettings__c(BuildConfigTestingEndDayIncrement__c = 14, BuildConfigTestingStartDayIncrement__c = 15, DesignDiscoveryEndDaysIncrement__c = -1, GoLiveAndPostGoLiveEndDayIncrement__c= 7, NonImplementationEndDateIncrement__c = 30.0);
        	insert dateSetting;
        
	        //Create a test account record
	        Account accountRecord = new Account();
	        accountRecord.Name ='Test My Account 001';
	        accountRecord.Practice__c = 'Southeast';    
	        insert accountRecord;           
	        
	        //Create a test invoice record 2
	        Invoice__c invoiceRecord3 = new Invoice__c();
	        invoiceRecord3.Account__c = accountRecord.Id; 
	        invoiceRecord3.CurrencyIsoCode = 'USD';
	        invoiceRecord3.Invoice_Status__c = 'Pending';                                                        
	        insert invoiceRecord3;                         
	                
	       	//Create a test time card record 1
	        Time_Card__c timeCardRecord1  = new Time_Card__c();
	        timeCardRecord1.Name = 'Test Time Card';
	        insert timeCardRecord1;
	        
	        Billing_Reason_Code__c billingReason = new Billing_Reason_Code__c(Name = 'Test Billing', Billable__c=true, Non_Billable__c=false, Unbilled_del__c=false);
	        insert billingReason;
	        
	        //Test Project Record   
	        Project__c objProject = new Project__c(Name = 'Test Project', Account__c = accountRecord.id);           
	        objProject.Kickoff__c = Date.today(); 
	        objProject.Hourly_Rate__c = 200;
	        objProject.Go_Live_Date__c =  Date.today().addDays(30);
	        objProject.sendToCashBoard__c = false;      
	        objProject.No_Budget__c = true;
	        objProject.Type__c = 'Other';
	        objProject.Status__c = 'Inactive';      
	        insert objProject;
        
        	//Test ProjectPhase Record       
	        Project_Phase__c phase = new Project_Phase__c();
	        phase.Phase_name__c = 'Build/Config/Testing';
	        phase.Start_Date__c = Date.today();
	        phase.End_Date__c = Date.today().AddDays(dateSetting.NonImplementationEndDateIncrement__c.intValue());
	        phase.Project__c = objProject.id;
	        insert phase;
        
	        //Test Deliverable Record    
	        Deliverable__c objDeliverable = new Deliverable__c(Name = 'Test Deliverable', Project__c = objProject.id);
	        insert objdeliverable;
	        
	        //Test BudgetItem Record 1    
	        Budget_Item__c objBudgetItem1 = new Budget_Item__c(Deliverable__c = objdeliverable.id);
	        insert objbudgetItem1;
        
	        //Create a time entry record
	        Time_Entry__c timeentryrecord1 = new Time_Entry__c();
	        timeentryrecord1.Time_Card__c = timeCardRecord1.Id;  
	        timeentryrecord1.Time__c = 10;
	        timeentryrecord1.CurrencyIsoCode = 'USD';
	        timeentryrecord1.Budget_Item__c = objBudgetItem1.Id;
	        timeentryrecord1.Billing_Reason__c = billingReason.Id;
	        timeentryrecord1.Notes__c = 'Test Notes';
	        insert timeentryrecord1;  
        
			Invoice_Line__c invoiceLineRecord4 = new Invoice_Line__c();
	        invoiceLineRecord4.Invoice__c = invoiceRecord3.Id;       
	        invoiceLineRecord4.CurrencyIsoCode = 'USD'; 
	        invoiceLineRecord4.Time_Entry__c = timeentryrecord1.Id;
	        invoiceLineRecord4.Quantity__c = 10;
	        insert invoiceLineRecord4;       
	        
	        try
	        {               
	            //Update Invoice_Status__c = 'Sent'
	            invoiceRecord3.Invoice_Status__c = 'Sent';   
	            update invoiceRecord3;          

	            //Validation check
	            invoiceLineRecord4.Time_Entry__c = null;    
	            invoiceLineRecord4.Quantity__c = 20;
	            update invoiceLineRecord4;   
	            
	            system.assertEquals([Select Id, Time_Entry__c from Invoice_Line__c Where Id = :invoiceLineRecord4.Id].Time_Entry__c, null);
	            system.assertEquals([Select Id, Quantity__c from Invoice_Line__c Where Id = :invoiceLineRecord4.Id].Quantity__c, 20);    
	            
	            //Update Invoice_Status__c = 'Canceled'
	            invoiceRecord3.Invoice_Status__c = 'Canceled';   
	            update invoiceRecord3;  
	            
	            //Updates as we are updating lookup field
	            invoiceLineRecord4.Time_Entry__c = timeentryrecord1.Id;  	            
	            update invoiceLineRecord4;   
	            
	            system.assertEquals([Select Id, Time_Entry__c from Invoice_Line__c Where Id = :invoiceLineRecord4.Id].Time_Entry__c, timeentryrecord1.Id);
	            
	            //Only updating the quantity field without updating lookup fields
	            invoiceLineRecord4.Quantity__c = 50;
	            update invoiceLineRecord4;     
	            
	            /* Last modified by 'Trekbin' on '12-feb-2016' as per the updated case '25221' */
	            system.assertEquals([Select Id, Quantity__c from Invoice_Line__c Where Id = :invoiceLineRecord4.Id].Quantity__c, 50);          
	        }
	        catch(Exception e)
	        {
	            system.debug('Validation error thrown...Exception Caught');
	            
	            //System.Assert(e.getMessage().contains('You cannot update the record fields as the Invoice Line Status is not Pending.'));
	        }      
        }     
    }
}