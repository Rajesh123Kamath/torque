/**
    Class Name : Torque_ExpenseBatch
    Created By : Trekbin
    Created on : 07-march-2016
    Description: Batch Class to process expense records
*/

global class Torque_ExpenseBatch implements Database.Batchable<sObject> {
         
    private Map<Id,Expense__c> mapOfIdToExpense = new map<Id,Expense__c>();
    private Map<Id,Invoice_Line__c> mapExpenseToInvoices = new Map<Id,Invoice_Line__c>();
    private Map<Id,Project__c> mapOfIdToProject = new Map<Id,Project__c>();
    private Set<Id> setProjIds = new Set<Id>();
    
    private Invoice__c objinvoice;
    
    private Id invoiceLineRecordTypeId;
    
    //Batch Constructor
    global Torque_ExpenseBatch(Invoice__c objinvoice, Map<Id,Project__c> mapOfIdToProject, Id invoiceLineRecordTypeId) {
        
        this.objinvoice = objinvoice;
        this.mapOfIdToProject = mapOfIdToProject;
        this.invoiceLineRecordTypeId = invoiceLineRecordTypeId;     
        
        setProjIds = mapOfIdToProject.keyset();
    }
    
    //Start Method
    global Database.QueryLocator start(Database.BatchableContext BC) {
                
        //Forming a query string for timeentry        
        String tmpQuery = 'SELECT Id, Project__c, Project__r.Account__c, Invoice_Comment__c, Date__c, Amount__c, Expense_Report__r.OwnerId FROM Expense__c ';
        tmpQuery += 'WHERE Project__c IN : setProjIds ';
        tmpQuery += 'AND Billable__c = TRUE ';
        tmpQuery += 'AND Approved__c = \'TRUE\' ';
        tmpQuery += 'AND Invoice_Line__c = NULL ';
        
        //Return the query to execute method      
        return Database.getQueryLocator(tmpQuery);
    }
   
    //Execute Method
    global void execute(Database.BatchableContext BC, List<Expense__c> lstExpenseRecords) {
        
        try {
            
            //get the Expenses related to Project
            for(Expense__c objExpense : lstExpenseRecords) {
                                                
                mapOfIdToExpense.put(objExpense.Id,objExpense);

                Invoice_Line__c objInvoiceLine = new Invoice_Line__c(Invoice__c = objinvoice.Id,
                                                                     Gears_Expense__c = objExpense.Id,
                                                                     Date_Delivered__c = objExpense.Date__c,
                                                                     Invoice_Line_Type__c = 'Expense',
                                                                     Performed_By__c = objExpense.Expense_Report__r.OwnerId,
                                                                     Price_Per__c = objExpense.Amount__c,
                                                                     Quantity__c = 1,
                                                                     Invoice_Line_Notes__c = objExpense.Invoice_Comment__c,
                                                                     RecordTypeId = invoiceLineRecordTypeId     
                                                                    );
                if(objExpense.Amount__c!=null) {
                    
                    objInvoiceLine.Total_Price__c = (objInvoiceLine.Price_Per__c * objInvoiceLine.Quantity__c);
                }
            
                mapExpenseToInvoices.put(objExpense.Id,objInvoiceLine); 
            }
        
       
            //Insert Invoice Lines related to Project
            if(!mapExpenseToInvoices.isEmpty())
                insert mapExpenseToInvoices.values();
        
            //Finding the Expense__c record where Expense__c.Id = Invoice_Line.Expense__c and setting Expense__c.Invoice_Line__c = Invoice_Line.Id
            for(Invoice_Line__c objInvoiceLine : mapExpenseToInvoices.values()) {
                
                if(objInvoiceLine.Gears_Expense__c != NULL && objInvoiceLine.Invoice_Line_Type__c == 'Expense'){
                    
                    if(mapExpenseToInvoices.containskey(objInvoiceLine.Gears_Expense__c)){
                        
                        mapOfIdToExpense.get(objInvoiceLine.Gears_Expense__c).Invoice_Line__c = mapExpenseToInvoices.get(objInvoiceLine.Gears_Expense__c).Id;
                    }
                }           
            }
        
            //update Expenses
            if(!mapOfIdToExpense.isEmpty())
                update mapOfIdToExpense.values();    
            
        }
        catch(Exception ex) {
            
            system.debug('!!!!!!!!!!!!!!!!!!!!!!!!!! EXCEPTION : '+ex.getMessage());
        }
    }
    
    //Finish Method
    global void finish(Database.BatchableContext BC) {
        
        //Call one more batch for processing expense records
         system.debug('!!!!!!!!!!!!!!!COMPLETED!!!!!!!!!!! ');
    }
}