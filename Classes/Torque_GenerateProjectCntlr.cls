/**
    @Name           :   Torque_GenerateProjectCntlr
    @Date           :   17 Feb, 2016
    @Description    :   An apex page controller for Torque_GenerateProject page.
    @TestClass      :   Torque_GenerateProjectCntlrTest.cls
    @lastModifiedBy : 	Trekbin on 10 Oct, 2016 - Case 00026497.
*/
public with sharing class Torque_GenerateProjectCntlr {
    
    /*Variable declaration - start*/
    
    //Added byTrekbin on 10 Oct, 2016 - Case 00026497.
    public Opportunity opp 																{ get;set; }
    public Project__c objProject                                        		    	{ get;set; }
    
    public List<WrapperDeliverableToLstBItems> lstWrapperDeliverableToLstBItems     	{ get;set; }      //Main wrapper list to be used in case of deliverable for page binding
    public List<WrapperBudgetItemToLstResBudget> lstWrapperBudgetItemToResourceBudget	{ get;set; }      //Wrapper list to be used in case of Budget Item for page binding
    
    private Integer rowNumOfDeliverable;                                      			
	private Integer rowNumOfParentDeliverableForBI;	                                    
    private Integer rowNumOfBudgetItem;			                                   	      
    private Integer rowNumOfResourceBudget;											     
    
    /*Variable declaration - end*/
    
    /**
        @Name           :   Torque_GenerateProjectCntlr 
        @Description    :   Main class constructor.
    */
    public Torque_GenerateProjectCntlr(ApexPages.StandardController stdController) {
        
        //Added byTrekbin on 10 Oct, 2016 - Case 00026497.
        opp = [	SELECT Account.Name, Hourly_Rate__c, Project__c FROM Opportunity WHERE ID = :stdController.getRecord().Id];
        
        if(opp.Project__c != null) {
	        /* Fetch the values from the URL once the enduser clicks on "Generate Project" button from the opportunity detail page */
	        objProject = [Select Id, Name, Minimum_Budget__c, Max_Budget__c, CreatedById, CreatedBy.Name From Project__c Where Id = :opp.Project__c];       
	             														
	 		//Initialize the project object        
	        rowNumOfDeliverable = rowNumOfParentDeliverableForBI = rowNumOfBudgetItem = rowNumOfResourceBudget = 0;
	        
	        lstWrapperDeliverableToLstBItems = new List<WrapperDeliverableToLstBItems >();					//Initialize the main wrapper list
	        lstWrapperBudgetItemToResourceBudget = new List<WrapperBudgetItemToLstResBudget>();				//Initialize the main wrapper list
        }
    }
   
    /**
        @Name           :   addDeliverableToSection
        @Description    :   adds deliverable, budgetItem and resourceProject records to a wrapper list for page binding
    */
    public void addDeliverableToSection()
    {
        //Initialization of wrapper instances
        lstWrapperBudgetItemToResourceBudget = new List<WrapperBudgetItemToLstResBudget>();        
        List<WrapperResBudgetToRowCounter> lstWrapperResourceBudgetToCounterVar = new List<WrapperResBudgetToRowCounter>();
        
        //Wrapper lst to hold ResourceBudget To CounterVar
        lstWrapperResourceBudgetToCounterVar.add(new WrapperResBudgetToRowCounter(new Resource_Budget__c(), 0));
        
        //Wrapper lst to hold budgetItem and list of resourceBudget wrapper & 0 in order to set the counterWrapVarForDeliverable counter to 0 at new creation
        lstWrapperBudgetItemToResourceBudget.add(new WrapperBudgetItemToLstResBudget(new Budget_Item__c(), lstWrapperResourceBudgetToCounterVar, 0));
        
        //Instantiate the main wrapper class with blank deliverable, budgetItem and resourceBudget instances to be displayed in the page for adding into a list 
        WrapperDeliverableToLstBItems objWrapperDeliverableToLstBItems = new WrapperDeliverableToLstBItems(new Deliverable__c(), lstWrapperBudgetItemToResourceBudget);
        
        //Increment the row count for the newly instantiated wrapper deliverable 
        rowNumOfDeliverable++;       
        
        //Assign the deliverable count to the main wrapper class variable 'counterWrapVarForDeliverable'
        objWrapperDeliverableToLstBItems.counterWrapVarForDeliverable = rowNumOfDeliverable;
         
        //Add the the main wrapper class instance to the Main wrapper list to be binded to the VF Page with blank deliverable, budgetItem and resourceBudget instances to be displayed in the page 
        lstWrapperDeliverableToLstBItems.add(objWrapperDeliverableToLstBItems);
    }
    
    /**
        @Name           :   removeDeliverableFromTheSection
        @Description    :   removes the deliverable and its related child objects i.e. BudgetItem & ResourceBudget from the section in the page
    */
    public void removeDeliverableFromTheSection()
    {
        //Fetch the deliverable row index to delete
        rowNumOfDeliverable = Integer.valueOf(Apexpages.currentPage().getParameters().get('deliverableIndex'));
        
        //Remove the deliverable from the wrapperList that we are using to bind in the page
        if(!lstWrapperDeliverableToLstBItems.isEmpty())
        {
	        for(Integer i = 0; i < lstWrapperDeliverableToLstBItems.size(); i++){
	        	if(rowNumOfDeliverable != null && lstWrapperDeliverableToLstBItems[i].counterWrapVarForDeliverable == rowNumOfDeliverable)        
	        		lstWrapperDeliverableToLstBItems.remove(i); 
	        }  
	        
	        //Decrement the counter
       		rowNumOfDeliverable--;
        }
    }
     
    /**
        @Name           :   addBudgetItemToDeliverable
        @Description    :   adds budgetItem and resourceProject record to a wrapper list for page binding
    */
    public void addBudgetItemToDeliverable()
    {
     	//Fetch the index of the parent record where the budgetItem needs to be added
     	rowNumOfParentDeliverableForBI = Integer.valueOf(Apexpages.currentPage().getParameters().get('parentDeliverableIndex'));   
     	
     	//Initialize
     	List<WrapperResBudgetToRowCounter> lstWrapperResourceBudgetToCounterVar = new List<WrapperResBudgetToRowCounter>();
     	
     	//wrapper list to hold resourceBudget record and providing record counter to it as 0
        lstWrapperResourceBudgetToCounterVar.add(new WrapperResBudgetToRowCounter(new Resource_Budget__c (), 0));
        
        //Instantiate child wrapper 'WrapperBudgetItemToLstResBudget' of the main wrapper wrapping budgetItem to list of resourceBudget records along with the size of the budgetItem wrapper list as the latest index
        WrapperBudgetItemToLstResBudget objWrapperBudgetItemToLstResBudget = new WrapperBudgetItemToLstResBudget(new Budget_Item__c(), lstWrapperResourceBudgetToCounterVar, lstWrapperDeliverableToLstBItems.get(rowNumOfParentDeliverableForBI).lstWrapperBudgetItemToResourceBudget.size());
        
        //list to bind to the page side
     	lstWrapperDeliverableToLstBItems.get(rowNumOfParentDeliverableForBI).lstWrapperBudgetItemToResourceBudget.add(objWrapperBudgetItemToLstResBudget);
	     	
    }
    
    /**
        @Name           :   removeBudgetItemFromTheDeliverable
        @Description    :   removes the budgetItem and its related child object i.e. ResourceBudget for the deliverable from the section in the page
    */
    public void removeBudgetItemFromTheDeliverable()
    {
        //Fetch the parent deliverable index from where the child has to be removed
        rowNumOfParentDeliverableForBI = Integer.valueOf(Apexpages.currentPage().getParameters().get('parentDeliverableIndexToRemove'));
        
        //Fetch the budget item row index to delete
        rowNumOfBudgetItem = Integer.valueOf(Apexpages.currentPage().getParameters().get('budgetItemIndex'));
        
        //Remove the budgetItem from the wrapperListOfBudgetItems
		lstWrapperDeliverableToLstBItems.get(rowNumOfParentDeliverableForBI).lstWrapperBudgetItemToResourceBudget.remove(rowNumOfBudgetItem);   
	    
	    
	    //For changing the row indexes of all the records after the particular record has been removed from the list
	    for(Integer i=rowNumOfBudgetItem+1; i<= lstWrapperDeliverableToLstBItems.get(rowNumOfParentDeliverableForBI).lstWrapperBudgetItemToResourceBudget.size(); i++)
	    {
	    	lstWrapperDeliverableToLstBItems.get(rowNumOfParentDeliverableForBI).lstWrapperBudgetItemToResourceBudget.get(i-1).counterWrapVarForBudgetItem = i-1;
	    }    
    }
    
    /**
        @Name           :   addResourceBudgetToBudgetItem
        @Description    :   adds ResourceBudget record to a wrapper list for page binding
    */
    public void addResourceBudgetToBudgetItem()
    {
        //Fetch the deliverable index i.e. grandparent pertaining to the relevant budgetItem i.e. parent
        rowNumOfParentDeliverableForBI = Integer.valueOf(Apexpages.currentPage().getParameters().get('parentDeliverableIndex')); 
       
		//Fetch the respective budgetItem record under which a resourceBudget record needs to be added on click of the add link
		rowNumOfBudgetItem = Integer.valueOf(Apexpages.currentPage().getParameters().get('parentBudgetItemIndex')); 
        
        //Add a new resourceBudget empty instance to a parent budgetItem relevant to the repective grandParent deliverable record along with the size for the latest indexing number
        lstWrapperDeliverableToLstBItems.get(rowNumOfParentDeliverableForBI).lstWrapperBudgetItemToResourceBudget.get(rowNumOfBudgetItem).lstWrapperResourceBudgetToCounterVar.add(new WrapperResBudgetToRowCounter(new Resource_Budget__c (), lstWrapperDeliverableToLstBItems.get(rowNumOfParentDeliverableForBI).lstWrapperBudgetItemToResourceBudget.get(rowNumOfBudgetItem).lstWrapperResourceBudgetToCounterVar.size()));
    
    }
     
    /**
        @Name           :   removeResourceBudgetFromTheBudgetItem
        @Description    :   removes ResourceBudget record  i.e. ResourceBudget for the BudgetItem from the section in the page
    */
    public void removeResourceBudgetFromTheBudgetItem()
    {
		//Fetch the deliverable index 
        rowNumOfParentDeliverableForBI = Integer.valueOf(Apexpages.currentPage().getParameters().get('parentDeliverableIndex')); 
       
		//Fetch the parentBudgetItemIndex pertaining to the grandparent record i.e. deliverable
		rowNumOfBudgetItem = Integer.valueOf(Apexpages.currentPage().getParameters().get('parentBudgetItemIndex')); 	
		
		//Fetch the resource budget record which needs to be removed from the parent-grandparent list
		rowNumOfResourceBudget = Integer.valueOf(Apexpages.currentPage().getParameters().get('resourceBudgetIndex')); 	
		
		//Remove the respective resourceBudget record from the list of resourceBudgets
		lstWrapperDeliverableToLstBItems.get(rowNumOfParentDeliverableForBI).lstWrapperBudgetItemToResourceBudget.get(rowNumOfBudgetItem).lstWrapperResourceBudgetToCounterVar.remove(rowNumOfResourceBudget);   
    
    	//For changing the row indexes of all the records after the particular record has been removed from the list
	    for(Integer i=rowNumOfResourceBudget+1; i<= lstWrapperDeliverableToLstBItems.get(rowNumOfParentDeliverableForBI).lstWrapperBudgetItemToResourceBudget.get(rowNumOfBudgetItem).lstWrapperResourceBudgetToCounterVar.size(); i++)
	    {
	    	lstWrapperDeliverableToLstBItems.get(rowNumOfParentDeliverableForBI).lstWrapperBudgetItemToResourceBudget.get(rowNumOfBudgetItem).lstWrapperResourceBudgetToCounterVar.get(i-1).counterWrapVarForResourceBudget = i-1;
	    }    
          
    }
    
    /**
        @Name           :   insertRecordsToDatabase
        @Description    :   Insert the project into the database.
    */
    public void insertRecordsToDatabase()
    {
        //List of project records to insert
        List<WrapperResBudgetToRowCounter> lstResourceBudgetsInWrapper = new List<WrapperResBudgetToRowCounter>();
        List<Deliverable__c> lstDeliverableToInsert = new List<Deliverable__c>();
        List<Budget_Item__c> lstBudgetItemToInsert = new List<Budget_Item__c>();
        List<Resource_Budget__c> lstResourceBudgetToInsert = new List<Resource_Budget__c>();
        Map<Deliverable__c, List<Budget_Item__c>> mapdeliverableToLstBudgetItems = new Map<Deliverable__c, List<Budget_Item__c>>();
        Map<Budget_Item__c, List<Resource_Budget__c>> mapBIToLstResBudgets = new Map<Budget_Item__c, List<Resource_Budget__c>>();
         
        
    	//Iterate over the main wrapper list that has Deliverable records with user inputed vales binded from page side
        for(WrapperDeliverableToLstBItems objWrapperDeliverableToLstBItems : lstWrapperDeliverableToLstBItems)
        {
        	//Create an instance and bind each deliverable to its related project record and add the deliverable to list for insertion
        	objWrapperDeliverableToLstBItems.objDeliverable.Project__c = objProject.Id;
        	lstDeliverableToInsert.add(objWrapperDeliverableToLstBItems.objDeliverable);
        }
         
        //Insert the deliverable record list
        if(!lstDeliverableToInsert.isEmpty())
        {
		    insert lstDeliverableToInsert;
		    		     
		    //Iterate over the main wrapper list that has Deliverable records with user inputed vales binded from page side
	        for(WrapperDeliverableToLstBItems objWrapperDeliverableToLstBItems : lstWrapperDeliverableToLstBItems)
	        {
	        	//Iterate over the budgetItem wrapper(Child wrapper)
	        	for(WrapperBudgetItemToLstResBudget objWrapperBudgetItems : objWrapperDeliverableToLstBItems.lstWrapperBudgetItemToResourceBudget)
	        	{	
	        		//Create a map of deliverable object[not ID bcoz we wont be getting ID as its not inserted yet] to list of BudgetItems
	        		if(!mapdeliverableToLstBudgetItems.containsKey(objWrapperDeliverableToLstBItems.objDeliverable))
	        			mapdeliverableToLstBudgetItems.put(objWrapperDeliverableToLstBItems.objDeliverable, new List<Budget_Item__c>());
	        		
	        		mapdeliverableToLstBudgetItems.get(objWrapperDeliverableToLstBItems.objDeliverable).add(objWrapperBudgetItems.objBudgetItem);
	        	}	
	        }
	        
		    //Iterate over all the deliverables in the list     
		    for(Deliverable__c objDeliverableToIterate : lstDeliverableToInsert)
		    {
		    	//Iterate over all the budgetItems from the map created above related to a particular deliverable
		    	for(Budget_Item__c objBudgetItemToIterate : mapdeliverableToLstBudgetItems.get(objDeliverableToIterate))
		    	{	
		    		//Assign deliverable parent to the budgetItem and add to the list        	
		        	objBudgetItemToIterate.Deliverable__c = objDeliverableToIterate.Id;	   
		        	objBudgetItemToIterate.Project__c = objProject.Id;     	
		        	lstBudgetItemToInsert.add(objBudgetItemToIterate);        	
		    	}
		    }
		    
		    //Insert the BudgetItem record list
		 	if(!lstBudgetItemToInsert.isEmpty())
		 	{
		        insert lstBudgetItemToInsert;
		        
		     	for(WrapperDeliverableToLstBItems objWrapperDeliverableToLstBItems : lstWrapperDeliverableToLstBItems)
        		{        
			        for(WrapperBudgetItemToLstResBudget objWrapperBudgetItems : objWrapperDeliverableToLstBItems.lstWrapperBudgetItemToResourceBudget)
		        	{
		        		//Iterate over all the resource budgets in the wrapper
		        		for(WrapperResBudgetToRowCounter objWrapperResBudgetToRowCounter : objWrapperBudgetItems.lstWrapperResourceBudgetToCounterVar)
		        		{
		        			//Create a map of budgetItem to lst of resourceBudgets
		        			if(!mapBIToLstResBudgets.containsKey(objWrapperBudgetItems.objBudgetItem))
		        				mapBIToLstResBudgets.put(objWrapperBudgetItems.objBudgetItem, new List<Resource_Budget__c>());
		        				
		        			mapBIToLstResBudgets.get(objWrapperBudgetItems.objBudgetItem).add(objWrapperResBudgetToRowCounter.objResourceBudget);
		        		}        		
		        	}
        		}
        		
			    //Iterate over BudgetItems in the list  
				for(Budget_Item__c objBudgetItemToIterate : lstBudgetItemToInsert)
				{	    
					if(mapBIToLstResBudgets.get(objBudgetItemToIterate) != null)
					{
						//Iterate over all the resource budgets from the map created above related to a particular budgetItem	
						for(Resource_Budget__c objResourceBudgetToIterate : mapBIToLstResBudgets.get(objBudgetItemToIterate))
						{
							//Assign budgetItem as parent to the resource budget and add to the list
							objResourceBudgetToIterate.Budget_Item__c = objBudgetItemToIterate.Id;	
							
							if(test.isRunningTest())
								objResourceBudgetToIterate.Assigned_User__c = userInfo.getUserId();
							
							lstResourceBudgetToInsert.add(objResourceBudgetToIterate);
						}
					}
				}      
				
				//Insert the ResourceBudget record list to insert             
			    if(!lstResourceBudgetToInsert.isEmpty())
			        insert lstResourceBudgetToInsert;
	        }
	    }
    
    	//Redirect to the project detail page
    	/*String url = System.URL.getSalesforceBaseUrl().toExternalForm();
        return new PageReference(url + '/' +objProject.Id);*/
        	
    }
    
    /**
        @Name           :   cancelAndRedirectToOppty
        @Description    :   Cancel and return back to the opportunity detail page
    */
    public PageReference cancelAndRedirectToOppty()
    {
    	//We are getting the base url of the page and redirecting it to the opportunity detail page by passing OPPORTUNITY Id
        String url = System.URL.getSalesforceBaseUrl().toExternalForm();       
        return new PageReference(url + '/' +opp.ID);          
    }
    
     /**
        @Name           :   WrapperResBudgetToRowCounter
        @Description    :   wrapper class to bind ResourceBudget record to the row counter
    */
    public class WrapperResBudgetToRowCounter {
        
   		public Resource_Budget__c objResourceBudget 		{ get;set; }
        
        public Integer counterWrapVarForResourceBudget		{ get;set; } 			
        
        //Wrapper class constructor 1, used to bind each ResourceBudget record with a counter 
        public WrapperResBudgetToRowCounter(Resource_Budget__c objResourceBudget, Integer counterWrapVarForResourceBudget) {
            
            this.objResourceBudget = objResourceBudget;
            this.counterWrapVarForResourceBudget = counterWrapVarForResourceBudget;
        }
    }
    
     /**
        @Name           :   WrapperBudgetItemToLstResBudget
        @Description    :   Wrapper class which we are binding into the main wrapper class
        					This wrapper class is for wrapping BudgetItem(Master of ResourceBudget) and list of its ResourceBudgets(Childs of BudgetItem)
    */
    public class WrapperBudgetItemToLstResBudget {
        
   		public Budget_Item__c objBudgetItem												{ get;set; }
   		
   		public List<WrapperResBudgetToRowCounter> lstWrapperResourceBudgetToCounterVar 	{ get;set; }
        
        public Integer counterWrapVarForBudgetItem										{ get;set; } 			//Counter variable for adding and removing budgetItem record from the wrapperlist
        
        //Wrapper class constructor 1, used to bind each budgetItem and list of resourceBudget objects along with a count var 
        public WrapperBudgetItemToLstResBudget(Budget_Item__c objBudgetItem, List<WrapperResBudgetToRowCounter> lstWrapperResourceBudgetToCounterVar, Integer counterWrapVarForBudgetItem) {
            
            this.objBudgetItem = objBudgetItem;
            this.lstWrapperResourceBudgetToCounterVar = lstWrapperResourceBudgetToCounterVar;
            this.counterWrapVarForBudgetItem = counterWrapVarForBudgetItem;
        }
    }
    
    /**
        @Name           :   WrapperDeliverableToLstBItems
        @Description    :   Main wrapper class that we will be using to bind in the page
        					This wrapper class is used to wrap Deliverable(Master of BudgetItem) and the above created WrapperBudgetItemToLstResBudget(wrapper class 
        					of BudgetItem(Child of Deliverable) and list of ResourceBudget(Child of BudgetItem)
    */
    public class WrapperDeliverableToLstBItems {
        
		public Deliverable__c objDeliverable												{ get;set; }
	        
        public List<WrapperBudgetItemToLstResBudget> lstWrapperBudgetItemToResourceBudget 	{ get;set; }
        
        public Integer counterWrapVarForDeliverable											{ get;set; }	//Counter variable for adding and removing deliverable record from the wrapperlist
               
        //Wrapper class constructor 1, used to bind each deliverable to the child wrapper class "WrapperBudgetItemToLstResBudget"
        public WrapperDeliverableToLstBItems(Deliverable__c objDeliverable, List<WrapperBudgetItemToLstResBudget> lstWrapperBudgetItemToResourceBudget) {
            
            this.objDeliverable = objDeliverable;
            this.lstWrapperBudgetItemToResourceBudget = lstWrapperBudgetItemToResourceBudget;
        }
    }
}