/**
    Class Name : Torque_TimeEntryBatch
    Created By : Trekbin
    Created on : 07-march-2016
    Description: Batch Class to process time entry records
*/

global class Torque_TimeEntryBatch implements Database.Batchable<sObject> {
         
    private Invoice__c objinvoice;
    private Date dateFilter;
    private Id invoiceLineRecordTypeId;
    
    private Map<Id,Time_Entry__c> mapOfIdToTimeEntry = new Map<Id,Time_Entry__c>();
    private Map<Id,Invoice_Line__c> mapTimeEntryToInvoices = new Map<Id,Invoice_Line__c>();
    private Map<Id,Project__c> mapOfIdToProject = new Map<Id,Project__c>();
    private Set<Id> setSelectedProjIds = new Set<Id>();
    
    //Batch Constructor
    global Torque_TimeEntryBatch(Invoice__c objinvoice, Map<Id,Project__c> mapOfIdToProject, Set<Id> setSelectedProjIds, Date dateFilter, Id invoiceLineRecordTypeId) {
        
        this.objinvoice = objinvoice;
        this.mapOfIdToProject = mapOfIdToProject;
        this.setSelectedProjIds = setSelectedProjIds;
        this.dateFilter = dateFilter;  
        this.invoiceLineRecordTypeId = invoiceLineRecordTypeId;                   
    }
    
    //Start Method
    global Database.QueryLocator start(Database.BatchableContext BC) {
          
        //Forming a query string for timeentry        
        String tmpQuery = 'SELECT Id, Project__c, Project__r.Account__c, Date__c, Performed_By__c, Time__c, Billable__c, Notes__c, Case_Number__c, Deliverable__c, Budget_Item__c, Budget_Item__r.Hourly_Rate__c '; 
        tmpQuery += 'FROM Time_Entry__c ';
        tmpQuery += 'WHERE Project__c IN : setSelectedProjIds ';
        tmpQuery += 'AND ((Billable__c = TRUE AND Unbilled__c = FALSE) OR Display_on_Invoice__c = TRUE) ';
        tmpQuery += 'AND Invoice_Line__c = NULL AND Migrated_from_CB__c = FALSE ';
        tmpQuery += dateFilter != null ? 'AND Date__c <= :dateFilter ' : ' ';
        
        //Return the query to execute method      
        return Database.getQueryLocator(tmpQuery);
    }
   
    //Execute Method
    global void execute(Database.BatchableContext BC, List<Time_Entry__c> lstTimeEntryRecords) {
        
        try {
            
            // get Time entries for each project
            for(Time_Entry__c objTimeEntry : lstTimeEntryRecords) {
             
                mapOfIdToTimeEntry.put(objTimeEntry.Id, objTimeEntry);
                
                Invoice_Line__c objInvoiceLine = new Invoice_Line__c(Invoice__c = objinvoice.Id,
                                                                     Time_Entry__c = objTimeEntry.Id,
                                                                     Date_Delivered__c = objTimeEntry.Date__c,
                                                                     Invoice_Line_Type__c = 'Time Entry',
                                                                     Performed_By__c = objTimeEntry.Performed_By__c,
                                                                     //Price_Per__c = mapOfIdToProjectRecord.get(objTimeEntry.Project__c).Hourly_Rate__c,
                                                                     Price_Per__c = objTimeEntry.Budget_Item__r.Hourly_Rate__c, //updated 14/1/2016 ap per requirement
                                                                     Quantity__c = objTimeEntry.Time__c,
                                                                     Billable__c = objTimeEntry.Billable__c,
                                                                     Invoice_Line_Notes__c = objTimeEntry.Notes__c,
                                                                     Case_Number__c = objTimeEntry.Case_Number__c,
                                                                     Deliverable__c = objTimeEntry.Deliverable__c,
                                                                     Budget_Item__c = objTimeEntry.Budget_Item__c,
                                                                     RecordTypeId = invoiceLineRecordTypeId 
                                                                    );
                if(objTimeEntry.Budget_Item__r.Hourly_Rate__c !=null) {
                    
                    objInvoiceLine.Total_Price__c = (objTimeEntry.Budget_Item__r.Hourly_Rate__c * objTimeEntry.Time__c);
                }
                
                mapTimeEntryToInvoices.put(objTimeEntry.Id, objInvoiceLine);
                
            }
        
        
        //insert InvoiceLines for Time entry
        if(!mapTimeEntryToInvoices.isEmpty()) 
            insert mapTimeEntryToInvoices.values(); 
            
        //Finding the Time_Entry record where Time_Entry.Id = Invoice_Line.Time_Entry__c and mapping Time_Entry.Invoice_Line__c = Invoice_Line.Id   
            
        for(Invoice_Line__c objInvoiceLine : mapTimeEntryToInvoices.values()) {
            
            if(objInvoiceLine.Time_Entry__c != NULL && objInvoiceLine.Invoice_Line_Type__c == 'Time Entry') {
                
                if(mapTimeEntryToInvoices.containskey(objInvoiceLine.Time_Entry__c)) {
                    
                    mapOfIdToTimeEntry.get(objInvoiceLine.Time_Entry__c).Invoice_Line__c = mapTimeEntryToInvoices.get(objInvoiceLine.Time_Entry__c).Id;
                }
            }        
        }
        
        //updating TimeEntry
        if(!mapOfIdToTimeEntry.isEmpty())
            update mapOfIdToTimeEntry.values();           
        }
        
        catch(Exception ex) {
            
            system.debug('!!!!!!!!!!!!!!!!!!!!!!!!!! EXCEPTION : '+ex.getMessage());
        }
    }
    
    //Finish Method
    global void finish(Database.BatchableContext BC) {
        
        
        /**         
            @CreatedBy  : Trekbin
            @CreatedOn  : 7-March-2016
            @Description: Call one more batch for processing expense records
        */ 
        Torque_ExpenseBatch objExpenseBatch = new Torque_ExpenseBatch(objinvoice, mapOfIdToProject, invoiceLineRecordTypeId );
        Database.executeBatch(objExpenseBatch, 50);
    }
}