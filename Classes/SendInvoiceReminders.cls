/* ************************************************************
 * Created By    : Raja Y(GearsCRM)
 * Created Date  : 11/25/2015 
 * Description   : ABBA Invoicing - Send Invoice Reminders
 *
 * Modified By   :   
 * Modified Date :  
 * Description   :
 * 
 * ************************************************************/
global class SendInvoiceReminders implements database.batchable<sobject>,schedulable 
{
    InvoiceDefaults__c idef = InvoiceDefaults__c.getinstance();  
    list<EmailServicesAddress> emailServiceAddr = new list<EmailServicesAddress>();
    string emailServAddress;
    
    private String GetQueryLocatorString()
    {
        string invoiceQuery = 'select id,name,Account__c, send_reminders__c,Next_Reminder_Date__c,Billing_Contact__c,Customer_Emails__c, RecordTypeId from   Invoice__c' +
                                     ' where   Invoice__c.Send_Reminders__c = TRUE' + 
                                    ' and Invoice__c.Next_Reminder_Date__c = Yesterday' +   
                                       ' and Invoice__c.Payment_Status__c <> \'Paid In Full\''; 
                                     
        return invoiceQuery;
    } 
     
    global SendInvoiceReminders() 
    { 
        system.debug('____emailServAddress___' + emailServAddress);
        emailServiceAddr = [select localPart, EmailDomainName from EmailServicesAddress where localpart = 'SendInvoiceReminders' LIMIT 1];
        
        if (emailServiceAddr != null && emailServiceAddr.size() > 0)
        {
            emailServAddress = emailServiceAddr[0].localPart + '@' + emailServiceAddr[0].EmailDomainName;
        }
         system.debug('____emailServAddress___' + emailServAddress);
     }
 
    global database.Querylocator start(database.batchableContext BC)
    { 
        return Database.getquerylocator(GetQueryLocatorString());    
    }
 
    global void execute(database.BatchableContext bc , list<sobject> scope)
    {
        // Variables Declaration
        
        list<Invoice_Comment__c> invCommentsList = new list<Invoice_Comment__c>();
        list<invoice__c> invList = new list<Invoice__c>();
        list<Invoice__c> updateInvList = new list<Invoice__c>();
        List<Messaging.singleEmailMessage> mails = new List<Messaging.singleEmailMessage>();
        map<id,list<contact>> invContactMap = new map<id,list<contact>>();
        map<id,list<id>> invContactIdMap = new map<id,list<id>>();
        list<contact> contacttoInsert = new list<contact>();
        map<id,attachment> mapInvPdf = new map<id,attachment>();
       
        //Invoice mandatory fields are Account, Project and SendReminders (true) checkbox 
        string defaultEmailTemplate = 'Initial_Invoice_Notification_Placeholder';
        invList.addall((list<Invoice__c>)scope);
        list<Emailtemplate> et = new list<Emailtemplate>();
        
  
        if(idef.Reminder_Invoice_Email_Template__c == null)
        {
            et =[select id,developername from emailtemplate where developername = :defaultEmailTemplate LIMIT 1];
        }
        else
        {
            et =[select id,developername from emailtemplate where id = :idef.Reminder_Invoice_Email_Template__c LIMIT 1];
        }
       
        User u = [select firstName,lastName from user where id=:userinfo.getuserid()];
        String userExecButton = u.firstName + ' ' + u.LastName;
        
        
        if(!invList.isempty())
        {
            for(attachment invAttachment : [select id,name,parentId, body, bodylength,LastModifiedDate from attachment where parentId in :invList order by LastModifiedDate desc limit 1])
            {
                if(!mapInvPdf.containsKey(invAttachment.parentId))
                {
                    mapInvPdf.put(invAttachment.parentId,invAttachment);
                }
             }
        }
        
        if(!invList.isempty())
        {	
        	/* Added by Trekbin on 14 July, 2016 - Case  00026365 */
        	Map<Id, Id> invoiceRecordTypeToOrgWideEmailIdMap = new Map<Id, Id>();
        	
        	for(Torque_Invoice_Record_Type__mdt objCustomMetadataType : [	SELECT OrgWideEmailID__c, InvoiceRecordTypeID__c
																			FROM Torque_Invoice_Record_Type__mdt 
																		]) {
														
				invoiceRecordTypeToOrgWideEmailIdMap.put(objCustomMetadataType.InvoiceRecordTypeID__c, objCustomMetadataType.OrgWideEmailID__c);								
			}
			
			system.debug('-----invoiceRecordTypeToOrgWideEmailIdMap----' + invoiceRecordTypeToOrgWideEmailIdMap);
										
            for(Invoice__c lstInv : invList)
            {
                list<string> emailCollection;
                if(lstInv.customer_emails__c !=null)
                {
                    emailCollection = lstInv.customer_emails__c.split(',');
                }
                
                // Create a new Email
                list<string> Emails =new list<string>();
         
                // Set who the email is sent from
                system.debug('_______invContactidMap_____' + invContactidMap);
                system.debug('_______lstInv.id____' + lstInv.id);
                system.debug('_______invContactidMap.containskey(lstInv.id)_____' + invContactidMap.containskey(lstInv.id));
                system.debug('_______invContactidMap.get(lstInv.id)_____' + invContactidMap.get(lstInv.id));
                
                if(lstInv.Billing_Contact__c != null)
                {
                    system.debug('______lstInv.id1_____' + lstInv.id);
                    Messaging.singleEmailMessage mail = new Messaging.singleEmailMessage();
                    system.debug('____emailServAddress____' + emailServAddress);
                    if(emailServAddress != null)
                       mail.setReplyTo(emailServAddress);
                    
                    /* Added by Trekbin on 14 July, 2016 - Case  00026365 */
                    if(invoiceRecordTypeToOrgWideEmailIdMap.containsKey(lstInv.RecordTypeId) && invoiceRecordTypeToOrgWideEmailIdMap.get(lstInv.RecordTypeId) != null)
                    	mail.setOrgWideEmailAddressId(invoiceRecordTypeToOrgWideEmailIdMap.get(lstInv.RecordTypeId));
                    
                    system.debug('____et____' + et);
                    
                    if(et.size() > 0 && et != null)
                       mail.settemplateid(et[0].id);
                    mail.setTargetObjectId(lstInv.Billing_Contact__c); 
                    
                   // mail.setplaintextbody('Test test');
                    system.debug('____lstInv.Billing_Contact__c____' + lstInv.Billing_Contact__c);
                    system.debug('____emailCollection____' + emailCollection);
                   
                    if(emailCollection != null) 
                        mail.setCCAddresses(emailCollection);                     
                    mail.setwhatid(lstInv.id);
                    if(mapInvPdf.containskey(lstInv.id) && mapInvPdf.get(lstInv.id) != null)
                    {
                        Messaging.EmailFileAttachment attach = new Messaging.EmailFileAttachment();
                        attach.setContentType('application/pdf');
                         system.debug('______lstInv.id2_____' + lstInv.id);
                   
                        attach.setFileName(mapInvPdf.get(lstInv.id).name);
                        attach.setInline(false);
                        attach.Body = mapInvPdf.get(lstInv.id).body;
                      
                        mail.setFileAttachments(new Messaging.EmailFileAttachment[] { attach });
                    }
                    mails.add(mail);
                  
                 // Update Next Reminder date in Invoice
                    invoice__c invoiceTempvar = new invoice__c();
                
                    Date NextRmndrDate = date.Today() + 10;
             
                    if(Idef.Days_Between_Reminders__c == null)
                    {
                        invoiceTempvar.Next_Reminder_Date__c = NextRmndrDate;
                    }
                    else
                    {
                        invoiceTempvar.Next_Reminder_Date__c = date.today().adddays(integer.valueOf(Idef.Days_Between_Reminders__c));
                    }
                    invoiceTempvar.id = lstInv.id;
                    updateInvList.add(invoiceTempvar);
            
                    Invoice_Comment__c invoiceComment = new Invoice_Comment__c();
                
                    invoiceComment.invoice__c = lstInv.id;
                    invoiceComment.Comment_Date__c = system.now() ;
                    invoiceComment.Comment_From__c = userExecButton;
                    invoiceComment.Invoice_Comment__c = 'Invoice Reminder Sent';
                
                    if (invoiceComment != null )
                    {
                        invCommentsList.add(invoiceComment);
                    }
                }   
            }
        }   
        
        List<Messaging.SendEmailResult> results =new  List<Messaging.SendEmailResult>() ;
        
        If(mails.size() > 0 && mails != null)
        {
            results =  Messaging.sendEmail(mails);
            if(results.get(0).isSuccess())
            {
                if(invCommentsList.size() > 0 && invCommentsList != null)
                {
                   /* try
                    {
                        Insert invCommentsList;
                    }
                    catch (exception e)
                    {
                        for(Invoice_Comment__c invcm: invCommentsList)
                        {
                            invcm.addError('There is some problem in inserting Invoice comments, please contact system admin');
                        }
                    }*/
                    
                    try 
                    {
                        List<Database.SaveResult> insResults = Database.insert(invCommentsList, false);
                        
                        for(integer i=0 ;i<insResults.size() ; i++)
                        {
                            if(!insResults.get(i).isSuccess())
                            {
                                system.debug('_______insResults.get(i).getErrors()[0]____' + insResults.get(i).getErrors()[0]);
                            }
                        }
                    }
                    catch (Dmlexception e)
                    {
                        system.debug('##### Exception: ' + e);
                    }
                     
                }
                if(updateInvList.size() > 0 && updateInvList!= null)
                {
                    /*try
                    {
                        update updateInvList;
                    }
                    catch (exception e)
                    {
                        for(invoice__c inv : invlist)
                        {
                            inv.addError('There is some problem in upserting invoice, please contact system admin');
                        }
                    }*/
                    
                    try 
                    {
                        List<Database.SaveResult> updResults = Database.update(updateInvList, false);
                        
                        for(integer i=0 ;i<updResults.size() ; i++)
                        {
                            if(!updResults.get(i).isSuccess())
                            {
                                system.debug('_______updResults.get(i).getErrors()[0]____' + updResults.get(i).getErrors()[0]);
                            }
                        }
                    }
                    catch (Dmlexception e)
                    {
                        system.debug('##### Exception: ' + e);
                    }
                }
            }
        }
    }
   
    global void finish(Database.BatchableContext BC)
    {
        
    }
    
    //***Schedule classes
    global void execute(SchedulableContext SC) 
    {
        ID batchprocessid = Database.executeBatch(new SendInvoiceReminders ());
    }
    
}