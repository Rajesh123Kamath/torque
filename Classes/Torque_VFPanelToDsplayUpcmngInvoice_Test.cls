/**
    @Name			:	Torque_VFPanelToDsplayUpcmngInvoice_Test
    @Date			:   25 May, 2016
    @Description	:	Test code for Torque_VFPanelToDsplayUpcmngInvoicesCntr.cls.
*/
@isTest
private class Torque_VFPanelToDsplayUpcmngInvoice_Test {

	static testMethod void testVFPage() {
		
		//Create a test account record.
		Account objAccount  = UTestData.getAccounts(1)[0];
		insert objAccount;
        
        //Create test Custom setting record
        List<ProjectDateSettings__c> lstProjectDateSettingsToInsert = UTestData.getProjectDateSettings(1);
        lstProjectDateSettingsToInsert[0].NonImplementationEndDateIncrement__c =  30;
        lstProjectDateSettingsToInsert[0].BuildConfigTestingEndDayIncrement__c = 14;
        lstProjectDateSettingsToInsert[0].BuildConfigTestingStartDayIncrement__c = 15;
        lstProjectDateSettingsToInsert[0].DesignDiscoveryEndDaysIncrement__c = -1;
        lstProjectDateSettingsToInsert[0].GoLiveAndPostGoLiveEndDayIncrement__c = 7;
        insert lstProjectDateSettingsToInsert;
        
        //Create test project records.
        List<Project__c> lstProjects = UTestData.getProjects(12);
        for(Project__c objProj : lstProjects) {
        	
        	objProj.Account__c = objAccount.Id;
        }
        insert lstProjects;
        
		ApexPages.StandardController controller = new ApexPages.StandardController(objAccount);
		
		Torque_VFPanelToDsplayUpcmngInvoicesCntr objVFPageCntr = new Torque_VFPanelToDsplayUpcmngInvoicesCntr(controller);
		objVFPageCntr.noRecordsFound = false;
		
		system.assertEquals(12, objVFPageCntr.totalNoOfRecords);
		system.assertEquals(true, objVFPageCntr.hasNext);
		system.assertEquals(false, objVFPageCntr.hasPrevious);
		
        objVFPageCntr.next();
        system.assertEquals(2, objVFPageCntr.pageNumber);
        
        objVFPageCntr.previous();
        system.assertEquals(1, objVFPageCntr.pageNumber);
        
        objVFPageCntr.last();
        system.assertEquals(2, objVFPageCntr.pageNumber);
        
		objVFPageCntr.first();
		system.assertEquals(1, objVFPageCntr.pageNumber);
	}
}