global class BonusUtilization {

	public static void calculateMonth(date calcDate)
	{
		for(User u : [select id, hire_date__c, UEndCurr__c from User where Include_in_Utilization_Calculation__c = true])
		{	
			//Date calcDate = Date.parse('7/1/2015');
	 
			//do 
			//{
				calculateMonth(u.id, calcDate, u.hire_date__c, u.UEndCurr__c);
				//calcDate = calcDate.addMonths(1);
				
			//} while (calcDate < system.today().addDays(-1));
		  
		}
	}
 
	public static void calculateMonth(id userId, date calculationMonth, date userStartDate, date userEndDate)
	{
		/////////////////////////////////
		// Determine Start and End Dates
		////////////////////////////////
		Date calculationStartDate = calculationMonth.toStartOfMonth();
		Date calculationEndDate = calculationStartDate.addDays(date.daysInMonth(calculationMonth.year(), calculationMonth.month())  - 1);

		if(userStartDate > calculationEndDate)
		{
			// user started after this period
			system.debug('-----1 user started after this period');
			return;
		}

		if(userEndDate != null && userEndDate < calculationStartDate)
		{
			// user ended prior to this period
			system.debug('-----2 user ended prior to this period');
			return;
		}
		
		if(userStartDate > calculationStartDate)
		{
			calculationStartDate = userStartDate;
		}
		if(userEndDate != null  && userEndDate < calculationEndDate)
		{
			calculationEndDate = userEndDate;
		}
		
		// only go up to today -1 
		if(system.today().addDays(-1) < calculationEndDate)
		{
			calculationEndDate = system.today().addDays(-1);
		}
		
		integer businessDays = determineBusinessDays(calculationStartDate.daysBetween(calculationEndDate), calculationStartDate);	
		
		Datetime dtMonth = datetime.newInstance(calculationMonth.year(),calculationMonth.month(),calculationMonth.day()); 
		
		string month = dtMonth.format('MMMMM');
		string externalKey = month + '-' + userId;
		
		
		system.debug('-----User Id = ' + userId);
		system.debug('-----Month = ' + month);
		system.debug('-----External Key = ' + externalKey);
		system.debug('-----Calculation Start Date = ' + calculationStartDate);
		system.debug('-----Calculation End Date = ' + calculationEndDate);
		system.debug('-----# Business Days = ' + businessDays);
		
		//////////////////////////////
		//Establish Buckets
		//////////////////////////////
		
		// hardcoded for now, but should be in a custom setting
		set <id> excludeProjectIds = new set <id>();
		//excludeProjectIds.add('a19G0000000eGrs');
		for(ExcludeFromUtilization__c e : ExcludeFromUtilization__c.getall().values())
		{
			excludeProjectIds.add((ID)e.id__c);	
		}

		set <id> ptoIds = new set <id>();
		//ptoIds.add('a1916000007em05AAA');
		for(PTOBudgetItems__c e : PTOBudgetItems__c.getall().values())
		{
			ptoIds.add((ID)e.id__c);	
		}
		
		set <id> internalAccountIds = new set <id>();
		internalAccountIds.add('001A0000003HDy0');
		
		
		//////////////////////////////
		//Get Hours
		/////////////////////////////
		
		double billingHours = 0;
		double clientHours = 0;
		double contributionHours = 0;
		double gearsHours = 0;
		double ptoHours = 0;
		double excludedHours = 0;
		double daysWithHours = 0;
		set <date> dates = new set <date>();
		
			
		for(Time_Entry__c c: [Select minutes__c,
									date__c,
									Project__r.Name, 
									Project__r.Id, 
									Performed_By__c,
									Project__r.Account__c,
									Billable__c,
									Budget_Item__c
							 From Time_Entry__c 
							 WHERE performed_by__c = :userId
							 and date__c >= :calculationStartDate
							 and date__c <= :calculationEndDate
							 and minutes__c > 0])	
		{	
			
			
			double hours = c.minutes__c/60;
			
			// need a count of the days that have at least one hour 
			Datetime dt = datetime.newInstance(c.date__c.year(), c.date__c.month(),c.date__c.day()); 
		    if(dt.format('E') != 'Sat' && dt.format('E') != 'Sun')
		    {
				dates.add(c.date__c);
		    }
			
			if(excludeProjectIds.contains(c.Budget_Item__c))
			{
				excludedHours += hours;	
			}
			else if(ptoIds.contains(c.Budget_Item__c))
		    {	
				ptoHours += hours;
			}
			else
			{
				contributionHours += hours;
				
				// just client time - regardless of billing
				if(internalAccountids.contains(c.Project__r.Account__c))
				{
					gearsHours += hours;
				}
				else
				{
					clientHours += hours;
					
					if(c.Billable__c == true)
					{
						billingHours += hours;
					}
				}
			}
		}
		
		daysWithHours = dates.size();
		
		// Upsert the data
		//Utilization_Data__c u = new Utilization_Data__c();
		Utilization_Snapshot__c u = new Utilization_Snapshot__c();
		u.billable_hours__c = billingHours;
		u.Utilization_business_Days__c = businessDays;
		u.Utilization_Start_Date__c = calculationStartDate;
		u.Utilization_End_Date__c = calculationEndDate;
		u.Contribution_Hours_Customer__c = clientHours;
		u.contribution_Hours__c = contributionHours;
		u.Total_Business_Days_with_Time__c = daysWithHours;
		u.External_Key__c = externalKey;
		u.Contribution_Hours_Gears__c = gearsHours;
		u.snapshot_month__c = calculationStartDate;
		u.pto_hours__c = ptoHours;
		u.Excluded_From_Contribution_Hours__c = excludedHours;
		u.user__c = userId;
		u.ownerId = userId;
		
		upsert u external_key__c ;
		
	}
	
	public static integer determineBusinessDays (integer difference, date startDate)
	{
		
		integer x;
		integer businessDays = 0;
		for(x = 0; x <= difference ; x++)
		{
			//system.debug('-----' + startDate.addDays(x).format());	
			DateTime myDateTime = (DateTime) startDate.addDays(x);
			myDateTime = myDateTime.addHours(12);
			if(myDateTime.format('E') != 'Sat' && myDateTime.format('E') != 'Sun' )
			{
				businessDays++;
			}
		}
		
		return businessDays;
		
	}

	public static Map <id, integer> calculateHasTime (Map <id, set<date>> userHasTime, Map <id, User> userMap )
	{
		Map <id, integer> hasTime = new Map <id, integer>();
		
		for(User u : userMap.values())
		{
			system.debug('-----' + u.ustart__c);
			system.debug('-----' + u.uendcurr__c);
		
			set <date> dates = userHasTime.get(u.id);
		
			hasTime.put(u.id, 0);
		
		
			date x = u.ustart__c;
			Datetime dt = datetime.newInstance(x.year(), x.month(),x.day()); 
		
			while (dt < u.uendcurr__c.addDays(1))
			{
				system.debug('-----dt = ' + dt);
				
				if(dt.format('E') != 'Sat' && dt.format('E') != 'Sun')
				{
					system.debug('-----business day');
					system.debug('-----business day');
					
					if(dates.contains(dt.date()))
					{
						hasTime.put(u.id, hasTime.get(u.id) + 1);	
					}
					
				}
				
				dt = dt.addDays(1);
			}
		}
			
			
		system.debug('-----' + hasTime);
		return hasTime;	
			
	}

   
}