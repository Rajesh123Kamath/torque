@isTest 
public class Test_SendInvoiceReminder
{
    public static testMethod void SendInvoiceReminderMeth()
    {    
        User u = [select id,name from User where id = :userinfo.getUserId()];

        //CashBoardSettings__c settings = new CashBoardSettings__c();
        //settings.userName__c = 'gearscrm';
        //settings.Password__c =  'ZLU-68E-FN4-1Y9';
        //settings.Production_Instance__c = 'gearsCrm';
        //settings.isProduction__c = false;
        //System.runAs(u) 
        //{/    
         //   insert settings;
        //}
        //system.debug('_____ isProduction()___' + settings.isProduction__c);

        EmailTemplate eTemp = new EmailTemplate();
        etemp.isActive = true;
        eTemp.Name = 'name';
        eTemp.DeveloperName = 'defaultEmailTemplate';
        eTemp.TemplateType = 'text';
        eTemp.body = 'This is to test Sendinvoice class';
        eTemp.Subject = 'Subject of Sendinvoice';
        eTemp.FolderId = u.id;
        System.runAs(u) 
        {    
            insert eTemp;
        }

        InvoiceDefaults__c idef = new InvoiceDefaults__c();
        idef.Days_between_Reminders__c = 10;
        idef.Initial_Invoice_Email_Template__c = '00XJ0000000O76a';
        idef.Reminder_Invoice_Email_Template__c = eTemp.id;
        System.runAs(u) 
        {    
            insert idef;
        }

        system.debug('_____ idef___' + idef);

        ProjectDateSettings__c pds = new ProjectDateSettings__c();
        pds.BuildConfigTestingEndDayIncrement__c = 14;
        pds.BuildConfigTestingStartDayIncrement__c = 15 ;
        pds.DesignDiscoveryEndDaysIncrement__c = -1;
        pds.GoLiveAndPostGoLiveEndDayIncrement__c =7;
        pds.NonImplementationEndDateIncrement__c = 30;
        System.runAs(u) 
        {    
            insert pds;
        }

        system.debug('____pds____' + pds);

        UserLicense UL =[select id,LicenseDefinitionKey,name from userlicense where name='Salesforce' limit 1];
        Profile p = [SELECT Id FROM Profile WHERE Name = 'System Administrator' and  UserLicense.id = :UL.id  LIMIT 1]; 

        system.debug('____eTemp____' + eTemp);

        //cretae Account...
        Account acc = new Account();
        acc.Type = 'End User - prospect';
        acc.Name = 'TestAcc';
        acc.Practice__c = 'Other';
        insert acc;
        system.debug('__acc__'+acc);

        //Create Contact..
        Contact con = new Contact();
        con.LastName= 'TestCon';
        con.AccountId = acc.Id;
        con.Email = 'test@test.com';
        insert con;  

        system.debug('__con__'+con);

        system.debug('____pds.DesignDiscoveryEndDaysIncrement22___' + pds.DesignDiscoveryEndDaysIncrement__c);

        //Create Project for Email
        Project__c pro = new Project__c();
        pro.name = 'Email Project test';
        pro.Account__c = acc.id;
        pro.Status__c = 'Planning';
        pro.Go_Live_Date__c = date.today().addDays(25);
        pro.Status__c = 'Active';
        pro.Hourly_Rate__c = 20.0;
        pro.Project_Budgeted_Hrs__c = 160.0;
        pro.Kickoff__c = date.today().addDays(-30);
        pro.Go_Live_Date__c = date.newInstance(2015, 03, 25);
        //pro.sendToCashBoard__c = true;
        pro.Current_State__c = 'On Track';
        Insert pro;
        system.debug('__pro__'+pro);

        //Create Project Phase
        Project_Phase__c pphs = new Project_Phase__c();
        pphs.Phase_name__c = 'Design/Discovery';
        pphs.Start_Date__c = pro.Kickoff__c;
        pphs.End_Date__c = pro.Kickoff__c.addDays(14);
        pphs.Project__c = pro.id;
        insert pphs;
        system.debug('__pphs__'+pphs);

        //Create Invoice
        Invoice__c inv = new Invoice__c();
        inv.Account__c = acc.id;
        inv.Send_Reminders__c = true;
        inv.Payment_Status__c = 'open';
        //inv.Project__c = pro.id;
        inv.Next_Reminder_Date__c = Date.Today().addDays(-1);
        inv.Customer_Emails__c = 'yrgopal2001@gmail.com,yrgopal2002@gmail.com';
        inv.Billing_Contact__c = con.Id;
        insert inv;

        system.debug('__inv__'+inv);

        Test.startTest();  
        SendInvoiceRemindersSched objSendInvoiceRemindersSched = new SendInvoiceRemindersSched();
        String sch2 = '0 0 * * * ?';       
        system.schedule('Test Check2', sch2, objSendInvoiceRemindersSched);  
        Test.stopTest();  



    }
}