/**
  * Trigger Name : Torque_TimeEntryTrigger
  * Created By   : Trekbin          
  * Created On   : 19-Nov-2015
  * Modified On  : 27-Jan-2016
  * Modified by  : Trekbin
  * Description  : Trigger on Time Entry.
  				   1. validates the Time Entry before update for status and before delete for invoice line and displays validation error.
  				   2. After insert/update/delete of Time Entry, update log hours on the related budget item with sum of all child time entry hours. 
	  			   3. After insert/update of Time Entry, if it is associated with a Pending Invoice Line, then the corresponding Invoice Line has to be updated to reflect the changes to Time Entry.
**/
trigger Torque_TimeEntryTrigger on Time_Entry__c (before update, before delete, after insert, after update, after delete) {
    
    if(Trigger.isAfter)
    	if(Trigger.isDelete)
    		Torque_TimeEntryHandler.updateInvLineAndHrsOnBudgetItem(Trigger.old, Trigger.newMap);
    	else
    		Torque_TimeEntryHandler.updateInvLineAndHrsOnBudgetItem(Trigger.new, Trigger.oldMap);
    
	if(Trigger.isBefore)
		if(Trigger.isUpdate || Trigger.isDelete)
			Torque_TimeEntryHandler.validateTEsAndDisplayError(Trigger.new, Trigger.old);
    
}