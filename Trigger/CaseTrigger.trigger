trigger CaseTrigger on Case (before insert, before update, before delete, after insert, after update, after delete, after undelete) {
      
      List<Case> cases = trigger.isDelete ? trigger.old : trigger.new;

      if(trigger.isBefore == true)
      {
            if(trigger.isInsert == true)
            {
                   
                  
                  UCase.AssignDeveloper(cases, trigger.oldMap);
                  UCase.testEscalate(cases,trigger.oldMap,'--Before Insert', trigger.isAfter);
                  
                  
            }
            else if(trigger.isUpdate == true)
            {
                    
                    UCase.AssignDeveloper(cases, trigger.oldMap);
                    UCase.testEscalate(cases,trigger.oldMap,'--Before Update', trigger.isAfter);
                    
            }
            else if(trigger.isDelete == true)
            {
                   
            }
      }     
      else if(trigger.isAfter == true)
      {
            if(trigger.isInsert == true)
            {
                UCase.onAfterInsert(trigger.new);
                UCase.testEscalate(cases,trigger.oldMap,'--After Insert', trigger.isAfter);
                 
                 /*Added by Trekbin on 11 May, 2016 for the Case  00026116 */
                 //updating the open cases count to parent project if case related to it is closed by user
                 UCase.updateOpenCasesCount(trigger.oldMap, trigger.newMap);
            }
            else if(trigger.isUpdate == true)
            {
                UCase.onAfterUpdate(trigger.newMap, trigger.oldMap);
                 UCase.testEscalate(cases,trigger.oldMap,'--After Update', trigger.isAfter);
                 
                 /*Added by Trekbin on 11 May, 2016 for the Case  00026116 */
                 //updating the open cases count to parent project if case related to it is closed by user
                 UCase.updateOpenCasesCount(trigger.oldMap, trigger.newMap);
                 
                 /*Added by Trekbin on 10 June, 2016 for the Case 00026102*/
                 //populate related Deployed items with case's inventory
                 UCase.populateInventoryOnChildDeployedItems(trigger.oldMap, trigger.newMap);
            }
            else if(trigger.isDelete == true)
            {
                       
            }
            else if(trigger.isUndelete == true)
            {
                  
            }
      }
}