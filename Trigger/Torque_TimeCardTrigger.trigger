/**
    @Name           :   Torque_TimeCardTrigger
    @Date           :   28 June, 2016
    @Description    :   Trigger on Time card before delete.
    @TestClass      :   Torque_TimeCardTriggerHandler_Test.cls
*/
trigger Torque_TimeCardTrigger on Time_Card__c (before delete) {
    
    if(Trigger.isBefore)
        if(Trigger.isDelete)
            Torque_TimeCardTriggerHandler.isBeforeDelete(Trigger.oldMap);
}