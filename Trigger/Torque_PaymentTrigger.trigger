/**
@ClassName    : Torque_PaymentTrigger 
@CreatedOn    : 
@CreatedBy    : Trekbin
@ModifiedBy   : 
@Description  : This trigger is used to sum the Amount of all payments related to Account and populate on Account Total_Payments__c field
**/

trigger Torque_PaymentTrigger on Payment__c (after delete, after insert, after update , before delete) {
    
    Torque_PaymentHandler objhandler = new Torque_PaymentHandler();
    
    if(Trigger.isAfter) {
        
        if(Trigger.isInsert){
            /* Last Modified By: Trekbin on 15-feb-2016 as per the case: 00025256*/
            objHandler.onAfterInsert(Trigger.New, Trigger.oldMap);
        }
        else if(Trigger.isupdate){
            /* Last Modified By: Trekbin on 15-feb-2016 as per the case: 00025256*/
            objHandler.onAfterUpdate(Trigger.New, Trigger.oldMap); 
        }
        else if(Trigger.isdelete)
            objHandler.onAfterDelete(Trigger.Old); 
             
    } else if(Trigger.isBefore && Trigger.isdelete){ 
            objHandler.onBeforeDelete (Trigger.oldMap);
     
    }
}