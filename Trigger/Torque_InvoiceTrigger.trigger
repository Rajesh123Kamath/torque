/**
  * Trigger Name : Torque_InvoiceTrigger
  * Created By   : Trekbin          
  * Created On   : 19-Nov-2015
  * Modified On  : 20-Nov-2015
  * Modified by  : Trekbin
  * Description  : Trigger to lock Invoice records from editing few fields / deletion based on criterias
**/
trigger Torque_InvoiceTrigger on Invoice__c(before update, before delete, after update,after insert) {  
    
    //To fire the trigger on after events.
    if(Trigger.isAfter) {
        if(Trigger.isInsert)
            Torque_InvoiceHandler.manualShareInvoiceRecords(Trigger.newMap, Trigger.oldMap);
        else if(Trigger.isUpdate) {
            Torque_InvoiceHandler.manualShareInvoiceRecords(Trigger.newMap, Trigger.oldMap);
            Torque_InvoiceHandler.updateAccountInvoiceTotal(Trigger.new);   
        }
    }
    
    //To fire the trigger on before events.
    if(Trigger.isBefore) 
		if(Trigger.isUpdate || Trigger.isDelete)
        	Torque_InvoiceHandler.validateInvoiceBeforeUpdateAndDelete(Trigger.new, Trigger.oldMap);    
}