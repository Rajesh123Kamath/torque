/**
  *	Trigger Name : Torque_InvoiceLineTrigger
  *	Created By   : Trekbin			
  *	Created On   : 19-Nov-2015
  * Modified On  : 28-Jan-2016
  * Modified by  : Trekbin
  * Description  : 1. Validates the Invoice Lines before update/delete and displays validation error.
**/
trigger Torque_InvoiceLineTrigger on Invoice_Line__c (before update, before delete) {
			
	if(Trigger.isBefore)
		if(Trigger.isUpdate || Trigger.isDelete)
			Torque_InvoiceLineHandler.validateInvLineBeforeUpdateAndDelete(Trigger.new, Trigger.oldMap);
}